package exairie.com.quranq;

import android.Manifest;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dinuscxj.progressbar.CircleProgressBar;
import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.HadistDBHelper;
import exairie.com.quranq.libs.OdojHistoryAdapter;
import exairie.com.quranq.libs.PrayTime;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.AdzanSettings;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.OdojData;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.services.DataLoader;
import exairie.com.quranq.services.PrayNotifier;
import exairie.com.quranq.services.PrayTimeService;
import exairie.com.quranq.services.PrayTracker;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class ActivityHome extends ExtActivity {
    String[] str = new String[]{};
    Class t = String[].class;
    static final int ACT_QURANQ = 1;
    static final int PROGRESS_MAX = 100;
    private static final int ACT_HADIST = 5;
    private static final int ACT_KAJIAN = 6;
    private static final int PERMISSION_LOCATION = 1;

    PendingIntent popupAdzanSubuh;
    PendingIntent popupAdzanDzuhur;
    PendingIntent popupAdzanAshar;
    PendingIntent popupAdzanMaghrib;
    PendingIntent popupAdzanIsya;


    //Jakarta
    double JAKARTA_LATITUDE = -6.1745;
    double JAKARTA_LONGITUDE = 106.8227;
    double JAKARTA_TIMEZONE = 7;


    double latitude = -6.1745;
    double longitude = 106.8227;
    double timezone = 7;

    // TODO : Install ActiveAndroid
    // TODO : Make Quran Read-View

    OdojHistoryAdapter historyAdapter;
    Timer timer = new Timer();

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.nav_view)
    NavigationView navView;

    @InjectView(R.id.drawer)
    DrawerLayout drawer;
    @InjectView(R.id.l_clock)
    TextView lClock;
    @InjectView(R.id.l_hijriyah_date)
    TextView lHijriyahDate;
    @InjectView(R.id.l_shalat_time_current)
    TextView lShalatTimeCurrent;
    @InjectView(R.id.l_pray_subuh)
    TextView lPraySubuh;
    @InjectView(R.id.l_pray_dzuhur)
    TextView lPrayDzuhur;
    @InjectView(R.id.l_pray_Ashar)
    TextView lPrayAshar;
    @InjectView(R.id.l_pray_maghrib)
    TextView lPrayMaghrib;
    @InjectView(R.id.l_pray_isya)
    TextView lPrayIsya;

    PrayTime.PrayTimeInformation prayTimeInformation;
    @InjectView(R.id.progress_odoj)
    ProgressBar progressOdoj;
    @InjectView(R.id.l_progress_odoj)
    TextView lProgressOdoj;
    @InjectView(R.id.recycler_history_odoj)
    RecyclerView recyclerHistoryOdoj;
    @InjectView(R.id.include_layout_odoj)
    ConstraintLayout layoutOdojhistory;
    @InjectView(R.id.img_expand)
    ImageView imgExpand;
    @InjectView(R.id.l_day_of_odoj)
    TextView lDayOfOdoj;
    @InjectView(R.id.l_shalat_time_remaining)
    TextView lShalatTimeRemaining;
    @InjectView(R.id.progressbar_shalat_time)
    CircleProgressBar progressbarShalatTime;
    @InjectView(R.id.l_cityname)
    TextView lCityname;

    @InjectView(R.id.btn_continuetadarus)
    Button btnContinuetadarus;
    @InjectView(R.id.btn_showmap)
    Button btnShowmap;
    @InjectView(R.id.btn_gototajwid)
    Button btnGototajwid;
    @InjectView(R.id.btn_gotokajian)
    Button btnGotokajian;
    @InjectView(R.id.l_hadistinfo)
    TextView lHadistinfo;
    @InjectView(R.id.btn_gotohadist)
    Button btnGotohadist;
    @InjectView(R.id.l_gurungaji)
    TextView lGurungaji;
    @InjectView(R.id.l_kajian)
    TextView lKajian;


    public ActivityHome() {
        super(R.layout.activity_home);
        for (int i = 0;i < 10;i++){}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("FIREBASE ID", "TOKEN : " + FirebaseInstanceId.getInstance().getToken());

        setExtActionBar(toolbar);
        setTitle("Quran-Qu");

        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_closed);
            toggle.setDrawerIndicatorEnabled(true);
            drawer.addDrawerListener(toggle);

            toggle.syncState();

        } catch (Exception e) {
            e.printStackTrace();
        }
        setupPermissions();
        SetupEvents();
        setupPrayTimeInfo();
        setupOdojHistory();
        setupTimer();
        setupLocation();

        setupAdzans();

        sendTokens();
        //setupNavViews();


        Option o = Configs.getConfiguration("firstload");
        if (o == null) {
            Intent i = new Intent(thisContext(), DataLoader.class);
            i.putExtra("flag", DataLoader.Companion.getFLAG_UPDATE_GURU());
            startService(i);

            Intent i2 = new Intent(thisContext(), DataLoader.class);
            i2.putExtra("flag", DataLoader.Companion.getFLAG_UPDATE_KAJIAN());
            startService(i2);

            Option no = new Option("firstload", "true");

            Configs.setConfiguration(no);
        }
    }

//    private void setupNavViews() {
//        View header = navView.getHeaderView(0);
//        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();
//        String deviceName = myDevice.getName();
//
//        TextView device = (TextView) header.findViewById(R.id.l_devicename);
//        TextView hari = (TextView) header.findViewById(R.id.l_hari);
//        TextView tanggal = (TextView) header.findViewById(R.id.l_tanggal);
//        TextView tanggalHijri = (TextView) header.findViewById(R.id.l_tanggal_hijri);
//
//        Calendar c = Calendar.getInstance();
//
//        device.setText(deviceName);
//        hari.setText(string_hari[c.get(Calendar.DAY_OF_WEEK)-1]);
//
//        tanggal.setText(String.format("%d %s %d", c.get(Calendar.DAY_OF_MONTH), string_bulan[c.get(Calendar.MONTH)-1], c.get(Calendar.YEAR)));
//
//        UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
//        String hijridate = String.format("%s %s %s",
//                String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH)),
//                ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH),
//                String.valueOf(ummalquraCalendar.get(Calendar.YEAR)));
//
//        tanggalHijri.setText(hijridate);
//    }

    private void sendTokens() {
        Option option = Configs.getConfiguration("firebase_token");
        if (option == null) return;
        String token = option.getValue();
        RequestBody body = new FormBody.Builder()
                .add("token", token)
                .build();
        Request request = new Request.Builder()
                .url(server("api/token"))
                .post(body)
                .build();
        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("TokenRefreshQuery", response.body().string());
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOCATION: {
                if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    setupLocation();
                }
            }
        }
    }

    private void setupLocation() {
        Log.d("SETUPLOC", "setupLocation: false");
        LocationListener l = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                //Toast.makeText(thisContext(), "Location changed", Toast.LENGTH_LONG).show();

                Option lastlat = new Option("last_latitude", String.valueOf(latitude));
                Option lastlong = new Option("last_longitude", String.valueOf(longitude));

                Configs.setConfiguration(lastlat);
                Configs.setConfiguration(lastlong);

                setupPrayTimeInfo();

                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Geocoder geocoder = new Geocoder(thisContext(), Locale.getDefault());

                        try {
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            if (addresses.size() > 0) {
                                String city;
                                if (addresses.get(0).getLocality() != null) {
                                    city = addresses.get(0).getLocality();
                                } else if (addresses.get(0).getAdminArea() != null) {
                                    city = addresses.get(0).getAdminArea();
                                } else {
                                    city = String.format("%.4f, %.4f", location.getLatitude(), location.getLongitude());
                                }
                                runOnUiThread(() -> lCityname.setText(city));

                                Option lastpos = new Option("last_loc", city);
                                Configs.setConfiguration(lastpos);
                            }
                        } catch (IOException e) {
                            String city = String.format("%.4f, %.4f", location.getLatitude(), location.getLongitude());

                            Option lastpos = new Option("last_loc", city);
                            Configs.setConfiguration(lastpos);
                            runOnUiThread(() -> lCityname.setText(city));
                            e.printStackTrace();
                        } finally {
                        }
                        return null;
                    }
                };
                task.execute();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (manager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("PERMISSION 217", "setupLocation: false");
                setupPermissions();
                return;
            }
            manager.requestSingleUpdate(LocationManager.GPS_PROVIDER, l, null);
        }
        if (manager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("PERMISSION 224", "setupLocation: false");
                setupPermissions();
                return;
            }
            manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, l, null);
        }
    }

    private void setupPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] req = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, req, PERMISSION_LOCATION);
        }
    }

    private void setupTimer() {
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                calculateTiming();
            }
        }, 0, 1000);
    }

    private void calculateTiming() {

        Calendar calendar = Calendar.getInstance();
        lClock.post(() -> {
            lClock.setText(DateFormat.format("HH:mm", calendar).toString(), TextView.BufferType.EDITABLE);
            PrayTime.CurrentPrayTime time = new PrayTime.CurrentPrayTime(prayTimeInformation);

            double progress = ((double) time.getDeltaTime() / (double) time.getTimeProgress()) * 1000;
            //Log.d("PROGRESS", String.format("%d / %d * 1000 = %.3f",time.getDeltaTime(), time.getTimeProgress(), progress));
            long remaining = time.getRemaining();

            progressbarShalatTime.setMax(1000);
            progressbarShalatTime.setProgress((int) progress);
            lShalatTimeCurrent.setText(time.getCurrentShalatName());
            lShalatTimeRemaining.setText(PrayTime.CurrentPrayTime.getHour(remaining));
            time = null;
        });
    }

    private void setupOdojHistory() {
        layoutOdojhistory.setClickable(true);
        layoutOdojhistory.setOnClickListener(view -> {
            if (recyclerHistoryOdoj.getVisibility() == View.GONE) {
                imgExpand.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_less_black_24dp));
                Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {
                        recyclerHistoryOdoj.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                recyclerHistoryOdoj.startAnimation(animation);
                return;
            }
            if (recyclerHistoryOdoj.getVisibility() == View.VISIBLE) {
                imgExpand.setImageDrawable(getResources().getDrawable(R.drawable.ic_expand_more_black_24dp));
                Animation animation = AnimationUtils.loadAnimation(this, R.anim.slide_down);
                animation.setInterpolator(v -> 1 - v);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        recyclerHistoryOdoj.setVisibility(View.GONE);
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                recyclerHistoryOdoj.startAnimation(animation);
            }

        });

        historyAdapter = new OdojHistoryAdapter(this);
        recyclerHistoryOdoj.setAdapter(historyAdapter);
        recyclerHistoryOdoj.setLayoutManager(new LinearLayoutManager(this));

        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                HadistDBHelper dbHelper = new HadistDBHelper(thisContext());
                Cursor c = dbHelper.getReadableDatabase().rawQuery("select count(*), count(distinct bab) from hadist", new String[]{});
                c.moveToFirst();
                runOnUiThread(() -> {
                    lHadistinfo.setText(String.format("%d Koleksi Hadist di %d bab Hadist", c.getInt(0), c.getInt(1)));
                    c.close();
                    dbHelper.close();
                });
                return null;
            }
        };
        task.execute();
        AsyncTask<Void, Void, Void> task2 = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBHelper dbHelper = new DBHelper(thisContext());
                Cursor c = dbHelper.getReadableDatabase().rawQuery("SELECT count(*) from guru", new String[]{});
                c.moveToFirst();
                runOnUiThread(() -> {
                    lGurungaji.setText(String.format("%d Guru terdaftar pada database", c.getInt(0)));
                    c.close();
                    dbHelper.close();
                });
                return null;
            }
        };
        task2.execute();
        AsyncTask<Void, Void, Void> task3 = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                DBHelper dbHelper = new DBHelper(thisContext());
                Cursor c = dbHelper.getReadableDatabase().rawQuery("SELECT count(*) from kajian", new String[]{});
                c.moveToFirst();
                runOnUiThread(() -> {
                    lKajian.setText(String.format("%d Kajian Islami", c.getInt(0)));
                    c.close();
                    dbHelper.close();
                });
                return null;
            }
        };
        task3.execute();
    }

    private void setupPrayTimeInfo() {
        Option last_lat = Configs.getConfiguration("last_latitude");
        Option last_long = Configs.getConfiguration("last_longitude");
        Option last_loc = Configs.getConfiguration("last_loc");
        latitude = last_lat == null ? JAKARTA_LATITUDE : Double.parseDouble(last_lat.getValue());
        longitude = last_long == null ? JAKARTA_LONGITUDE : Double.parseDouble(last_long.getValue());
        String loc = last_loc == null ? "" : last_loc.getValue();

        lCityname.setText(loc);

        Intent i = new Intent(thisContext(), PrayTracker.class);
        startService(i);


        Option adzanSettingsOpt = Configs.getConfiguration("adzan_settings");
        AdzanSettings _adzanSettings = null;
        if(adzanSettingsOpt != null){
            try {
                _adzanSettings = Configs.mapper().readValue(adzanSettingsOpt.getValue(),AdzanSettings.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        final AdzanSettings adzanSettings = _adzanSettings;
        prayTimeInformation = PrayTime.getInformation(latitude, longitude, timezone, adzanSettings);

        lPraySubuh.setText(prayTimeInformation.getSubuh());
        lPrayDzuhur.setText(prayTimeInformation.getDzuhur());
        lPrayAshar.setText(prayTimeInformation.getAshar());
        lPrayMaghrib.setText(prayTimeInformation.getMaghrib());
        lPrayIsya.setText(prayTimeInformation.getIsya());

        UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
        String hijridate = String.format("%s %s %s",
                String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH)),
                ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH),
                String.valueOf(ummalquraCalendar.get(Calendar.YEAR)));

        lHijriyahDate.setText(hijridate);
    }


    void setupTodayOdojInfo() {
        new Handler().post(() -> {
            final List<OdojData> data = DBHelper.getOdojHistory(this, true);
            runOnUiThread(() -> {
                if (data.size() < 1) {
                    lProgressOdoj.setText("-");
                    progressOdoj.setMax(100);
                    progressOdoj.setProgress(0);
                } else {
                    OdojData d = data.get(0);
                    lProgressOdoj.setText(String.format("%.1f Juz", d.getJuzCalculation()));
                    progressOdoj.setMax(PROGRESS_MAX);
                    int current = (int) (d.getJuzCalculation() * PROGRESS_MAX);
                    current = current > PROGRESS_MAX ? PROGRESS_MAX : current;
                    progressOdoj.setProgress(current);

                }
            });
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        setupPrayTimeInfo();
        setupOdojHistory();
        setupTimer();
        setupLocation();

        setupAdzans();
        historyAdapter.reload();
        setupTodayOdojInfo();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                drawer.openDrawer(GravityCompat.START);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    void SetupEvents() {
        //navView = (NavigationView)findViewById(R.id.nav_view);
        navView.setNavigationItemSelectedListener(item -> {
            switch (item.getItemId()) {
                case R.id.mn_quranq: {
                    startActivityForResult(new Intent(ActivityHome.this, ActivityQuranSurahList.class), ACT_QURANQ);
                }
                break;
                case R.id.mn_bookmark: {
                    Intent i = new Intent(this, ActivityQuranSurahList.class);
                    i.putExtra("mode", ActivityQuranSurahList.QURAN_MODE_BOOKMARK);
                    startActivityForResult(i, ACT_QURANQ);
                }
                break;
                case R.id.mn_bacaan_terakhir: {
                    Intent i = new Intent(this, ActivityQuranSurahList.class);
                    i.putExtra("mode", ActivityQuranSurahList.QURAN_MODE_LASTREAD);
                    startActivityForResult(i, ACT_QURANQ);
                }
                break;
                case R.id.mn_hadits_q: {
                    startActivityForResult(new Intent(this, ActivityHadistList.class), ACT_HADIST);
                }
                break;
                case R.id.mn_kajian: {
                    startActivityForResult(new Intent(this, ActivityKajian.class), ACT_KAJIAN);
                }
                break;
                case R.id.mn_cari_ayat: {
                    Intent i = new Intent(this, ActivityJumptoAyat.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_tajwid: {
                    Intent i = new Intent(this, ActivityReadTajwid.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_settings: {
                    Intent i = new Intent(thisContext(), ActivitySettings.class);
                    startActivity(i);
                }
            }

            return false;
        });

        btnContinuetadarus.setOnClickListener(view -> {
            int ayatid;
            Option option = Configs.getConfiguration(Option.KEY_LAST_READ);
            if (option == null) {
                new AlertDialog.Builder(thisContext())
                        .setMessage("Anda belum menambahkan bacaan terakhir!")
                        .setTitle("Bacaan terakhir tidak ditemukan")
                        .setNegativeButton("Tutup", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show();
                return;
            }

            ayatid = Integer.parseInt(option.getValue());
            Ayat a = QuranDBHelper.getAyat(thisContext(), ayatid);
            if (a != null) {
                Intent i = new Intent(thisContext(), ActivityReadQuran.class);
                i.putExtra("id", a.getSurah_no());
                i.putExtra("jump", a.getId());

                startActivity(i);
            }
        });

        btnGotokajian.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityKajian.class);
            startActivity(i);
        });
        btnGototajwid.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityReadTajwid.class);
            startActivity(i);
        });
        btnGotohadist.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityHadistList.class);
            startActivity(i);
        });
    }

    void setupAdzans() {
        if (prayTimeInformation == null) return;
        Calendar now = Calendar.getInstance();
        Calendar tSubuh = (Calendar) prayTimeInformation.c_subuh.clone();
        Calendar tDzuhur = (Calendar) prayTimeInformation.c_dzuhur.clone();
        Calendar tAshar = (Calendar) prayTimeInformation.c_ashar.clone();
        Calendar tMaghrib = (Calendar) prayTimeInformation.c_maghrib.clone();
        Calendar tIsya = (Calendar) prayTimeInformation.c_isya.clone();


        if (tSubuh.getTimeInMillis() < System.currentTimeMillis()) {
            tSubuh.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
            tSubuh.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent iSubuh = new Intent(getApplicationContext(), PrayNotifier.class);
        iSubuh.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, ActivitySettings.ADZAN_ID_SUBUH);
        iSubuh.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, "Waktu Subuh");
        iSubuh.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, lCityname.getText());
        iSubuh.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm", tSubuh));
        iSubuh.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, tSubuh.getTimeInMillis());

        popupAdzanSubuh = (PendingIntent.getService(getApplicationContext(), 10 + ActivitySettings.ADZAN_ID_SUBUH, iSubuh, 0));

        if (tDzuhur.getTimeInMillis() < System.currentTimeMillis()) {
            tDzuhur.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
            tDzuhur.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent iDzuhur = new Intent(getApplicationContext(), PrayNotifier.class);
        iDzuhur.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, ActivitySettings.ADZAN_ID_DZUHUR);
        iDzuhur.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, "Waktu Dzuhur");
        iDzuhur.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, lCityname.getText());
        iDzuhur.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm", tDzuhur));
        iDzuhur.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, tDzuhur.getTimeInMillis());

        popupAdzanDzuhur = (PendingIntent.getService(getApplicationContext(), 10 + ActivitySettings.ADZAN_ID_DZUHUR, iDzuhur, 0));

        if (tAshar.getTimeInMillis() < System.currentTimeMillis()) {
            tAshar.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
            tAshar.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent iAshar = new Intent(getApplicationContext(), PrayNotifier.class);
        iAshar.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, ActivitySettings.ADZAN_ID_ASHAR);
        iAshar.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, "Waktu Ashar");
        iAshar.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, lCityname.getText());
        iAshar.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm", tAshar));
        iAshar.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, tAshar.getTimeInMillis());

        popupAdzanAshar = (PendingIntent.getService(getApplicationContext(), 10 + ActivitySettings.ADZAN_ID_ASHAR, iAshar, 0));

        if (tMaghrib.getTimeInMillis() < System.currentTimeMillis()) {
            tMaghrib.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
            tMaghrib.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent iMaghrib = new Intent(getApplicationContext(), PrayNotifier.class);
        iMaghrib.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, ActivitySettings.ADZAN_ID_MAGHRIB);
        iMaghrib.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, "Waktu Maghrib");
        iMaghrib.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, lCityname.getText());
        iMaghrib.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm", tMaghrib));
        iMaghrib.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, tMaghrib.getTimeInMillis());

        popupAdzanMaghrib = (PendingIntent.getService(getApplicationContext(), 10 + ActivitySettings.ADZAN_ID_MAGHRIB, iMaghrib, 0));

        if (tIsya.getTimeInMillis() < System.currentTimeMillis()) {
            tIsya.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
            tIsya.add(Calendar.DAY_OF_MONTH, 1);
        }
        Intent iIsya = new Intent(getApplicationContext(), PrayNotifier.class);
        iIsya.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, ActivitySettings.ADZAN_ID_ISYA);
        iIsya.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, "Waktu Isya");
        iIsya.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, lCityname.getText());
        iIsya.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm", tIsya));
        iIsya.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, tIsya.getTimeInMillis());

        popupAdzanIsya = (PendingIntent.getService(getApplicationContext(), 10 + ActivitySettings.ADZAN_ID_ISYA, iIsya, 0));

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Log.d("Set Alarm", "SUBUH   " + DateFormat.format("dd MM yyyy H:mm:ss", tSubuh.getTimeInMillis()));
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, tSubuh.getTimeInMillis(), popupAdzanSubuh);
            Log.d("Set Alarm", "DZUHUR  " + DateFormat.format("dd MM yyyy H:mm:ss", tDzuhur.getTimeInMillis()) + tDzuhur.getTimeInMillis());
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, tDzuhur.getTimeInMillis(), popupAdzanDzuhur);
            Log.d("Set Alarm", "ASHAR   " + DateFormat.format("dd MM yyyy H:mm:ss", tAshar.getTimeInMillis()));
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, tAshar.getTimeInMillis(), popupAdzanAshar);
            Log.d("Set Alarm", "MAGHRIB " + DateFormat.format("dd MM yyyy H:mm:ss", tMaghrib.getTimeInMillis()));
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, tMaghrib.getTimeInMillis(), popupAdzanMaghrib);
            Log.d("Set Alarm", "ISYA    " + DateFormat.format("dd MM yyyy H:mm:ss", tIsya.getTimeInMillis()));
            alarmManager.setExact(AlarmManager.RTC_WAKEUP, tIsya.getTimeInMillis(), popupAdzanIsya);
        } else {
            alarmManager.set(AlarmManager.RTC_WAKEUP, tSubuh.getTimeInMillis(), popupAdzanSubuh);
            alarmManager.set(AlarmManager.RTC_WAKEUP, tDzuhur.getTimeInMillis(), popupAdzanDzuhur);
            alarmManager.set(AlarmManager.RTC_WAKEUP, tAshar.getTimeInMillis(), popupAdzanAshar);
            alarmManager.set(AlarmManager.RTC_WAKEUP, tMaghrib.getTimeInMillis(), popupAdzanMaghrib);
            alarmManager.set(AlarmManager.RTC_WAKEUP, tIsya.getTimeInMillis(), popupAdzanIsya);
        }
    }


}
