package exairie.com.quranq.services

import android.app.Service
import android.content.ContentValues
import android.content.Intent
import android.database.Cursor
import android.os.IBinder
import android.util.Log
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.OdojData
import exairie.com.quranq.models.RequestResult
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ODOJSynchronizer : Service() {
    override fun onBind(intent: Intent): IBinder? =// TODO: Return the communication channel to the service.
            null
    companion object {
        var lastID : Long = -1
        val ODOJ_SYNCHRONIZED : String = "exairie.com.quranq.ODOJ_SYNCHRONIZED"
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if(intent == null){
            return START_NOT_STICKY
        }
        if (intent!!.getStringExtra("pullall") != null){
            pullOdojData()
        }else{
            synchronize()
        }
        return START_NOT_STICKY
    }

    private fun pullOdojData() {
        val user = Configs.getLoginInfo();
        if (user ==null){
            Log.d("Odoj Sync","No user logged in! Skipping pull sync")
            stopSelf()
            return
        }

        val request = Request.Builder()
                .get()
                .url(ExtActivity.server("api2/odoj/${user.id}"))
                .build()

        ExtActivity.ApiCall(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val db = DBHelper(this@ODOJSynchronizer)

                db.writableDatabase.delete("odoj","", arrayOf())

                val json = JSONObject(string).getString("data")
                val odata = Configs.mapper().readValue(json,Array<OdojData>::class.java)
                for (o in odata){
                    val values = ContentValues();
                    values.put("id",o.odojId)
                    values.put("time",o.time)
                    values.put("ayat_id",o.ayat_id)
                    values.put("ayat_to",o.ayat_to)
                    values.put("letter_no",o.letter_count)

                    db.writableDatabase.insert("odoj","null",values)
                }

                sendBroadcast(Intent(ODOJ_SYNCHRONIZED))
            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }
        },this)
    }

    fun synchronize(){
        Log.d("Sync Odoj", "ID : " + lastID)
        val userdata = Configs.getLoginInfo()
        if (userdata == null) {
            Log.d("Odoj Sync","No user logged in! Skipping sync")
            stopSelf()
            return
        }
        val db = DBHelper(this)
        val c : Cursor
        if(lastID == -1L){
            c = db.readableDatabase.rawQuery("SELECT * FROM odoj ORDER BY id asc", arrayOf<String>())
        }else{
            c = db.readableDatabase.rawQuery("SELECT * FROM odoj where id > ? ORDER BY id asc", arrayOf(lastID.toString()))
        }
        if (c.count < 1){
            Log.d("Odoj Sync","Cursor count < 1. Stopping sync")
            c.close()
            db.close()
            stopSelf()
            return
        }

        c.moveToFirst()
        val odoj = OdojData.createFromCursor(c)

        c.close()
        db.close()

        val body = FormBody.Builder()
                .add("id_sync",odoj.odojId.toString())
                .add("from",odoj.ayat_id.toString())
                .add("to",odoj.ayat_to.toString())
                .add("letters",odoj.letter_count.toString())
                .add("id_user",userdata.id)
                .add("time",odoj.time.toString())
        val request = Request.Builder()
                .post(body.build())
                .url(ExtActivity.server("api2/odoj"))
                .build()

        ExtActivity.ApiCall(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                try {
                    val req = Configs.mapper().readValue(string,RequestResult::class.java)
                    lastID = odoj.odojId
                    synchronize()
                }catch (e : Exception){
                    e.printStackTrace()
                }
            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {

            }
        },this)
    }
}
