package exairie.com.quranq.services

import android.app.Service
import android.content.ContentValues
import android.content.Intent
import android.os.IBinder
import exairie.com.quranq.ActivityNotification
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.Notifications
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class NotificationLoader : Service() {
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        val loginInfo = Configs.getLoginInfo()
        if (loginInfo == null) {
            stopSelf()
            return START_NOT_STICKY
        }

        val body = FormBody.Builder()
                .add("id_user",loginInfo.id.toString())
                .build()
        val request = Request.Builder()
                .post(body)
                .url(ExtActivity.server("api2/notif"))
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val array = JSONObject(string).getString("data")
                val notifications = Configs.mapper().readValue(array, Array<Notifications>::class.java)
                var firstId : Long = -1;
                if(notifications.size < 1) return;

                val db = DBHelper(this@NotificationLoader)
                for(notif in notifications){
                    if(firstId == -1L) firstId = notif.id
                    notif.parseCreate_date(notif.create)
                    notif.parseExpiration_date(notif.expire)


                    val values = ContentValues()
                    values.put("id",notif.id)
                    values.put("notif_text",notif.notif_text)
                    values.put("notif_desc",notif.notif_desc)
                    values.put("notif_img",notif.notif_img)
                    values.put("expiration_date",notif.expiration_date)
                    values.put("create_date",notif.create_date)
                    values.put("viewed",0)

                    db.writableDatabase.insert("notifications_pending",null,values)

                }

                db.close()

                val notifIntent = Intent(applicationContext,ActivityNotification::class.java)
                notifIntent.putExtra("id",firstId)

                startActivity(notifIntent)


            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }

        },this)

        return START_STICKY
    }
    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        return null
    }
}
