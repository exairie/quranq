package exairie.com.quranq.services;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;

import java.io.IOException;
import java.util.Calendar;

import exairie.com.quranq.FragmentDashboardContent;
import exairie.com.quranq.R;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.PrayTime;
import exairie.com.quranq.models.AdzanSettings;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.ProfileData;

public class PrayTimeService extends Service implements LocationListener {
    public static final String ODOJ_NOTIFIER = "exairie.com.quranq.ODOJ_NOTIFIER";
    public static final String PROFILEDATA_UPDATER = "exairie.com.qurqnq.PROFILEDATA_UPDATER";

    @Override
    public void onLocationChanged(Location location) {
        latitude = location.getLatitude();
        longitude = location.getLongitude();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    static double latitude = -1;
    static double longitude = -1;

    public PrayTimeService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent.getBooleanExtra("REMIND_ODOJ",false)){
            alertForOdoj();
        }
        if (latitude == -1 || longitude == -1){
            LocationManager m = (LocationManager) getSystemService(LOCATION_SERVICE);
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                if (m.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                {
                    m.requestSingleUpdate(LocationManager.GPS_PROVIDER, this, null);
                }
                if (m.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)){
                    m.requestSingleUpdate(LocationManager.NETWORK_PROVIDER,this, null);
                }
            }
        }
        Option last_lat = Configs.getConfiguration("last_latitude");
        Option last_long = Configs.getConfiguration("last_longitude");

        latitude = last_lat == null? FragmentDashboardContent.JAKARTA_LATITUDE:Double.parseDouble(last_lat.getValue());
        longitude = last_long == null? FragmentDashboardContent.JAKARTA_LONGITUDE:Double.parseDouble(last_long.getValue());
//        if(intent != null) {
//            longitude = intent.getDoubleExtra("longitude", -1);
//            latitude = intent.getDoubleExtra("latitude", -1);
//        }
        calculateTime(intent);

        return START_STICKY;
    }

    private void alertForOdoj() {

    }

    void calculateTime(Intent intent){
        Option adzanSettingsOpt = Configs.getConfiguration("adzan_settings");
        AdzanSettings adzanSettings = null;
        if(adzanSettingsOpt != null){
            try {
                adzanSettings = Configs.mapper().readValue(adzanSettingsOpt.getValue(),AdzanSettings.class);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
        PrayTime.PrayTimeInformation information = PrayTime.getInformation(latitude,longitude,7, adzanSettings);
        PrayTime.CurrentPrayTime currentPrayTime = new PrayTime.CurrentPrayTime(information);

        Option o = Configs.getConfiguration("adzan_settings");
        if(o != null){
            try {
                AdzanSettings as = Configs.mapper().readValue(o.getValue(),AdzanSettings.class);
                currentPrayTime.useSettings(as);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        NotificationManager nm = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        Intent intentNew = new Intent(this,this.getClass());
        intentNew.putExtra("recurring",true);

        PendingIntent i = PendingIntent.getService(this,100, intentNew, 0);

        NotificationCompat.Builder notif = new NotificationCompat.Builder(getApplicationContext());
        notif.setContentIntent(i)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.app_icon))
                .setSmallIcon(R.drawable.ic_timer_black_24dp);

        notif.setContentTitle(currentPrayTime.getCurrentShalatName())
                .setContentText(String.format("%s remaining", PrayTime.CurrentPrayTime.getHour(currentPrayTime.getRemaining()).substring(0,5)));

        AlarmManager alarm = (AlarmManager)getSystemService(ALARM_SERVICE);
        alarm.set(AlarmManager.RTC,System.currentTimeMillis() + (60*1000),i);
        startForeground(1,notif.build());
    }
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        //throw new UnsupportedOperationException("Not yet implemented");
        return null;
    }
}
