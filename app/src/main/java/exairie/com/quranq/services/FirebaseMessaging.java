package exairie.com.quranq.services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.Random;

import exairie.com.quranq.ActivityEventDetail;
import exairie.com.quranq.R;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.Event;
import exairie.com.quranq.models.Option;

public class FirebaseMessaging extends FirebaseMessagingService {
    public final static String MSG_COMMANDKEY = "command";
    public final static String COMMAND_REFRESH_GURU = "1";
    public final static String COMMAND_REFRESH_TAJWID = "2";
    public final static String COMMAND_REFRESH_KAJIAN = "4";
    public final static String COMMAND_NEW_EVENT = "8";
    public final static String COMMAND_TRIGGER_NOTIFICATION = "16";

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
        Log.d("HANDLEINTENT","IM CALLED BIATCH");
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("FIREBASE","MSG RECEIVED");
        Map<String, String> data = remoteMessage.getData();
        if (!data.containsKey(MSG_COMMANDKEY)) return;

        String command = data.get(MSG_COMMANDKEY);

        if (command.equals(COMMAND_REFRESH_GURU)){
            Intent i = new Intent(getApplicationContext(), DataLoader.class);
            i.putExtra("flag", DataLoader.Companion.getFLAG_UPDATE_GURU());
            startService(i);
        }
        if (command.equals(COMMAND_REFRESH_KAJIAN)){
            Intent i = new Intent(getApplicationContext(), DataLoader.class);
            i.putExtra("flag", DataLoader.Companion.getFLAG_UPDATE_KAJIAN());
            startService(i);
        }
        if(command.equals(COMMAND_NEW_EVENT)){
            Long event_id = Long.valueOf(data.get("event_id"));
            sendEventNotification(event_id);
        }
        if(command.equals(COMMAND_TRIGGER_NOTIFICATION)){
            if(Configs.getLoginInfo() == null) return;

            Intent i = new Intent(getApplicationContext(),NotificationLoader.class);
            startService(i);
        }

    }
    public void sendEventNotification(Long event_id){
        DataLoader.Companion.getEventDetail(event_id, new DataLoader.LoadResult<JSONObject>() {
            @Override
            public void finish(JSONObject result) {
                if(result != null){
                    try {
                        Event e = Configs.mapper().readValue(result.optString("data"),Event.class);
                        if(e.notification_radius <= 0) return;
                        Option lastlat = Configs.getConfiguration("last_latitude");
                        Option lastlong = Configs.getConfiguration("last_longitude");

                        if(lastlat == null || lastlong == null) return;

                        Location myLocation = new Location("");
                        myLocation.setLatitude(Double.parseDouble(lastlat.getValue()));
                        myLocation.setLongitude(Double.parseDouble(lastlong.getValue()));

                        Location eventLocation = new Location("");
                        eventLocation.setLatitude(e.latitude);
                        eventLocation.setLongitude(e.longitude);

                        Double meters = e.notification_radius * 1000;

                        if(myLocation.distanceTo(eventLocation) <= meters){
                            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            NotificationCompat.Builder notification = new NotificationCompat.Builder(FirebaseMessaging.this);

                            Intent notificationIntent = new Intent(getApplicationContext(), ActivityEventDetail.class);
                            notificationIntent.putExtra("id",e.id);

                            notification.setContentTitle("Event baru didekat anda!");
                            notification.setContentText(String.format("%s, %s",e.nama_event, DateFormat.format("dd MMM yyyy",e.date_start)));
                            notification.setContentIntent(PendingIntent.getActivity(getApplicationContext(),1099,notificationIntent,0));

                            notification.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.logoapp));
                            notification.setSmallIcon(R.drawable.ic_notifications_amber_500_24dp);

                            notificationManager.notify(new Random().nextInt(),notification.build());
                        }
                    } catch (JsonParseException e) {
                        e.printStackTrace();
                    } catch (JsonMappingException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }else{

                }
            }
        },this);
    }
}
