package exairie.com.quranq.services

import android.app.Service
import android.content.ContentValues
import android.content.Intent
import android.os.IBinder
import android.text.TextUtils
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Event
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.RequestResult
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class EventLoader : Service() {

    override fun onBind(intent: Intent): IBinder? =  null

    companion object {
        const val EVENT_LOADED: String = "exairie.com.quranq.EVENT_LOADED"
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val search = intent?.getStringExtra("search")
        val filter = intent?.getStringExtra("filter")
        var lastid : Long = -1
        val singleid = intent?.getLongExtra("id",-1) ?: -1
        val db = DBHelper(this)
        val c = db.readableDatabase.rawQuery("SELECT max(id) FROM events", arrayOf())

        if (c.count > 0){
            c.moveToFirst()
            lastid = c.getLong(0)
        }

        c.close()
        db.close()

        var url = if (singleid < 0) "api2/event" else "api2/event/$singleid"

        val lst = mutableListOf<String>()
        if (search != null) lst.add("search="+search)
        if (filter != null) lst.add("filter="+filter)
        if (lastid != -1L) lst.add("lastid="+lastid)
        if (lst.size > 0) {
            url += "?"
            url += TextUtils.join("&", lst.toTypedArray())
        }

        val request = Request.Builder()
                .url(ExtActivity.server(url))
                .get()
                .build()
        ExtActivity.ApiCall(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) {

            }

            override fun OnSuccess(string: String?) {
                val res = Configs.mapper().readValue(string, RequestResult::class.java)
                if (res.status == "ok"){
                    if (res.message == "success"){
                        val JSON = JSONObject(string)
                        val jData = JSON.getString("data")

                        val events = Configs.mapper().readValue(jData, Array<Event>::class.java)

                        val db = DBHelper(this@EventLoader)
                        for (e in events){
                            val c = ContentValues()
                            c.put("id",e.id)
                            c.put("id_user",e.id_user)
                            c.put("nama_event",e.nama_event)
                            c.put("deskripsi",e.deskripsi)
                            c.put("latitude",e.latitude)
                            c.put("longitude",e.longitude)
                            c.put("notification_radius",e.notification_radius)
                            c.put("date_start",e.date_start)
                            c.put("date_end",e.date_end)
                            c.put("date_create",e.date_create)
                            c.put("date_edit",e.date_edit)
                            c.put("contact_person",e.contact_person)
                            c.put("status",e.status)

                            db.writableDatabase.replace("events","null",c)
                        }
                        db.close()
                        sendBroadcast(Intent(EVENT_LOADED))
                    }
                    else if(res.message == "single"){
                        val JSON = JSONObject(string)
                        val data = JSON.getString("data")

                        val e = Configs.mapper().readValue(data,Event::class.java)
                        val c = ContentValues()
                        c.put("id",e.id)
                        c.put("id_user",e.id_user)
                        c.put("nama_event",e.nama_event)
                        c.put("deskripsi",e.deskripsi)
                        c.put("latitude",e.latitude)
                        c.put("longitude",e.longitude)
                        c.put("notification_radius",e.notification_radius)
                        c.put("date_start",e.date_start)
                        c.put("date_end",e.date_end)
                        c.put("date_create",e.date_create)
                        c.put("date_edit",e.date_edit)
                        c.put("contact_person",e.contact_person)
                        c.put("status",e.status)

                        val db = DBHelper(this@EventLoader)
                        val id = db.writableDatabase.replace("events","null",c)
                        db.close()

                        val i = Intent(EVENT_LOADED)
                        i.putExtra("id",id)
                        sendBroadcast(i)
                    }
                }
            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }

        },this)


        return START_STICKY
    }
}
