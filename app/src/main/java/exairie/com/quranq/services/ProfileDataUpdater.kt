package exairie.com.quranq.services

import android.app.AlarmManager
import android.app.IntentService
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import android.util.Log
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.R
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.ProfileData
import exairie.com.quranq.models.QoriVid
import exairie.com.quranq.services.PrayTimeService.PROFILEDATA_UPDATER
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import java.util.*


/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 *
 * TODO: Customize class - update intent actions and extra parameters.
 */
class ProfileDataUpdater : IntentService("ProfileDataUpdater") {
    override fun onHandleIntent(intent: Intent?) {
        val downloadMode = intent!!.getStringExtra("download")
        if (downloadMode != null) {
            initiateDownload()
        } else {
            initiateUpload()
        }
    }
    private fun initiateUpload(){
        setupTask()
        val loginInfo = Configs.getLoginInfo();

        if(loginInfo == null){
            Log.d("ProfileDataUpdater","Logininfo is null");
            return
        }

        val db = DBHelper(this)
        val data = ProfileData()
        data.populate(db)
        val jsonData = Configs.mapper().writeValueAsString(data)

        Log.d("JSONDATA", jsonData)

        val body = FormBody.Builder()
                .add("id_user",loginInfo.id.toString())
                .add("profile_data",jsonData)
                .add("time",System.currentTimeMillis().toString())
        val request = Request.Builder()
                .url(ExtActivity.server("api2/profile"))
                .post(body.build())
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notification = NotificationCompat.Builder(this@ProfileDataUpdater)
                        .setContentTitle("Profile data")
                        .setContentText("Your profile data has been synchronized")
                        .setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.app_icon))
                        .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                notifManager.notify(1201,notification.build())
            }
            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }

        },this)
    }

    private fun setupTask() {
        val today = Calendar.getInstance()
        today.set(Calendar.HOUR_OF_DAY, 12)
        today.set(Calendar.MINUTE, 30)
        val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val iProfileUpdater = Intent(this, ProfileDataUpdater::class.java)
        iProfileUpdater.action = PROFILEDATA_UPDATER
//        iProfileUpdater.setAction();
        val iChk2 = PendingIntent.getService(this, 1201, iProfileUpdater, PendingIntent.FLAG_NO_CREATE)
        if (iChk2 == null) {
            val i2 = Intent(this, ProfileDataUpdater::class.java)

            val pProfileUpdater = PendingIntent.getService(applicationContext, 1201, i2, 0)

            alarm.set(AlarmManager.RTC, System.currentTimeMillis() + (1000 * 60 * 60 * 24), pProfileUpdater)
        }
    }

    private fun initiateDownload() {
        val loginInfo = Configs.getLoginInfo();

        if(loginInfo == null){
            Log.d("ProfileDataUpdater","Logininfo is null");
            return
        }

        val db = DBHelper(this)
        val data = ProfileData()
        data.populate(db)

        val body = FormBody.Builder()
                .add("id_user",loginInfo.id.toString())

        val request = Request.Builder()
                .url(ExtActivity.server("api2/profile"))
                .post(body.build())
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val json = JSONObject(string)

                val data = ProfileData()
                data.fetch(json.optJSONObject("data").optString("profile_data"), DBHelper(this@ProfileDataUpdater))


                val notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notification = NotificationCompat.Builder(this@ProfileDataUpdater)
                        .setContentTitle("Profile data")
                        .setContentText("Your new profile data has been synchronized")
                        .setLargeIcon(BitmapFactory.decodeResource(resources,R.mipmap.app_icon))
                        .setSmallIcon(R.drawable.ic_notifications_black_24dp)
                notifManager.notify(1201,notification.build())


            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }

        },this)
    }
}
