package exairie.com.quranq.services

import android.Manifest
import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.location.LocationManager
import android.content.pm.PackageManager
import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.content.Context
import android.location.Location
import android.location.LocationListener
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import exairie.com.quranq.services.PrayTimeService.longitude
import exairie.com.quranq.services.PrayTimeService.latitude
import exairie.com.quranq.FragmentDashboardContent
import exairie.com.quranq.services.PrayTimeService.longitude
import exairie.com.quranq.services.PrayTimeService.latitude
import exairie.com.quranq.libs.Configs
import io.fabric.sdk.android.services.settings.IconRequest.build
import android.app.AlarmManager
import exairie.com.quranq.libs.PrayTime
import exairie.com.quranq.R.drawable.ic_timer_black_24dp
import exairie.com.quranq.R.mipmap.app_icon
import android.graphics.BitmapFactory
import android.app.PendingIntent
import android.app.NotificationManager
import android.app.PendingIntent.*
import android.graphics.Bitmap
import android.os.Handler
import android.support.v4.app.NotificationCompat
import exairie.com.quranq.R
import exairie.com.quranq.models.AdzanSettings
import exairie.com.quranq.services.PrayTimeService.longitude
import exairie.com.quranq.services.PrayTimeService.latitude
import java.io.IOException
import java.util.*


class PrayTracker : Service(),LocationListener {

    private var notifBigIcon: Bitmap? = null
    override fun onLocationChanged(location: Location?) {
        latitude = location?.getLatitude() ?: FragmentDashboardContent.JAKARTA_LATITUDE ;
        longitude = location?.getLongitude() ?: FragmentDashboardContent.JAKARTA_LONGITUDE ;
    }

    override fun onStatusChanged(p0: String?, p1: Int, p2: Bundle?) {
    }

    override fun onProviderEnabled(p0: String?) {
    }

    override fun onProviderDisabled(p0: String?) {
    }

    override fun onBind(intent: Intent): IBinder? =// TODO: Return the communication channel to the service.
            null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val m = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (m.allProviders.contains(LocationManager.GPS_PROVIDER)) {
                m.requestLocationUpdates(LocationManager.GPS_PROVIDER,10000,10f, this)
            }
            if (m.allProviders.contains(LocationManager.NETWORK_PROVIDER)) {
                m.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,10000,10f, this)
            }
        }

        val last_lat = Configs.getConfiguration("last_latitude")
        val last_long = Configs.getConfiguration("last_longitude")

        latitude = if (last_lat == null) FragmentDashboardContent.JAKARTA_LATITUDE else java.lang.Double.parseDouble(last_lat.value)
        longitude = if (last_long == null) FragmentDashboardContent.JAKARTA_LONGITUDE else java.lang.Double.parseDouble(last_long.value)

        val timer = Timer()
        timer.schedule(object : TimerTask() {
            override fun run() {
                calculateTime()
            }

        },0,60*1000)


        setupOdojNotifier()
        setupDataSync()

        return START_STICKY
    }

    private fun setupDataSync() {
        if(!syncStarted){
            val iSync = Intent(this,ProfileDataUpdater::class.java)
            val piSync = PendingIntent.getService(this,101,iSync, 0)
            val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,0,12 * 60 * 60 * 1000,
                    piSync)
            syncStarted = true
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        notifBigIcon?.recycle()
        notifBigIcon = null
    }
    private fun setupOdojNotifier() {
        val now = Calendar.getInstance()
        val iOdoj = Intent(applicationContext,OdojNotifier::class.java)

        if(now.get(Calendar.HOUR_OF_DAY) > 12){
            now.add(Calendar.DAY_OF_MONTH,1)
        }
        now.set(Calendar.HOUR_OF_DAY,12)
        now.set(Calendar.MINUTE,0)
        now.set(Calendar.SECOND,0)
        val interval = now.timeInMillis - Calendar.getInstance().timeInMillis

        if(!odojStarted){

            val piOdoj = PendingIntent.getService(this,100,iOdoj,0)
            val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP,interval,24 * 60 * 60 * 1000,piOdoj)
            odojStarted = true
        }
    }


    fun calculateTime() {
        val adzanSettingsOpt = Configs.getConfiguration("adzan_settings")
        var adzanSettings: AdzanSettings? = null
        if (adzanSettingsOpt != null) {
            try {
                adzanSettings = Configs.mapper().readValue(adzanSettingsOpt.value, AdzanSettings::class.java)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }
        val information = PrayTime.getInformation(latitude, longitude, 7.0, adzanSettings)
        val currentPrayTime = PrayTime.CurrentPrayTime(information)

        val o = Configs.getConfiguration("adzan_settings")
        if (o != null) {
            try {
                val `as` = Configs.mapper().readValue(o.value, AdzanSettings::class.java)
                currentPrayTime.useSettings(`as`)
            } catch (e: IOException) {
                e.printStackTrace()
            }

        }

        val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val intentNew = Intent(this, this.javaClass)
        intentNew.putExtra("recurring", true)

        val i = PendingIntent.getService(this, 100, intentNew, 0)

        if(notifBigIcon == null){
            notifBigIcon = BitmapFactory.decodeResource(resources, R.mipmap.app_icon)
        }
        val notif = NotificationCompat.Builder(applicationContext)
        notif.setContentIntent(i)
                .setLargeIcon(notifBigIcon!!)
                .setSmallIcon(R.drawable.ic_timer_black_24dp)

        notif.setContentTitle(currentPrayTime.currentShalatName)
                .setContentText(String.format("%s remaining", PrayTime.CurrentPrayTime.getHour(currentPrayTime.remaining).substring(0, 5)))

        startForeground(1, notif.build())
    }

    companion object {
        var odojStarted = false
        var syncStarted = false
        var latitude = -1.0
        var longitude = -1.0
    }
}
