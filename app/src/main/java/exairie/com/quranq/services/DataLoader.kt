package exairie.com.quranq.services

import android.app.NotificationManager
import android.app.Service
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.IBinder
import android.support.v4.app.NotificationCompat
import android.util.Log

import java.io.IOException
import java.util.ArrayList

import exairie.com.quranq.ExtActivity
import exairie.com.quranq.R
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper

import exairie.com.quranq.ExtActivity.server
import exairie.com.quranq.models.*
import okhttp3.*
import org.json.JSONObject

class DataLoader : Service() {
    internal val NOTIFICATION_ID_GURU = 18
    internal val NOTIFICATION_ID_KAJIAN = 16
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        val updateFlag = intent.getIntExtra("flag", -1)
        if (updateFlag and FLAG_UPDATE_GURU == FLAG_UPDATE_GURU) {
            loadGuru()
        }
        if (updateFlag and FLAG_UPDATE_KAJIAN == FLAG_UPDATE_KAJIAN) {
            loadKajian()
        }
        return Service.START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    private fun loadKajian() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notification = NotificationCompat.Builder(this)

        notification.setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_icon))
                .setSmallIcon(R.drawable.ic_sync_black_24dp)
                .setContentTitle("QuranQu - Updating Database")
                .setContentText("Preparing")
                .setOngoing(true)

        notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.build())
        val client = OkHttpClient()
        val request = Request.Builder()
                .url(server("api/kajian"))
                .build()
        notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Meminta data kajian").build())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
                e.printStackTrace()
                notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Terjadi kesalahan!").setOngoing(false).build())
                return
            }

            @Throws(IOException::class)
            override fun onResponse(call: okhttp3.Call, response: Response) {
                notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Memproses data kajian").build())
                val responseString = response.body().string()
                Log.d("NETREQUESTRESULT", responseString)
                val mapper = Configs.mapper()
                try {
                    val r = mapper.readValue(responseString, RequestResult::class.java)

                    if (r.status == "ok") {
                        //onAfterRequest.OnSuccess(responseString);
                        val dataGuru = mapper.readValue(responseString, iKajian::class.java)

                        if (dataGuru.listKajian.size > 0) {
                            val db = DBHelper(this@DataLoader)

                            val g = ArrayList<Kajian>()
                            val ids = ArrayList<Long>()
                            val c = db.readableDatabase.rawQuery("SELECT * FROM kajian", arrayOf())
                            if (c.count > 0) {
                                c.moveToFirst()
                                while (!c.isAfterLast) {
                                    g.add(Kajian.create(c))
                                    c.moveToNext()
                                }
                            }

                            for (kajian in dataGuru.listKajian) {
                                val values = ContentValues()
                                values.put("id", kajian.id)
                                values.put("judul", kajian.judul)
                                values.put("gambar", kajian.gambar)
                                values.put("isi", kajian.isi)
                                values.put("create_date", kajian.create_date)

                                db.writableDatabase.replace("kajian", "null", values)
                            }
                            for (kajian in g) {
                                var pass = false
                                Log.d("CHECK KAJIAN", String.format("idlocal %d check", kajian.id))
                                var gr: Kajian? = null
                                for (gc in dataGuru.listKajian) {
                                    Log.d("CHECK KAJIAN", String.format("idupdate %d", gc.id))
                                    if (gc.id == kajian.id) {
                                        Log.d("CHECK KAJIAN", String.format("id %d pass", kajian.id))
                                        pass = true
                                        gr = gc
                                        break
                                    }
                                }
                                if (!pass) {
                                    ids.add(kajian.id)
                                    Log.d("CHECK KAJIAN", String.format("id %d delete", kajian.id))
                                }
                            }
                            if (ids.size > 0) {
                                for (d in ids) {
                                    db.writableDatabase.delete("kajian", "id = ?", arrayOf(d.toString()))
                                    Log.d("DELETE KAJIAN", "ID " + d)
                                }
                            }
                            c.close()
                        }
                        notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Data Kajian Tersimpan!").setOngoing(false).build())
                        sendBroadcast(Intent(BROADCAST_DATA_KAJIAN_UPDATED))

                    } else {
                        notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Terjadi kesalahan server").setOngoing(false).build())
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    notificationManager.notify(NOTIFICATION_ID_KAJIAN, notification.setContentText("Terjadi kesalahan penyimpanan").setOngoing(false).build())
                    //onAfterRequest.OnFail(e.getMessage());
                } finally {
                }
            }
        })
    }

    fun loadGuru() {
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        val notification = NotificationCompat.Builder(this)

        notification.setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_icon))
                .setSmallIcon(R.drawable.ic_sync_black_24dp)
                .setContentTitle("QuranQu - Updating Database")
                .setContentText("Preparing")
                .setOngoing(true)

        notificationManager.notify(NOTIFICATION_ID_GURU, notification.build())
        val client = OkHttpClient()
        val request = Request.Builder()
                .url(server("api/guru"))
                .build()
        notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Meminta data guru").build())
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
                e.printStackTrace()
                notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Terjadi kesalahan!").setOngoing(false).build())
                return
            }

            @Throws(IOException::class)
            override fun onResponse(call: okhttp3.Call, response: Response) {
                notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Memproses data guru").build())
                val responseString = response.body().string()
                Log.d("NETREQUESTRESULT", responseString)
                val mapper = Configs.mapper()
                try {
                    val r = mapper.readValue(responseString, RequestResult::class.java)

                    if (r.status == "ok") {
                        //onAfterRequest.OnSuccess(responseString);
                        val dataGuru = mapper.readValue(responseString, iGuru::class.java)

                        if (dataGuru.listGuru.size > 0) {
                            val db = DBHelper(this@DataLoader)

                            val g = ArrayList<Guru>()
                            val ids = ArrayList<Long>()
                            val c = db.readableDatabase.rawQuery("SELECT * FROM guru", arrayOf())
                            if (c.count > 0) {
                                c.moveToFirst()
                                while (!c.isAfterLast) {
                                    g.add(Guru.create(c))
                                    c.moveToNext()
                                }
                            }

                            for (guru in dataGuru.listGuru) {
                                val values = ContentValues()
                                values.put("id", guru.id)
                                values.put("nama_guru", guru.nama_guru)
                                values.put("phone", guru.phone)
                                values.put("location_lat", guru.location_lat)
                                values.put("location_long", guru.location_long)
                                values.put("address", guru.address)

                                db.writableDatabase.replace("guru", "null", values)
                            }
                            for (guru in g) {
                                var pass = false
                                Log.d("CHECK GURU", String.format("idlocal %d check", guru.id))
                                var gr: Guru? = null
                                for (gc in dataGuru.listGuru) {
                                    Log.d("CHECK GURU", String.format("idupdate %d", gc.id))
                                    if (gc.id == guru.id) {
                                        Log.d("CHECK GURU", String.format("id %d pass", guru.id))
                                        pass = true
                                        gr = gc
                                        break
                                    }
                                }
                                if (!pass) {
                                    ids.add(guru.id)
                                    Log.d("CHECK GURU", String.format("id %d delete", guru.id))
                                }
                            }
                            if (ids.size > 0) {
                                for (d in ids) {
                                    db.writableDatabase.delete("guru", "id = ?", arrayOf(d.toString()))
                                    Log.d("DELETE GURU", "ID " + d)
                                }

                            }
                            c.close()
                        }
                        notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Data Guru Tersimpan!").setOngoing(false).build())
                        sendBroadcast(Intent(BROADCAST_DATA_GURU_UPDATED))

                    } else {
                        notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Terjadi kesalahan server").setOngoing(false).build())
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    notificationManager.notify(NOTIFICATION_ID_GURU, notification.setContentText("Terjadi kesalahan penyimpanan").setOngoing(false).build())
                    //onAfterRequest.OnFail(e.getMessage());
                } finally {
                }
            }
        })
    }

    companion object {
        val FLAG_UPDATE_GURU = 1
        val FLAG_UPDATE_KAJIAN = 2
        val BROADCAST_DATA_GURU_UPDATED = "exairie.com.quranq.GURU_UPDATED"
        val BROADCAST_DATA_KAJIAN_UPDATED = "exairie.com.quranq.KAJIAN_UPDATED"

        fun getEventDetail(id_event : Long, callback : LoadResult<JSONObject?>, c:Context){
            val request = Request.Builder()
                    .get()
                    .url(ExtActivity.server("api2/event/"+id_event.toString()))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun getUserData(id : Long, callback : LoadResult<UserLogin?>, c : Context){
            val body = FormBody.Builder()
                    .add("id",id.toString())
                    .build()
            val request = Request.Builder()
                    .post(body)
                    .url(ExtActivity.server("api/userdata"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = Configs.mapper().readValue(JSONObject(string).getString("data"), UserLogin::class.java)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun getEventRegisterStatus(id_event : Long, callback : LoadResult<JSONObject?>, c : Context){
            val login = Configs.getLoginInfo();
            if(login == null){
                callback.finish(null)
                return
            }

            val request = Request.Builder()
                    .get()
                    .url(ExtActivity.server("api2/myevent?id=$id_event&id_user=${login.id}"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun getCurrentRegisteredEvent(callback : LoadResult<JSONObject?>, c : Context){
            val login = Configs.getLoginInfo()
            if(login == null){
                callback.finish(null)
                return
            }
            val request = Request.Builder()
                    .get()
                    .url(ExtActivity.server("api2/myevent?id_user=${login.id}"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun getCurrentOrganizingEvent(callback : LoadResult<JSONObject?>, c : Context){
            val login = Configs.getLoginInfo()
            if(login == null){
                callback.finish(null)
                return
            }
            val request = Request.Builder()
                    .get()
                    .url(ExtActivity.server("api2/currentevent/${login.id}"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun getEventParticipationData(id_event : Long, callback : LoadResult<JSONObject?>, c : Context){
            val body = FormBody.Builder()
                    .add("id_event",id_event.toString())
                    .build()
            val request = Request.Builder()
                    .post(body)
                    .url(ExtActivity.server("api/eventregistrationdata"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }
        fun cancelEventRegistration(book_id: Long, callback : LoadResult<JSONObject?>,c : Context){
            val body = FormBody.Builder()
                    .add("id", book_id.toString())
                    .build()
            val request = Request.Builder()
                    .delete(body)
                    .url(ExtActivity.server("api2/registerevent"))
                    .build()

            ExtActivity.ApiCall(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val data = JSONObject(string)
                    callback.finish(data)
                }

                override fun OnFail(msg: String?) {
                    callback.finish(null)
                }

                override fun Finish() {
                }

            },c)
        }

    }
    interface LoadResult<T>{
        fun finish(result : T?)
    }

}
