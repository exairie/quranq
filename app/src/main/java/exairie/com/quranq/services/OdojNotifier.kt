package exairie.com.quranq.services

import android.app.AlarmManager
import android.app.IntentService
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v4.app.NotificationCompat
import exairie.com.quranq.R
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.services.PrayTimeService.ODOJ_NOTIFIER
import java.util.*


/**
 * An [IntentService] subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 *
 *
 * TODO: Customize class - update intent actions and extra parameters.
 */
class OdojNotifier : IntentService("OdojNotifier"){
    override fun onHandleIntent(p0: Intent?) {
        val o = Configs.getConfiguration("odoj_reminder")
        if (o != null) {
            if (o.value.toBoolean()) {
                val nm = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                val notif = NotificationCompat.Builder(applicationContext)

                notif.setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.app_icon))
                        .setSmallIcon(R.drawable.ic_timer_black_24dp)
                        .setContentText("Pengingat untuk anda")
                        .setContentTitle("One Day One Juz")

                nm.notify(100, notif.build())
            }
        }

        setupTask()
    }

    fun setupTask(){
        val today = Calendar.getInstance()
        today.set(Calendar.HOUR_OF_DAY, 12)
        today.set(Calendar.MINUTE, 30)
        val alarm = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val o = Configs.getConfiguration("odoj_reminder")
        val i = Intent(applicationContext, OdojNotifier::class.java)
        i.action = ODOJ_NOTIFIER


        val iChk = PendingIntent.getService(this, 6905, i, PendingIntent.FLAG_NO_CREATE)
        if (iChk == null) {

            val pendingIntent = PendingIntent.getService(applicationContext, 6905, i, 0)

            alarm.setInexactRepeating(AlarmManager.RTC, today.getTimeInMillis(), (1000 * 60 * 60 * 24).toLong(), pendingIntent)
        }
    }
}
