package exairie.com.quranq.services

import android.app.Service
import android.content.Intent
import android.os.IBinder
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.QoriVid
import org.json.JSONArray
import org.json.JSONObject

class UserDataSynchronizer : Service() {

    override fun onBind(intent: Intent): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val db = DBHelper(this)
        val json = JSONObject()

        var c = db.readableDatabase.rawQuery("SELECT * FROM qori_recent order by time desc ", arrayOf())


        if (c.count > 0){
            c.moveToFirst()
            val jArray = JSONArray()
            while (!c.isAfterLast) {
                val json = JSONObject()
                json.put("id",c.getLong(0))
                json.put("recent",c.getLong(1))
                json.put("time",c.getLong(2))
                jArray.put(json)
                c.moveToNext()
            }
            json.put("qori_recent",jArray)
        }
        c.close()

        c = db.readableDatabase.rawQuery("SELECT * FROM qori_fav", arrayOf())


        if (c.count > 0){
            c.moveToFirst()
            val jArray = JSONArray()
            while (!c.isAfterLast) {
                val json = JSONObject()
                json.put("id",c.getLong(0))
                json.put("favorite",c.getLong(1))
                jArray.put(json)
                c.moveToNext()
            }
            json.put("qori_fav",jArray)
        }
        c.close()

        c = db.readableDatabase.rawQuery("SELECT * FROM fahmil_result", arrayOf())


        if (c.count > 0){
            c.moveToFirst()
            val jArray = JSONArray()
            while (!c.isAfterLast) {
                val json = JSONObject()
                json.put("id",c.getLong(c.getColumnIndexOrThrow("id")))
                json.put("question",c.getString(c.getColumnIndexOrThrow("question")))
                json.put("answer_true",c.getString(c.getColumnIndexOrThrow("answer_true")))
                json.put("answer_false",c.getString(c.getColumnIndexOrThrow("answer_false")))
                json.put("session_start",c.getLong(c.getColumnIndexOrThrow("session_start")))
                json.put("session_end",c.getLong(c.getColumnIndexOrThrow("session_end")))
                jArray.put(json)
                c.moveToNext()
            }
            json.put("fahmil_result",jArray)
        }
        c.close()

        c = db.readableDatabase.rawQuery("SELECT * FROM quran_bookmark", arrayOf())


        if (c.count > 0){
            c.moveToFirst()
            val jArray = JSONArray()
            while (!c.isAfterLast) {
                val json = JSONObject()
                json.put("id",c.getLong(c.getColumnIndexOrThrow("id")))
                json.put("bookmark_time",c.getLong(c.getColumnIndexOrThrow("bookmark_time")))
                json.put("ayat_id",c.getInt(c.getColumnIndexOrThrow("ayat_id")))

                jArray.put(json)
                c.moveToNext()
            }
            json.put("quran_bookmark",jArray)
        }
        c.close()


        return START_STICKY
    }
}
