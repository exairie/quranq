package exairie.com.quranq.services

import android.app.Service
import android.content.ContentValues
import android.content.Intent
import android.os.IBinder
import android.text.TextUtils
import android.util.Log
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.QoriVid
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONArray
import org.json.JSONObject

class QoriVidLoader : Service() {
    companion object {
        val BROADCAST_QORIVID_UPDATED : String = "exairie.com.quranq.services.QORIVID_UPDATED"
    }
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        val search = intent!!.getStringExtra("search")
        val type = intent.getStringExtra("type")

        val db = DBHelper(this)
        val c = db.readableDatabase.rawQuery("SELECT id,added from qori", arrayOf<String>())

        val ids = mutableListOf<QoriVid.QoriSync>()

        if (c.count > 0){
            c.moveToFirst()

            while (!c.isAfterLast){
                val qs = QoriVid.QoriSync()
                qs.id = c.getLong(0)
                qs.timestamp = c.getLong(1)
                ids.add(qs)

                c.moveToNext()
            }
        }

        c.close()

        val sync = Configs.mapper().writeValueAsString(ids.toTypedArray())
        Log.d("SYNCVAL",String.format("%s",sync.toString()))

        val body = FormBody.Builder()
                .add("sync",sync.toString())
        if (search != null) body.add("search",search.toString())
        if (type != null) body.add("type",type.toString())
        val request = Request.Builder()
                .url(ExtActivity.server("api/qori_vid"))
                .post(body.build())
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
                //TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun OnSuccess(string: String?) {
                val o = JSONObject(string)
                if (o.getString("status") == "ok"){
                    val json = o.getString("data")
                    val syncJson = o.getString("sync");
                    val vids = Configs.mapper().readValue(json, Array<QoriVid>::class.java)


                        val db = DBHelper(this@QoriVidLoader)

                    if (syncJson != null){
                        val arr = JSONArray(syncJson)
                        for (i in 0 until arr.length()){
                            val syncObj = arr.getJSONObject(i)
                            if (syncObj.getString("id") == null) continue

                            if (syncObj.getString("status") == "ok") continue
                            else if(syncObj.getString("status") == "renew"){
                                val v = Configs.mapper().readValue(syncObj.getString("data"), QoriVid::class.java)
                                val c = ContentValues()
                                c.put("id",v.id)
                                c.put("title",v.title)
                                c.put("category",v.category)
                                c.put("reciter",v.reciter)
                                c.put("reference",v.reference)
                                c.put("added",v.added)
                                c.put("youtube_url",v.youtube_url)
                                c.put("cat",v.cat)
                                db.writableDatabase.update("qori",c,"id = ?", arrayOf(v.id.toString()))
                            }
                            else if(syncObj.getString("status") == "delete"){
                                db.writableDatabase.delete("qori","id = ?", arrayOf(syncObj.getString("id")))
                            }

                        }
                    }

                    for (v in vids){
                        val c = ContentValues()
                        c.put("id",v.id)
                        c.put("title",v.title)
                        c.put("category",v.category)
                        c.put("reciter",v.reciter)
                        c.put("reference",v.reference)
                        c.put("added",v.added)
                        c.put("youtube_url",v.youtube_url)
                        c.put("cat",v.cat)
                        db.writableDatabase.replace("qori","null",c)
                    }

                    db.close()

                    sendBroadcast(Intent(BROADCAST_QORIVID_UPDATED))
                }


            }

            override fun OnFail(msg: String?) {

            }

            override fun Finish() {

            }

        },this)
        return START_REDELIVER_INTENT
    }
    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        //throw UnsupportedOperationException("Not yet implemented")
        return null
    }
}
