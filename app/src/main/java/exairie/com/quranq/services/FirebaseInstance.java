package exairie.com.quranq.services;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.*;

import java.io.IOException;

import exairie.com.quranq.ExtActivity;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.Option;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static exairie.com.quranq.ExtActivity.server;

public class FirebaseInstance extends FirebaseInstanceIdService {
    public FirebaseInstance() {
    }

    @Override
    public void onTokenRefresh() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Option tokenOpt = new Option("firebase_token",token);

        Log.d("FIREBASETOKEN",token);
        Configs.setConfiguration(tokenOpt);

        RequestBody body = new FormBody.Builder()
                .add("token",token)
                .build();
        Request request = new Request.Builder()
                .url(server("api/token"))
                .post(body)
                .build();
        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("TokenRefreshQuery",response.body().string());
            }
        });
    }
}
