package exairie.com.quranq.services

import android.app.IntentService
import android.content.Intent
import android.util.Log
import com.google.firebase.iid.FirebaseInstanceId
import exairie.com.quranq.ExtActivity
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.AdzanSettings
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.Option
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class AdzanSettingsUpdater : IntentService("AdzanSettingsUpdater") {
    companion object {
        val ADZAN_SETTINGS_UPLOADED = "exairie.com.quranq.ADZAN_SETTINGS_UPLOADED"
        val ADZAN_SETTINGS_UPDATED = "exairie.com.quranq.ADZAN_SETTINGS_UPDATED"
    }
    override fun onHandleIntent(intent: Intent?) {
        if(intent!!.getStringExtra("type") == "upload"){
            uploadSettings()
        }
        if(intent!!.getStringExtra("type") == "download"){
            downloadSettings()
        }
    }
    fun downloadSettings(){
        val body = FormBody.Builder()
                .add("firebase_id",FirebaseInstanceId.getInstance().token ?:"-")

        val last_lat = Configs.getConfiguration("last_latitude")
        val last_long = Configs.getConfiguration("last_longitude")

        if(last_lat != null){
            body.add("latitude",last_lat.value)
        }
        if(last_long != null){
            body.add("longitude",last_long.value)
        }

        val request = Request.Builder()
                .put(body.build())
                .url(ExtActivity.server("api/adzan_settings"))
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val json = JSONObject(string).getString("data")
                val settings = Configs.mapper().readValue(json,AdzanSettings::class.java)
                val option = Option("adzan_settings",Configs.mapper().writeValueAsString(settings))
                Configs.setConfiguration(option)
                sendBroadcast(Intent(ADZAN_SETTINGS_UPDATED))
            }

            override fun OnFail(msg: String?) {
                Log.d("AdzanUpdater","Error : $msg")
            }

            override fun Finish() {
            }
        },this)
    }
    fun uploadSettings(){
        val option = Configs.getConfiguration("adzan_settings")
        if(option == null) return

        val settings = Configs.mapper().readValue(option.value,AdzanSettings::class.java)
        val body = FormBody.Builder()
                .add("subuh",settings.subuh.toString())
                .add("dzuhur",settings.dzuhur.toString())
                .add("ashar",settings.ashar.toString())
                .add("maghrib",settings.maghrib.toString())
                .add("isya",settings.isya.toString())

        val last_lat = Configs.getConfiguration("last_latitude")
        val last_long = Configs.getConfiguration("last_longitude")
        val city = Configs.getConfiguration("last_loc")
        if(last_lat != null){
            body.add("latitude",last_lat.value)
        }

        if(last_long != null){
            body.add("longitude",last_long.value)
        }

        body.add("city", if(city == null) "" else city.value)

        body.add("firebase_id", FirebaseInstanceId.getInstance().token)

        val request = Request.Builder()
                .post(body.build())
                .url(ExtActivity.server("api/adzan_settings"))
                .build()

        ExtActivity.ApiCall(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                sendBroadcast(Intent(ADZAN_SETTINGS_UPLOADED))
            }

            override fun OnFail(msg: String?) {
                Log.d("AdzanSettings","Error : $msg")
            }

            override fun Finish() {
            }
        },this)
    }

}
