package exairie.com.quranq.services;

import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.text.format.DateFormat;
import android.util.Log;

import java.util.Calendar;

import exairie.com.quranq.ActivityAdzanScreen;
import exairie.com.quranq.ActivitySettings;
import exairie.com.quranq.R;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.Option;


/**
 * Created by exain on 7/16/2017.
 */

public class PrayNotifier extends Service {
    private int adzanOptions;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        long adzantime = intent.getLongExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS,-1);
        int adzanID = intent.getIntExtra(ActivityAdzanScreen.KEY_ADZAN_ID,0);
        Option option = Configs.getConfiguration("adzan_" + adzanID);

        if (option != null){
            try {
                adzanOptions = Integer.parseInt(option.getValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        notifyAdzan(intent);

        Calendar now = Calendar.getInstance();
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(adzantime);


        c.set(Calendar.DAY_OF_MONTH, now.get(Calendar.DAY_OF_MONTH));
        c.add(Calendar.DAY_OF_MONTH,1);


        PendingIntent popupAdzanIsya;

        intent.removeExtra(ActivityAdzanScreen.KEY_ADZAN_TIME);
        intent.removeExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS);

        intent.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, DateFormat.format("HH:mm",c));
        intent.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, c.getTimeInMillis());

        popupAdzanIsya = (PendingIntent.getActivity(getApplicationContext(),10 + adzanID,intent,0));

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,c.getTimeInMillis(),popupAdzanIsya);
        }else{
            alarmManager.set(AlarmManager.RTC_WAKEUP,c.getTimeInMillis(),popupAdzanIsya);
        }

        if (adzantime < System.currentTimeMillis() - (1000*60*5)) {
            return START_NOT_STICKY;
        }else{
            if ((adzanOptions & ActivitySettings.FLAG_PREADZAN) == ActivitySettings.FLAG_PREADZAN){
                Intent i = new Intent(getApplicationContext(),ActivityAdzanScreen.class);
                i.putExtra(ActivityAdzanScreen.KEY_ADZAN_ID, intent.getIntExtra(ActivityAdzanScreen.KEY_ADZAN_ID,-1));
                i.putExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS, intent.getLongExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS,-1));
                i.putExtra(ActivityAdzanScreen.KEY_ADZAN_TIME, intent.getStringExtra(ActivityAdzanScreen.KEY_ADZAN_TIME));
                i.putExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION, intent.getStringExtra(ActivityAdzanScreen.KEY_ADZAN_LOCATION));
                i.putExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL, intent.getStringExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL));
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        }

        return START_NOT_STICKY;
    }

    private void notifyAdzan(Intent intent) {
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(intent.getLongExtra(ActivityAdzanScreen.KEY_ADZAN_MILIS,0));
        NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        NotificationCompat.Builder notif = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_timer_black_24dp)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.app_icon))
                .setContentTitle(intent.getStringExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL))
                .setContentText(String.format("%s Telah tiba - %s",intent.getStringExtra(ActivityAdzanScreen.KEY_ADZAN_LABEL),DateFormat.format("H:mm:ss",c)));
        int notifID = 105;

        manager.notify(notifID, notif.build());

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
