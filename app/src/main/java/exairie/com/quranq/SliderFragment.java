package exairie.com.quranq;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

public class SliderFragment extends Fragment {
    int imgID;

    public SliderFragment() {
    }

    public static SliderFragment newInstance(int imgID) {
        Bundle b = new Bundle();
        b.putInt("imgid", imgID);
        SliderFragment f = new SliderFragment();
        f.setArguments(b);

        return f;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.view_slider_item, container, false);
        ImageView image = v.findViewById(R.id.sliderImg);
        if (getArguments() != null) {
            imgID = getArguments().getInt("imgid");
            Bitmap b = BitmapFactory.decodeResource(getResources(), imgID);
            if (b != null) {
                double hratio = 200.0 / b.getHeight();
                Picasso.with(this.getContext()).load(imgID).resize((int) (b.getWidth() * hratio), 200).into(image);
                b.recycle();
            }
        }
        return v;
    }
}
