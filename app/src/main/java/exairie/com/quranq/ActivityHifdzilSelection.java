package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.QuranDBHelper;

public class ActivityHifdzilSelection extends ExtActivity {
    public static int SELECTION_MODE_SURAT = 1;
    public static int SELECTION_MODE_JUZ = 2;
    public static int SELECTION_MODE_ALL = 4;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.recycler_select)
    RecyclerView recyclerSelect;
    int selectiontype;
    @InjectView(R.id.l_selection_desc)
    TextView lSelectionDesc;
    private SelectionAdapter adapter;
    private Bitmap iconBitmap;

    public ActivityHifdzilSelection() {
        super(R.layout.activity_hifdzil_selection, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);

        Intent i = getIntent();

        selectiontype = i.getIntExtra("mode", -1);

        if (selectiontype == -1) {
            finish();
            return;
        }

        String selection = "juz";

        if(selectiontype == SELECTION_MODE_JUZ)
            selection = "juz";
        else if(selectiontype == SELECTION_MODE_SURAT)
            selection = "surah";

        lSelectionDesc.setText(String.format("Silahkan pilih %s yang akan di quizkan. \nJumlah soal akan di acak dan disesuaikan dengan jumlah ayat yang tersedia",selection));
        setupRecycler();

        Bitmap bmp = BitmapFactory.decodeResource(getResources(), R.drawable.kaligrafiquran_white);
        iconBitmap = Bitmap.createScaledBitmap(bmp, 200, 200, false);
    }

    private void setupRecycler() {
        adapter = new SelectionAdapter();
        recyclerSelect.setAdapter(adapter);
        recyclerSelect.setLayoutManager(new LinearLayoutManager(thisContext()));
    }

    String query_juz() {
        return "select a.juz, a.totalayat, a.minsurat, b.surah_name as minsurahname, a.maxsurat, c.surah_name as maxsurahname from" +
                " (" +
                "    select juz, max(surah_no) as maxsurat, min(surah_no) as minsurat, count(*) as totalayat from quran" +
                "    group by juz" +
                " ) as a, quran_name as b, quran_name as c" +
                " where a.minsurat = b.surah_no and a.maxsurat = c.surah_no";
    }

    String query_surat() {
        return "select a.surah_no, surah_name, b.countayat" +
                " from quran_name a" +
                " left join(" +
                "    select count(id) as countayat, surah_no" +
                "    from quran" +
                "    group by surah_no" +
                " ) as b on a.surah_no = b.surah_no";
    }

    class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.Holder> {

        List<String[]> data;


        public SelectionAdapter() {
            data = new ArrayList<>();
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    QuranDBHelper db = new QuranDBHelper(thisContext());

                    if (selectiontype == SELECTION_MODE_JUZ) {
                        Cursor c = db.getReadableDatabase().rawQuery(query_juz(), new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                                String[] d = new String[3];
                                d[0] = String.valueOf(c.getInt(0));
                                d[1] = String.valueOf(c.getInt(1));
                                d[2] = String.format("Juz %d : %s", c.getInt(0), c.getString(3));
                                if (!c.getString(5).equals(c.getString(3))) {
                                    d[2] += " - " + c.getString(5);
                                }

                                data.add(d);

                                c.moveToNext();
                            }
                        }
                    } else if (selectiontype == SELECTION_MODE_SURAT) {
                        Cursor c = db.getReadableDatabase().rawQuery(query_surat(), new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                                String[] d = new String[3];
                                d[0] = String.valueOf(c.getInt(0));
                                d[1] = String.valueOf(c.getInt(2));
                                d[2] = c.getString(1);

                                data.add(d);

                                c.moveToNext();
                            }
                        }
                    }

                    runOnUiThread(() -> notifyDataSetChanged());

                    return null;
                }
            };
            task.execute();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_hifdzil_selection, viewGroup, false);

            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            String[] data = this.data.get(i);
            holder.lDetail.setText(data[2]);
            holder.lNumofayat.setText(String.format("%s ayat", data[1]));
            holder.layout.setOnClickListener(view -> {
                Intent intent = new Intent(thisContext(), ActivityHifdzilQuiz.class);
                intent.putExtra("mode", selectiontype);
                intent.putExtra("id", Integer.parseInt(data[0]));
                startActivity(intent);
                finish();
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.img)
            ImageView img;
            @InjectView(R.id.l_detail)
            TextView lDetail;
            @InjectView(R.id.l_numofayat)
            TextView lNumofayat;
            @InjectView(R.id.layout)
            LinearLayout layout;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
                img.setImageBitmap(iconBitmap);
            }
        }
    }
}
