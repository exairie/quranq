package exairie.com.quranq;

import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.net.URLDecoder;

import butterknife.ButterKnife;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.Application;
import exairie.com.quranq.models.FinishedRequest;
import exairie.com.quranq.models.RequestResult;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by exain on 5/7/2017.
 */

public class ExtActivity extends AppCompatActivity {
    public static final String SERVER_URL = "http://quranqu.deventri.com";
    public static String server(String dest){
        return String.format("%s/index.php/%s",SERVER_URL,dest);
    }
    private final int layout;
    final int STATUSBAR_COLOR_MAIN = R.color.colorPrimaryDark;
    final int STATUSBAR_COLOR_BAQ = R.color.colorSheme2Dark;

    int STATUSBAR_COLOR = STATUSBAR_COLOR_MAIN;

    public ExtActivity(int layout) {
        this.layout = layout;
    }
    public ExtActivity(int layout, boolean baqpage){
        this.layout = layout;
        //if (baqpage) STATUSBAR_COLOR = STATUSBAR_COLOR_BAQ;
    }
    protected void setExtActionBar(Toolbar toolbar){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
        setSupportActionBar(toolbar);
    }
    protected void hasBackButton(boolean istrue){
        try {
            getSupportActionBar().setDisplayHomeAsUpEnabled(istrue);
            getSupportActionBar().setHomeButtonEnabled(istrue);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }
    protected Context thisContext(){
        return this;
    }
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(layout);
        //ButterKnife.setDebug(true);
        ButterKnife.inject(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(this.getResources().getColor(STATUSBAR_COLOR));
        }

        Log.d("ONCREATE", "onCreate: Inject");
        //MobileAds.initialize(getApplicationContext(),getString(R.string.ads_id));


    }
    public void changeStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = this.getWindow();
            window.setStatusBarColor(this.getResources().getColor(color));
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        Log.d("BACKPRESS","ONOPTIONSPRESSED");
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                Log.d("BACKPRESS","PRESSED");
                return true;

            default:
                super.onOptionsItemSelected(item);
        }
        return super.onOptionsItemSelected(item);
    }
    public static void showDismissableAlert(Context c, String title, String message, String dismissLabel){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(dismissLabel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
    }
    @SuppressLint("ObsoleteSdkInt")
    public String getPathFromURI(Uri uri){
        String realPath="";
// SDK < API11
        if (Build.VERSION.SDK_INT < 11) {
            String[] proj = { MediaStore.Images.Media.DATA };
            @SuppressLint("Recycle") Cursor cursor = getContentResolver().query(uri, proj, null, null, null);
            int column_index = 0;
            String result="";
            if (cursor != null) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                realPath=cursor.getString(column_index);
            }
        }
        // SDK >= 11 && SDK < 19
        else if (Build.VERSION.SDK_INT < 19){
            String[] proj = { MediaStore.Images.Media.DATA };
            CursorLoader cursorLoader = new CursorLoader(this, uri, proj, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            if(cursor != null){
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();
                realPath = cursor.getString(column_index);
            }
        }
        // SDK > 19 (Android 4.4)
        else{
            String url = URLDecoder.decode(uri.toString());
            String wholeID = DocumentsContract.getDocumentId(Uri.parse(url));
            // Split at colon, use second item in the array
            String id = wholeID.split(":")[1];
            String[] column = { MediaStore.Images.Media.DATA };
            // where id is equal to
            String sel = MediaStore.Images.Media._ID + "=?";
            Cursor cursor = getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, column, sel, new String[]{ id }, null);
            int columnIndex = 0;
            if (cursor != null) {
                columnIndex = cursor.getColumnIndex(column[0]);
                if (cursor.moveToFirst()) {
                    realPath = cursor.getString(columnIndex);
                }
                cursor.close();
            }
        }
        return realPath;
    }
    @Nullable
    public static String getPath(Context context, Uri uri) {
        // DocumentProvider
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            } else if (isDownloadsDocument(uri)) {// DownloadsProvider
                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return getDataColumn(context, contentUri, null, null);

            } else if (isMediaDocument(uri)) { // MediaProvider
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};
                return getDataColumn(context, contentUri, selection, selectionArgs);

            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {// MediaStore (and general)
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();
            return getDataColumn(context, uri, null, null);

        } else if ("file".equalsIgnoreCase(uri.getScheme())) {// File
            return uri.getPath();
        }
        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};
        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    public static void showDismissableAlertWithPositiveBtn(Context c, String title, String message, String dismissLabel, String okLabel, DialogInterface.OnClickListener okExecute){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okLabel,okExecute)
                .setNegativeButton(dismissLabel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
    }
    public void NetRequest(Request request, final FinishedRequest onAfterRequest){
        Log.d("NETREQUEST",request.url().toString());
        request = request.newBuilder()
                .addHeader("Authorization",String.valueOf(Configs.getLoginToken()))
                .build();

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        OkHttpClient client = clientBuilder.build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
                onAfterRequest.OnFail(e.getMessage());
                onAfterRequest.Finish();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                Log.d("NETREQUESTRESULT", responseString);
                ObjectMapper mapper = Configs.mapper();
                try {
                    RequestResult r = mapper.readValue(responseString, RequestResult.class);
                    if(r.code == 403){
                        runOnUiThread(() -> {
                            new AlertDialog.Builder(thisContext())
                                    .setMessage("Login failed or Session expired. Please re-login")
                                    .setPositiveButton("Re-login",(dialogInterface, i) -> {
                                        startActivity(new Intent(thisContext(),ActivityLogin.class));
                                        finish();
                                    }).setCancelable(false)
                                    .show();
                        });
                        return;
                    }
                    if (r.status.equals("ok")){
                        onAfterRequest.OnSuccess(responseString);
                    }else
                    {
                        onAfterRequest.OnFail(r.message);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    onAfterRequest.OnFail(e.getMessage());
                }finally {
                    onAfterRequest.Finish();
                }

            }
        });
    }
    public static void ApiCall(Request request, final FinishedRequest onAfterRequest, Context c){
        Log.d("NETREQUEST",request.url().toString());
        request = request.newBuilder()
                .addHeader("Authorization",String.valueOf(Configs.getLoginToken()))
                .build();
        Log.d("LoginToken",""+String.valueOf(Configs.getLoginToken()));
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        OkHttpClient client = clientBuilder.build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onAfterRequest.OnFail(e.getMessage());
                onAfterRequest.Finish();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                Log.d("NETREQUESTRESULT", responseString);
                ObjectMapper mapper = Configs.mapper();
                try {
                    RequestResult r = mapper.readValue(responseString, RequestResult.class);
                    if(r.code == 403){
                        Log.d("Login Expired","");
                        if(ActivityLogin.login_stat == -1){
                            ActivityLogin.login_stat = 1;
                            Intent i2 = new Intent(c.getApplicationContext(),ActivityLogin.class);
                            i2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            i2.putExtra("message","Sesi login anda telah berakhir. Silahkan login kembali");
                            c.startActivity(i2);
                        }
                        return;
                    }
                    if (r.status.equals("ok")){
                        onAfterRequest.OnSuccess(responseString);
                    }else
                    {
                        onAfterRequest.OnFail(r.message);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    onAfterRequest.OnFail(e.getMessage());
                }finally {
                    onAfterRequest.Finish();
                }

            }
        });
    }
}

