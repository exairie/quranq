package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.models.Kajian;

public class ActivityKajian extends ExtActivity {
    KajianAdapter adapter;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.recycler_kajian)
    RecyclerView recyclerKajian;

    public ActivityKajian() {
        super(R.layout.activity_kajian);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        changeStatusBarColor(R.color.blue_darken_2);
        setExtActionBar(toolbar);
        setTitle("Kajian");
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setupRecycler();
    }

    private void setupRecycler() {
        adapter = new KajianAdapter();
        recyclerKajian.setAdapter(adapter);
        recyclerKajian.setLayoutManager(new LinearLayoutManager(this));

        adapter.loadDB();
    }

    class KajianAdapter extends RecyclerView.Adapter<Holder> {
        List<Kajian> data;
        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_kajian_list, parent, false);

            return new Holder(v);
        }
        public void loadDB(){
            data = new ArrayList<>();
            DBHelper db = new DBHelper(thisContext());
            Cursor c = db.getReadableDatabase()
                    .rawQuery("SELECT * FROM kajian order by create_date desc", new String []{});
            if (c.getCount() < 1){
                c.close();
                return;
            }

            c.moveToFirst();
            while (!c.isAfterLast()){
                data.add(Kajian.create(c));
                c.moveToNext();
            }

            c.close();

            notifyDataSetChanged();
        }
        @Override
        public void onBindViewHolder(Holder holder, int position) {
            Kajian k = data.get(position);
            String tgl = DateFormat.format("yyyy-MM-dd HH:mm", k.create_date).toString();
            holder.lTgl.setText(tgl);
            holder.lTitle.setText(Html.fromHtml(k.judul));
            String kajianBody = Html.fromHtml(k.isi).toString();
            holder.lContent.setText(kajianBody.length() > 300 ? kajianBody.substring(0, 299) : kajianBody);
            holder.cardLayout.setOnClickListener(view -> {
                Intent i = new Intent(thisContext(),ActivityReadKajian.class);
                i.putExtra("id",k.id);
                startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }
    }

    class Holder extends RecyclerView.ViewHolder {
        @InjectView(R.id.l_tgl)
        TextView lTgl;
        @InjectView(R.id.l_title)
        TextView lTitle;
        @InjectView(R.id.l_content)
        TextView lContent;
        @InjectView(R.id.card_layout)
        CardView cardLayout;
        public Holder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
