package exairie.com.quranq.libs;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.util.ArrayList;
import java.util.List;

import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Surah;

/**
 * Created by exain on 5/12/2017.
 */

public class QuranDBHelper extends SQLiteAssetHelper {
    public static final String DBNAME_QURAN = "qurandb.db";
    public QuranDBHelper(Context context) {
        super(context, DBNAME_QURAN, null, 1);
    }
    public SQLiteDatabase getDB(){
        return getReadableDatabase();
    }
    public static Surah getSurahInfo(Context context, int surah_no){
        QuranDBHelper db = new QuranDBHelper(context);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM quran_name WHERE surah_no = ?", new String[]{String.valueOf(surah_no)});
        if (c.getCount() < 1) return null;
        c.moveToFirst();
        Surah s = Surah.createFromCursor(c);
        c.close();
        return s;
    }
    public List<Surah> getSurahList(){
        List<Surah> surahList = new ArrayList<>();

        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("SELECT * FROM quran_name ORDER BY id asc ", new String[]{});
        c.moveToFirst();
        while (!c.isAfterLast()){
            Surah s = Surah.createFromCursor(c);

            Cursor c2 = db.rawQuery("select min(juz), max(juz) from quran where surah_no = ?", new String[]{String.valueOf(s.getSurah_no())});
            c2.moveToFirst();
            s.setJuzFrom(c2.getInt(0));
            s.setJuzTo(c2.getInt(1));
            c2.close();

            surahList.add(s);
            c.moveToNext();
        }
        c.close();
        db.close();
        return surahList;
    }
    public List<Ayat> getAyatFiltered(String filter){
        SQLiteDatabase db = getDB();
        List<Ayat> ayatList = new ArrayList<>();
        Cursor c = db.rawQuery(String.format("SELECT * FROM quran WHERE indonesian like '%%%s%%' and length(indonesian) < 270 ORDER BY ayat_id asc", filter),new String[]{});
        c.moveToFirst();

        while(!c.isAfterLast()){
            ayatList.add(Ayat.createFromCursor(c));
            c.moveToNext();
        }
        c.close();
        db.close();
        return ayatList;
    }
    public List<Ayat> getAyatList(int surahid){
        SQLiteDatabase db = getDB();
        List<Ayat> ayatList = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM quran WHERE surah_no = ? ORDER BY ayat_id asc", new String[]{String.valueOf(surahid)});
        c.moveToFirst();

        while(!c.isAfterLast()){
            ayatList.add(Ayat.createFromCursor(c));
            c.moveToNext();
        }
        c.close();
        db.close();
        return ayatList;
    }
    public static  Ayat getAyat(Context context, int ayatId) {
        QuranDBHelper dbHelper = new QuranDBHelper(context);
        SQLiteDatabase db = dbHelper.getDB();
        List<Ayat> ayatList = new ArrayList<>();
        Cursor c = db.rawQuery("SELECT * FROM quran WHERE id = ?", new String[]{String.valueOf(ayatId)});

        if(c.getCount() < 1) return null;
        c.moveToFirst();
        Ayat ayat = Ayat.createFromCursor(c);
        c.close();
        dbHelper.close();
        return ayat;
    }
    public static Surah getSurahInfo(Context c, Ayat ayat){
        QuranDBHelper dbHelper = new QuranDBHelper(c);
        Cursor cursor = dbHelper.getReadableDatabase().rawQuery("SELECT * FROM quran_name where surah_no = ?",new String[]{String.valueOf(ayat.getSurah_no())});
        cursor.moveToFirst();

        Surah s = Surah.createFromCursor(cursor);

        cursor.close();
        dbHelper.close();

        return s;
    }
}
