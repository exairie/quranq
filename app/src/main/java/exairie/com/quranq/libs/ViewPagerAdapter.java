package exairie.com.quranq.libs;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Exairie on 2/6/2018.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    final List<String> mFragmentTitleList = new ArrayList<>();
    private final List<Integer> iconList = new ArrayList<>();
    ;

    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {
        //tab.getTabAt(position).setIcon(R.drawable.home);
        return mFragmentList.get(position);
    }

    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        iconList.add(-1);
    }

    public void addFragment(Fragment fragment, String title, int iconRes) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
        iconList.add(iconRes);
    }

    @Override
    public CharSequence getPageTitle(int position) {

        return mFragmentTitleList.get(position);
    }

}