package exairie.com.quranq.libs

import android.text.TextUtils
import android.util.Log
import org.codehaus.jackson.annotate.JsonProperty

/**
 * Created by exain on 11/23/2017.
 */
class SentenceLearner(sentence : String){
    val unusedwords = arrayOf("dan","yang","oleh","di","karena","maka","oleh","kamu","aku","jangan","janganlah","mereka","dari","hai","kepada","sedang","apabila","ada")
    class SentenceValue(str : String, count : Int) {
        constructor() : this("",0) {

        }
        @JsonProperty("str")
        var str : String? = null
        @JsonProperty("count")
        var count : Int = 0
        init {
            Log.d("SentenceLearner","INITIATE")
            this.str = str;
            this.count = count;
        }
    }
    val array : List<String>
    var finArray : MutableList<String>
    var redArray : MutableList<SentenceValue>
    init {
        array = sentence.split(" ")
        finArray = mutableListOf()
        for (a in array)
        {
            if (a.trim().length > 0 && !unusedwords.contains(a.toLowerCase().trim())){
                finArray.add(a.toLowerCase().replace("(","").replace(")",""))
            }
        }

        redArray = mutableListOf()

        for (a in finArray){
            for (b in redArray){
                if (a == b.str){
                    b.count++

                    Log.d("Counter",String.format("%s : %d",b.str,b.count))
                    break
                }
            }
            val sv = SentenceValue(a,1)
            redArray.add(sv)
            Log.d("Counter",String.format("%s : %d",sv.str,sv.count))
        }
        Log.d("Sorting","Sorting values")
        redArray.sortByDescending { sentenceValue -> sentenceValue.count }
        Log.d("Sorting","Done ")
        for (i in redArray){
            Log.d("Sorted","${i.str} : ${i.count}")
        }
    }

    fun getTopThree() : List<SentenceValue> = if (redArray.size >= 3 ) redArray.subList(0,2) else redArray;

    fun resultCount() : Int = redArray.size

    fun getQuery(table : String, searchField : String) : String{
//
        val likes = mutableListOf<String>()

        for(sv in getTopThree()){
            likes.add(String.format("%s LIKE '%%%s%%'",searchField,sv.str))
        }

        return String.format("SELECT id FROM %s %s %s limit 0,10",table,(if(likes.size > 0) "WHERE" else ""),TextUtils.join(" OR ", likes.toTypedArray()))
    }
}