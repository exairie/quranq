package exairie.com.quranq.libs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.UserLogin;

/**
 * Created by exain on 5/17/2017.
 */

public class Configs {
    public static final String BQ_YTB_URL = "https://youtube.com/channel/UCoxRiVzxbYOpARRq98lWvNg";
    public static final String BQ_INS_URL = "https://instagram.com/baitulquran.id";
    public static final String BQ_LAZ_URL = "https://bsmu.or.id";
    public static final String SERVER_URL = "http://quranqu.fsialbiruni.org/admin";
    public static String server(String dest){
        return String.format("%s/index.php/%s",SERVER_URL,dest);
    }
    public static ObjectMapper mapper(){
        ObjectMapper o = new ObjectMapper();
        o.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return o;
    }
    private String wQuery = "select sum(words)/count(words) from (" +
            "   SELECT (sum(length(arabic))) as words, juz from `quran` group by juz\n" +
            ") as T";
    public final static double LETTER_PER_JUZ = 23175  ;
    public static Context appContext;
    public static void init(Context context){
        appContext = context;
    }
    private Configs instance = null;
    public Configs getInstance(){
        if(instance == null)
            instance = new Configs();

        return instance;
    }
    public static int deleteConfiguration(String key){
        DBHelper db = new DBHelper(appContext);
        return db.getWritableDatabase().delete("config","ckey = ?", new String[]{key});
    }
    public static void setConfiguration(Option option){
        DBHelper db = new DBHelper(appContext);
        db.getWritableDatabase().delete("config","ckey = ?",new String[]{option.getKey()});

        ContentValues values = new ContentValues();
        values.put("ckey",option.getKey());
        values.put("cvalue",option.getValue());
        db.getWritableDatabase().insert("config",null,values);
        //db.getWritableDatabase().execSQL("INSERT INTO config (ckey,cvalue) VALUES ('?','?')", new String[]{option.getKey(),option.getOptionValue()});
        db.close();
    }
    public static Option getConfiguration(String key){
        DBHelper db = new DBHelper(appContext);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM config WHERE ckey = ?",new String[] {key});
        if(c.getCount() < 1){
            return null;
        }
        c.moveToFirst();
        Option o = new Option();
        o.setKey(c.getString(1));
        o.setValue(c.getString(2));
        c.close();
        db.close();
        return o;
    }
    public static String getLoginToken(){
        Option o = getConfiguration("login_token");
        if (o == null) return null;
        else return o.getValue();
    }
    public static void setLoginToken(String token){
        Option o = new Option("login_token",token);
        setConfiguration(o);
    }
    public static UserLogin getLoginInfo(){
        Option o = getConfiguration("login_info");
        if (o == null) return null;
        else {
            try {
                return mapper().readValue(o.getValue(),UserLogin.class);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
    public static void setLoginInfo(UserLogin login){
        try {
            Option o = new Option("login_info",mapper().writeValueAsString(login));
            setConfiguration(o);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
