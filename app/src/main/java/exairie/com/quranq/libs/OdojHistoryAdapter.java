package exairie.com.quranq.libs;

import android.app.Activity;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.ActivityHome;
import exairie.com.quranq.R;
import exairie.com.quranq.models.OdojData;

public class OdojHistoryAdapter extends RecyclerView.Adapter<OdojHistoryAdapter.Holder> {
        List<OdojData> data;
        Activity activity;
        Runnable callback;
        public OdojHistoryAdapter(Activity activity) {
            //data = new ArrayList<>();
            this.activity = activity;
            data = new ArrayList<>();
            reload();
        }
        public OdojHistoryAdapter(Activity activity, Runnable callback) {
            //data = new ArrayList<>();
            this.activity = activity;
            data = new ArrayList<>();
            this.callback = callback;
            reload();
        }

        public void reload() {
            AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    data = DBHelper.getOdojHistory(activity, false);
                    activity.runOnUiThread(() -> notifyDataSetChanged());
                    if(callback != null) activity.runOnUiThread(() -> callback.run());
                    return null;
                }
            };
            task.execute();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_progress_odoj, parent, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            OdojData data = this.data.get(position);
            holder.lProgressOdoj.setText(String.format("%.1f Juz", data.getJuzCalculation()));
            holder.textView6.setText(DateFormat.format("E, dd MMM yyyy", data.getTimeInCalendar()).toString());
            int max = 100;
            int current = (int) (data.getJuzCalculation() * 100);
            current = current > max ? max : current;

            holder.progressOdoj.setMax(max);
            holder.progressOdoj.setProgress(current);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.progress_odoj)
            ProgressBar progressOdoj;
            @InjectView(R.id.l_progress_odoj)
            TextView lProgressOdoj;
            @InjectView(R.id.l_day_of_odoj)
            TextView textView6;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }
    }