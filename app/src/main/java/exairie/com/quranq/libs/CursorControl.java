package exairie.com.quranq.libs;

import android.database.Cursor;

import java.util.Iterator;
import java.util.function.Consumer;

/**
 * Created by exain on 11/14/2017.
 */

public class CursorControl {
    public static interface Iterator{
        public void call(Cursor c);
    }
    public static interface Callback<T>{
        public void callback(T var);
    }
    private Cursor c;


    public CursorControl(Cursor c){
        this.c = c;
    }
    public boolean isNull(String columnname){
        return c.isNull(c.getColumnIndexOrThrow(columnname));
    }
    public String getString(String columnname){
        return c.getString(c.getColumnIndexOrThrow(columnname));
    }
    public int getInt(String columnname){
        return c.getInt(c.getColumnIndexOrThrow(columnname));
    }
    public long getLong(String columnname){
        return c.getLong(c.getColumnIndexOrThrow(columnname));
    }
    public double getDouble(String columnname) {
        return c.getDouble(c.getColumnIndexOrThrow(columnname));
    }
    public int count(){
        return c.getCount();
    }
    public void iterate(Iterator i){
        if (c.getCount() < 1) return;
        c.moveToFirst();
        while (!c.isAfterLast()){
            i.call(c);
            c.moveToNext();
        }
    }

    public Cursor firstRow(){
        if (c.getCount() < 1) return null;

        c.moveToFirst();
        return c;
    }
    public Cursor get(int i){
        if (c.getCount() < 1) return null;
        c.moveToPosition(i);
        return c;

    }
    public void close(){
        c.close();
    }

    @Override
    protected void finalize() throws Throwable {
        if (c == null) return;
        if (!c.isClosed())
            c.close();
        super.finalize();
    }
}
