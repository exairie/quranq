package exairie.com.quranq.libs;

import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.text.TextUtils;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import exairie.com.quranq.models.HadistData;

/**
 * Created by exain on 5/12/2017.
 */

public class HadistDBHelper extends SQLiteAssetHelper {
    public static class HadistBab{
        public static HadistBab createFromCursor(Cursor c){
            HadistBab h = new HadistBab();
            h.setBab(c.getString(0));
            h.setNama(c.getString(1));
            return h;
        }
        String bab;
        String nama;

        public String getBab() {
            return bab;
        }

        public void setBab(String bab) {
            this.bab = bab;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }
    }
    public static class Hadist{
        public static Hadist createFromCursor(Cursor c){
            Hadist h = new Hadist();
            h.setId(c.getLong(0));
            h.setNama(c.getString(1));
            h.setArabic(c.getString(2));
            h.setGundul(c.getString(3));
            h.setIndonesia(c.getString(4));
            h.setAlbani(c.getString(5));
            h.setBab(c.getString(6));
            h.setBab_arabic(c.getString(7));

            return h;
        }
        long id;
        String nama;
        String arabic;
        String gundul;
        String indonesia;
        String albani;
        String bab;
        String bab_arabic;

        public long getId() {
            return id;
        }

        public void setId(long id) {
            this.id = id;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getArabic() {
            return arabic;
        }

        public void setArabic(String arabic) {
            this.arabic = arabic;
        }

        public String getGundul() {
            return gundul;
        }

        public void setGundul(String gundul) {
            this.gundul = gundul;
        }

        public String getIndonesia() {
            return indonesia;
        }

        public void setIndonesia(String indonesia) {
            this.indonesia = indonesia;
        }

        public String getAlbani() {
            return albani;
        }

        public void setAlbani(String albani) {
            this.albani = albani;
        }

        public String getBab() {
            return bab;
        }

        public void setBab(String bab) {
            this.bab = bab;
        }

        public String getBab_arabic() {
            return bab_arabic;
        }

        public void setBab_arabic(String bab_arabic) {
            this.bab_arabic = bab_arabic;
        }
    }
    public static final String DBNAME_HADIST = "hadistdb.db";
    public HadistDBHelper(Context context) {
        super(context, DBNAME_HADIST, null, 1);
    }
    public static List<HadistBab> getHadistBab(Context context, String name, String bab){
        return getHadistBab(context,name,bab,null);
    }
    public static List<HadistBab> getHadistBab(Context context, String name, String bab, String search){
        List<HadistBab> list = new ArrayList<>();

        HadistDBHelper db = new HadistDBHelper(context);
        String query = "SELECT DISTINCT(bab), nama FROM hadist ";

        List<String> wClause = new ArrayList<>();
        if (name!=null) wClause.add(String.format("nama = %s",DatabaseUtils.sqlEscapeString(name)));
        if (bab!=null)wClause.add(String.format("bab = %s",DatabaseUtils.sqlEscapeString(bab)));
        if (search!=null)wClause.add(String.format("indonesia like '%%%s%%'", search.replace("'","''")));

        if (wClause.size() > 0){
            query += "WHERE ";
            query += TextUtils.join(" AND ", wClause.toArray());
        }

        Cursor c = db.getReadableDatabase().rawQuery(query,new String[]{});
        if (c.getCount() < 1)return list;

        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(HadistBab.createFromCursor(c));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
    public static List<String> getNames(Context context){
        List<String> list = new ArrayList<>();
        list.add("Semua");
        HadistDBHelper db = new HadistDBHelper(context);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT DISTINCT(nama) FROM hadist",new String[]{});
        if (c.getCount() < 1) return list;

        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(c.getString(0));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
    public static List<HadistData> getNamesAndBooks(Context context){
        List<HadistData> list = new ArrayList<>();
        HadistDBHelper db = new HadistDBHelper(context);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT nama, count(distinct(bab)) FROM hadist group by nama",new String[]{});
        if (c.getCount() < 1) return list;

        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(new HadistData(c.getString(0), String.format("%s bab", c.getString(1))));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
    public static List<String> getBab(Context context, String filterName){
        List<String> list = new ArrayList<>();
        list.add("Semua");

        HadistDBHelper db = new HadistDBHelper(context);

        Cursor c;

        if(filterName != null){
            c = db.getReadableDatabase().rawQuery("SELECT DISTINCT(bab) FROM hadist where nama = ?",new String[]{filterName});
        }else{
            c = db.getReadableDatabase().rawQuery("SELECT DISTINCT(bab) FROM hadist ",new String[]{});
        }
        if (c.getCount() < 1) return list;

        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(c.getString(0));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
    public static List<Hadist> getHadist(Context context, String name, String bab){
        List<Hadist> list = new ArrayList<>();

        HadistDBHelper db = new HadistDBHelper(context);

        String query = "";
        List<String> whereClause = new ArrayList<>();
        if (name != null){
            whereClause.add(String.format("nama = %s", DatabaseUtils.sqlEscapeString(name)));
        }
        if(bab != null){
            whereClause.add(String.format("bab = %s", DatabaseUtils.sqlEscapeString(bab)));
        }

        query = "SELECT * FROM hadist ";

        if(whereClause.size() > 0){
            query += "WHERE ";
            query += TextUtils.join(" AND ",whereClause.toArray());
        }

        query += " ORDER BY id ASC";

        Cursor c = db.getReadableDatabase().rawQuery(query,new String[]{});

        if(c.getCount() < 1) return list;

        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(Hadist.createFromCursor(c));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
    public static List<Long> getReference(Context context, SentenceLearner learner){
        List<Long> list = new ArrayList<>();

        HadistDBHelper db = new HadistDBHelper(context);

        String query = learner.getQuery("hadist","indonesia");

        Cursor c = db.getReadableDatabase().rawQuery(query,new String[]{});

        if(c.getCount() < 1) return list;

        Log.d("Hadist","Num : " + c.getCount());
        c.moveToFirst();
        while (!c.isAfterLast()){
            list.add(c.getLong(0));
            c.moveToNext();
        }
        c.close();
        db.close();
        return list;
    }
}
