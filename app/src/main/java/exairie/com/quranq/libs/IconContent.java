package exairie.com.quranq.libs;

import android.content.Context;
import android.content.res.TypedArray;
import android.media.Image;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import exairie.com.quranq.R;

/**
 * Created by exain on 8/26/2017.
 */

public class IconContent extends LinearLayout {
    ImageView img;
    TextView labelText;

    public ImageView getImg() {
        return img;
    }

    public TextView getLabelText() {
        return labelText;
    }

    public IconContent(Context context) {
        super(context);
        initialize(context);
    }
    void initialize(Context context){
        this.setClickable(true);
        this.setBackgroundResource(R.drawable.border);
        this.setOrientation(LinearLayout.VERTICAL);
        this.setPadding(4,4,4,4);

        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_icontext,this,true);

        img = (ImageView) findViewById(R.id.img);
        labelText = (TextView) findViewById(R.id.label);
    }

    public IconContent(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize(context);

        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.IconContent);

        int imgResSrc = array.getResourceId(R.styleable.IconContent_src,R.drawable.bookmark);
        String label = array.getString(R.styleable.IconContent_label);

        img.setImageResource(imgResSrc);
        labelText.setText(label);

    }
}
