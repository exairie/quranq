package exairie.com.quranq.libs;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import exairie.com.quranq.interfaces.AppException;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.OdojData;
import exairie.com.quranq.models.QuranBookmark;
import exairie.com.quranq.models.Surah;
import exairie.com.quranq.models.TodayOdojEntry;

/**
 * Created by exain on 5/17/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static String name = "data.sqlite";
    public DBHelper(Context context) {
        super(context, name, null, 12);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE IF NOT EXISTS config (" +
                "id integer primary key autoincrement," +
                "ckey text," +
                "cvalue text)");
        db.execSQL("CREATE TABLE IF NOT EXISTS odoj ( " +
                "    id      INTEGER PRIMARY KEY AUTOINCREMENT," +
                "    time    NUMERIC," +
                "    ayat_id INTEGER, " +
                "    ayat_to INTEGER," +
                "    letter_no INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS quran_bookmark (" +
                "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "   bookmark_time NUMERIC," +
                "   ayat_id INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS guru(" +
                "   id integer primary key autoincrement," +
                "   nama_guru varchar(255)," +
                "   phone varchar(255)," +
                "   location_lat real(18,4)," +
                "   location_long real(18,4)," +
                "   address varchar(255))");
        db.execSQL("CREATE TABLE IF NOT EXISTS kajian (" +
                "  id integer primary key autoincrement," +
                "  judul varchar(200)," +
                "  gambar varchar(255)," +
                "  isi text," +
                "   create_date integer" +
                ") ");
        db.execSQL("CREATE TABLE IF NOT EXISTS qori(" +
                " id integer primary key," +
                " title varchar(255)," +
                " youtube_url varchar(255)," +
                " reciter varchar(255)," +
                " category integer," +
                " reference varchar(255)," +
                " cat varchar(255)," +
                " added integer" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS qori_fav(" +
                " id integer primary key autoincrement," +
                " favorite integer" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS qori_recent(" +
                " id integer primary key autoincrement," +
                " recent integer," +
                " time integer" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS fahmil_result(" +
                " id integer primary key," +
                " question varchar(255)," +
                " answer_true varchar(255)," +
                " answer_false varchar(255)," +
                " session_start integer," +
                " session_end integer" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS events(" +
                " id integer primary key," +
                " id_user integer," +
                " nama_event varchar(255)," +
                " deskripsi varchar(65536)," +
                " latitude decimal(18,4)," +
                " longitude decimal(18,4)," +
                " notification_radius decimal(18,4)," +
                " date_start integer," +
                " date_end integer," +
                " date_create integer," +
                " date_edit integer," +
                " capacity integer," +
                " contact_person varchar(255)," +
                " status integer)");
        db.execSQL("CREATE TABLE IF NOT EXISTS coin_trans(" +
                " id integer primary key," +
                " user_id integer," +
                " coin_transaction decimal(18,4)," +
                " transaction_date integer," +
                " remark varchar(255)" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS notifications_pending(" +
                " id integer primary key," +
                " notif_text varchar(255)," +
                " notif_desc varchar(255)," +
                " notif_img varchar(255)," +
                " expiration_date integer," +
                " create_date integer," +
                " viewed integer" +
                ")");
        db.execSQL("CREATE TABLE IF NOT EXISTS hifdzil_reesult(" +
                " quizdate integer," +
                " surah_name varchar(255)," +
                " ayat_id varchar(255)," +
                " answer_true integer"+
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        if(i1 >= 2){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS odoj ( \n" +
                        "    id      INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "    time    NUMERIC," +
                        "    ayat_id INTEGER " +
                        ")");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 3){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS quran_bookmark (" +
                        "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "   bookmark_time NUMERIC," +
                        "   ayat_id INTEGER)");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE odoj ADD ayat_to INTEGER");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE odoj ADD letter_no INTEGER");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 4){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS guru(" +
                        "   id integer primary key autoincrement," +
                        "   nama_guru varchar(255)," +
                        "   phone varchar(255)," +
                        "   location_lat real(18,4)," +
                        "   location_long real(18,4)," +
                        "   address varchar(255))");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 5){
            db.execSQL("CREATE TABLE IF NOT EXISTS kajian (" +
                    "  id integer primary key autoincrement," +
                    "  judul varchar(200)," +
                    "  gambar varchar(255)," +
                    "  isi text," +
                    "   create_date integer" +
                    ") ");
        }
        if (i1 >= 6){
            db.execSQL("CREATE TABLE IF NOT EXISTS qori(" +
                    " id integer primary key," +
                    " title varchar(255)," +
                    " youtube_url varchar(255)," +
                    " reciter varchar(255)," +
                    " category integer," +
                    " reference varchar(255)," +
                    " cat varchar(255)," +
                    " added integer" +
                    ")");
        }
        if (i1 >= 7){
            db.execSQL("CREATE TABLE IF NOT EXISTS qori_fav(" +
                    " id integer primary key," +
                    " favorite integer" +
                    ")");
            db.execSQL("CREATE TABLE IF NOT EXISTS qori_recent(" +
                    " id integer primary key," +
                    " recent integer," +
                    " time integer" +
                    ")");
        }
        if (i1 >= 8){
            db.execSQL("CREATE TABLE IF NOT EXISTS fahmil_result(" +
                    " id integer primary key," +
                    " question varchar(255)," +
                    " answer_true varchar(255)," +
                    " answer_false varchar(255)," +
                    " session_start integer," +
                    " session_end integer" +
                    ")");
        }
        if (i1 >= 9){
            db.execSQL("CREATE TABLE IF NOT EXISTS events(" +
                    " id integer primary key," +
                    " id_user integer," +
                    " nama_event varchar(255)," +
                    " deskripsi varchar(65536)," +
                    " latitude decimal(18,4)," +
                    " longitude decimal(18,4)," +
                    " notification_radius decimal(18,4)," +
                    " date_start integer," +
                    " date_end integer," +
                    " date_create integer," +
                    " date_edit integer," +
                    " capacity integer," +
                    " contact_person varchar(255)," +
                    " status integer)");
        }
        if (i1 >= 10){
            db.execSQL("CREATE TABLE IF NOT EXISTS coin_trans(" +
                    " id integer primary key," +
                    " user_id integer," +
                    " coin_transaction decimal(18,4)," +
                    " transaction_date integer," +
                    " remark varchar(255)" +
                    ")");
        }
        if(i1 >= 11){
            db.execSQL("CREATE TABLE IF NOT EXISTS notifications_pending(" +
                    " id integer primary key," +
                    " notif_text varchar(255)," +
                    " notif_desc varchar(255)," +
                    " notif_img varchar(255)," +
                    " expiration_date integer," +
                    " create_date integer" +
                    ")");
        }
        if (i1 >= 12){
            db.execSQL("ALTER TABLE notifications_pending ADD viewed INTEGER");
            db.execSQL("CREATE TABLE IF NOT EXISTS hifdzil_reesult(" +
                    " quizdate integer," +
                    " surah_name varchar(255)," +
                    " ayat_id varchar(255)," +
                    " answer_true integer"+
                    ")");
        }
    }
    public static List<QuranBookmark> getQuranBookmark(Context c){
        List<QuranBookmark> list = new ArrayList<>();
        DBHelper db = new DBHelper(c);
        QuranDBHelper quranDBHelper = new QuranDBHelper(c);

        Cursor cr = db.getReadableDatabase().rawQuery("" +
                "SELECT * FROM quran_bookmark order by bookmark_time desc", new String[]{});

        if(cr.getCount() < 1) return list;
        cr.moveToFirst();
        while (!cr.isAfterLast()){
            QuranBookmark b = QuranBookmark.createFromCursor(cr);

            Cursor qCursor = quranDBHelper.getReadableDatabase().rawQuery("SELECT * FROM quran WHERE id = ?", new String[]{String.valueOf(b.getAyat_id())});
            if(qCursor.getCount() > 0){
                qCursor.moveToFirst();
                Ayat a = Ayat.createFromCursor(qCursor);
                b.setAyatInfo(a);
            }
            qCursor.close();
            if(b.getAyatInfo() != null) {
                qCursor = quranDBHelper.getReadableDatabase().rawQuery("SELECT * FROM quran_name WHERE surah_no = ?", new String[]{String.valueOf(b.getAyatInfo().getSurah_no())});
                if(qCursor.getCount() > 0){
                    qCursor.moveToFirst();
                    Surah s = Surah.createFromCursor(qCursor);
                    b.setSurahInfo(s);
                }
                qCursor.close();
            }
            list.add(b);
            cr.moveToNext();
        }
        return list;

    }
    public static long removeQuranBookmark(Context c, int AyatID){
        DBHelper dbHelper = new DBHelper(c);
        return dbHelper.getWritableDatabase().delete("quran_bookmark","ayat_id = ?",new String[]{String.valueOf(AyatID)});

    }
    public static long addQuranBookmark(Context c, int AyatID) throws AppException.BookmarkExistException, AppException.AyatNotFoundException{
        QuranDBHelper quranDBHelper = new QuranDBHelper(c);
        DBHelper dbHelper = new DBHelper(c);

        if(quranDBHelper.getReadableDatabase().rawQuery("SELECT * FROM quran WHERE id = ?",new String[]{String.valueOf(AyatID)}).getCount() < 1){
            throw new AppException.AyatNotFoundException();
        }

        if(dbHelper.getReadableDatabase().rawQuery("SELECT * FROM quran_bookmark WHERE ayat_id = ?", new String[]{String.valueOf(AyatID)}).getCount() > 0){
            throw new AppException.BookmarkExistException();
        }

        ContentValues values = new ContentValues();
        values.put("bookmark_time", System.currentTimeMillis());
        values.put("ayat_id", AyatID);

        return dbHelper.getWritableDatabase().insert("quran_bookmark","NULL",values);
    }
    public static void saveODOJ(Context context, int ayatid, int lettercount){
        long time = System.currentTimeMillis();
        DBHelper db = new DBHelper(context);

        ContentValues values = new ContentValues();
        values.put("time",String.valueOf(time));
        values.put("ayat_id", String.valueOf(getLastODOJEntry(context)));
        values.put("ayat_to", String.valueOf(ayatid));
        values.put("letter_no", lettercount);

        db.getWritableDatabase().insert("odoj",null,values);
//        db.getWritableDatabase().execSQL(
//                "INSERT INTO odoj (time,ayat_id) VALUES ('?','?')",
//                new String[]{String.valueOf(time), String.valueOf(ayatid)}
//        );
        db.close();
    }
    public static void saveODOJ(Context context, int ayatfrom, int ayatid, int lettercount){
        long time = System.currentTimeMillis();
        DBHelper db = new DBHelper(context);

        ContentValues values = new ContentValues();
        values.put("time",String.valueOf(time));
        values.put("ayat_id", String.valueOf(ayatfrom));
        values.put("ayat_to", String.valueOf(ayatid));
        values.put("letter_no", lettercount);

        db.getWritableDatabase().insert("odoj",null,values);
//        db.getWritableDatabase().execSQL(
//                "INSERT INTO odoj (time,ayat_id) VALUES ('?','?')",
//                new String[]{String.valueOf(time), String.valueOf(ayatid)}
//        );
        db.close();
    }
    public static int getLastODOJEntry(Context context){
        DBHelper db = new DBHelper(context);

        Cursor c = db.getReadableDatabase().rawQuery(
                "SELECT * FROM odoj ORDER BY TIME DESC LIMIT 0,1",
                new String[]{}
        );

        if(c.getCount() < 1) return -1;

        c.moveToFirst();
        int result = c.getInt(3);
        c.close();
        return result;
    }
    public static List<OdojData> getOdojHistory(Context c, boolean isToday){
        DBHelper db = new DBHelper(c);

        List<OdojData> list = new ArrayList<>();
        Calendar cl = Calendar.getInstance();
        cl.set(Calendar.HOUR_OF_DAY,0);
        cl.set(Calendar.MINUTE, 0);
        cl.set(Calendar.SECOND, 0);
        String query = String.format("SELECT sum(letter_no), min(ayat_id), max(ayat_to)," +
                "strftime('%%Y %%m %%d', time/1000, 'unixepoch'), max(time),letter_no from odoj %s group by strftime('%%Y %%m %%d', time/1000, 'unixepoch') " +
                "having sum(letter_no) is not null " +
                "order by strftime('%%Y %%m %%d', time/1000, 'unixepoch') desc limit 0,7", (isToday?" WHERE time >  " + cl.getTimeInMillis():""));
        Cursor oCursor = db.getReadableDatabase().rawQuery(query, new String[]{});
        if (oCursor.getCount() < 1) return list;

        oCursor.moveToFirst();
        while(!oCursor.isAfterLast()){
            OdojData data = new OdojData();
            data.setAyat_id(oCursor.getInt(1));
            data.setTime(oCursor.getLong(4));
            data.setAyat_to(oCursor.getInt(2));
            data.setLetter_count(oCursor.getInt(0));
            data.set_ayat_to(QuranDBHelper.getAyat(c,data.getAyat_to()));
            data.set_ayat_from(QuranDBHelper.getAyat(c, data.getAyat_id()));
            list.add(data);
            oCursor.moveToNext();
        }
        oCursor.close();
        return list;
    }
    public static TodayOdojEntry getTodayOdojPercentage(Context context){
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,0);
        c.set(Calendar.MINUTE,0);
        c.set(Calendar.SECOND,0);

        DBHelper db = new DBHelper(context);
        Cursor cursor = db.getReadableDatabase().rawQuery("SELECT * FROM odoj WHERE time > ?",new String[]{String.valueOf(c.getTimeInMillis())});
        double accumulated = 0;
        if (cursor.getCount() < 1) {
            cursor.close();
            db.close();
            return null;
        }

        cursor.moveToFirst();
        while (!cursor.isAfterLast()){
            accumulated += cursor.getDouble(cursor.getColumnIndexOrThrow("letter_no"));
            cursor.moveToNext();
        }

        if(accumulated < 1) {
            cursor.close();
            db.close();
            return null;
        }

        double juz = accumulated/Configs.LETTER_PER_JUZ;

        cursor.close();
        db.close();

        TodayOdojEntry entry = new TodayOdojEntry();
        entry.juzcount = juz;

        cursor = db.getReadableDatabase().rawQuery("SELECT ayat_id FROM odoj WHERE time > ? order by time asc limit 0,1",new String[]{String.valueOf(c.getTimeInMillis())});

        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            int ayatid = cursor.getInt(0);
            entry.ayat_from = QuranDBHelper.getAyat(context,ayatid);

        }
        cursor.close();
        cursor = db.getReadableDatabase().rawQuery("SELECT ayat_to FROM odoj WHERE time > ? order by time desc limit 0,1",new String[]{String.valueOf(c.getTimeInMillis())});

        if (cursor.getCount() > 0){
            cursor.moveToFirst();
            int ayatid = cursor.getInt(0);
            entry.ayat_to = QuranDBHelper.getAyat(context,ayatid);

        }
        cursor.close();
        db.close();

        return entry;
    }
}
