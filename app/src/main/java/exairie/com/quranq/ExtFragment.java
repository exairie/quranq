package exairie.com.quranq;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

import butterknife.ButterKnife;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.FinishedRequest;
import exairie.com.quranq.models.RequestResult;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by exain on 5/7/2017.
 */

public class ExtFragment extends Fragment {
    public static final String SERVER_URL = "http://quranqu.fsialbiruni.org/admin";
    public static String server(String dest){
        return String.format("%s/index.php/%s",SERVER_URL,dest);
    }
    private int layout;
    final int STATUSBAR_COLOR_MAIN = R.color.colorPrimaryDark;
    final int STATUSBAR_COLOR_BAQ = R.color.colorSheme2Dark;

    int STATUSBAR_COLOR = STATUSBAR_COLOR_MAIN;
    public ExtFragment(){}
//    public ExtFragment(int layout) {
//        this.layout = layout;
//    }
//    public ExtFragment(int layout, boolean baqpage){
//        this.layout = layout;
//        //if (baqpage) STATUSBAR_COLOR = STATUSBAR_COLOR_BAQ;
//    }
    protected void setExtActionBar(Toolbar toolbar){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(5);
        }
    }
    protected Activity thisContext(){
        return getActivity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle data = getArguments();
        layout = data.getInt("view");
        View v = LayoutInflater.from(container.getContext()).inflate(layout,container,false);
        return v;
    }

//    protected void onCreateView(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        //setContentView(layout);
//        //ButterKnife.setDebug(true);
//        ButterKnife.inject(this);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            Window window = this.getWindow();
//            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//            window.setStatusBarColor(this.getResources().getColor(STATUSBAR_COLOR));
//        }
//
//        Log.d("ONCREATE", "onCreate: Inject");
//        //MobileAds.initialize(getApplicationContext(),getString(R.string.ads_id));
//
//
//    }
    public void changeStatusBarColor(int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getActivity().getWindow();
            window.setStatusBarColor(this.getResources().getColor(color));
        }
    }
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        Log.d("BACKPRESS","ONOPTIONSPRESSED");
//        switch (item.getItemId()){
//            case android.R.id.home:
//                onBackPressed();
//                Log.d("BACKPRESS","PRESSED");
//                return true;
//
//            default:
//                super.onOptionsItemSelected(item);
//        }
//        return super.onOptionsItemSelected(item);
//    }
    public static void showDismissableAlert(Context c, String title, String message, String dismissLabel){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton(dismissLabel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
    }
    public static void showDismissableAlertWithPositiveBtn(Context c, String title, String message, String dismissLabel, String okLabel, DialogInterface.OnClickListener okExecute){
        new AlertDialog.Builder(c)
                .setTitle(title)
                .setMessage(message)
                .setPositiveButton(okLabel,okExecute)
                .setNegativeButton(dismissLabel, (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                })
                .show();
    }
    public void NetRequest(Request request, final FinishedRequest onAfterRequest){
        Log.d("NETREQUEST",request.url().toString());
        request = request.newBuilder()
                .build();

        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();

        OkHttpClient client = clientBuilder.build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                onAfterRequest.OnFail(e.getMessage());
                onAfterRequest.Finish();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String responseString = response.body().string();
                Log.d("NETREQUESTRESULT", responseString);
                ObjectMapper mapper = Configs.mapper();
                try {
                    RequestResult r = mapper.readValue(responseString, RequestResult.class);

                    if (r.status.equals("ok")){
                        onAfterRequest.OnSuccess(responseString);
                    }else
                    {
                        onAfterRequest.OnFail(r.message);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    onAfterRequest.OnFail(e.getMessage());
                }finally {
                    onAfterRequest.Finish();
                }

            }
        });
    }
}
