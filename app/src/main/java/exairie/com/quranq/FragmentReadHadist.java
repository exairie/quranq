package exairie.com.quranq;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.HadistDBHelper;
import exairie.com.quranq.models.HadistData;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this playerFragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentReadHadist#newInstance} factory method to
 * create an instance of this playerFragment.
 */
public class FragmentReadHadist extends Fragment {
    static List<HadistData> data;
    static boolean data_loaded = false;
    // TODO: Rename parameter arguments, choose names that match
    // the playerFragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @InjectView(R.id.grid_kitab)
    GridView gridKitab;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ActivityDashboard activityDashboard;

    public FragmentReadHadist() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this playerFragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of playerFragment FragmentReadHadist.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentReadHadist newInstance(String param1, String param2) {
        FragmentReadHadist fragment = new FragmentReadHadist();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();


    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this playerFragment
        View view = inflater.inflate(R.layout.fragment_read_hadist, container, false);
        ButterKnife.inject(this, view);
        injected();

        activityDashboard = (ActivityDashboard)getActivity();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void injected() {
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                KitabAdapter adapter = new KitabAdapter();


                try {
                    getActivity().runOnUiThread(() -> {
                        if (gridKitab != null){
                            gridKitab.setAdapter(adapter);
                        }
                    });
                    adapter.load();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return null;
            }
        };
        task.execute();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
    }

    /**
     * This interface must be implemented by activities that contain this
     * playerFragment to allow an interaction in this playerFragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    class KitabAdapter extends BaseAdapter {


        int[] colors = new int[]{
                R.color.red_darken_3, R.color.blue_darken_3, R.color.amber_darken_3,
                R.color.green_darken_3, R.color.blue_grey_darken_3, R.color.teal_darken_3
        };
        int colorIndex = 0;

        public KitabAdapter() {

        }
        void load(){
            if(!data_loaded){
                data = HadistDBHelper.getNamesAndBooks(getActivity());
                data_loaded = true;
                getActivity().runOnUiThread(this::notifyDataSetChanged);
            }
        }

        public void add(HadistData ndata) {
            data.add(ndata);
        }

        @Override
        public int getCount() {
            return data!=null?data.size():0;
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v;
            if (view == null) {
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.view_kitab, viewGroup, false);
                //v.setLayoutParams(new GridView.LayoutParams(gridKitab.getWidth()/gridKitab.getNumColumns(),v.getHeight()));
            } else {
                v = view;
            }

            v.setOnClickListener(view1 -> {
                Intent intent = new Intent(getActivity(),ActivityHadistList.class);
                intent.putExtra("name",data.get(i).labelRiwayat);
                startActivity(intent);

            });
            CardView card = (CardView)v.findViewById(R.id.card);
            RelativeLayout layoutLabel = (RelativeLayout) v.findViewById(R.id.layout_label);
            TextView hadistRiwayat = (TextView) v.findViewById(R.id.hadist_riwayat);
            TextView hadistJumlahbab = (TextView) v.findViewById(R.id.hadist_jumlahbab);

            if (colorIndex > colors.length - 1) colorIndex = 0;
            layoutLabel.setBackgroundColor(ContextCompat.getColor(getActivity(), colors[colorIndex]));
            colorIndex++;

            hadistRiwayat.setText(data.get(i).labelRiwayat);
            hadistJumlahbab.setText(data.get(i).jumlahBab);
            return v;
        }
    }


}
