package exairie.com.quranq;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Spinner;

import java.util.List;

import butterknife.InjectView;
import exairie.com.quranq.libs.HadistDBHelper;

public class ABottomSheetSelectBab extends ExtActivity {
    @InjectView(R.id.bottom_sheet)
    FrameLayout bottomSheet;
    @InjectView(R.id.touch_outside)
    View touchOutside;
    BottomSheetBehavior bottomSheetBehavior;
    @InjectView(R.id.spinner_name)
    Spinner spinnerName;
    @InjectView(R.id.spinner_bab)
    Spinner spinnerBab;
    @InjectView(R.id.btn_finish)
    Button btnFinish;

    public ABottomSheetSelectBab() {
        super(R.layout.activity_abottom_sheet_select_bab);

    }

    private void setupEvents() {
        touchOutside.setOnClickListener(view -> {
            Log.d("TouchOuside", "setupEvents: click");
            finish();
        });
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_COLLAPSED) finish();
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
        btnFinish.setOnClickListener(view -> {
            Intent i = new Intent();

            String name = spinnerName.getSelectedItem().toString();
            name = name.equals("Semua")?null:name;
            i.putExtra("name",name);

            String bab = spinnerBab.getSelectedItem().toString();
            bab = bab.equals("Semua")?null:bab;
            i.putExtra("bab",bab);

            setResult(RESULT_OK,i);
            finish();
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
        setResult(RESULT_CANCELED);
        setupEvents();
        setupSpinners();
    }

    private void setupSpinners() {
        List<String> names = HadistDBHelper.getNames(thisContext());
        ArrayAdapter<String> adapterNames = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
        for (String s : names) {
            adapterNames.add(s);
        }
        spinnerName.setAdapter(adapterNames);
        spinnerName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setupSpinnerBab();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void setupSpinnerBab() {
        List<String> bab = HadistDBHelper.getBab(thisContext(), spinnerName.getSelectedItem().toString());
        ArrayAdapter<String> adapterBab = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
        for (String s : bab) {
            adapterBab.add(s);
        }
        spinnerBab.setAdapter(adapterBab);
    }
}
