package exairie.com.quranq

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.AsyncTask
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Event
import exairie.com.quranq.services.EventLoader
import kotlinx.android.synthetic.main.activity_event.*
import kotlinx.android.synthetic.main.view_event.view.*
import java.util.*

class ActivityEvents : ExtActivity(R.layout.activity_event) {
    lateinit var eventAdapter: ActivityEvents.EventsAdapter
    lateinit var sliderAdapter: EventSliderAdapter
    val listener : BroadcastReceiver
    init {
        listener = object : BroadcastReceiver(){
            override fun onReceive(p0: Context?, p1: Intent?) {
                Log.d("Event Load","Triggered")
                eventAdapter.loadData()
            }

        }
    }

    override fun onStart() {
        super.onStart()
        try {
            registerReceiver(listener, IntentFilter(EventLoader.EVENT_LOADED))

        }catch (e:Exception) {
            e.printStackTrace()
        }
        startService(Intent(thisContext(),EventLoader::class.java))
        eventAdapter.loadData()
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            unregisterReceiver(listener)
        }catch (e:Exception) {
            e.printStackTrace()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        this.pager_slideshow.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                changePage(position)
            }
        })

        setExtActionBar(this.toolbar as Toolbar)
        hasBackButton(true)
        setupPager()
        
        recycler_event.isNestedScrollingEnabled = false
        title = "Events"
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_event,menu)

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.mn_createevent -> {
                val i = Intent(thisContext(),ActivityStartEvent::class.java)
                startActivity(i)
            }
            R.id.mn_myevent ->{
                val i = Intent(thisContext(),ActivityMyEvent::class.java)
                startActivity(i)
            }
        }
        return true
    }
    private fun setupPager() {
        eventAdapter = EventsAdapter()
        sliderAdapter = EventSliderAdapter(supportFragmentManager, eventAdapter)

        sliderAdapter.refresh()

        this.layout_event_indicator.removeAllViews()

        var size = sliderAdapter.fragmentList.size
        val density = this.resources.displayMetrics.density
        for (i in 0..size){
            val v = View(this)
            val params = LinearLayout.LayoutParams((8*density).toInt(),(8*density).toInt())
            params.setMargins((4f*density).toInt(),0,0,0)
            v.setBackgroundResource(R.drawable.dot_grey)
            v.layoutParams = params
            this.layout_event_indicator.addView(v)
        }

        this.recycler_event.layoutManager = LinearLayoutManager(this)
        this.recycler_event.adapter = eventAdapter
        this.pager_slideshow.adapter = sliderAdapter
        changePage(0)
    }

    fun changePage(pos : Int){
        if(layout_event_indicator.childCount - 1 < pos) return
        for (v : Int in 0..this.layout_event_indicator.childCount - 1){
            this.layout_event_indicator.getChildAt(v).setBackgroundResource(R.drawable.dot_grey)
        }
        this.layout_event_indicator.getChildAt(pos).setBackgroundResource(R.drawable.dot_green)
    }
    inner class EventsAdapter : RecyclerView.Adapter<EventsAdapter.Holder>(){
        var data : MutableList<Event>
        init {
            data = mutableListOf(Event())
        }
        fun loadData(){
            data.clear()
            val task = object : AsyncTask<Void, Void, Void>() {
                override fun doInBackground(vararg p0: Void?): Void? {
                    val db = DBHelper(thisContext())
                    val c = db.readableDatabase.rawQuery("SELECT * FROM events where date_start >= ? ORDER BY date_start ASC ",
                            arrayOf<String>(System.currentTimeMillis().toString()))
                    if (c.count < 1) {
                        c.close()
                        db.close()
                        return null
                    }

                    c.moveToFirst()
                    while (!c.isAfterLast){
                        val e = Event.fromCursor(c)
                        Log.d("Event","ADD " + e.nama_event)

                        data.add(e)
                        c.moveToNext()

                    }
                    runOnUiThread {
                        layout_noevent.visibility = if (data.size > 0) View.VISIBLE else View.GONE
                    }

                    c.close()
                    db.close()
                    runOnUiThread({
                        try{
                            notifyDataSetChanged()
                        }catch(e : Exception){
                            e.printStackTrace()
                        }
                    })
                    return null;
                }

            }
            task.execute()
        }
        fun addData(e : Event){
            data.add(e)
            notifyDataSetChanged()
        }
        fun getTopThree() : List<Event>?{
            var lst: MutableList<Event> = mutableListOf()
            data.sortBy({ event -> event.date_start * -1 })
            var i = 0
            for (d in data){
                if(i < 3)
                lst.add(d)
                i++
            }
            return lst

        }
        override fun onBindViewHolder(h: Holder, p1: Int) {
            val ev = data[p1]
            h.itemView.l_title.text = ev.nama_event
            h.itemView.l_time.text = android.text.format.DateFormat.format("dd-MMM-yyyy HH:mm:ss", ev.date_start)
            h.itemView.l_participant.text = String.format("%d/%d Participant",ev.capacity,ev.capacity)
            h.itemView.btn_event_info.setOnClickListener({
                val i = Intent(thisContext(),ActivityEventDetail::class.java)
                i.putExtra("id",ev.id)
                startActivity(i)
            })
            h.itemView.btn_register.setOnClickListener({
                val i = Intent(thisContext(),ActivityRegisterEvent::class.java)
                i.putExtra("id",ev.id)
                startActivity(i)
            })
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val v = LayoutInflater.from(p0.context)
                    .inflate(R.layout.view_event,p0,false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.size

        inner class Holder(vw: View) : RecyclerView.ViewHolder(vw)
    }
    inner class EventSliderAdapter(fm : FragmentManager, recAdapter : EventsAdapter) : FragmentPagerAdapter(fm){
        val recAdapter : EventsAdapter
        init {
            this.recAdapter = recAdapter
        }
        val fragmentList : MutableList<Fragment> = mutableListOf()
        override fun getItem(p0: Int): Fragment = fragmentList[p0]

        override fun getCount(): Int = fragmentList.size
        fun refresh(){
            val events = recAdapter.getTopThree()
            if(events == null){
                pager_slideshow.visibility = View.GONE
            }else{
                pager_slideshow.visibility = View.VISIBLE
            }
            for(ev in events!!.iterator()){
                addFragment(FragmentSlide.newInstance(ev))
            }

            notifyDataSetChanged()

        }
        fun addFragment(fragment: Fragment){
            fragmentList.add(fragment)
        }
    }
}
