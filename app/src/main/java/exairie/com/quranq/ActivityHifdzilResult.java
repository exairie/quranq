package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.HifdzilResult;

import static exairie.com.quranq.ActivityHifdzilSelection.SELECTION_MODE_ALL;
import static exairie.com.quranq.ActivityHifdzilSelection.SELECTION_MODE_JUZ;
import static exairie.com.quranq.ActivityHifdzilSelection.SELECTION_MODE_SURAT;

public class ActivityHifdzilResult extends ExtActivity {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.progress_soal)
    ProgressBar progressSoal;
    @InjectView(R.id.l_result_desc)
    TextView lResultDesc;
    @InjectView(R.id.l_result_percentage)
    TextView lResultPercentage;
    @InjectView(R.id.recycler_soal)
    RecyclerView recyclerSoal;
    @InjectView(R.id.l_quizopt)
    TextView lQuizopt;
    private int falseCount;
    private int trueCount;
    private ArrayList<String> surahs;
    private int mode;
    private int quizId;
    private ArrayList<Integer> answers;

    public ActivityHifdzilResult() {
        super(R.layout.activity_hifdzil_result);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);

        setTitle("Hasil Quiz Hifdzil Quran");
        hasBackButton(true);

        Intent i = getIntent();
        trueCount = i.getIntExtra("true", -1);
        falseCount = i.getIntExtra("false", -1);
        surahs = i.getStringArrayListExtra("surahs");
        answers = i.getIntegerArrayListExtra("answers");
        mode = i.getIntExtra("mode", -1);
        quizId = i.getIntExtra("id", -1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String tname = i.getStringExtra("transition");
            progressSoal.setTransitionName(tname);
        }

        recyclerSoal.setNestedScrollingEnabled(false);
        calculateSurah();

    }

    private void calculateSurah() {
        if (mode == SELECTION_MODE_SURAT) {
            lQuizopt.setText(String.format("%s\n%s", "Quiz per Surat", "Surat " + quizId));
        } else if (mode == SELECTION_MODE_JUZ) {
            lQuizopt.setText(String.format("%s\n%s", "Quiz per Juz", "Juz " + quizId));
        } else if (mode == SELECTION_MODE_ALL) {
            lQuizopt.setText(String.format("%s", "Quiz Random"));
        }

        progressSoal.setMax(trueCount + falseCount);
        progressSoal.setProgress(trueCount);
        lResultDesc.setText(String.format("%d/%d soal dijawab dengan benar", trueCount, (trueCount + falseCount)));
        lResultPercentage.setText(String.format("%.0f", ((double) trueCount / ((double) trueCount + (double) falseCount) * 100)));

        recyclerSoal.setLayoutManager(new LinearLayoutManager(thisContext()));
        recyclerSoal.setAdapter(new QuizRecycler());
    }

    class QuizRecycler extends RecyclerView.Adapter<QuizRecycler.Holder> {
        List<String[]> data = new ArrayList<>();


        public QuizRecycler() {
            DBHelper db = new DBHelper(thisContext());
            QuranDBHelper helper = new QuranDBHelper(thisContext());
            final long time = System.currentTimeMillis();
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    Cursor c = helper.getReadableDatabase().rawQuery("" +
                            String.format("SELECT quran.*,quran_name.surah_name FROM quran,quran_name WHERE quran.id in (%s) and quran.surah_no = quran_name.surah_no", TextUtils.join(",", surahs.toArray())), new String[]{});
                    if (c.getCount() < 1) return null;
                    c.moveToFirst();
                    int index = 0;
                    while (!c.isAfterLast()) {
                        String[] d = new String[3];
                        d[0] = c.getString(c.getColumnIndexOrThrow("surah_name"));
                        d[1] = c.getString(c.getColumnIndexOrThrow("ayat_id"));
                        d[2] = String.valueOf(answers.get(index));
                        Log.i("Answer", String.valueOf(answers.get(index)));
                        data.add(d);
                        c.moveToNext();

                        HifdzilResult result = new HifdzilResult();
                        result.answer_true = answers.get(index);
                        result.ayat_id = d[1];
                        result.surah_name = d[0];
                        result.quizdate = time;

                        db.getWritableDatabase().insert("hifdzil_reesult",null,result.getContentValues());



                        index++;
                    }
                    c.close();
                    db.close();
                    helper.close();
                    return null;
                }
            };
            task.execute();
            notifyDataSetChanged();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_result_soal, viewGroup, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            String[] strings = data.get(i);
            holder.lSurahname.setText(strings[0].replace("Surah","").trim());
            holder.lAyatno.setText(String.format("Ayat %s",strings[1]));
            holder.imgTruefalse.setImageResource(strings[2].equals("1")?R.drawable.ic_check_circle_green_700_24dp:R.drawable.ic_assignment_late_red_700_24dp);
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.l_surahname)
            TextView lSurahname;
            @InjectView(R.id.l_ayatno)
            TextView lAyatno;
            @InjectView(R.id.img_truefalse)
            ImageView imgTruefalse;
            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this,itemView);
            }
        }
    }
}
