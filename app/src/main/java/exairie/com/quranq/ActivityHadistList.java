package exairie.com.quranq;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.HadistDBHelper;
import exairie.com.quranq.models.Option;

public class ActivityHadistList extends ExtActivity {
    private static final int ACT_FILTER = 1;
    float textSizeHadist = 25;

    MenuItem searchMenuItem;
    SearchView searchView;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.recycler_hadist)
    RecyclerView recyclerHadist;
    HadistBabAdapter adapter;
    @InjectView(R.id.btn_applyfilter)
    FloatingActionButton btnApplyfilter;
    boolean firstload = true;
    @InjectView(R.id.progress_load)
    ProgressBar progressLoad;

    String name = null;
    String bab = null;
    String searchQuery = null;
    public ActivityHadistList() {
        super(R.layout.activity_hadist_list);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ACT_FILTER && resultCode == RESULT_OK){
            name = data.getStringExtra("name");
            bab = data.getStringExtra("bab");
            adapter.load();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_quranlist,menu);
        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.mn_search);
        searchView = (SearchView)searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(query.length() > 0){
                    searchQuery = query;
                    AsyncTask task = new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {
                            runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                            adapter.loadWithFilter(query);
                            runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                            return null;
                        }
                    };
                    task.execute();
                }
                //ayatAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) {

                    searchQuery = null;
                    AsyncTask task = new AsyncTask() {
                        @Override
                        protected Object doInBackground(Object[] objects) {
                            runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                            adapter.load();
                            runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                            return null;
                        }
                    };
                    task.execute();
                }
                return false;
            }
        });
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);
        setTitle("Daftar Bab Hadist");
        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Option optFontSize = Configs.getConfiguration("fontsize");
        if (optFontSize != null){
            try {
                textSizeHadist = Float.parseFloat(optFontSize.getValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        name = getIntent().getStringExtra("name");
        setupRecycler();
        setupEvents();

    }

    private void setupEvents() {
        btnApplyfilter.setOnClickListener(view -> {
            Intent i = new Intent(this, ABottomSheetSelectBab.class);
            startActivityForResult(i, ACT_FILTER);
        });
    }

    private void setupRecycler() {
        adapter = new HadistBabAdapter();
        recyclerHadist.setAdapter(adapter);
        recyclerHadist.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (firstload) {
            AsyncTask task = new AsyncTask() {
                @Override
                protected Object doInBackground(Object[] objects) {
                    runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                    adapter.load();
                    runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                    firstload = false;
                    return null;
                }
            };
            task.execute();
        }
    }

    class HadistBabAdapter extends RecyclerView.Adapter<Holder> {
        List<HadistDBHelper.HadistBab> data;


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_hadist_kitab, parent, false);

            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            HadistDBHelper.HadistBab data = this.data.get(position);
            holder.lNo.setText(String.valueOf(position + 1));
            String bab;

            if (searchQuery != null){
                bab = data.getBab().replace(searchQuery,"<strong><i>"+searchQuery+"</i></strong>");
            }else{
                bab = data.getBab();
            }

            holder.lBab.setText(Html.fromHtml(bab));
            holder.lNama.setText(data.getNama());
            holder.btnReadkitab.setOnClickListener(view -> {
                Intent i = new Intent(thisContext(), ActivityViewHadist.class);
                i.putExtra("name", data.getNama());
                i.putExtra("bab", data.getBab());
                i.putExtra("search_query",searchQuery);
                startActivity(i);
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public HadistBabAdapter() {
            this.data = new ArrayList<>();
        }

        public void load() {
            this.data = HadistDBHelper.getHadistBab(ActivityHadistList.this,name,bab);
            runOnUiThread(() -> {
                try{
                    notifyDataSetChanged();
                }catch(Exception exc){
                    exc.printStackTrace();
                }finally{

                }
            });
        }
        public void loadWithFilter(String query){
            this.data = HadistDBHelper.getHadistBab(thisContext(),null,null,query);
            runOnUiThread(() -> {
                try{
                    notifyDataSetChanged();
                }catch(Exception exc){
                    exc.printStackTrace();
                }finally{

                }
            });
        }
    }

    class Holder extends RecyclerView.ViewHolder {
        @InjectView(R.id.l_nama)
        TextView lNama;
        @InjectView(R.id.l_bab)
        TextView lBab;
        @InjectView(R.id.l_no)
        TextView lNo;
        @InjectView(R.id.btn_readkitab)
        Button btnReadkitab;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.inject(this, itemView);
        }
    }
}
