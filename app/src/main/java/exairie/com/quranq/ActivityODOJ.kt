package exairie.com.quranq

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.Toolbar
import android.view.View
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.libs.OdojHistoryAdapter
import exairie.com.quranq.libs.QuranDBHelper
import exairie.com.quranq.models.Option
import exairie.com.quranq.models.TodayOdojEntry
import exairie.com.quranq.services.ODOJSynchronizer
import kotlinx.android.synthetic.main.activity_odoj.*
import org.codehaus.jackson.smile.Tool

class ActivityODOJ : ExtActivity(R.layout.activity_odoj) {

    private lateinit var historyAdapter: OdojHistoryAdapter
    var todayData : TodayOdojEntry? = null
    internal var odojBCReceiver: BroadcastReceiver? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recycler_history_odoj.setNestedScrollingEnabled(false)
        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        setTitle("One Day One Juz")
        setupOdojHistory()

        if(Configs.getLoginInfo() != null){
            odojBCReceiver = object : BroadcastReceiver() {
                override fun onReceive(context: Context, intent: Intent) {
                    setupOdojHistory()
                }
            }
            registerReceiver(odojBCReceiver, IntentFilter(ODOJSynchronizer.ODOJ_SYNCHRONIZED))
            val bcLoaderIntent = Intent(getApplicationContext(), ODOJSynchronizer::class.java)
            bcLoaderIntent.putExtra("pullall", "true")
            startService(bcLoaderIntent)
        }
        layout_no_data.visibility = View.VISIBLE
        todayData = DBHelper.getTodayOdojPercentage(thisContext())

        setupViews()
        setupEvents()
    }

    private fun setupEvents() {
        check_reminder.setOnCheckedChangeListener({compoundButton, b ->
            Configs.setConfiguration(Option("odoj_reminder",b.toString()))
            if (b){
                l_reminder_status.setText("Pengingat ODOJ Aktif")
                l_reminder_status.setTextColor(resources.getColor(R.color.green_darken_3))
            }else{
                l_reminder_status.setText("Pengingat ODOJ Tidak Aktif")
                l_reminder_status.setTextColor(resources.getColor(R.color.red_darken_3))
            }
        })

        val opt = Configs.getConfiguration("odoj_reminder")
        if(opt == null){
            check_reminder.setChecked(true)
            check_reminder.setChecked(false)
        }else{
            val isActive = opt.value.toBoolean()
            check_reminder.setChecked(!isActive)
            check_reminder.setChecked(isActive)
        }
    }

    private fun setupViews() {
        if(todayData == null){

            progress_juzcount.max = 0;
            progress_juzcount.progress = 0

            l_surahdetail.setText("-")

            l_juzno.setText("Anda belum melaporkan entri ODOJ hari ini")
            return
        }
        progress_juzcount.max = 100;
        progress_juzcount.progress = if (todayData!!.juzcount > 1) 100 else (todayData!!.juzcount * 100).toInt()

        if (todayData!!.ayat_from != null && todayData!!.ayat_to != null){
            val sFrom = QuranDBHelper.getSurahInfo(thisContext(),todayData!!.ayat_from.surah_no)
            val sTo = QuranDBHelper.getSurahInfo(thisContext(),todayData!!.ayat_to.surah_no)

            l_surahdetail.setText("${sFrom.surah_name}:${todayData!!.ayat_from.ayat_id} - ${sTo.surah_name}:${todayData!!.ayat_to.ayat_id}")

            l_juzno.setText(String.format("%.2f juz dibaca hari ini",todayData!!.juzcount))
        }
    }

    private fun setupOdojHistory() {
        historyAdapter = OdojHistoryAdapter(this, Runnable { layout_no_data.visibility = View.GONE })
        recycler_history_odoj.setAdapter(historyAdapter)
        recycler_history_odoj.setLayoutManager(LinearLayoutManager(thisContext()))
    }

    override fun onDestroy() {
        try {
            unregisterReceiver(odojBCReceiver)
        } catch (e: Exception) {
        }

        super.onDestroy()
    }
}
