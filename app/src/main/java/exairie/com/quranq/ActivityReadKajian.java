package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.widget.TextView;

import butterknife.InjectView;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.models.Kajian;

public class ActivityReadKajian extends ExtActivity {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.l_tgl)
    TextView lTgl;
    @InjectView(R.id.l_title)
    TextView lTitle;
    @InjectView(R.id.l_content)
    TextView lContent;

    public ActivityReadKajian() {
        super(R.layout.activity_acivity_readkajian);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setExtActionBar(toolbar);
        changeStatusBarColor(R.color.blue_darken_2);

        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setTitle("Baca Kajian");
        Intent i = getIntent();
        long id = i.getLongExtra("id", -1);

        DBHelper db = new DBHelper(thisContext());

        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM kajian WHERE id = ?", new String[]{String.valueOf(id)});

        if (c.getCount() < 1){
            c.close();
            finish();
            return;
        }

        c.moveToFirst();
        Kajian kajian = Kajian.create(c);

        String tgl = DateFormat.format("yyyy-MM-dd HH:mm", kajian.create_date).toString();
        lTgl.setText(tgl);
        lTitle.setText(Html.fromHtml(kajian.judul));
        String kajianBody = Html.fromHtml(kajian.isi).toString();
        lContent.setText(kajianBody);

    }
}
