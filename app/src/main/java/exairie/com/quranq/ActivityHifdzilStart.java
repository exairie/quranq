package exairie.com.quranq;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;

import butterknife.InjectView;
import butterknife.OnClick;

public class ActivityHifdzilStart extends ExtActivity {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.select_quiz_perjuz)
    LinearLayout selectQuizPerjuz;
    @InjectView(R.id.select_quiz_acak)
    LinearLayout selectQuizAcak;
    @InjectView(R.id.select_quiz_persurat)
    LinearLayout selectQuizPersurat;

    public ActivityHifdzilStart() {
        super(R.layout.activity_hifdzil_start, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);
    }

    @OnClick(R.id.select_quiz_perjuz)
    public void onSelectQuizPerjuzClicked() {
        Intent i = new Intent(thisContext(),ActivityHifdzilSelection.class);
        i.putExtra("mode",ActivityHifdzilSelection.SELECTION_MODE_JUZ);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.select_quiz_acak)
    public void onSelectQuizAcakClicked() {
        Intent i = new Intent(thisContext(),ActivityHifdzilQuiz.class);
        i.putExtra("mode",ActivityHifdzilSelection.SELECTION_MODE_ALL);
        startActivity(i);
        finish();
    }

    @OnClick(R.id.select_quiz_persurat)
    public void onSelectQuizPersuratClicked() {
        Intent i = new Intent(thisContext(),ActivityHifdzilSelection.class);
        i.putExtra("mode",ActivityHifdzilSelection.SELECTION_MODE_SURAT);
        startActivity(i);
        finish();
    }
}
