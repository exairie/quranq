package exairie.com.quranq;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.interfaces.AppException;
import exairie.com.quranq.interfaces.RecyclerItemScroll;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.HadistDBHelper;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.libs.SentenceLearner;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.Surah;

public class ActivityReadQuran extends ExtActivity {
    private static final int ACT_TRACK_ODOJ_FROM_FLAG = 1;

    List<RecyclerItemScroll> animScrollListeners = new ArrayList<>();
    float textSizeQuran = 25;
    int surahID;
    int jumpID;
    int jump_ayat;
    MenuItem searchMenuItem;
    SearchView searchView;
    QuranAyatAdapter ayatAdapter;
    Surah currentSurah;

    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.recycler_ayat)
    RecyclerView recyclerAyat;

    public ActivityReadQuran() {
        super(R.layout.activity_read_quran);
    }
    public void addRecyclerScrollListener(RecyclerItemScroll listener){
        animScrollListeners.add(listener);
    }
    public void removeRecyclerScrollListener(RecyclerItemScroll listener){
        animScrollListeners.remove(listener);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_readquran,menu);

        SearchManager searchManager = (SearchManager)getSystemService(Context.SEARCH_SERVICE);
        searchMenuItem = menu.findItem(R.id.mn_search);
        searchView = (SearchView)searchMenuItem.getActionView();

        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                ayatAdapter.filter(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() < 1) {
                    ayatAdapter.filter("");
                    return true;
                }
                return false;
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.mn_nextsurah:{
                surahID++;
                if (surahID > 114) surahID = 1;

                reloadAdapter();
            }
            break;
            case R.id.mn_prevsurah:{
                surahID--;
                if (surahID < 1) surahID = 114;

                reloadAdapter();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);

        Intent i = getIntent();
        surahID = i.getIntExtra("id", -1);
        jumpID = i.getIntExtra("jump",-1);
        jump_ayat = i.getIntExtra("jump_ayat",-1);


        Option optFontSize = Configs.getConfiguration("fontsize");
        if (optFontSize != null){
            try {
                textSizeQuran = Float.parseFloat(optFontSize.getValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        setupAdapter();
    }
    void refreshSurahName(){
        currentSurah = QuranDBHelper.getSurahInfo(this, surahID);


        String surahName = currentSurah.getSurah_name();
        try {
            getSupportActionBar().setTitle(surahName == null ? "Unknown Surah" : surahName);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    void reloadAdapter(){
        ayatAdapter.load(surahID);
        refreshSurahName();
    }
    void setupAdapter() {
        if (surahID == -1) {
            new AlertDialog.Builder(this)
                    .setMessage("Error reading metadata!\n")
                    .setTitle("Error")
                    .setNegativeButton("Close", (dialogInterface, i) -> {
                        dialogInterface.dismiss();
                        finish();
                    }).show();
        }
        ayatAdapter = new QuranAyatAdapter();
        recyclerAyat.setAdapter(ayatAdapter);
        recyclerAyat.setLayoutManager(new LinearLayoutManager(this));
        recyclerAyat.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                Log.d("Scroll","ScrollEvent");
                clearClickEvent();
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        reloadAdapter();
    }
    void clearClickEvent(){
        if(animScrollListeners.size() > 0){
            List<RecyclerItemScroll> toDelete = new ArrayList<>();
            for (RecyclerItemScroll sc : animScrollListeners) {
                sc.onScroll();
                toDelete.add(sc);
            }
            for(RecyclerItemScroll dl : toDelete){
                animScrollListeners.remove(dl);
            }
            toDelete = null;

        }
    }

    class QuranAyatAdapter extends RecyclerView.Adapter<QuranAyatAdapter.Holder> {

        final static int AYAT_NORMAL = 1;
        final static int AYAT_BISMILLAH = 2;
        List<Ayat> data;
        List<Ayat> filteredData;
        boolean useFilter = false;
        Animation newAnimObj() {
            return AnimationUtils.loadAnimation(ActivityReadQuran.this, R.anim.roll_from_left);
        }
        Animation newAnimContainer(){
            return AnimationUtils.loadAnimation(ActivityReadQuran.this,R.anim.alpha);
        }
        class ImgOnstartAnim implements Animation.AnimationListener{
            View v;
            public ImgOnstartAnim(View v){
                this.v = v;
            }
            @Override
            public void onAnimationStart(Animation animation) {
                v.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        }

        public QuranAyatAdapter() {
            this.data = new ArrayList<>();
        }
        public void filter(String filter){
            Log.d("FILTER", "filter: " +filter);
            if (filter.length() < 1){
                filteredData = null;
                useFilter = false;
                notifyDataSetChanged();
                return;
            }
            filteredData = new ArrayList<>();
            for (Ayat a : data){
                if (a.getIndonesian().toLowerCase().contains(filter.toLowerCase())){
                    filteredData.add(a);
                    Log.d("FILTER", "filter: found " +a.getIndonesian());
                }
            }

            useFilter = true;
            notifyDataSetChanged();

        }
        public void load(int surah_no) {

            this.data = new QuranDBHelper(ActivityReadQuran.this).getAyatList(surah_no);
            notifyDataSetChanged();

            if(jumpID != -1){
                Log.d("JUMP", "JUMP ID: " + jumpID);
                for (int i=0;i<data.size();i++){
                    if(data.get(i).getId() == jumpID){
                        recyclerAyat.scrollToPosition(i);
                        break;
                    }
                }
            }
            if (jump_ayat != -1){
                for (int i = 0;i < data.size();i++){
                    if (data.get(i).getAyat_id() == jump_ayat){
                        recyclerAyat.scrollToPosition(i);
                        break;
                    }
                }
            }
        }


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate((viewType == AYAT_NORMAL?R.layout.view_quran_read:R.layout.view_quran_bismillah), parent, false);
            return new Holder(v);
        }


        @Override
        public void onBindViewHolder(Holder holder, int position) {
            Ayat ayat = useFilter?filteredData.get(position):data.get(position);
            holder.lAyatNumber.setText(String.valueOf(ayat.getAyat_id()));
            holder.lSurahAyat.setText(ayat.getArabic());
            holder.lSurahTranslation.setText(ayat.getIndonesian());

            holder.imgCopy.setOnClickListener(view -> {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("quranqu", String.format("QS %d:%d : \n%s \n\n %s \n\n - Quran-Qu",ayat.getSurah_no(),ayat.getAyat_id(),ayat.getArabic(),ayat.getIndonesian()));
                clipboard.setPrimaryClip(clip);
                runOnUiThread(() -> {
                    new AlertDialog.Builder(thisContext())
                            .setTitle("Copy ayat")
                            .setMessage("Ayat telah disalin ke clipboard")
                            .setPositiveButton("Tutup",(dialogInterface, i) -> dialogInterface.dismiss())
                            .show();
                });
            });

            if (getItemViewType(position) == AYAT_NORMAL){
                setupEvents(holder, position);
            }
        }

        @Override
        public int getItemViewType(int position) {
            return (filteredData != null?filteredData:data).get(position).getAyat_id() == 0?AYAT_BISMILLAH:AYAT_NORMAL;
        }

        private void setupEvents(Holder holder, int position) {
            Ayat ayat = useFilter?filteredData.get(position):data.get(position);
            holder.layoutConstraint.setOnClickListener(view -> {
                clearClickEvent();
                holder.animOpenMenu.run();
            });

            holder.layoutConstraintHiddenmenu.setOnClickListener(view -> {
                removeRecyclerScrollListener(holder.scrollListener);
                holder.animDismissMenu.run();
            });
            holder.imgLastread.setOnClickListener(view -> {
                new Handler().post(() -> {
                    Option option = new Option();
                    option.setKey(Option.KEY_LAST_READ);
                    option.setValue(String.valueOf(ayat.getId()));
                    Configs.setConfiguration(option);
                    
                    runOnUiThread(() -> {
                        new AlertDialog.Builder(ActivityReadQuran.this)
                                .setTitle("Bacaan terakhir disimpan")
                                .setMessage("Apakah anda juga ingin melaporkan ODOJ?")
                                .setPositiveButton("Ya",(dialogInterface, i) -> {
                                    Intent intent = new Intent(ActivityReadQuran.this,ActivityTrackODOJ.class);
                                    intent.putExtra("id",ayat.getId());
                                    startActivityForResult(intent, ACT_TRACK_ODOJ_FROM_FLAG);
                                    dialogInterface.dismiss();
                                })
                                .setNegativeButton("Tidak", (dialogInterface, i) -> {
                                    dialogInterface.dismiss();
                                })
                                .show();
                    });
                });
            });
            holder.imgBookmark.setOnClickListener(view -> {
                int ayatID = ayat.getId();
                DBHelper db = new DBHelper(ActivityReadQuran.this);
                try {
                    long res = DBHelper.addQuranBookmark(ActivityReadQuran.this, ayatID);
                    showDismissableAlert(ActivityReadQuran.this,"Tambah bookmark",
                            res > 0?"Bookmark ditambahkan!":"Bookmark gagal ditambahkan!","Tutup");
                } catch (AppException.BookmarkExistException | AppException.AyatNotFoundException e) {
                    runOnUiThread(() -> {
                        showDismissableAlertWithPositiveBtn(ActivityReadQuran.this,
                                "Bookmark sudah ada", "Apakah anda ingin menghapus bookmark?","Tidak","Ya",
                                (dialogInterface, i) -> {
                                    dialogInterface.dismiss();
                                    long res = DBHelper.removeQuranBookmark(ActivityReadQuran.this, ayatID);
                                    showDismissableAlert(ActivityReadQuran.this,"Penghapusan bookmark",
                                            res > 0?"Bookmark dihapus!":"Bookmark gagal dihapus!","Tutup");
                                });
                    });
                }
            });
//            holder.imgHasReference.setOnClickListener(view -> {
//                ProgressDialog dialog = new ProgressDialog(thisContext());
//                dialog.setMessage("Mencari referensi...");
//                dialog.show();
//                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//                    @Override
//                    protected Void doInBackground(Void... voids) {
//                        SentenceLearner learner = new SentenceLearner(ayat.getIndonesian());
//                        if (learner.resultCount() > 0){
//                            List<Long> h = HadistDBHelper.getReference(thisContext(),learner);
//                            if (h.size() > 1){
//                                dialog.dismiss();
//                                Intent i = new Intent(thisContext(),ActivityViewHadist.class);
//                                try {
//                                    i.putExtra("tags",Configs.mapper().writeValueAsString(learner.getTopThree()));
//                                    i.putExtra("refs",Configs.mapper().writeValueAsString(h));
//                                    startActivity(i);
//                                    return null;
//
//                                } catch (IOException e) {
//                                    e.printStackTrace();
//                                }
//                            }
//                        }
//                        dialog.dismiss();
//                        runOnUiThread(() -> {
//                            new AlertDialog.Builder(thisContext())
//                                    .setMessage("Referensi hadist tidak tersedia")
//                                    .setNegativeButton("Tutup",(dialogInterface, i) -> dialogInterface.dismiss())
//                                    .show();
//                        });
//
//                        return null;
//                    }
//                };
//                task.execute();
//            });
        }
        @Override
        public int getItemCount() {
            return useFilter?filteredData.size():data.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            RecyclerItemScroll scrollListener;
            Runnable animDismissMenu;
            Runnable animOpenMenu;
            @InjectView(R.id.l_ayat_number)
            TextView lAyatNumber;
            @InjectView(R.id.l_surah_ayat)
            TextView lSurahAyat;
            @InjectView(R.id.l_surah_translation)
            TextView lSurahTranslation;
            @InjectView(R.id.img_copy)
            ImageView imgCopy;
            @InjectView(R.id.img_bookmark)
            ImageView imgBookmark;
            @InjectView(R.id.img_fav)
            ImageView imgFav;
            @InjectView(R.id.img_lastread)
            ImageView imgLastread;
//            @InjectView(R.id.img_hasreference)
//            ImageView imgHasReference;
            @InjectView(R.id.layout_constraint)
            ConstraintLayout layoutConstraint;
            @InjectView(R.id.layout_constraint_hiddenmenu)
            ConstraintLayout layoutConstraintHiddenmenu;

            Typeface tf;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);

                if (tf ==null)tf = Typeface.createFromAsset(getAssets(), "quranfontustmani.otf");

                lSurahAyat.setTypeface(tf);
                lSurahAyat.setTextSize(TypedValue.COMPLEX_UNIT_DIP,textSizeQuran);
                layoutConstraintHiddenmenu.setAnimation(newAnimContainer());

                imgLastread.setAnimation(newAnimContainer());
                imgLastread.getAnimation().setStartOffset(300);

                animDismissMenu = ()->{
                    layoutConstraintHiddenmenu.setVisibility(View.GONE);
                    imgCopy.setVisibility(View.INVISIBLE);
                    imgBookmark.setVisibility(View.INVISIBLE);
//                    imgFav.setVisibility(View.INVISIBLE);
                    imgLastread.setVisibility(View.INVISIBLE);
//                    imgHasReference.setVisibility(View.INVISIBLE);
//                    Animation anim = newAnimContainer();
//                    anim.setInterpolator(v -> 1 - v);
//                    anim.setAnimationListener(new Animation.AnimationListener() {
//                        @Override
//                        public void onAnimationStart(Animation animation) {
//
//                        }
//
//                        @Override
//                        public void onAnimationEnd(Animation animation) {
//                            Log.d("ANIMEND","CALL");
//
//
//                        }
//
//                        @Override
//                        public void onAnimationRepeat(Animation animation) {
//
//                        }
//                    });
//                    layoutConstraintHiddenmenu.startAnimation(anim);
                };
                scrollListener = () -> animDismissMenu.run();
                animOpenMenu = () -> {
                    Animation animation = newAnimContainer();
                    animation.setAnimationListener(new Animation.AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {

                            layoutConstraintHiddenmenu.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationEnd(Animation animation) {
                            Animation animImgCopy = newAnimObj();
                            Animation animImgBookmark = newAnimObj();
                            Animation animImgFav = newAnimObj();
                            Animation animImgLastRead = newAnimObj();
                            Animation animImgHasRef = newAnimObj();

                            animImgCopy.setStartOffset(0);
                            animImgBookmark.setStartOffset(100);
                            animImgFav.setStartOffset(200);
                            animImgLastRead.setStartOffset(300);
                            animImgHasRef.setStartOffset(400);

                            animImgCopy.setAnimationListener(new ImgOnstartAnim(imgCopy));
                            animImgBookmark.setAnimationListener(new ImgOnstartAnim(imgBookmark));
                            animImgFav.setAnimationListener(new ImgOnstartAnim(imgFav));
                            animImgLastRead.setAnimationListener(new ImgOnstartAnim(imgLastread));
//                            animImgHasRef.setAnimationListener( new ImgOnstartAnim(imgHasReference));

                            imgCopy.startAnimation(animImgCopy);

                            imgBookmark.startAnimation(animImgBookmark);

//                            imgFav.startAnimation(animImgFav);

                            imgLastread.startAnimation(animImgLastRead);

//                            imgHasReference.startAnimation(animImgHasRef);

                        }

                        @Override
                        public void onAnimationRepeat(Animation animation) {

                        }
                    });
                    layoutConstraintHiddenmenu.setVisibility(View.INVISIBLE);
                    layoutConstraintHiddenmenu.startAnimation(animation);
                    addRecyclerScrollListener(scrollListener);
                };

            }


        }
        class RefHolder{
            public RefHolder(int id, List<HadistDBHelper.Hadist> references) {
                this.id = id;
                this.references = references;
            }

            public int id;
            public List<HadistDBHelper.Hadist> references;
            public void setupIntent(Intent i){
                try {
                    String json = Configs.mapper().writeValueAsString(references);
                    i.putExtra("refs",json);
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
    }
}
