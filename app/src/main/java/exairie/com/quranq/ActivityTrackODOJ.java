package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.StringWithID;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Surah;
import exairie.com.quranq.services.ODOJSynchronizer;

public class ActivityTrackODOJ extends ExtActivity {
    @InjectView(R.id.spinner_list_surah_start)
    Spinner spinnerListSurahStart;
    @InjectView(R.id.spinner_list_ayat_start)
    Spinner spinnerListAyatStart;
    @InjectView(R.id.spinner_list_surah_finish)
    Spinner spinnerListSurahFinish;
    @InjectView(R.id.spinner_list_ayat_finish)
    Spinner spinnerListAyatFinish;
    @InjectView(R.id.l_calculation)
    TextView lCalculation;
    @InjectView(R.id.btn_save)
    Button btnSave;
    int count;
    int odoj_surahid_start = -1;
    int odoj_surahid_finish = -1;
    @InjectView(R.id.progress_load)
    ProgressBar progressLoad;

    public ActivityTrackODOJ() {
        super(R.layout.activity_track_odoj);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        Intent i = getIntent();
        odoj_surahid_start = DBHelper.getLastODOJEntry(this);

        odoj_surahid_finish = i.getIntExtra("id", -1);

        setupSpinners();
        setupEvents();
    }

    @Override
    protected void onStart() {
        super.onStart();

        preSelectOdojEntry();
    }

    private void preSelectOdojEntry() {

        if (odoj_surahid_start != -1) {

            AsyncTask<Void, Void, Void> t = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    Log.d("ODOJSTART", "PRECALCULATE");
                    runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                    Ayat ayatStart = QuranDBHelper.getAyat(ActivityTrackODOJ.this, odoj_surahid_start);
                    runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                    if (ayatStart == null) {
                        Log.d("AYATSTART", "CANT FIND AYAT");
                        return null;
                    }

                    runOnUiThread(() -> {
                        if (spinnerListSurahStart.getAdapter() == null) {
                            Log.d("SPINNERSTART", "SPINNER ADAPTER NULL");
                            return;
                        }

                        for (int i = 0; i < spinnerListSurahStart.getAdapter().getCount(); i++) {
                            StringWithID s = (StringWithID) spinnerListSurahStart.getAdapter().getItem(i);
                            if (s.getId() == ayatStart.getSurah_no()) {
                                spinnerListSurahStart.setSelection(i, true);
                                View v = spinnerListSurahStart.getChildAt(i);

                                spinnerListSurahStart.getOnItemSelectedListener()
                                        .onItemSelected(spinnerListSurahStart, v, i, spinnerListSurahStart.getAdapter().getItemId(i));
                                break;
                            }
                        }

                        if (spinnerListAyatStart.getAdapter() == null) {
                            Log.d("AYATSTART", "SPINNER ADAPTER NULL");
                            return;
                        }
                    });
                    return null;
                }
            };
            t.execute();

        }

        if (odoj_surahid_finish != -1) {

            AsyncTask<Void, Void, Void> t = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    Log.d("ODOJFINISH", "PRECALCULATE");
                    runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                    Ayat ayatFinish = QuranDBHelper.getAyat(ActivityTrackODOJ.this, odoj_surahid_finish);
                    runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                    if (ayatFinish == null) {
                        Log.d("AYATFINISH", "CANT FIND AYAT");
                        return null;
                    }

                    runOnUiThread(() -> {
                        if (spinnerListSurahFinish.getAdapter() == null) {
                            Log.d("SPINNERFINISH", "SPINNER ADAPTER NULL");
                            return;
                        }


                        Log.d("AYATSTART", "SETSELECTION FINISH");
                        for (int i = 0; i < spinnerListSurahFinish.getAdapter().getCount(); i++) {
                            StringWithID s = (StringWithID) spinnerListSurahFinish.getAdapter().getItem(i);
                            if (s.getId() == ayatFinish.getSurah_no()) {
                                spinnerListSurahFinish.setSelection(i, true);
                                View v = spinnerListSurahFinish.getChildAt(i);

                                spinnerListSurahFinish.getOnItemSelectedListener()
                                        .onItemSelected(spinnerListSurahFinish, v, i, spinnerListSurahFinish.getAdapter().getItemId(i));
                                break;
                            }
                        }


                        Log.d("AYATFINISH", "SETSELECTION START");
                        if (spinnerListAyatFinish.getAdapter() == null) {
                            Log.d("AYATFINISH", "SPINNER ADAPTER NULL");
                            return;
                        }

                        Log.d("AYATFINISH", "SETSELECTION FINISH");
                        //spinnerListSurahFinish.setOnItemSelectedListener(oldListener);
                    });
                    return null;
                }
            };
            t.execute();


        }
    }

    private void setupEvents() {
        btnSave.setOnClickListener(view -> {
            DBHelper.saveODOJ(this, odoj_surahid_start, odoj_surahid_finish, count);

            Toast.makeText(this, "Entri ODOJ telah disimpan!", Toast.LENGTH_LONG).show();

            Intent i = new Intent(thisContext(), ODOJSynchronizer.class);
            startService(i);

            finish();
        });
    }

    private void setupSpinners() {
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                runOnUiThread(() -> progressLoad.setVisibility(View.VISIBLE));
                QuranDBHelper quranDBHelper = new QuranDBHelper(thisContext());
                List<Surah> surahList = quranDBHelper.getSurahList();
                runOnUiThread(() -> progressLoad.setVisibility(View.GONE));
                ArrayAdapter<StringWithID> adapterStart = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
                for (Surah s : surahList) {
                    adapterStart.add(new StringWithID(s.getSurah_no(), s.getSurah_name()));
                }

                runOnUiThread(() -> {
                    spinnerListSurahStart.setAdapter(adapterStart);

                    spinnerListSurahStart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("SPINNER START", "SELECTED");
                            Cursor c = quranDBHelper.getReadableDatabase().rawQuery(
                                    "SELECT * FROM quran WHERE surah_no = ?", new String[]{String.valueOf(((StringWithID) adapterView.getSelectedItem()).getId())}
                            );

                            if (c.getCount() < 1) {
                                return;
                            }

                            c.moveToFirst();

                            ArrayAdapter<StringWithID> ayatAdapterStart = new ArrayAdapter<>(ActivityTrackODOJ.this, android.R.layout.simple_spinner_dropdown_item);

                            while (!c.isAfterLast()) {
                                ayatAdapterStart.add(new StringWithID(c.getInt(0), String.valueOf(c.getInt(2))));
                                c.moveToNext();
                            }

                            spinnerListAyatStart.setAdapter(ayatAdapterStart);

                            Log.d("AYATSTART", "SETSELECTION");
                            if (odoj_surahid_start != -1) {
                                for (int i2 = 0; i2 < spinnerListAyatStart.getAdapter().getCount(); i2++) {
                                    StringWithID s = (StringWithID) spinnerListAyatStart.getAdapter().getItem(i2);
                                    if (s.getId() == odoj_surahid_start) {
                                        spinnerListAyatStart.setSelection(i2, true);
                                        View v = spinnerListAyatStart.getChildAt(i2);
                                        spinnerListAyatStart.getOnItemSelectedListener()
                                                .onItemSelected(spinnerListAyatStart, v, i2, spinnerListAyatStart.getAdapter().getItemId(i2));
                                        Log.d("AYATSTART", "SELECTION " + i2);
                                        break;
                                    }
                                }
                            }

                            c.close();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {
                            Log.d("SPINNER START", "NOTHING SELECTED");
                        }
                    });
                });

                ArrayAdapter<StringWithID> adapterFinish = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
                for (Surah s : surahList) {
                    adapterFinish.add(new StringWithID(s.getSurah_no(), s.getSurah_name()));
                }
                runOnUiThread(() -> {
                    spinnerListSurahFinish.setAdapter(adapterFinish);


                    spinnerListSurahFinish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            Log.d("SPINNER SURAH FINISH", "spinnerListSurahFinish");
                            Cursor c = quranDBHelper.getReadableDatabase().rawQuery(
                                    "SELECT * FROM quran WHERE surah_no = ?", new String[]{String.valueOf(((StringWithID) adapterView.getSelectedItem()).getId())}
                            );

                            if (c.getCount() < 1) {
                                return;
                            }

                            c.moveToFirst();

                            ArrayAdapter<StringWithID> ayatAdapterFinish = new ArrayAdapter<>(ActivityTrackODOJ.this, android.R.layout.simple_spinner_dropdown_item);

                            while (!c.isAfterLast()) {
                                ayatAdapterFinish.add(new StringWithID(c.getInt(0), String.valueOf(c.getInt(2))));
                                c.moveToNext();
                            }

                            spinnerListAyatFinish.setAdapter(ayatAdapterFinish);

                            if (odoj_surahid_finish != -1) {

                                for (int i2 = 0; i2 < spinnerListAyatFinish.getAdapter().getCount(); i2++) {
                                    StringWithID s = (StringWithID) spinnerListAyatFinish.getAdapter().getItem(i2);
                                    if (s.getId() == odoj_surahid_finish) {
                                        spinnerListAyatFinish.setSelection(i2, true);
                                        View v = spinnerListAyatFinish.getChildAt(i2);
                                        spinnerListAyatFinish.getOnItemSelectedListener()
                                                .onItemSelected(spinnerListAyatFinish, v, i2, spinnerListAyatFinish.getAdapter().getItemId(i2));
                                        break;
                                    }
                                }
                            }

                            c.close();
                            Log.d("SPINNER SURAH FINISH", "spinnerListSurahFinish done");
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinnerListAyatStart.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            odoj_surahid_start = ((StringWithID) adapterView.getSelectedItem()).getId();
                            calculateOdoj();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinnerListAyatFinish.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            odoj_surahid_finish = ((StringWithID) adapterView.getSelectedItem()).getId();
                            calculateOdoj();
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                });
                return null;
            }
        };
        task.execute();
    }

    void calculateOdoj() {
        if (odoj_surahid_finish == -1 || odoj_surahid_start == -1) return;

        QuranDBHelper dbHelper = new QuranDBHelper(this);

        Cursor c = dbHelper.getReadableDatabase().rawQuery("SELECT SUM(LENGTH(arabic)) as READED FROM quran WHERE id BETWEEN ? and ?",
                new String[]{String.valueOf(odoj_surahid_start), String.valueOf(odoj_surahid_finish)});

        if (c.getCount() < 1) {
            lCalculation.setText("Tidak dapat menghitung jumlah juz");
            return;
        }
        c.moveToFirst();
        count = c.getInt(0);

        c.close();

        dbHelper.close();

        double odoj_percentage = count / Configs.LETTER_PER_JUZ;

        lCalculation.setText(String.format("Anda telah membaca %.1f juz", odoj_percentage));
    }
}
