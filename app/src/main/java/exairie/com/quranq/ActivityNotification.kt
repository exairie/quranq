package exairie.com.quranq

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.os.Bundle
import android.util.Log
import android.view.View
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Notifications
import exairie.com.quranq.services.NotificationLoader
import kotlinx.android.synthetic.main.activity_notification.*
import kotlinx.android.synthetic.main.view_hadist_kitab.*

class ActivityNotification : ExtActivity(R.layout.activity_notification) {
    var notifID: Long = 0
    var nextNotif : Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        btn_detail_notif.visibility = View.GONE
        notifID = intent.getLongExtra("id",-1)

        if(notifID == -1L){
            Log.d("NotifAct","Notification ID Invalid! finish() called")
//            finish()
            return
        }

        val forceOpen = intent.getBooleanExtra("forceopen",false)

        val db = DBHelper(thisContext())

        var c : Cursor
        if(forceOpen){
            c = db.readableDatabase.rawQuery("SELECT * FROM notifications_pending WHERE id = ?", arrayOf(notifID.toString()))
        }else{
            c = db.readableDatabase.rawQuery("SELECT * FROM notifications_pending WHERE id = ? and viewed = 0 or viewed is null", arrayOf(notifID.toString()))
        }
        if(c.count < 1){
            Log.d("NotifAct","Notification ID Invalid! finish() called")
            finish()
            return
        }
        c.moveToFirst()

        val notification = Notifications.fromCursor(c)
        l_notif_title.setText(notification.notif_text)
        l_notif_desc.setText(notification.notif_desc)
        btn_close.setOnClickListener({finish()})

        c.close()

        val values = ContentValues();
        values.put("viewed",1)
        db.readableDatabase.update("notifications_pending",values,"id = ?", arrayOf(notifID.toString()))

        c = db.readableDatabase.rawQuery("SELECT * FROM notifications_pending where viewed = 0 or viewed is null order by create_date ASC", arrayOf())

        if(c.count > 0){
            c.moveToFirst()

            nextNotif = Notifications.fromCursor(c).id
        }

        c.close()
        db.close()
    }

    override fun onDestroy() {
        if(nextNotif != -1L){
            val intent = Intent(applicationContext,this::class.java)
            intent.putExtra("id",nextNotif)
            startActivity(intent)
        }
        super.onDestroy()
    }
    companion object {
        fun checkIncomingNotifications(context : Context){
            val db = DBHelper(context)

            val c = db.readableDatabase.rawQuery("SELECT * FROM notifications_pending where viewed != 1 or viewed is null order by create_date ASC", arrayOf())
            var nextNotif : Long = -1
            if(c.count > 0){
                c.moveToFirst()
                nextNotif = Notifications.fromCursor(c).id

            }

            c.close()
            db.close()

            if(nextNotif != -1L){
                val intent = Intent(context.applicationContext,ActivityNotification::class.java)
                intent.putExtra("id",nextNotif)
                context.startActivity(intent)
            }

            val i = Intent(context.getApplicationContext(), NotificationLoader::class.java)
            context.startService(i)
        }
    }
}
