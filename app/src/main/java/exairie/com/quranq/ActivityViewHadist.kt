package exairie.com.quranq

import android.graphics.Typeface
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Html
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.HORIZONTAL
import android.widget.TextView

import java.util.ArrayList

import butterknife.ButterKnife
import butterknife.InjectView
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.HadistDBHelper
import exairie.com.quranq.libs.SentenceLearner
import kotlinx.android.synthetic.main.activity_view_hadist.*
import kotlinx.android.synthetic.main.view_hadist_content.*
import kotlinx.android.synthetic.main.view_hadist_content.view.*
import kotlinx.android.synthetic.main.view_hadist_tag.view.*

class ActivityViewHadist : ExtActivity(R.layout.activity_view_hadist) {
    internal var refs : Array<Long>? = null
    internal var tags : Array<SentenceLearner.SentenceValue>? = null

    internal var name: String? = null
    internal var bab: String? = null
    internal var searchQuery: String? = null

    internal var adapter: HadistAdapter? = null
    private var textSizeHadist = 25f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExtActionBar(this.toolbar as Toolbar)
        val actionBar = supportActionBar

        try {
            actionBar!!.setDisplayHomeAsUpEnabled(true)
            actionBar.setDisplayShowHomeEnabled(true)
        } catch (e: Exception) {
            e.printStackTrace()
        }

        val i = intent
        if(i.getStringExtra("refs") == null){
            name = i.getStringExtra("name")
            bab = i.getStringExtra("bab")
            searchQuery = i.getStringExtra("search_query")


            val optFontSize = Configs.getConfiguration("fontsize")
            textSizeHadist = if (optFontSize == null) 17f else optFontSize.value.toFloat()

            title = String.format("Hadist %s", name)

            layout_tagref.visibility = View.GONE
        }else{
            layout_tagref.visibility = View.VISIBLE
            refs = Configs.mapper().readValue(i.getStringExtra("refs"), Array<Long>::class.java)
            tags = Configs.mapper().readValue(i.getStringExtra("tags"),Array<SentenceLearner.SentenceValue>::class.java)
        }
    }

    private fun setupRecycler() {
        if (adapter == null) adapter = HadistAdapter()
        this.recycler_hadist!!.layoutManager = LinearLayoutManager(this)
        this.recycler_hadist!!.adapter = adapter

        if(tags != null){
            val tagsAdapter = object : RecyclerView.Adapter<HolderTag>(){
                override fun onBindViewHolder(p0: HolderTag, p1: Int) {
                    p0!!.itemView.l_tag.setText("#${tags!![p1].str.toString()}")
                }

                override fun getItemCount(): Int = tags!!.size

                override fun onCreateViewHolder(p0: ViewGroup, p1: Int): HolderTag {
                    val v = LayoutInflater.from(p0!!.context)
                            .inflate(R.layout.view_hadist_tag,p0,false)
                    return HolderTag(v)
                }
            }
            recycler_reference.adapter = tagsAdapter
            recycler_reference.layoutManager = LinearLayoutManager(thisContext(),HORIZONTAL,false)
        }
    }

    override fun onStart() {
        super.onStart()
        if (adapter == null) {
            setupRecycler()
            val task = object : AsyncTask<Void,Void,Void>() {
                override fun doInBackground(objects: Array<Void>): Void? {
                    runOnUiThread { progress_load!!.visibility = View.VISIBLE }
                    adapter!!.load()
                    runOnUiThread { progress_load!!.visibility = View.GONE }
                    return null
                }
            }
            task.execute()
        }

    }

    internal inner class HadistAdapter : RecyclerView.Adapter<Holder>() {
        var data: MutableList<HadistDBHelper.Hadist>


        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_hadist_content, parent, false)
            return Holder(v)
        }

        override fun onBindViewHolder(holder: Holder, position: Int) {
            val data = this.data[position]

            if (searchQuery != null) {
                data.indonesia = data.indonesia.replace(searchQuery!!, "<strong><i>$searchQuery</i></strong>")
            }

            holder.itemView.l_albani.text = if (data.albani.length < 1) "Shahih" else data.albani
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                holder.itemView.l_arabic.text = Html.fromHtml(data.arabic, Html.FROM_HTML_MODE_COMPACT)
                holder.itemView.l_indonesian.text = Html.fromHtml(data.indonesia, Html.FROM_HTML_MODE_COMPACT)
            } else {
                holder.itemView.l_indonesian.text = Html.fromHtml(data.indonesia)
                holder.itemView.l_arabic.text = Html.fromHtml(data.arabic)
            }
            holder.itemView.l_name_no.text = String.format("HR %s", data.nama)
        }

        override fun getItemCount(): Int = data.size

        fun load() {
            if(refs == null){
                data = HadistDBHelper.getHadist(thisContext(), name, bab)
                runOnUiThread { this.notifyDataSetChanged() }
                runOnUiThread {
                    if (searchQuery != null) {
                        for (i in data.indices) {
                            if (data[i].indonesia.toLowerCase().contains(searchQuery!!.toLowerCase())) {
                                recycler_hadist!!.scrollToPosition(i)
                                break
                            }
                        }
                    }
                }
            }else{
                val db = HadistDBHelper(thisContext())
                val inString = TextUtils.join(",",refs)
                val c = db.readableDatabase.rawQuery("SELECT * FROM hadist WHERE id in ($inString)", arrayOf())
                if(c.count < 1){
                    c.close()
                    db.close()
                    data = mutableListOf()
                    return
                }
                data = mutableListOf()
                c.moveToFirst()
                while(!c.isAfterLast){
                    data.add(HadistDBHelper.Hadist.createFromCursor(c))
                    c.moveToNext()
                }

                c.close()
                db.close()

            }
        }

        init {
            data = ArrayList<HadistDBHelper.Hadist>()
        }
    }

    internal inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tf: Typeface

        init {
            ButterKnife.inject(this, itemView)
            tf = Typeface.createFromAsset(assets, "quranfontustmani.otf")
            itemView.l_arabic.typeface = tf
            itemView.l_arabic.setTextSize(TypedValue.COMPLEX_UNIT_DIP, textSizeHadist)
        }
    }
    internal inner class HolderTag (itemView: View) : RecyclerView.ViewHolder(itemView){

    }
}
