package exairie.com.quranq;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioButton;

import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.FinishedRequest;
import exairie.com.quranq.models.RequestResult;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

public class ActivitySignUp extends ExtActivity {
    Animation enterAnim;
    Animation exitAnim;
    @InjectView(R.id.t_username)
    TextInputEditText tUsername;
    @InjectView(R.id.t_password)
    TextInputEditText tPassword;
    @InjectView(R.id.t_firsname)
    TextInputEditText tFirsname;
    @InjectView(R.id.t_lastname)
    TextInputEditText tLastname;
    @InjectView(R.id.t_email)
    TextInputEditText tEmail;
    @InjectView(R.id.t_phone)
    TextInputEditText tPhone;
    @InjectView(R.id.radio_pria)
    RadioButton radioPria;
    @InjectView(R.id.radio_wanita)
    RadioButton radioWanita;
    @InjectView(R.id.btn_register)
    Button btnRegister;
    @InjectView(R.id.card)
    CardView card;
    @InjectView(R.id.progress_signup)
    ProgressBar progressSignup;

    public ActivitySignUp() {
        super(R.layout.activity_sign_up);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupRadio();
        changeStatusBarColor(R.color.transparent);

        btnRegister.setOnClickListener(view -> signUp());

        enterAnim = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade_close);
        exitAnim = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade);


        exitAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                progressSignup.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        enterAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                progressSignup.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });


    }
    void promptEmpty(String input){
        new AlertDialog.Builder(thisContext())
                .setMessage("Isian kosong")
                .setMessage(String.format("Anda harus mengisi %s!",input))
                .setNegativeButton("Tutup",(dialogInterface, i) -> dialogInterface.dismiss())
                .show();
    }
    private void signUp() {
        if (!validate()) return;

        card.startAnimation(exitAnim);

        FormBody body = new FormBody.Builder()
                .add("form-login_type", "internal")
                .add("form-username", tUsername.getText().toString())
                .add("form-password", tPassword.getText().toString())
                .add("form-firstname", tFirsname.getText().toString())
                .add("form-lastname", tLastname.getText().toString())
                .add("form-phone", tPhone.getText().toString())
                .add("form-email", tEmail.getText().toString())
                .add("form-gender", (radioPria.isChecked() ? "Pria" : "Wanita"))
                .add("form-fcm_token", String.valueOf(FirebaseInstanceId.getInstance().getToken()))
                .build();
        Request request = new Request.Builder()
                .url(server("auth/signup"))
                .post(body)
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    RequestResult requestResult = Configs.mapper().readValue(string, RequestResult.class);
                    runOnUiThread(() ->
                            new AlertDialog.Builder(thisContext())
                                    .setTitle("Sukses")
                                    .setMessage("User berhasil di daftarkan! anda dapat login dengan username dan password yang anda daftarkan")
                                    .setPositiveButton("Selesai", (dialogInterface, i) -> {
                                        dialogInterface.dismiss();
                                        if (getIntent().getStringExtra("origin") == null || getIntent().getStringExtra("origin").equals("login")){
                                            finish();
                                        }else if (getIntent().getStringExtra("origin") != null && getIntent().getStringExtra("origin").equals("home")){
                                            Intent i2 = new Intent(thisContext(),ActivityLogin.class);
                                            startActivity(i2);
                                            finish();
                                        }
                                    }).show());
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() ->
                {
                    try{
                        new AlertDialog.Builder(thisContext())
                                .setTitle("Error")
                                .setMessage(msg)
                                .setNegativeButton("Close", (dialogInterface, i) -> dialogInterface.dismiss())
                                .show();
                    }catch(Exception exc){

                    }finally{

                    }
                });
                runOnUiThread(() -> card.startAnimation(enterAnim));
            }

            @Override
            public void Finish() {

            }
        });
    }

    private boolean validate() {
        if(tUsername.getText().toString().length() < 1){
            promptEmpty("Username");
            return false;
        }
        if(tPassword.getText().toString().length() < 1){
            promptEmpty("Password");
            return false;
        }
        if(tFirsname.getText().toString().length() < 1){
            promptEmpty("Nama Depan");
            return false;
        }
        if(tLastname.getText().toString().length() < 1){
            promptEmpty("Nama Belakang");
            return false;
        }
        if(tPhone.getText().toString().length() < 1){
            promptEmpty("No. Handphone");
            return false;
        }
        if(tEmail.getText().toString().length() < 1){
            promptEmpty("Email");
            return false;
        }
        return true;
    }

    private void setupRadio() {
        radioPria.setChecked(true);

    }
}
