package exairie.com.quranq

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import exairie.com.quranq.libs.CursorControl
import exairie.com.quranq.libs.DBHelper
import kotlinx.android.synthetic.main.activity_fahmil_report.*
import kotlinx.android.synthetic.main.view_fahmil_report.view.*
import java.util.*

class ActivityFahmilReport : ExtActivity(R.layout.activity_fahmil_report) {
    lateinit var adapter : RecyclerView.Adapter<RecyclerView.ViewHolder>
    override fun onDestroy() {
        db.close()
        cc.close()
        super.onDestroy()
    }
    lateinit var db : DBHelper
    lateinit var cc : CursorControl
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        setTitle("Laporan Quiz Fahmil Quran")

        db = DBHelper(thisContext())
        val query = "select session_start," +
                "        sum(case when answer_false is null then 1 else 0 end) as false," +
                "        (count(id) - sum(case when answer_false is null then 1 else 0 end)) as true," +
                "        count(id) as total," +
                "        (cast(count(id) as double) - sum(case when answer_false is null then 1 else 0 end))/count(id) * 100 as score" +
                "        from fahmil_result group by session_start"

        cc = CursorControl(db.readableDatabase.rawQuery(query, arrayOf()))

        adapter = object : RecyclerView.Adapter<RecyclerView.ViewHolder>(){
            override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
                cc.get(p1)

                val session_start = cc.getLong("session_start")
                val session_end = cc.getLong("session_start")

                val date_start = Date(session_start)
                val date_end = Date(session_end)

                p0.itemView.l_score.text = String.format("%.0f",cc.getDouble("score"))
                p0.itemView.l_quizayat.text = String.format("Quiz #%d",session_start)
                p0.itemView.l_quizdate.text = android.text.format.DateFormat.format("dd-MMM-yyyy", date_start)
                p0.itemView.l_quiztime.text = String.format("%s - %s",
                        android.text.format.DateFormat.format("HH:MM", date_start),
                        android.text.format.DateFormat.format("HH:MM", date_end))

                p0.itemView.setOnClickListener(View.OnClickListener {
                    val i = Intent(thisContext(), ActivityFahmilResult::class.java)
                    i.putExtra("session",session_start)
                    startActivity(i)
                })
            }

            override fun getItemCount(): Int = cc.count()

            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
                val v = LayoutInflater.from(p0.context)
                        .inflate(R.layout.view_fahmil_report,p0,false)
                return Holder(v)
            }

            inner class Holder(v : View) : RecyclerView.ViewHolder(v)
        }

        recycler_quizresult.adapter = adapter
        recycler_quizresult.layoutManager = LinearLayoutManager(thisContext())
    }
}
