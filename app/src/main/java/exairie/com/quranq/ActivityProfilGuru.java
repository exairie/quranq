package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.InjectView;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.models.Guru;

public class ActivityProfilGuru extends ExtActivity {
    @InjectView(R.id.t_namaguru)
    TextView tNamaguru;
    @InjectView(R.id.t_alamatguru)
    TextView tAlamatguru;
    @InjectView(R.id.t_teleponguru)
    TextView tTeleponguru;
    @InjectView(R.id.layout_phone)
    LinearLayout layoutPhone;

    public ActivityProfilGuru() {
        super(R.layout.activity_profil_guru);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        long id = i.getLongExtra("id", -1);

        if (id == -1) {
            finish();
            return;
        }

        displayOnView(id);

        layoutPhone.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + tTeleponguru.getText().toString()));
            startActivity(intent);
        });
    }

    private void displayOnView(long id) {
        DBHelper db = new DBHelper(thisContext());
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM guru WHERE id = ?", new String[]{String.valueOf(id)});

        if (c.getCount() < 1) {
            c.close();
            db.close();
            finish();
            return;
        }

        c.moveToFirst();
        Guru g = Guru.create(c);

        c.close();

        db.close();

        tNamaguru.setText(g.nama_guru);
        tAlamatguru.setText(g.address);
        tTeleponguru.setText(g.phone);

    }
}
