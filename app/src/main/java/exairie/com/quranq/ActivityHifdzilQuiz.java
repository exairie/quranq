package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.StringWithID;

public class ActivityHifdzilQuiz extends ExtActivity {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.l_surahname)
    TextView lSurahname;
    @InjectView(R.id.l_ayatno)
    TextView lAyatno;
    @InjectView(R.id.l_quizayat)
    TextView lQuestion;
    @InjectView(R.id.recycler_answers)
    RecyclerView recyclerAnswers;
    @InjectView(R.id.btn_next)
    Button btnNext;
    @InjectView(R.id.l_countsoal)
    TextView lCountsoal;
    @InjectView(R.id.progress_soal)
    ProgressBar progressSoal;
    @InjectView(R.id.l_question)
    TextView lQuizQuestion;
    @InjectView(R.id.cardView3)
    FrameLayout cardView3;
    private int quizType;
    private int selectionID;
    ArrayList<Ayat> ayatList;
    ArrayList<String> surahName;
    ArrayList<Ayat> questionIds;
    ArrayList<String> qsurahNames;
    ArrayList<Integer> result;
    int questionIndex = 0;
    int questionCount = 0;
    Random random;
    private boolean inverse;
    private String ayatInQuestion;
    private int ayatLength;
    private int cutPosition;
    private String ayatCutted;
    private int trueAnswer;
    private int falseAnswer;
    private AnswerAdapter adapter;

    Typeface tf;

    public ActivityHifdzilQuiz() {
        super(R.layout.activity_hifdzil_quiz, true);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);

        random = new Random(System.currentTimeMillis());
        ayatList = new ArrayList<>();
        surahName = new ArrayList<>();
        questionIds = new ArrayList<>();
        qsurahNames = new ArrayList<>();
        result = new ArrayList<>();
        Intent i = getIntent();

        adapter = new AnswerAdapter();
        recyclerAnswers.setAdapter(adapter);
        recyclerAnswers.setLayoutManager(new LinearLayoutManager(thisContext()));

        quizType = i.getIntExtra("mode", -1);
        if (quizType == -1) {
            finish();
            return;
        }

        selectionID = i.getIntExtra("id", -1);
        if ((quizType == ActivityHifdzilSelection.SELECTION_MODE_JUZ && selectionID == -1)
                || (quizType == ActivityHifdzilSelection.SELECTION_MODE_SURAT && selectionID == -1)) {
            finish();
            return;
        }
        if (tf == null) tf = Typeface.createFromAsset(getAssets(), "quranfontustmani.otf");

        lQuestion.setTypeface(tf);
        createQuestions();
    }

    private void createQuestions() {
        QuranDBHelper helper = new QuranDBHelper(thisContext());
        Cursor c;
        if (quizType == ActivityHifdzilSelection.SELECTION_MODE_JUZ) {
            c = helper.getReadableDatabase().rawQuery("SELECT *,b.surah_name FROM quran a, quran_name b " +
                    "WHERE a.juz = ? and a.surah_no = b.surah_no", new String[]{String.valueOf(selectionID)});
            if (c.getCount() < 1) {
                FirebaseAnalytics.getInstance(thisContext()).logEvent("cant read juz " + String.valueOf(selectionID), null);
                c.close();
                finish();
                return;
            }

            c.moveToFirst();
            while (!c.isAfterLast()) {
                ayatList.add(Ayat.createFromCursor(c));
                surahName.add(c.getString(c.getColumnIndexOrThrow("surah_name")));

                c.moveToNext();
            }
            c.close();
            int basen = (int) (((double) c.getCount()) * (2 / 3));
            int max = c.getCount();
            questionCount = random.nextInt(max - basen);
            questionCount = questionCount > 15 ? 15 : questionCount;
        } else if (quizType == ActivityHifdzilSelection.SELECTION_MODE_SURAT) {
            c = helper.getReadableDatabase().rawQuery("SELECT *,b.surah_name FROM quran a, quran_name b " +
                    "WHERE a.surah_no = ? and a.surah_no = b.surah_no", new String[]{String.valueOf(selectionID)});
            if (c.getCount() < 1) {
                FirebaseAnalytics.getInstance(thisContext()).logEvent("cant read juz " + String.valueOf(selectionID), null);
                c.close();
                finish();
                return;
            }

            c.moveToFirst();
            while (!c.isAfterLast()) {
                ayatList.add(Ayat.createFromCursor(c));
                surahName.add(c.getString(c.getColumnIndexOrThrow("surah_name")));

                c.moveToNext();
            }
            c.close();
            int basen = (int) (((double) c.getCount()) * (2 / 3));
            int max = c.getCount();
            questionCount = random.nextInt(max - basen);
            questionCount = questionCount > 15 ? 15 : questionCount;
        } else if (quizType == ActivityHifdzilSelection.SELECTION_MODE_ALL) {
            c = helper.getReadableDatabase().rawQuery("SELECT *,b.surah_name FROM quran a, quran_name b " +
                    "WHERE a.surah_no = b.surah_no", new String[]{});
            if (c.getCount() < 1) {
                FirebaseAnalytics.getInstance(thisContext()).logEvent("cant read juz " + String.valueOf(selectionID), null);
                c.close();
                finish();
                return;
            }

            c.moveToFirst();
            while (!c.isAfterLast()) {
                ayatList.add(Ayat.createFromCursor(c));
                surahName.add(c.getString(c.getColumnIndexOrThrow("surah_name")));

                c.moveToNext();
            }
            c.close();
            int basen = (int) (((double) c.getCount()) * (2 / 3));
            int max = c.getCount();
            questionCount = basen + random.nextInt(max - basen);
            questionCount = questionCount > 15 ? 15 : questionCount;
        }
        markQuestion();
    }

    void updateProgress() {
        lCountsoal.setText(String.format("%s/%s", questionIndex + 1, questionCount));
        progressSoal.setMax(questionCount);
        progressSoal.setProgress(questionIndex + 1);
    }

    void markQuestion() {
        Log.d("marking question", "Number " + questionIndex);
        inverse = random.nextInt() % 2 == 0;
        int i = random.nextInt(ayatList.size() - 1);
        Ayat ayat = ayatList.get(i);
        while (ayat.getAyat_id() == 0) {
            ayatList.remove(ayat);
            surahName.remove(surahName.get(i));
            i = random.nextInt(ayatList.size() - 1);
            ayat = ayatList.get(i);
        }
        questionIds.add(ayat);
        qsurahNames.add(surahName.get(i));

        ayatList.remove(ayat);
        surahName.remove(surahName.get(i));


        Ayat a = questionIds.get(questionIndex);
        ayatInQuestion = a.getArabic();

        ayatLength = ayatInQuestion.length() - 1;
        int ldown = (int) (((double) ayatLength) * 4.0 / 10.0);
        int lup = (int) (((double) ayatLength) * 6.0 / 10.0);
        cutPosition = ldown;

        showQuestion();
        updateProgress();
    }

    void showQuestion() {
        Ayat a = questionIds.get(questionIndex);
        lSurahname.setText(qsurahNames.get(questionIndex));
        lAyatno.setText(String.format("Ayat %d", a.getAyat_id()));

        if (inverse) {
            ayatCutted = ayatInQuestion.substring(cutPosition, ayatLength);
            lQuestion.setText(String.format("... %s", ayatCutted));
        } else {
            ayatCutted = ayatInQuestion.substring(cutPosition);
            lQuestion.setText(String.format("%s ...", ayatCutted));
        }

        lQuizQuestion.setText("Ayat tersebut valid jika digabung dengan");

        adapter.rebind();
    }

    void answerQuestion(long answer) {
        String astring;
        if (answer == 1) {
            result.add(1);
            trueAnswer++;
        } else {
            result.add(0);
            falseAnswer++;
        }
        Log.d("Question", String.format("Answer %d/%d : %s", questionIndex, questionCount, answer));
        questionIndex++;
        if (questionIndex >= questionCount) {
            showResult();
        } else {
            markQuestion();
        }

    }

    private void showResult() {
        ArrayList<String> qids = new ArrayList<>();
        Intent i = new Intent(thisContext(), ActivityHifdzilResult.class);
        i.putExtra("true", trueAnswer);
        i.putExtra("false", falseAnswer);
        i.putExtra("answers", result);
        for (Ayat a : questionIds) {
            qids.add(String.valueOf(a.getId()));
        }
        i.putExtra("surahs", qids);
        i.putExtra("transition", "trans");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            ActivityOptionsCompat optionsCompat = ActivityOptionsCompat.makeSceneTransitionAnimation(this, progressSoal, "trans");
            progressSoal.setTransitionName("trans");
            startActivity(i, optionsCompat.toBundle());
        } else {
            startActivity(i);
        }
        finish();
    }

    class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.Holder> {
        int length;
        List<StringWithID> data;

        public AnswerAdapter() {
            data = new ArrayList<>();
        }

        public void rebind() {
            length = ayatLength - (cutPosition - 1);
            data.clear();
            for (int i = 0; i < 3; i++) {
                if(ayatList.size() < 1){
                    continue;
                }
                String r = ayatList.get(random.nextInt(ayatList.size() - 1)).getArabic();
                data.add(new StringWithID(0, r.length() > cutPosition ? r.substring(0, cutPosition) : r));
            }

            data.add(new StringWithID(1, ayatInQuestion.replace(ayatCutted, "")));

            List<StringWithID> _scrambled = new ArrayList<>();

            while (data.size() > 0) {
                int r_id = data.size() > 1 ? random.nextInt(data.size() - 1) : 0;
                _scrambled.add(data.get(r_id));
                data.remove(r_id);

            }

            this.data = _scrambled;

            notifyDataSetChanged();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_answer, viewGroup, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            holder.lAnswer.setText(data.get(i).toString());
            holder.layoutAnswerBtn.setOnClickListener(view ->
            {
                answerQuestion(data.get(i).getId());
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        class Holder extends RecyclerView.ViewHolder {

            @InjectView(R.id.l_answer)
            TextView lAnswer;

            @InjectView(R.id.layout_answer_btn)
            LinearLayout layoutAnswerBtn;
            public Holder(View itemView) {
                super(itemView);
                if (tf == null)
                    tf = Typeface.createFromAsset(getAssets(), "quranfontustmani.otf");
                ButterKnife.inject(this, itemView);

                lAnswer.setTypeface(tf);
            }
        }
    }
}
