package exairie.com.quranq;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.webkit.WebView;

import butterknife.InjectView;

public class ActivityReadTajwid extends ExtActivity {
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.webview)
    WebView webview;

    public ActivityReadTajwid() {
        super(R.layout.activity_read_tajwid);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setExtActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        try {
            setTitle("Tajwid");
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        webview.loadUrl("file:///android_asset/webpages/tajwid.htm");
    }
}
