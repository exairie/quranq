package exairie.com.quranq;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.QuranBookmark;
import exairie.com.quranq.models.Surah;

public class FragmentQuranSurahList extends ExtFragment {
    private static final int ACT_READ_QURAN = 1;
    private static final int ACT_REPORT_ODOJ = 2;
    public static final int QURAN_MODE_ORDINARY = 0;
    public static final int QURAN_MODE_BOOKMARK = 1;
    public static final int QURAN_MODE_LASTREAD = 2;
    private static final int QURAN_MODE_DEFAULT = QURAN_MODE_ORDINARY;
    private int CURRENT_QURAN_MODE = -1;


    int colorWhite;
    int colorBlack;
    int colorGrey;
    int colorPrimaryDark;


    MenuItem searchMenuItem;
    SearchView searchView;

    @InjectView(R.id.bottom_layout)
    LinearLayout bottomLayout;
    @InjectView(R.id.progress_surahload)
    ProgressBar progressSurahload;


    SurahAdapter ordQuranAdapter;
    BookmarkAdapter bookmarkAdapter;
    LastReadAdapter lastReadAdapter;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.recycler_surah)
    RecyclerView recyclerSurah;
    @InjectView(R.id.fab_odoj)
    FloatingActionButton fabOdoj;
    @InjectView(R.id.layout_quran)
    ConstraintLayout layoutQuran;
    @InjectView(R.id.layout_bookmark)
    ConstraintLayout layoutBookmark;
    @InjectView(R.id.layout_lastread)
    ConstraintLayout layoutLastread;
    @InjectView(R.id.layout_srcayat)
    ConstraintLayout layoutSrcayat;
    private ActivityDashboard activityDashboard;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.inject(this,v);

        CURRENT_QURAN_MODE = QURAN_MODE_DEFAULT;
        setupEvents();

        colorWhite = ContextCompat.getColor(thisContext(), R.color.White);
        colorBlack = ContextCompat.getColor(thisContext(), R.color.Black);
        colorGrey = ContextCompat.getColor(thisContext(), R.color.grey_darken_4);
        colorPrimaryDark = ContextCompat.getColor(thisContext(), R.color.colorPrimaryDark);

        layoutSrcayat.setOnClickListener(view -> {
            startActivity(new Intent(thisContext(), ActivityJumptoAyat.class));
        });
        toolbar.setVisibility(View.GONE);
        activityDashboard = (ActivityDashboard)getActivity();
        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onStart() {
        super.onStart();

        applyQuranMode();
    }

    private void setupEvents() {
        View.OnClickListener bottomLayoutClickListener = (v) -> {
            clearQuranModeUI();
            if (v.getId() == layoutQuran.getId()) {
                CURRENT_QURAN_MODE = QURAN_MODE_ORDINARY;

            }
            if (v.getId() == layoutBookmark.getId()) {
                CURRENT_QURAN_MODE = QURAN_MODE_BOOKMARK;

            }
            if (v.getId() == layoutLastread.getId()) {
                CURRENT_QURAN_MODE = QURAN_MODE_LASTREAD;

            }
//            if(v.getId() == layout.getId()){
//                CURRENT_QURAN_MODE = QURAN_MODE_ORDINARY;
//                return;
//            }
            applyQuranMode();
        };
        layoutQuran.setOnClickListener(bottomLayoutClickListener);
        layoutBookmark.setOnClickListener(bottomLayoutClickListener);
        layoutLastread.setOnClickListener(bottomLayoutClickListener);
        fabOdoj.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityTrackODOJ.class);
            startActivityForResult(i, ACT_REPORT_ODOJ);
        });
    }

    void applyQuranMode() {
        CURRENT_QURAN_MODE = CURRENT_QURAN_MODE == -1 ? QURAN_MODE_DEFAULT : CURRENT_QURAN_MODE;
        clearQuranModeUI();
        switch (CURRENT_QURAN_MODE) {
            case QURAN_MODE_ORDINARY: {
                setupRecyclerQuranNormal();
            }
            break;
            case QURAN_MODE_BOOKMARK: {
                setupRecyclerQuranBookmark();
            }
            break;
            case QURAN_MODE_LASTREAD:{
                setupRecyclerQuranLastRead();
            }break;
        }
    }

    void clearQuranModeUI() {

        for (int i = 0; i < bottomLayout.getChildCount(); i++) {
            if (!(bottomLayout.getChildAt(i) instanceof ConstraintLayout)) continue;

            ConstraintLayout l = (ConstraintLayout) bottomLayout.getChildAt(i);
            l.setBackgroundColor(colorWhite);
            ((ImageView) l.getChildAt(0)).setColorFilter(colorBlack);
            ((TextView) l.getChildAt(1)).setTextColor(colorBlack);
        }

//        layoutQuran.setBackgroundColor(colorWhite);
//        ((ImageView) layoutQuran.getChildAt(0)).setColorFilter(colorGrey);
//        ((TextView) layoutQuran.getChildAt(1)).setTextColor(colorBlack);
    }

    void setupRecyclerQuranNormal() {
        if (ordQuranAdapter == null)
        {
            ordQuranAdapter = new SurahAdapter();
            recyclerSurah.setAdapter(ordQuranAdapter);
            recyclerSurah.setLayoutManager(new LinearLayoutManager(thisContext()));

            AsyncTask<Void, Void, Void> t = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try{
                        thisContext().runOnUiThread(()-> {
                            if (progressSurahload != null){
                                progressSurahload.setVisibility(View.VISIBLE);
                            }
                        });
                        ordQuranAdapter.load();
                        thisContext().runOnUiThread(() -> {
                            if (progressSurahload != null){
                                progressSurahload.setVisibility(View.GONE);
                            }
                        });
                    }catch(Exception exc){
                        exc.printStackTrace();
                    }finally{

                    }
                    return null;
                }
            };
            t.execute();

//        new Handler().post(() -> {
//            long t = System.currentTimeMillis();
//
//            long t2 = System.currentTimeMillis();
//            Log.d("LOAD", "setupRecyclerQuranNormal: "+(t2-t) + " SECS");
//        });
        }else{
            recyclerSurah.setAdapter(ordQuranAdapter);
        }

        layoutQuran.setBackgroundColor(colorPrimaryDark);
        ((ImageView) layoutQuran.getChildAt(0)).setColorFilter(colorWhite);
        ((TextView) layoutQuran.getChildAt(1)).setTextColor(colorWhite);
    }

    void setupRecyclerQuranBookmark() {
        if (bookmarkAdapter == null)
            bookmarkAdapter = new BookmarkAdapter();
        recyclerSurah.setAdapter(bookmarkAdapter);
        recyclerSurah.setLayoutManager(new LinearLayoutManager(thisContext()));
        long t = System.currentTimeMillis();
        bookmarkAdapter.load();
        long t2 = System.currentTimeMillis();
        Log.d("LOAD", "setupRecyclerQuranBookmark: " + (t2 - t) + " SECS");

        layoutBookmark.setBackgroundColor(colorPrimaryDark);
        ((ImageView) layoutBookmark.getChildAt(0)).setColorFilter(colorWhite);
        ((TextView) layoutBookmark.getChildAt(1)).setTextColor(colorWhite);
    }

    void setupRecyclerQuranLastRead() {
        if (lastReadAdapter == null)
        {
            lastReadAdapter = new LastReadAdapter();
            recyclerSurah.setAdapter(lastReadAdapter);
            recyclerSurah.setLayoutManager(new LinearLayoutManager(thisContext()));
            long t = System.currentTimeMillis();
            lastReadAdapter.load();
            long t2 = System.currentTimeMillis();
        }else
        {
            recyclerSurah.setAdapter(lastReadAdapter);
            lastReadAdapter.load();
        }

        layoutLastread.setBackgroundColor(colorPrimaryDark);
        ((ImageView) layoutLastread.getChildAt(0)).setColorFilter(colorWhite);
        ((TextView) layoutLastread.getChildAt(1)).setTextColor(colorWhite);
    }

    class SurahAdapter extends RecyclerView.Adapter<Holder> {
        List<Surah> data;
        List<Surah> data_filtered;
        String searchFilter = "";

        public void load() {
            QuranDBHelper db = new QuranDBHelper(thisContext());
            data = db.getSurahList();
            try {
                thisContext().runOnUiThread(this::notifyDataSetChanged);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        public void loadWithFilter(String filter){
            searchFilter = filter;
            thisContext().runOnUiThread(() -> progressSurahload.setVisibility(View.VISIBLE));
            if (filter.length() < 1){
                data_filtered = null;
                thisContext().runOnUiThread(() -> progressSurahload.setVisibility(View.GONE));
                thisContext().runOnUiThread(this::notifyDataSetChanged);
            }
            else{
                AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        data_filtered = new ArrayList<>();
                        QuranDBHelper db = new QuranDBHelper(thisContext());
                        String query = String.format("select a.*,b.indonesian,b.ayat_id,b.juz from quran_name a " +
                                "left join quran b on a.surah_no = b.surah_no " +
                                "where b.indonesian like '%%%s%%'",filter);

                        Cursor c = db.getReadableDatabase().rawQuery(query,new String[]{});
                        if (c.getCount() < 1) {
                            thisContext().runOnUiThread(() -> notifyDataSetChanged());
                            thisContext().runOnUiThread(() -> progressSurahload.setVisibility(View.GONE));
                            return null;
                        }

                        c.moveToFirst();
                        while (!c.isAfterLast()){
                            Surah s = Surah.createFromCursor(c);
                            s.setNum_of_ayat(c.getInt(8));
                            s.setMeaning(c.getString(7));
                            s.setJuzFrom(c.getInt(9));

                            data_filtered.add(s);
                            c.moveToNext();
                        }
                        c.close();
                        db.close();

                        thisContext().runOnUiThread(() -> {
                            try {
                                notifyDataSetChanged();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        thisContext().runOnUiThread(() -> {
                            try {
                                progressSurahload.setVisibility(View.GONE);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                        return null;
                    }
                };
                task.execute();
            }
        }
        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_quran_entry, parent, false);

            return new Holder(v, viewType);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            thisContext().runOnUiThread(() -> {
                try{
                    Surah data = data_filtered == null?this.data.get(position):this.data_filtered.get(position);
                    holder.lNo.setText(String.format("%d.", position + 1));
                    holder.lSurahName.setText(data.getSurah_name());

                    String meaning;
                    if (data_filtered != null){
                        meaning = data.getMeaning().replace(searchFilter, "<strong>"+searchFilter+"</strong>");
                    }else
                    {
                        meaning = data.getMeaning();
                    }

                    holder.lSurahMeaning.setText(Html.fromHtml(meaning));
                    holder.lSuratNumber.setText(String.valueOf(data.getSurah_no()));
                    holder.lAyatNumber.setText(String.valueOf(data.getNum_of_ayat()));

                    holder.cardLayout.setOnClickListener((v) -> {
                        Intent i = new Intent(thisContext(), ActivityReadQuran.class);
                        i.putExtra("id", data.getSurah_no());
                        i.putExtra("surah_name", data.getSurah_name());
                        if (data_filtered != null){
                            i.putExtra("jump_ayat", data.getNum_of_ayat());
                        }
                        startActivityForResult(i, ACT_READ_QURAN);
                    });

                    String juzInfo = data.getJuzTo() == data.getJuzFrom() ?
                            String.valueOf(data.getJuzFrom()) :
                            String.format("%d - %d", data.getJuzFrom(), data.getJuzTo());
                    holder.lJuzNumber.setText(juzInfo);
                }catch(Exception exc){
                    exc.printStackTrace();
                }
            });
        }

        @Override
        public int getItemCount() {
            return data_filtered == null?data.size():data_filtered.size();
        }

        public SurahAdapter() {
            data = new ArrayList<>();
        }


    }

    class BookmarkAdapter extends RecyclerView.Adapter<Holder> {
        List<QuranBookmark> data;

        public void load() {
            data = DBHelper.getQuranBookmark(thisContext());
            notifyDataSetChanged();
        }


        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_quran_entry, parent, false);

            return new Holder(v, viewType);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            QuranBookmark data = this.data.get(position);
            holder.lNo.setText(String.format("%d.", position + 1));
            holder.lSurahName.setText(data.getSurahInfo().getSurah_name());
            holder.lSurahMeaning.setText(data.getAyatInfo().getArabic().substring(0,data.getAyatInfo().getArabic().length() > 80?79:data.getAyatInfo().getArabic().length()-1));
            holder.lSuratNumber.setText(String.valueOf(data.getSurahInfo().getSurah_no()));
            holder.lAyatNumber.setText(String.valueOf(data.getAyatInfo().getAyat_id()));
            holder.lJuzNumber.setText(String.valueOf(data.getAyatInfo().getJuz()));
            holder.cardLayout.setOnClickListener((v) -> {
                Intent i = new Intent(thisContext(), ActivityReadQuran.class);
                i.putExtra("id", data.getSurahInfo().getSurah_no());
                i.putExtra("surah_name", data.getSurahInfo().getSurah_name());
                i.putExtra("jump", data.getAyatInfo().getId());
                startActivityForResult(i, ACT_READ_QURAN);
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public BookmarkAdapter() {
            data = new ArrayList<>();
        }


    }

    class LastReadAdapter extends RecyclerView.Adapter<Holder> {
        List<QuranBookmark> data;

        public void load() {
            data.clear();
            Option option = Configs.getConfiguration(Option.KEY_LAST_READ);
            if (option == null) return;

//            QuranDBHelper db = new QuranDBHelper(thisContext());
            Ayat ayatInfo = QuranDBHelper.getAyat(thisContext(), Integer.parseInt(option.getValue()));
            if(ayatInfo == null) return;
            Surah surahInfo = QuranDBHelper.getSurahInfo(thisContext(),ayatInfo.getSurah_no());

            QuranBookmark bookmark = new QuranBookmark();
            bookmark.setAyat_id(ayatInfo.getId());
            bookmark.setSurahInfo(surahInfo);
            bookmark.setAyatInfo(ayatInfo);
            data.add(bookmark);
            thisContext().runOnUiThread(() -> notifyDataSetChanged());
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.view_quran_entry, parent, false);

            return new Holder(v, viewType);
        }

        @Override
        public void onBindViewHolder(Holder holder, int position) {
            thisContext().runOnUiThread(() -> {
                QuranBookmark data = this.data.get(position);
                holder.lNo.setText(String.format("%d.", position + 1));
                holder.lSurahName.setText(data.getSurahInfo().getSurah_name());
                holder.lSurahMeaning.setText(data.getAyatInfo().getArabic().substring(0,data.getAyatInfo().getArabic().length() > 80?79:data.getAyatInfo().getArabic().length()-1));
                holder.lSuratNumber.setText(String.valueOf(data.getSurahInfo().getSurah_no()));
                holder.lAyatNumber.setText(String.valueOf(data.getAyatInfo().getAyat_id()));
                holder.lJuzNumber.setText(String.valueOf(data.getAyatInfo().getJuz()));
                holder.cardLayout.setOnClickListener((v) -> {
                    Intent i = new Intent(thisContext(), ActivityReadQuran.class);
                    i.putExtra("id", data.getSurahInfo().getSurah_no());
                    i.putExtra("surah_name", data.getSurahInfo().getSurah_name());
                    i.putExtra("jump", data.getAyatInfo().getId());
                    startActivityForResult(i, ACT_READ_QURAN);
                });
            });
        }

        @Override
        public int getItemCount() {
            return data.size();
        }

        public LastReadAdapter() {
            data = new ArrayList<>();
        }


    }

    class Holder extends RecyclerView.ViewHolder {
        @InjectView(R.id.l_no)
        TextView lNo;
        @InjectView(R.id.l_surah_name)
        TextView lSurahName;
        @InjectView(R.id.l_surah_meaning)
        TextView lSurahMeaning;
        @InjectView(R.id.l_surat)
        TextView lSurat;
        @InjectView(R.id.l_surat_no)
        TextView lSuratNumber;
        @InjectView(R.id.l_juz)
        TextView lJuz;
        @InjectView(R.id.l_juz_number)
        TextView lJuzNumber;
        @InjectView(R.id.l_ayat)
        TextView lAyat;
        @InjectView(R.id.l_ayat_number)
        TextView lAyatNumber;
        @InjectView(R.id.layout)
        ConstraintLayout layout;
        @InjectView(R.id.card_layout)
        CardView cardLayout;

        public Holder(View itemView, int vType) {
            super(itemView);
            ButterKnife.inject(this, itemView);
            Log.d("HOLDER", "Holder: ");
        }
    }
}
