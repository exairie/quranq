package exairie.com.quranq

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.*
import exairie.com.quranq.libs.CursorControl
import exairie.com.quranq.libs.DBHelper
import kotlinx.android.synthetic.main.activity_fahmil_result.*
import kotlinx.android.synthetic.main.view_review_fahmil.view.*

class ActivityFahmilResult : ExtActivity(R.layout.activity_fahmil_result) {
    lateinit var recyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>
    lateinit var db : DBHelper
    lateinit var cc : CursorControl
    var session_id : Long = 0L

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        session_id = intent.getLongExtra("session",-1)


        if (session_id == -1L) {
            finish()
            return
        }
        setTitle(String.format("Quiz #%d",session_id))
        setupData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val m = menu!!.add("Tutup")
        m.setOnMenuItemClickListener(MenuItem.OnMenuItemClickListener {
            finish()
            true
        })
        return true
    }
    override fun onDestroy() {
        super.onDestroy()
        if (db != null)
            db.close()
        cc.close()
    }
    fun setupData(){
        db = DBHelper(thisContext())



        var allQuestion : Double= 0.0
        var trueAnswer : Double = 0.0

        cc = CursorControl(db.readableDatabase.rawQuery("SELECT * FROM fahmil_result where session_start = ?",arrayOf<String>(session_id.toString())))
        if(cc.count() < 1) return;

        cc.iterate { c ->
            allQuestion++
            if (c.isNull(c.getColumnIndexOrThrow("answer_false"))) trueAnswer++
        }

        l_result_score.text = String.format("%.0f", ((trueAnswer/allQuestion * 100).toDouble()))

        recyclerAdapter = object : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            inner class Holder(v : View) : RecyclerView.ViewHolder(v){

            }
            override fun onCreateViewHolder(p0: ViewGroup, p1: Int): RecyclerView.ViewHolder {
                val v = LayoutInflater.from(p0.context)
                        .inflate(R.layout.view_review_fahmil,p0,false)
                return Holder(v)

            }

            override fun onBindViewHolder(p0: RecyclerView.ViewHolder, p1: Int) {
                cc.get(p1)
                p0.itemView.l_quizayat.text = cc.getString("question")
                if (!cc.isNull("answer_true")){
                    p0.itemView.l_trueanswer.text = cc.getString("answer_true")
                }
                if (!cc.isNull("answer_false")){
                    p0.itemView.l_falseanswer.text = cc.getString("answer_false")
                    p0.itemView.layout_truefalse.setBackgroundColor(resources.getColor(R.color.red_darken_3))
                    p0.itemView.img_check_cross.setImageResource(R.drawable.ic_close_white_48dp)
                }else{
                    p0.itemView.layout_falseanswer.visibility = View.GONE
                    p0.itemView.layout_truefalse.setBackgroundColor(resources.getColor(R.color.green_darken_3))
                    p0.itemView.img_check_cross.setImageResource(R.drawable.ic_check_white_48dp)
                }

            }

            override fun getItemCount(): Int = cc.count()

        }
        recycler_review.isNestedScrollingEnabled = false
        recycler_review.adapter = recyclerAdapter
        recycler_review.layoutManager = LinearLayoutManager(thisContext())
    }
}
