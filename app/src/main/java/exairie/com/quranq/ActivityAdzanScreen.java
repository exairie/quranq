package exairie.com.quranq;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.widget.TextView;

import java.util.List;
import java.util.Random;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.Surah;

public class ActivityAdzanScreen extends ExtActivity {
    public final static String KEY_ADZAN_LABEL = "adzanlabel";
    public final static String KEY_ADZAN_TIME = "adzantime";
    public final static String KEY_ADZAN_LOCATION = "adzanloc";
    public final static String KEY_ADZAN_ID = "adzanid";
    public final static String KEY_ADZAN_MILIS = "adzantmilis";
    @InjectView(R.id.l_time)
    TextView lTime;
    @InjectView(R.id.l_adzan_label)
    TextView lAdzanLabel;
    @InjectView(R.id.l_adzan_location)
    TextView lAdzanLocation;
    @InjectView(R.id.fab_mute)
    FloatingActionButton fabMute;
    @InjectView(R.id.l_adzan_ayat)
    TextView lAdzanAyat;
    int adzanID;
    int adzanOptions;
    MediaPlayer adzanPlayer;

    public ActivityAdzanScreen() {
        super(R.layout.activity_adzan_screen);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();

        adzanID = i.getIntExtra(KEY_ADZAN_ID,0);
        lAdzanLabel.setText(String.format("%s",i.getStringExtra(KEY_ADZAN_LABEL)));
        lTime.setText(i.getStringExtra(KEY_ADZAN_TIME));
        lAdzanLocation.setText(i.getStringExtra(KEY_ADZAN_LOCATION));

        Option option = Configs.getConfiguration("adzan_" + adzanID);

        if (option != null){
            try {
                adzanOptions = Integer.parseInt(option.getValue());
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        QuranDBHelper db = new QuranDBHelper(this);
        List<Ayat> adzanAyat = db.getAyatFiltered("shalat");
        Log.d("ADZANAYAT", "NUM : " + adzanAyat.size());
        if (adzanAyat.size() > 0) {
            showAdzanRandomAyat(adzanAyat);
        }

        db.close();

        fabMute.setOnClickListener(view -> {
            mute();
        });


    }

    @Override
    protected void onStart() {
        super.onStart();

        setNotifications();

    }

    @Override
    protected void onDestroy() {
        if (adzanPlayer != null){
            adzanPlayer.stop();
            adzanPlayer.release();
        }
        super.onDestroy();
    }

    private void setNotifications() {
        if ((adzanOptions & ActivitySettings.FLAG_ADZAN) == ActivitySettings.FLAG_ADZAN){
            adzanPlayer = MediaPlayer.create(thisContext(),R.raw.adzan);
            adzanPlayer.start();
        }
        if ((adzanOptions & ActivitySettings.FLAG_VIBRATE) == ActivitySettings.FLAG_VIBRATE){
            Vibrator vibrator = (Vibrator)getSystemService(VIBRATOR_SERVICE);
            vibrator.vibrate(new long[]{0,500,500,500,500,500,500,500,1000},-1);
        }

    }

    void showAdzanRandomAyat(List<Ayat> ayatList) {
        Random r = new Random(System.currentTimeMillis());
        int random = r.nextInt(ayatList.size() - 1);
        Ayat randomAyat = ayatList.get(random);
        Surah s = QuranDBHelper.getSurahInfo(this,randomAyat.getSurah_no());
        lAdzanAyat.setText(String.format("\"%s\" - %s : %d",randomAyat.getIndonesian(),s.getSurah_name(),randomAyat.getAyat_id()));
    }
    void mute(){
        finish();
    }
}
