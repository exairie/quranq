package exairie.com.quranq

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup


/**
 * A simple [Fragment] subclass.
 * Activities that contain this playerFragment must implement the
 * [FragmentOdoj.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentOdoj.newInstance] factory method to
 * create an instance of this playerFragment.
 */
class FragmentOdoj : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            mParam1 = arguments?.getString(ARG_PARAM1) ?: ""
            mParam2 = arguments?.getString(ARG_PARAM2) ?: ""
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this playerFragment
        return inflater!!.inflate(R.layout.fragment_odoj, container, false)
    }

    companion object {
        // TODO: Rename parameter arguments, choose names that match
        // the playerFragment initialization parameters, e.g. ARG_ITEM_NUMBER
        private val ARG_PARAM1 = "param1"
        private val ARG_PARAM2 = "param2"

        /**
         * Use this factory method to create a new instance of
         * this playerFragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of playerFragment FragmentOdoj.
         */
        // TODO: Rename and change types and number of parameters
        fun newInstance(param1: String, param2: String): FragmentOdoj {
            val fragment = FragmentOdoj()
            val args = Bundle()
            args.putString(ARG_PARAM1, param1)
            args.putString(ARG_PARAM2, param2)
            fragment.arguments = args
            return fragment
        }
    }


}// Required empty public constructor
