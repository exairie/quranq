package exairie.com.quranq;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.ViewPagerAdapter;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.UserLogin;
import exairie.com.quranq.services.AdzanSettingsUpdater;
import exairie.com.quranq.services.ODOJSynchronizer;
import exairie.com.quranq.services.PrayNotifier;
import exairie.com.quranq.services.PrayTimeService;
import exairie.com.quranq.services.PrayTracker;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityDashboard extends ExtActivity implements FragmentDashboardContent.OnFragmentInteractionListener {
    public static String[] string_hari = new String[]{"Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"};
    public static String[] string_bulan = new String[]{"Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"};
    public static final String LOCATION_CHANGED = "exairie.com.quranq.LOCATION_CHANGE";
    public double timezone = 7;
    public double latitude = -6.1745;
    public double longitude = 106.8227;

    private static final int ACT_QURANQ = 1;
    private static final int ACT_HADIST = 5;
    private static final int ACT_KAJIAN = 6;

    public String cityname;

    private static final int PERMISSION_LOCATION = 100;
    @InjectView(R.id.viewpager)
    ViewPager viewpager;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.tab)
    TabLayout tab;
    @InjectView(R.id.nav_view)
    NavigationView navView;
    @InjectView(R.id.drawer)
    DrawerLayout drawer;
    FragmentDashboardContent dashboard;

    BroadcastReceiver odojBCReceiver;
    @InjectView(R.id.laznas_logo)
    ImageView laznasLogo;

    public ActivityDashboard() {
        super(R.layout.activity_dashboard, true);
    }

    public void ChangeTitle(String title) {
        if (toolbar != null) toolbar.setTitle(title);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Option firstload = Configs.getConfiguration("firstload");
        if (firstload == null) {
            Intent i = new Intent(thisContext(), ActivitySlider.class);
            startActivity(i);
            finish();
            return;
        }


        setExtActionBar(toolbar);

        ActionBar actionBar = getSupportActionBar();
        try {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);

            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_open, R.string.drawer_closed);
            toggle.setDrawerIndicatorEnabled(true);
            drawer.addDrawerListener(toggle);

            toggle.syncState();

        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.setElevation(0);
        }
        setupNavListener();
        setupNavViews();

        setupViewPage();


//        for (int position = 0; position < eventAdapter.mFragmentTitleList.size(); position++) {
//            LinearLayout l = (LinearLayout) LayoutInflater.from(thisContext()).inflate(R.layout.header_label, null);
//            TextView t = (TextView) l.findViewById(R.id.tabContent);
//            t.setText(eventAdapter.mFragmentTitleList.get(position));
//            t.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.quran, 0, 0);
//            tab.getTabAt(position).setCustomView(l);
//        }

        Log.d("FIREBASE ID", "TOKEN : " + FirebaseInstanceId.getInstance().getToken());

        setTitle("QuranQu - BaitulQuran");

        sendTokens();
        setupLocation();
        checkLogin();
        installAd();
        Intent i = new Intent(thisContext(), ODOJSynchronizer.class);
        startService(i);

        laznasLogo.setOnClickListener(view -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(Configs.BQ_LAZ_URL));
            startActivity(browserIntent);
        });

    }

    private void installAd() {
        InterstitialAd intAd = new InterstitialAd(this);
        AdRequest request = new AdRequest.Builder()
                .addTestDevice("EB1DBC5F40E284EB568134257ACFC0EE")
                .addTestDevice("116C295249E121532EE8532440F7B5BD")
                .addTestDevice("116C295249E121532EE8532440F7B5BD")
                .build();
        // set the adUnitId (defined in values/strings.xml)
        intAd.setAdUnitId(getString(R.string.ad_unit_inter_dashboard));
        intAd.loadAd(request);
        intAd.setAdListener(new AdListener() {
            @Override
            public void onAdClosed() {
                super.onAdClosed();
                Log.d("AD", "Insterstitial Closed");
            }

            @Override
            public void onAdLoaded() {
                super.onAdLoaded();
                intAd.show();
            }

        });
    }

    private void setupNavViews() {
        View header = navView.getHeaderView(0);
        BluetoothAdapter myDevice = BluetoothAdapter.getDefaultAdapter();

        String deviceName = myDevice == null ? "n/a" : myDevice.getName();

        TextView device = (TextView) header.findViewById(R.id.l_devicename);
        TextView hari = (TextView) header.findViewById(R.id.l_hari);
        TextView tanggal = (TextView) header.findViewById(R.id.l_tanggal);
        TextView tanggalHijri = (TextView) header.findViewById(R.id.l_tanggal_hijri);

        Calendar c = Calendar.getInstance();

        device.setText(deviceName);
        hari.setText(string_hari[c.get(Calendar.DAY_OF_WEEK) - 1]);

        tanggal.setText(String.format("%d %s %d", c.get(Calendar.DAY_OF_MONTH), string_bulan[c.get(Calendar.MONTH)], c.get(Calendar.YEAR)));

        UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
        String hijridate = String.format("%s %s %s",
                String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH)),
                ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH),
                String.valueOf(ummalquraCalendar.get(Calendar.YEAR)));

        tanggalHijri.setText(hijridate);
    }

    private void setupViewPage() {
        dashboard = new FragmentDashboardContent();
        Bundle dashboardBundle = new Bundle();
        if (getIntent().getBooleanExtra("loginstatus", false)) {
            dashboardBundle.putBoolean("loginstatus", getIntent().getBooleanExtra("loginstatus", false));
        }
        dashboard.setArguments(dashboardBundle);

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(dashboard, "Dashboard");

        Fragment qlist = new FragmentQuranSurahList();
        Bundle bundle = new Bundle();
        bundle.putInt("view", R.layout.activity_quran_surah_list);
        qlist.setArguments(bundle);

        adapter.addFragment(qlist, "Quran");


        adapter.addFragment(new FragmentReadHadist(), "Hadist");

        viewpager.setAdapter(adapter);


        tab.setupWithViewPager(viewpager);
    }

    private void checkLogin() {
        applyLoginInformations();
//        AsyncTask<Void, Void, Void> loginChecker = new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                UserLogin login = Configs.getLoginInfo();
//                if (login == null) {
//                    runOnUiThread(() -> {
//                        new AlertDialog.Builder(thisContext())
//                                .setTitle("Anda belum login")
//                                .setMessage("Anda menggunakan aplikasi tanpa login. Apakah anda ingin login?")
//                                .setPositiveButton("Login", (dialogInterface, i) -> {
//                                    dialogInterface.dismiss();
//                                    Intent i2 = new Intent(thisContext(), ActivityLogin.class);
//                                    startActivity(i2);
//                                    finish();
//                                })
//                                .setNegativeButton("Tidak perlu", (dialogInterface, i) -> dialogInterface.dismiss())
//                                .setNeutralButton("Daftarkan saya", (dialogInterface, i) -> {
//                                    dialogInterface.dismiss();
//                                    Intent i2 = new Intent(thisContext(), ActivitySignUp.class);
//                                    i2.putExtra("origin", "home");
//                                    startActivity(i2);
//                                }).show();
//                    });
//                } else {
//
//                }
//                return null;
//            }
//        };
//        loginChecker.execute();
    }

    private void applyLoginInformations() {
        UserLogin login = Configs.getLoginInfo();
        dashboard.setupUserInfo(login);

        TextView navuser = (TextView) navView.getHeaderView(0).findViewById(R.id.l_devicename);

        navuser.setText(login.username);
    }

    void setupNavListener() {
        UserLogin login = Configs.getLoginInfo();
        if (login == null) {
            navView.getMenu().findItem(R.id.mn_logout).setVisible(false);
        }

        ImageView profileView = (ImageView) navView.getHeaderView(0).findViewById(R.id.l_profile);
        profileView.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivityProfileInfo.class);
            startActivity(i);
        });
        navView.setNavigationItemSelectedListener(menuItem -> {
            switch (menuItem.getItemId()) {
                case R.id.mn_about: {
                    Intent i = new Intent(this, ActivityAbout.class);
                    startActivity(i);
                }
                break;
//                case R.id.mn_bantuan: {
//                    Intent i = new Intent(this, ActivityLogin.class);
//                    startActivity(i);
//                }
//                break;
                case R.id.mn_logout: {
                    new AlertDialog.Builder(thisContext())
                            .setTitle("Konfirmasi")
                            .setMessage("Anda yakin ingin logout?")
                            .setPositiveButton("Logout", (dialogInterface, i) -> {
                                Configs.deleteConfiguration("login_token");
                                Configs.deleteConfiguration("login_info");
                                Intent i1 = new Intent(thisContext(), ActivityLogin.class);
                                i1.putExtra("logout", "logout");
                                startActivity(i1);

                                finish();
                            })
                            .setNegativeButton("Batal", (dialogInterface, i) -> dialogInterface.dismiss())
                            .show();
                }
                break;
                case R.id.mn_quranq: {
                    startActivityForResult(new Intent(thisContext(), ActivityQuranSurahList.class), ACT_QURANQ);
                }
                break;
                case R.id.mn_bookmark: {
                    Intent i = new Intent(this, ActivityQuranSurahList.class);
                    i.putExtra("mode", ActivityQuranSurahList.QURAN_MODE_BOOKMARK);
                    startActivityForResult(i, ACT_QURANQ);
                }
                break;
                case R.id.mn_bacaan_terakhir: {
                    Intent i = new Intent(this, ActivityQuranSurahList.class);
                    i.putExtra("mode", ActivityQuranSurahList.QURAN_MODE_LASTREAD);
                    startActivityForResult(i, ACT_QURANQ);
                }
                break;
                case R.id.mn_hadits_q: {
                    startActivityForResult(new Intent(this, ActivityHadistList.class), ACT_HADIST);
                }
                break;
                case R.id.mn_kajian: {
                    startActivityForResult(new Intent(this, ActivityNotification.class), ACT_KAJIAN);
                }
                break;
                case R.id.mn_cari_ayat: {
                    Intent i = new Intent(this, ActivityJumptoAyat.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_tajwid: {
                    Intent i = new Intent(this, ActivityReadTajwid.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_settings: {
                    Intent i = new Intent(thisContext(), ActivitySettings.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_requests: {
                    Intent i = new Intent(thisContext(), ActivityVideoRequestList.class);
                    startActivity(i);
                }
                break;
                case R.id.mn_myevents: {
                    Intent i = new Intent(thisContext(), ActivityMyEvent.class);
                    startActivity(i);
                }
            }
            return true;
        });
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_LOCATION: {
                if (grantResults.length >= 2 && grantResults[0] == PackageManager.PERMISSION_GRANTED
                        && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    setupLocation();
                }
            }
        }
    }

    private void setupLocation() {
        Log.d("SETUPLOC", "setupLocation: false");
        LocationListener l = new LocationListener() {
            @Override
            public void onLocationChanged(final Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                //Toast.makeText(thisContext(), "Location changed", Toast.LENGTH_LONG).show();

                Option lastlat = new Option("last_latitude", String.valueOf(latitude));
                Option lastlong = new Option("last_longitude", String.valueOf(longitude));

                Configs.setConfiguration(lastlat);
                Configs.setConfiguration(lastlong);

                //setupPrayTimeInfo();
                Intent i = new Intent(thisContext(), PrayTracker.class);
                startService(i);
                sendBroadcast(new Intent(LOCATION_CHANGED));

                AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        Geocoder geocoder = new Geocoder(thisContext(), Locale.getDefault());

                        try {
                            List<Address> addresses = geocoder.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                            if (addresses.size() > 0) {
                                String city;
                                if (addresses.get(0).getLocality() != null) {
                                    city = addresses.get(0).getLocality();
                                } else if (addresses.get(0).getAdminArea() != null) {
                                    city = addresses.get(0).getAdminArea();
                                } else {
                                    city = String.format("%.4f, %.4f", location.getLatitude(), location.getLongitude());
                                }
                                cityname = city;
                                //runOnUiThread(() -> lCityname.setText(city));

                                Option lastpos = new Option("last_loc", city);
                                Configs.setConfiguration(lastpos);
                            }
                        } catch (Exception e) {
                            String city = String.format("%.4f, %.4f", location.getLatitude(), location.getLongitude());

                            Option lastpos = new Option("last_loc", city);
                            Configs.setConfiguration(lastpos);
                            cityname = city;
                            //runOnUiThread(() -> lCityname.setText(city));
                            e.printStackTrace();
                        } finally {
                        }
                        return null;
                    }
                };
                task.execute();
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {

            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };
        LocationManager manager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (manager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("PERMISSION 217", "setupLocation: false");
                setupPermissions();
                return;
            }
            manager.requestSingleUpdate(LocationManager.GPS_PROVIDER, l, null);
        }
        if (manager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Log.d("PERMISSION 224", "setupLocation: false");
                setupPermissions();
                return;
            }
            manager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, l, null);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        ActivityNotification.Companion.checkIncomingNotifications(thisContext());
        Intent i = new Intent(getApplicationContext(), AdzanSettingsUpdater.class);
        i.putExtra("type", "download");
        startService(i);
    }

    private void setupPermissions() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            String[] req = new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};
            ActivityCompat.requestPermissions(this, req, PERMISSION_LOCATION);
        }
    }

    private void sendTokens() {
        Option option = Configs.getConfiguration("firebase_token");
        Option lastlat = Configs.getConfiguration("last_latitude");
        Option lastlong = Configs.getConfiguration("last_longitude");
        UserLogin login = Configs.getLoginInfo();
        if (option == null) return;
        String token = option.getValue();
        FormBody.Builder body = new FormBody.Builder()
                .add("token", token);

        if (login != null) {
            body.add("user_id", login.id);
        }

        if (lastlat != null) {
            body.add("latitude", lastlat.getValue());
        }
        if (lastlong != null) {
            body.add("longitude", lastlong.getValue());
        }

        Request request = new Request.Builder()
                .url(server("api/token"))
                .post(body.build())
                .build();
        new OkHttpClient().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.d("TokenRefreshQuery", response.body().string());
            }
        });
    }


}
