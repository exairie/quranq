package exairie.com.quranq;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.CursorControl;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.models.Application;
import exairie.com.quranq.models.QoriVid;
import exairie.com.quranq.services.QoriVidLoader;

public class ActivityQoriList extends ExtActivity {

    List<QoriVid> data;
    @InjectView(R.id.toolbar)
    Toolbar toolbar;

    //RecyclerView recyclerQori;


    @InjectView(R.id.recycler_qori)
    GridView recyclerQori;


    QoriGridAdaper adapter;
    @InjectView(R.id.overlay)
    View overlay;
    @InjectView(R.id.card_vidpreview)
    CardView cardVidpreview;
    @InjectView(R.id.tv_video)
    FrameLayout tvVideo;
    @InjectView(R.id.btn_play)
    ImageView btnPlay;
    @InjectView(R.id.seekbar)
    SeekBar seekbar;
    @InjectView(R.id.layout_filter_all)
    LinearLayout layoutFilterAll;
    @InjectView(R.id.layout_filter_favorite)
    LinearLayout layoutFilterFavorite;
    @InjectView(R.id.layout_filter_recent)
    LinearLayout layoutFilterRecent;
    @InjectView(R.id.layout_bottombar)
    LinearLayout layoutBottombar;
    @InjectView(R.id.heading)
    LinearLayout heading;
    //    @InjectView(R.id.spinner_category)
//    Spinner spinnerCategory;
    @InjectView(R.id.l_title)
    TextView lTitle;
    @InjectView(R.id.l_reciter)
    TextView lReciter;
    @InjectView(R.id.img_extra)
    ImageView imgExtra;
    @InjectView(R.id._circlebg)
    ImageView Circlebg;
    @InjectView(R.id.l_qori_title)
    TextView lQoriTitle;
    @InjectView(R.id.layout_request)
    LinearLayout layoutRequest;

    Animation fadeAnimOverlay, fadeAnimCard;
    public static final String APIKEY = "AIzaSyDs6e6IgY-QOMulK4f_eroVqVWloEdWfJ4";
    @InjectView(R.id.ad_banner)
    AdView adBanner;
    private int ERROR_YOUTUBE = 1;

    ArrayAdapter<String> categoryAdapter;
    BroadcastReceiver receiver;
    YouTubePlayerSupportFragment playerFragment;
    YouTubePlayer currentPlayer;
    private boolean can_make_request = false;
    private BroadcastReceiver remoteReceiver;

    public ActivityQoriList() {
        super(R.layout.activity_qori_list, true);
    }

    void initializeAllowRequest() {
        FirebaseRemoteConfig cfg = FirebaseRemoteConfig.getInstance();
        remoteReceiver = new BroadcastReceiver() {
            public void onReceive(Context p0, Intent p1) {
                can_make_request = cfg.getBoolean("video_request_enabled");
                Log.d("Allow Request", "Updated :" + can_make_request);

            }
        };
        can_make_request = cfg.getBoolean("video_request_enabled");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initializeAllowRequest();
        Application.fetchRemoteConfig();
//        Picasso.with(thisContext()).setLoggingEnabled(true);
        setExtActionBar(toolbar);
        Intent i = getIntent();
        if (i.getStringExtra("category") == null) {
            finish();
            return;
        }

        setupData();
        setTitle(i.getStringExtra("category"));
        lQoriTitle.setText(i.getStringExtra("category"));
        hasBackButton(true);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                adapter.refresh();
            }
        };

        startService(new Intent(thisContext(), QoriVidLoader.class));

        fadeAnimOverlay = AnimationUtils.loadAnimation(this, R.anim.alpha);
        fadeAnimCard = AnimationUtils.loadAnimation(this, R.anim.alpha);

        fadeAnimCard.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cardVidpreview.setVisibility(View.VISIBLE);

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeAnimOverlay.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                overlay.setVisibility(View.VISIBLE);
                cardVidpreview.startAnimation(fadeAnimCard);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        overlay.setOnClickListener(view -> StopVideo());

        layoutFilterAll.setOnClickListener((v) -> {
            cleanSelection();
            setSelection((short) 1);
        });
        layoutFilterFavorite.setOnClickListener((v) -> {
            cleanSelection();
            setSelection((short) 2);
        });
        layoutFilterRecent.setOnClickListener((v) -> {
            cleanSelection();
            setSelection((short) 3);
        });

        layoutRequest.setOnClickListener(view -> {
            if (!can_make_request) {
                new AlertDialog.Builder(thisContext())
                        .setTitle("Mohon maaf")
                        .setMessage("Saat ini fitur request video Tilawah sedang di nonaktifkan\nSilahkan cek beberapa hari lagi dan pantau terus Social Media kami!")
                        .setNegativeButton("Close", (dialogInterface, i1) -> {
                            dialogInterface.dismiss();
                        }).show();
                return;
            }
            AlertDialog dialog = new AlertDialog.Builder(thisContext())
                    .setView(getLayoutInflater().inflate(R.layout.view_dialog_request, null))
                    .show();
            Button cancelBtn = (Button) dialog.findViewById(R.id.btn_cancel);
            Button requestBtn = (Button) dialog.findViewById(R.id.btn_request);
            TextView coinCost = (TextView) dialog.findViewById(R.id.l_coin_cost);

            if (cancelBtn != null) {
                cancelBtn.setOnClickListener(view1 -> {
                    dialog.dismiss();
                });
            }

            if (requestBtn != null) {
                requestBtn.setOnClickListener(view1 -> {
                    Intent i2 = new Intent(thisContext(), ActivityVideoRequest.class);
                    startActivity(i2);
                    dialog.dismiss();
                });
            }

            if (coinCost != null) {
                coinCost.setText(FirebaseRemoteConfig.getInstance().getString("cost_coin_request"));
            }


        });


    }

    void setupData() {
        categoryAdapter = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
        DBHelper db = new DBHelper(thisContext());
        Cursor c = db.getReadableDatabase().rawQuery("SELECT DISTINCT cat from qori order by reciter asc", new String[]{});
        categoryAdapter.add("Semua");
        if (c.getCount() > 0) {
            c.moveToFirst();
            while (!c.isAfterLast()) {
                categoryAdapter.add(c.getString(0));
                c.moveToNext();
            }
        }

//        spinnerCategory.setAdapter(categoryAdapter);
//        spinnerCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                String category = (String) adapterView.getSelectedItem();
//                if (category.toLowerCase().equals("semua"))
//                    adapter.updateFilter(adapter.search, "");
//                else
//                    adapter.updateFilter(adapter.search, category);
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//                adapter.updateFilter(adapter.search, "");
//            }
//        });
        adapter = new QoriGridAdaper();
        recyclerQori.setAdapter(adapter);
//        recyclerQori.setLayoutManager(new LinearLayoutManager(thisContext()));

        cleanSelection();
        setSelection((short) 1);


    }

    void cleanSelection() {
        ((ImageView) layoutFilterAll.getChildAt(0)).setImageResource(R.drawable.ic_format_list_bulleted_grey_900_24dp);
        ((TextView) layoutFilterAll.getChildAt(1)).setTextColor(getResources().getColor(R.color.grey_darken_4));

        ((ImageView) layoutFilterFavorite.getChildAt(0)).setImageResource(R.drawable.ic_favorite_grey_900_24dp);
        ((TextView) layoutFilterFavorite.getChildAt(1)).setTextColor(getResources().getColor(R.color.grey_darken_4));

        ((ImageView) layoutFilterRecent.getChildAt(0)).setImageResource(R.drawable.ic_access_time_grey_900_24dp);
        ((TextView) layoutFilterRecent.getChildAt(1)).setTextColor(getResources().getColor(R.color.grey_darken_4));
    }

    void setSelection(short selection) {
        switch (selection) {
            case 1: {
                ((ImageView) layoutFilterAll.getChildAt(0)).setImageResource(R.drawable.ic_format_list_bulleted_green_700_24dp);
                ((TextView) layoutFilterAll.getChildAt(1)).setTextColor(getResources().getColor(R.color.green_darken_3));
            }
            break;
            case 2: {
                ((ImageView) layoutFilterFavorite.getChildAt(0)).setImageResource(R.drawable.ic_favorite_red_700_24dp);
                ((TextView) layoutFilterFavorite.getChildAt(1)).setTextColor(getResources().getColor(R.color.red_darken_3));
            }
            break;
            case 3: {
                ((ImageView) layoutFilterRecent.getChildAt(0)).setImageResource(R.drawable.ic_access_time_blue_700_24dp);
                ((TextView) layoutFilterRecent.getChildAt(1)).setTextColor(getResources().getColor(R.color.blue_darken_3));
            }
        }
        adapter.updateSelection(selection);
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            registerReceiver(receiver, new IntentFilter(QoriVidLoader.Companion.getBROADCAST_QORIVID_UPDATED()));
            registerReceiver(remoteReceiver, new IntentFilter(Application.REMOTE_CONFIG_UPDATED));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try {
            unregisterReceiver(receiver);
            unregisterReceiver(remoteReceiver);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        if (overlay.getVisibility() == View.VISIBLE) {
            StopVideo();
            return;
        }
        super.onBackPressed();

    }

    void PlayVideo(int i) {
        final QoriVid vid = data.get(i);
        overlay.setVisibility(View.INVISIBLE);
        cardVidpreview.setVisibility(View.INVISIBLE);
        overlay.startAnimation(fadeAnimOverlay);

        lTitle.setText(vid.title);
        lReciter.setText(vid.reciter);

        //I9qNz2KZNuY
        playerFragment = YouTubePlayerSupportFragment.newInstance();
        playerFragment.initialize(APIKEY, new YouTubePlayer.OnInitializedListener() {
            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                currentPlayer = youTubePlayer;
                youTubePlayer.cueVideo(vid.youtube_url);

                AdRequest adRequest = new AdRequest.Builder()
                        .addTestDevice("EB1DBC5F40E284EB568134257ACFC0EE")
                        .addTestDevice("116C295249E121532EE8532440F7B5BD")
                        .addTestDevice("116C295249E121532EE8532440F7B5BD")
                        .build();
                adBanner.loadAd(adRequest);

                youTubePlayer.setOnFullscreenListener(b1 -> {
                    if (b1) {
//                        Intent fullScreenPlay = YouTubeStandalonePlayer.createVideoIntent(ActivityQoriList.this,APIKEY,vid.youtube_url);
//                        startActivity(fullScreenPlay);
                    } else {
//                        StopVideo();
                    }
                });
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                Toast.makeText(ActivityQoriList.this, "Tidak dapat memulai video. Apa anda memiliki aplikasi Youtube terbaru?", Toast.LENGTH_LONG).show();
//                youTubeInitializationResult.getErrorDialog(ActivityQoriList.this, ERROR_YOUTUBE);
            }
        });

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.tv_video, playerFragment);
        transaction.commit();

        DBHelper db = new DBHelper(thisContext());
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM qori_recent where recent = ?", new String[]{String.valueOf(vid.id)});
        int count = c.getCount();
        c.close();
        if (count > 0) {
            ContentValues v = new ContentValues();
            v.put("time", System.currentTimeMillis());
            long update = db.getWritableDatabase().update("qori_recent", v, "recent = ?", new String[]{String.valueOf(vid.id)});
            Log.d("UPDATE", String.valueOf(update));
        } else {
            ContentValues v = new ContentValues();
            v.put("time", System.currentTimeMillis());
            v.put("recent", vid.id);
            long insert = db.getWritableDatabase().insert("qori_recent", "null", v);
            Log.d("INSERT", String.valueOf(insert));
        }
        db.close();
    }

    void StopVideo() {
        if (currentPlayer != null) {
            currentPlayer.pause();
            currentPlayer.setFullscreen(false);
        }
        Animation anim1 = AnimationUtils.loadAnimation(this, R.anim.alpha);
        Animation anim2 = AnimationUtils.loadAnimation(this, R.anim.alpha);

        anim1.setInterpolator(v -> 1 - v);
        anim2.setInterpolator(v -> 1 - v);

        anim1.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cardVidpreview.setVisibility(View.GONE);
                overlay.startAnimation(anim2);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        anim2.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                overlay.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        cardVidpreview.startAnimation(anim1);
    }

    private void toggleFavorite(int pos, CursorControl.Callback r) {
        QoriVid q = data.get(pos);
        DBHelper db = null;
        try {
            db = new DBHelper(thisContext());
            CursorControl c = new CursorControl(db.getReadableDatabase().rawQuery("SELECT * FROM qori_fav where favorite=?", new String[]{String.valueOf(q.id)}));
            if (c.count() == 0) {
                ContentValues v = new ContentValues();
                v.put("favorite", q.id);
                db.getWritableDatabase().insert("qori_fav", "null", v);
            } else {
                db.getWritableDatabase().delete("qori_fav", "favorite = ?", new String[]{String.valueOf(q.id)});
            }
            c.close();
        } finally {
            if (db != null) db.close();
        }
    }


    class QoriRecyclerAdapter extends RecyclerView.Adapter<QoriRecyclerAdapter.Holder> {
        public String category = "";
        public String search = "";

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.img_thumb)
            YouTubeThumbnailView imgThumb;
            @InjectView(R.id.qori_title)
            TextView qoriTitle;
            @InjectView(R.id.qori_subtitle)
            TextView qoriSubtitle;
            @InjectView(R.id.btn_extra)
            ImageView btnExtra;
            boolean video_loaded = true;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }

        private short selection = 1;

        public QoriRecyclerAdapter() {
            data = new ArrayList<>();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_qori_grid, viewGroup, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {


            holder.itemView.setOnClickListener(view1 -> {
                PlayVideo(i);
            });
            initView(holder, i);
        }

        public void updateFilter(String search, String category) {
            this.category = category == null ? "" : category;
            this.search = search == null ? "" : search;
            refresh();
        }

        public void updateSelection(short select) {
            selection = select;
            refresh();
        }

        public void refresh() {
            data = new ArrayList<>();
            DBHelper db = new DBHelper(thisContext());
            Cursor c = null;
            if (selection == 1) { //All Video
                c = db.getReadableDatabase().rawQuery("SELECT a.*, b.id as fav FROM qori a left join qori_fav b on a.id = b.favorite where a.cat like ? order by added desc", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = !c.isNull(c.getColumnIndexOrThrow("fav"));
                        data.add(q);
                        c.moveToNext();
                    }
                }
            } else if (selection == 2) { //Favorite Only
                c = db.getReadableDatabase().rawQuery("SELECT a.* FROM qori_fav b left join qori a on b.favorite = a.id where a.cat like ?", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = true;
                        data.add(q);
                        c.moveToNext();
                    }
                }
            } else if (selection == 3) { //recent Only
                c = db.getReadableDatabase().rawQuery("SELECT a.*, c.id as fav FROM qori_recent b left join qori a on b.recent = a.id" +
                        " left join qori_fav c on b.recent = c.favorite order by time desc where a.cat like ?", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = !c.isNull(c.getColumnIndexOrThrow("fav"));
                        data.add(q);
                        c.moveToNext();
                    }
                }
            }

            if (c != null) c.close();
            db.close();

            notifyDataSetChanged();
        }


        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public int getItemCount() {
            return data.size();
        }


        private void initView(Holder h, int pos) {
            QoriVid vid = data.get(pos);

            if (h.video_loaded) {
                h.video_loaded = false;
                h.imgThumb.initialize(APIKEY, new YouTubeThumbnailView.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubeThumbnailView youTubeThumbnailView, final YouTubeThumbnailLoader youTubeThumbnailLoader) {
                        youTubeThumbnailLoader.setVideo(vid.youtube_url);
                        youTubeThumbnailLoader.setOnThumbnailLoadedListener(new YouTubeThumbnailLoader.OnThumbnailLoadedListener() {
                            @Override
                            public void onThumbnailLoaded(YouTubeThumbnailView youTubeThumbnailView, String s) {
                                youTubeThumbnailLoader.release();
                            }

                            @Override
                            public void onThumbnailError(YouTubeThumbnailView youTubeThumbnailView, YouTubeThumbnailLoader.ErrorReason errorReason) {

                            }
                        });
                    }

                    @Override
                    public void onInitializationFailure(YouTubeThumbnailView youTubeThumbnailView, YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });
            }
//            Picasso.with(thisContext())
//                    .load(String.format("https://img.youtube.com/vi/%s/1.jpg", vid.youtube_url))
//                    .into(imgThumb);

            h.qoriTitle.setText(vid.title);
            h.qoriSubtitle.setText(vid.reciter);

            View.OnClickListener lst = view -> {
                PopupMenu menu = new PopupMenu(thisContext(), view);
                getMenuInflater().inflate(R.menu.menu_popup_qori, menu.getMenu());
                menu.getMenu().findItem(R.id.mn_favorite).setTitle(data.get(pos).isinfav ? "Remove from Favorite" : "Add to Favorite");
                menu.setOnMenuItemClickListener(menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.mn_favorite: {
                            toggleFavorite(pos, new CursorControl.Callback<Boolean>() {
                                @Override
                                public void callback(Boolean var) {
                                    if (var) {
                                        menu.getMenu().findItem(R.id.mn_favorite).setTitle("Remove from Favorite");
                                        vid.isinfav = true;
                                    } else {
                                        menu.getMenu().findItem(R.id.mn_favorite).setTitle("Add to Favorite");
                                        vid.isinfav = false;
                                    }
                                }
                            });
                        }
                        break;
                    }

                    return true;
                });
                menu.show();
            };
            h.btnExtra.setOnClickListener(lst);
            imgExtra.setOnClickListener(lst);


        }
    }

    class QoriGridAdaper extends BaseAdapter {

        private short selection = 1;
        public String category = "";
        public String search = "";

        public void updateFilter(String search, String category) {
            this.category = category == null ? "" : category;
            this.search = search == null ? "" : search;
            refresh();
        }

        public void updateSelection(short select) {
            selection = select;
            refresh();
        }

        public void refresh() {
            if (getIntent().getStringExtra("category") != null) {
                this.category = getIntent().getStringExtra("category");
            }
            data = new ArrayList<>();
            DBHelper db = new DBHelper(thisContext());
            Cursor c = null;
            if (selection == 1) { //All Video
                c = db.getReadableDatabase().rawQuery("SELECT a.*, b.id as fav FROM qori a left join qori_fav b on a.id = b.favorite where a.cat like ? order by added desc", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = !c.isNull(c.getColumnIndexOrThrow("fav"));
                        data.add(q);
                        c.moveToNext();
                    }
                }
            } else if (selection == 2) { //Favorite Only
                c = db.getReadableDatabase().rawQuery("SELECT a.* FROM qori_fav b left join qori a on b.favorite = a.id where a.cat like ?", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = true;
                        data.add(q);
                        c.moveToNext();
                    }
                }
            } else if (selection == 3) { //recent Only
                c = db.getReadableDatabase().rawQuery("SELECT a.*, c.id as fav FROM qori_recent b left join qori a on b.recent = a.id" +
                        " left join qori_fav c on b.recent = c.favorite where a.cat like ? order by time desc ", new String[]{"%" + category + "%"});

                if (c.getCount() > 0) {
                    c.moveToFirst();
                    while (!c.isAfterLast()) {
                        QoriVid q = QoriVid.fromCursor(c);
                        q.isinfav = !c.isNull(c.getColumnIndexOrThrow("fav"));
                        data.add(q);
                        c.moveToNext();
                    }
                }
            }

            if (c != null) c.close();
            db.close();


            notifyDataSetChanged();
            recyclerQori.invalidate();
        }

        public QoriGridAdaper() {
            data = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = null;
            if (view == null) {
                v = LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.view_qori_grid, viewGroup, false);
            } else {
                v = view;
            }

            QoriVid vid = data.get(i);
            ImageView imgThumb = (ImageView) v.findViewById(R.id.img_thumb);
            TextView qoriTitle = (TextView) v.findViewById(R.id.qori_title);
            TextView qoriSubtitle = (TextView) v.findViewById(R.id.qori_subtitle);
            ImageView btnExtra = (ImageView) v.findViewById(R.id.btn_extra);

            Picasso.with(thisContext())
                    .load(String.format("https://i.ytimg.com/vi/%s/maxresdefault.jpg", vid.youtube_url))
                    .error(R.mipmap.app_icon)
                    .placeholder(R.drawable.tilawah)
                    .into(imgThumb);
            qoriSubtitle.setText(vid.reciter);
            qoriTitle.setText(vid.title);

            View.OnLongClickListener lst = vw -> {
                PopupMenu menu = new PopupMenu(thisContext(), vw);
                getMenuInflater().inflate(R.menu.menu_popup_qori, menu.getMenu());
                menu.getMenu().findItem(R.id.mn_favorite).setTitle(vid.isinfav ? "Remove from Favorite" : "Add to Favorite");
                menu.setOnMenuItemClickListener(menuItem -> {
                    switch (menuItem.getItemId()) {
                        case R.id.mn_favorite: {
                            toggleFavorite(i, new CursorControl.Callback<Boolean>() {
                                @Override
                                public void callback(Boolean var) {
                                    if (var) {
                                        menu.getMenu().findItem(R.id.mn_favorite).setTitle("Remove from Favorite");
                                        vid.isinfav = true;
                                    } else {
                                        menu.getMenu().findItem(R.id.mn_favorite).setTitle("Add to Favorite");
                                        vid.isinfav = false;
                                    }
                                }
                            });
                        }
                        break;
                    }

                    return true;
                });
                menu.show();
                return true;
            };
            imgThumb.setOnLongClickListener(lst);
//            imgExtra.setOnClickListener(lst);

            imgThumb.setOnClickListener(view1 -> {
                PlayVideo(i);
            });

            ViewGroup.LayoutParams params = v.getLayoutParams();
            params.height = params.width;

            v.setLayoutParams(params);


            return v;
        }
    }


}
