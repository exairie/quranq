package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import java.io.IOException;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.AdzanSettings;
import exairie.com.quranq.models.Ayat;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.services.AdzanSettingsUpdater;

public class ActivitySettings extends ExtActivity {
    public final static int MIN_FONTSIZE_DP = 18;
    public final static int MAX_FONTSIZE_DP = 70;
    public final static int FLAG_ADZAN = 1;
    public final static int FLAG_VIBRATE = 2;
    public final static int FLAG_PREADZAN = 4;

    public final static int ADZAN_ID_SUBUH = 1;
    public final static int ADZAN_ID_DZUHUR = 2;
    public final static int ADZAN_ID_ASHAR = 4;
    public final static int ADZAN_ID_MAGHRIB = 8;
    public final static int ADZAN_ID_ISYA = 16;


    @InjectView(R.id.toolbar)
    Toolbar toolbar;
    @InjectView(R.id.sw_adzan_subuh)
    SwitchCompat swAdzanSubuh;
    @InjectView(R.id.sw_vibrate_subuh)
    SwitchCompat swVibrateSubuh;
    @InjectView(R.id.sw_popup_subuh)
    SwitchCompat swPopupSubuh;
    @InjectView(R.id.sw_adzan_dzuhur)
    SwitchCompat swAdzanDzuhur;
    @InjectView(R.id.sw_vibrate_dzuhur)
    SwitchCompat swVibrateDzuhur;
    @InjectView(R.id.sw_popup_dzuhur)
    SwitchCompat swPopupDzuhur;
    @InjectView(R.id.sw_adzan_ashar)
    SwitchCompat swAdzanAshar;
    @InjectView(R.id.sw_vibrate_ashar)
    SwitchCompat swVibrateAshar;
    @InjectView(R.id.sw_popup_ashar)
    SwitchCompat swPopupAshar;
    @InjectView(R.id.sw_adzan_maghrib)
    SwitchCompat swAdzanMaghrib;
    @InjectView(R.id.sw_vibrate_maghrib)
    SwitchCompat swVibrateMaghrib;
    @InjectView(R.id.sw_popup_maghrib)
    SwitchCompat swPopupMaghrib;
    @InjectView(R.id.sw_adzan_isya)
    SwitchCompat swAdzanIsya;
    @InjectView(R.id.sw_vibrate_isya)
    SwitchCompat swVibrateIsya;
    @InjectView(R.id.sw_popup_isya)
    SwitchCompat swPopupIsya;
    @InjectView(R.id.seek_ayat_fontsize)
    SeekBar seekAyatFontsize;
    @InjectView(R.id.l_preview_ayat)
    TextView lPreviewAyat;
    float fontvalue;
    @InjectView(R.id.t_offset_adzan_subuh)
    EditText tOffsetAdzanSubuh;
    @InjectView(R.id.t_offset_adzan_dzuhur)
    EditText tOffsetAdzanDzuhur;
    @InjectView(R.id.t_offset_adzan_ashar)
    EditText tOffsetAdzanAshar;
    @InjectView(R.id.t_offset_adzan_maghrib)
    EditText tOffsetAdzanMaghrib;
    @InjectView(R.id.t_offset_adzan_isya)
    EditText tOffsetAdzanIsya;

    public ActivitySettings() {
        super(R.layout.activity_settings);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_toolbar_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.mn_save: {
                saveAdzanSettings();
                saveFontSizeSettings();
                runOnUiThread(() -> {
                    new AlertDialog.Builder(thisContext())
                            .setMessage("Settings saved!")
                            .setPositiveButton("OK", (dialogInterface, i) -> {
                                dialogInterface.dismiss();
                                finish();
                            }).show();

                });
            }
            break;
        }
        return super.onOptionsItemSelected(item);
    }

    void setupAyatPreview() {
        QuranDBHelper db = new QuranDBHelper(thisContext());
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM quran WHERE surah_no = 1 and ayat_id = 1", new String[]{});
        if (c.getCount() < 1) return;
        c.moveToFirst();

        Ayat a = Ayat.createFromCursor(c);

        c.close();
        db.close();

        lPreviewAyat.setText(a.getArabic());
        seekAyatFontsize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                fontvalue = MIN_FONTSIZE_DP + ((float) seekBar.getProgress() / (float) seekBar.getMax() * (MAX_FONTSIZE_DP - MIN_FONTSIZE_DP));
                lPreviewAyat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontvalue);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle("Pengaturan");
        setExtActionBar(toolbar);
        try {
            ActionBar actionBar = getSupportActionBar();
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadOptions();
        setupAyatPreview();
    }

    void saveFontSizeSettings() {
        Option optFontSize = new Option("fontsize", String.valueOf(fontvalue < 1?1:fontvalue));
        Configs.setConfiguration(optFontSize);
    }

    void loadOptions() {
        Option optSubuh = Configs.getConfiguration("adzan_" + ADZAN_ID_SUBUH);
        Option optDzuhur = Configs.getConfiguration("adzan_" + ADZAN_ID_DZUHUR);
        Option optAshar = Configs.getConfiguration("adzan_" + ADZAN_ID_ASHAR);
        Option optMaghrib = Configs.getConfiguration("adzan_" + ADZAN_ID_MAGHRIB);
        Option optIsya = Configs.getConfiguration("adzan_" + ADZAN_ID_ISYA);

        if (optSubuh != null) {
            int optValSubuh = Integer.parseInt(optSubuh.getValue());

            swAdzanSubuh.setChecked((optValSubuh & FLAG_ADZAN) == FLAG_ADZAN);
            swVibrateSubuh.setChecked((optValSubuh & FLAG_VIBRATE) == FLAG_VIBRATE);
            swPopupSubuh.setChecked((optValSubuh & FLAG_PREADZAN) == FLAG_PREADZAN);
        }
        if (optDzuhur != null) {
            int optValDzuhur = Integer.parseInt(optDzuhur.getValue());

            swAdzanDzuhur.setChecked((optValDzuhur & FLAG_ADZAN) == FLAG_ADZAN);
            swVibrateDzuhur.setChecked((optValDzuhur & FLAG_VIBRATE) == FLAG_VIBRATE);
            swPopupDzuhur.setChecked((optValDzuhur & FLAG_PREADZAN) == FLAG_PREADZAN);
        }
        if (optAshar != null) {
            int optValAshar = Integer.parseInt(optAshar.getValue());

            swAdzanAshar.setChecked((optValAshar & FLAG_ADZAN) == FLAG_ADZAN);
            swVibrateAshar.setChecked((optValAshar & FLAG_VIBRATE) == FLAG_VIBRATE);
            swPopupAshar.setChecked((optValAshar & FLAG_PREADZAN) == FLAG_PREADZAN);
        }
        if (optMaghrib != null) {
            int optValMaghrib = Integer.parseInt(optMaghrib.getValue());

            swAdzanMaghrib.setChecked((optValMaghrib & FLAG_ADZAN) == FLAG_ADZAN);
            swVibrateMaghrib.setChecked((optValMaghrib & FLAG_VIBRATE) == FLAG_VIBRATE);
            swPopupMaghrib.setChecked((optValMaghrib & FLAG_PREADZAN) == FLAG_PREADZAN);
        }
        if (optIsya != null) {
            int optValIsya = Integer.parseInt(optIsya.getValue());

            swAdzanIsya.setChecked((optValIsya & FLAG_ADZAN) == FLAG_ADZAN);
            swVibrateIsya.setChecked((optValIsya & FLAG_VIBRATE) == FLAG_VIBRATE);
            swPopupIsya.setChecked((optValIsya & FLAG_PREADZAN) == FLAG_PREADZAN);
        }

        Option optFontSize = Configs.getConfiguration("fontsize");
        if (optFontSize != null) {
            try {
                fontvalue = Float.parseFloat(optFontSize.getValue());
                float seekValue;
                seekValue = (((fontvalue - MIN_FONTSIZE_DP) / (MAX_FONTSIZE_DP - MIN_FONTSIZE_DP)) * 100);
                Log.d("SeekValue", String.format("%.3f - %.1f", seekValue, fontvalue));
                seekAyatFontsize.setProgress((int) seekValue);
                lPreviewAyat.setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontvalue);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        Option adzanSettings = Configs.getConfiguration("adzan_settings");
        if(adzanSettings != null){
            try {
                AdzanSettings offsets = Configs.mapper().readValue(adzanSettings.getValue(),AdzanSettings.class);
                tOffsetAdzanSubuh.setText(String.valueOf(offsets.subuh));
                tOffsetAdzanDzuhur.setText(String.valueOf(offsets.dzuhur));
                tOffsetAdzanAshar.setText(String.valueOf(offsets.ashar));
                tOffsetAdzanMaghrib.setText(String.valueOf(offsets.maghrib));
                tOffsetAdzanIsya.setText(String.valueOf(offsets.isya));
            } catch (IOException e) {
                e.printStackTrace();
            }

        }



    }

    void saveAdzanSettings() {
        int flag_subuh = 0;
        int flag_dzuhur = 0;
        int flag_ashar = 0;
        int flag_maghrib = 0;
        int flag_isya = 0;

        if (swAdzanSubuh.isChecked()) flag_subuh += FLAG_ADZAN;
        if (swVibrateSubuh.isChecked()) flag_subuh += FLAG_VIBRATE;
        if (swPopupSubuh.isChecked()) flag_subuh += FLAG_PREADZAN;

        if (swAdzanDzuhur.isChecked()) flag_dzuhur += FLAG_ADZAN;
        if (swVibrateDzuhur.isChecked()) flag_dzuhur += FLAG_VIBRATE;
        if (swPopupDzuhur.isChecked()) flag_dzuhur += FLAG_PREADZAN;

        if (swAdzanAshar.isChecked()) flag_ashar += FLAG_ADZAN;
        if (swVibrateAshar.isChecked()) flag_ashar += FLAG_VIBRATE;
        if (swPopupAshar.isChecked()) flag_ashar += FLAG_PREADZAN;

        if (swAdzanMaghrib.isChecked()) flag_maghrib += FLAG_ADZAN;
        if (swVibrateMaghrib.isChecked()) flag_maghrib += FLAG_VIBRATE;
        if (swPopupMaghrib.isChecked()) flag_maghrib += FLAG_PREADZAN;

        if (swAdzanIsya.isChecked()) flag_isya += FLAG_ADZAN;
        if (swVibrateIsya.isChecked()) flag_isya += FLAG_VIBRATE;
        if (swPopupIsya.isChecked()) flag_isya += FLAG_PREADZAN;


        Option optAdzanSubuh = new Option("adzan_" + ADZAN_ID_SUBUH, String.valueOf(flag_subuh));
        Option optAdzanDzuhur = new Option("adzan_" + ADZAN_ID_DZUHUR, String.valueOf(flag_dzuhur));
        Option optAdzanAshar = new Option("adzan_" + ADZAN_ID_ASHAR, String.valueOf(flag_ashar));
        Option optAdzanMaghrib = new Option("adzan_" + ADZAN_ID_MAGHRIB, String.valueOf(flag_maghrib));
        Option optAdzanIsya = new Option("adzan_" + ADZAN_ID_ISYA, String.valueOf(flag_isya));

        Configs.setConfiguration(optAdzanSubuh);
        Configs.setConfiguration(optAdzanDzuhur);
        Configs.setConfiguration(optAdzanAshar);
        Configs.setConfiguration(optAdzanMaghrib);
        Configs.setConfiguration(optAdzanIsya);


        AdzanSettings offsets = new AdzanSettings();
        offsets.subuh = tOffsetAdzanSubuh.getText().length() > 0?Integer.parseInt(tOffsetAdzanSubuh.getText().toString()):0;
        offsets.dzuhur = tOffsetAdzanDzuhur.getText().length() > 0?Integer.parseInt(tOffsetAdzanDzuhur.getText().toString()):0;
        offsets.ashar = tOffsetAdzanAshar.getText().length() > 0?Integer.parseInt(tOffsetAdzanAshar.getText().toString()):0;
        offsets.maghrib = tOffsetAdzanMaghrib.getText().length() > 0?Integer.parseInt(tOffsetAdzanMaghrib.getText().toString()):0;
        offsets.isya = tOffsetAdzanIsya.getText().length() > 0?Integer.parseInt(tOffsetAdzanIsya.getText().toString()):0;

        try {
            Configs.setConfiguration(new Option("adzan_settings",Configs.mapper().writeValueAsString(offsets)));


            Intent i = new Intent(getApplicationContext(), AdzanSettingsUpdater.class);
            i.putExtra("type","upload");
            startService(i);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
