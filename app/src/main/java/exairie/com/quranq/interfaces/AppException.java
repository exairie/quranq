package exairie.com.quranq.interfaces;

/**
 * Created by exain on 5/24/2017.
 */

public class AppException  {
    public static class BookmarkExistException extends Exception {
        public BookmarkExistException() {
            super("Surat yang anda pilih sebagai bookmark telah terdaftar!");
        }
    }
    public static class AyatNotFoundException extends Exception{
        public AyatNotFoundException(){
            super("Ayat yang anda pilih tidak terdaftar!");
        }
    }
}
