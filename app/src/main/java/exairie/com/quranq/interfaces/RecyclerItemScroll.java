package exairie.com.quranq.interfaces;

import android.view.View;

/**
 * Created by exain on 5/16/2017.
 */

public interface RecyclerItemScroll {
    void onScroll();
}
