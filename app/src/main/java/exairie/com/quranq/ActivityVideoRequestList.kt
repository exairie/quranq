package exairie.com.quranq

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.QoriVid
import kotlinx.android.synthetic.main.activity_video_request_list.*
import kotlinx.android.synthetic.main.view_video_request.view.*
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ActivityVideoRequestList : ExtActivity(R.layout.activity_video_request_list) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        setTitle("Video Requests")

        val request = Request.Builder()
                .url(server("api/videorequest"))
                .get()
                .build()
        NetRequest(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) = Unit

            override fun OnSuccess(string: String?) {
                val json = JSONObject(string)
                val data = Configs.mapper().readValue(json.getString("data"),Array<QoriVid.Request>::class.java)
                populateRecycler(data)
            }

            override fun OnFail(msg: String?) {

            }

            override fun Finish() {
            }

        })
    }

    private fun populateRecycler(data: Array<QoriVid.Request>?) {
        runOnUiThread({
            if(data!!.size > 0){
                recycler_requests.visibility = View.VISIBLE
                layout_loader.visibility = View.GONE
            }else{
                recycler_requests.visibility = View.GONE
                l_status_loader.setText("No request found")
                progress_loader.visibility = View.GONE
            }
            recycler_requests.adapter = RequestAdapter(data)
            recycler_requests.layoutManager = LinearLayoutManager(thisContext())
        })
    }

    inner class Holder(view : View) : RecyclerView.ViewHolder(view);

    inner class RequestAdapter(requests : Array<QoriVid.Request>) : RecyclerView.Adapter<Holder>(){
        val data : Array<QoriVid.Request>
        init {
            data = requests
        }
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val v = LayoutInflater.from(p0!!.context)
                    .inflate(R.layout.view_video_request,p0,false)
            return Holder(v)
        }

        override fun onBindViewHolder(p0: Holder, p1: Int) {
            var req = data[p1]
            p0!!.itemView.l_request_time.setText(DateFormat.format("dd MMM yyyy",req.request_date))
            p0.itemView.l_request_title.setText(req.request_title)
            p0.itemView.l_request_desc.setText(req.request_description)
            when(req.request_status){
                "Requested"->{
                    p0.itemView.btn_request_status.setTextColor(ContextCompat.getColor(thisContext(),R.color.amber_darken_3))
                    p0.itemView.btn_request_status.setText("Relayed")
                }
                "Processed"->{
                    p0.itemView.btn_request_status.setTextColor(ContextCompat.getColor(thisContext(),R.color.blue_darken_3))
                    p0.itemView.btn_request_status.setText("Processed")
                }
                "Finished"->{
                    p0.itemView.btn_request_status.setTextColor(ContextCompat.getColor(thisContext(),R.color.green_darken_3))
                    p0.itemView.btn_request_status.setText("Available")
                    p0.itemView.btn_request_status.setOnClickListener({
                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(req.video_url))
                        startActivity(browserIntent)
                    })
                }
            }
        }

        override fun getItemCount(): Int = data.size

    }


}
