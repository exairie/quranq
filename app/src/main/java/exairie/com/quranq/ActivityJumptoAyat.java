package exairie.com.quranq;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.List;

import butterknife.InjectView;
import exairie.com.quranq.libs.QuranDBHelper;
import exairie.com.quranq.models.StringWithID;
import exairie.com.quranq.models.Surah;

public class ActivityJumptoAyat extends ExtActivity {
    @InjectView(R.id.spinner_surah_list)
    Spinner spinnerSurahList;
    @InjectView(R.id.btn_ok)
    Button btnOk;
    @InjectView(R.id.t_inputayat)
    EditText tInputayat;
    @InjectView(R.id.textInputLayout)
    TextInputLayout textInputLayout;

    public ActivityJumptoAyat() {
        super(R.layout.activity_jumpto_ayat);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setupSpinner();

//        tInputayat.setFilters(new InputFilter[]{new NumberFilter(1, 1)});
        btnOk.setOnClickListener(view -> {
            StringWithID surahSelected = ((StringWithID) spinnerSurahList.getSelectedItem());
            if(surahSelected == null) return;
            int surahid = surahSelected.getId();
            int ayatid = 1;
            try {
                ayatid = Integer.parseInt(tInputayat.getText().toString());
            } catch (NumberFormatException e) {
                e.printStackTrace();
                ayatid = 1;
            }
            Intent i = new Intent(this, ActivityReadQuran.class);
            i.putExtra("id", surahid);
            i.putExtra("jump_ayat", ayatid);

            startActivity(i);
        });
    }

    class NumberFilter implements InputFilter {
        int min, max;

        public int getMin() {
            return min;
        }

        public void setMin(int min) {
            this.min = min;
        }

        public int getMax() {
            return max;
        }

        public void setMax(int max) {
            this.max = max;
        }

        @Override
        public CharSequence filter(CharSequence charSequence, int i, int i1, Spanned spanned, int i2, int i3) {
            try {
                int input = Integer.parseInt(spanned.toString() + charSequence.toString());
                if ((input < min || input > max)) {
                    return null;
                }
                return "";
            } catch (Exception exc) {

            }
            return "";
        }

        public NumberFilter(int min, int max) {
            this.min = min;
            this.max = max;
        }

    }

    private void setupSpinner() {
        AsyncTask<Void,Void,Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                List<Surah> surahList = new QuranDBHelper(thisContext()).getSurahList();
                ArrayAdapter<StringWithID> adapterFinish = new ArrayAdapter<>(thisContext(), android.R.layout.simple_spinner_dropdown_item);
                for (Surah s : surahList) {
                    adapterFinish.add(new StringWithID(s.getSurah_no(), s.getSurah_name()));
                }

                runOnUiThread(() -> {
                    spinnerSurahList.setAdapter(adapterFinish);
                    spinnerSurahList.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            StringWithID id = (StringWithID) adapterView.getSelectedItem();
                            getAyatList(id.getId());
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                });
                return null;
            }
        };
        task.execute();
    }

    private void getAyatList(int id) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                QuranDBHelper db = new QuranDBHelper(thisContext());
                Cursor c = db.getReadableDatabase().rawQuery("SELECT MIN(ayat_id), MAX(ayat_id) from quran where surah_no = ?", new String[]{String.valueOf(id)});
                c.moveToFirst();
                runOnUiThread(() -> {
                    NumberFilter n = new NumberFilter(0,0);
                    n.setMin(c.getInt(0));
                    n.setMax(c.getInt(1));
                    textInputLayout.setHint(String.format("%d - %d", n.getMin(), n.getMax()));
                    tInputayat.setHint(String.format("%d - %d", n.getMin(), n.getMax()));
                });
                return null;
            }
        };
        task.execute();
    }
}
