package exairie.com.quranq

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager

import exairie.com.quranq.libs.ViewPagerAdapter

class SliderPageViewAdapter(manager: FragmentManager, internal var fragments: List<Fragment>) : ViewPagerAdapter(manager) {

    override fun getItem(position: Int): Fragment = fragments[position]

    override fun getCount(): Int = fragments.size
}
