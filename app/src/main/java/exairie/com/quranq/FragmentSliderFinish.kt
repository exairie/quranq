package exairie.com.quranq

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Option
import kotlinx.android.synthetic.main.fragment_fragment_slider_finish.*


class FragmentSliderFinish : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        return inflater!!.inflate(R.layout.fragment_fragment_slider_finish, container, false)
    }

    override fun onStart() {
        super.onStart()
        card_goto_instagram.setOnClickListener(View.OnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW,Uri.parse(Configs.BQ_INS_URL))
            startActivity(browserIntent)
        })
        card_goto_youtub.setOnClickListener({
            val browserIntent = Intent(Intent.ACTION_VIEW,Uri.parse(Configs.BQ_YTB_URL))
            startActivity(browserIntent)
        })
        card_goto_laznas.setOnClickListener({
            val browserIntent = Intent(Intent.ACTION_VIEW,Uri.parse(Configs.BQ_LAZ_URL))
            startActivity(browserIntent)
        })
        btn_proceed.setOnClickListener({
            val opt = Option("firstload","ok")
            Configs.setConfiguration(opt)
            val i = Intent(activity,ActivityDashboard::class.java)
            startActivity(i)
            activity?.finish()
        })
    }

}
