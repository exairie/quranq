package exairie.com.quranq

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.RecyclerView.HORIZONTAL
import android.support.v7.widget.Toolbar
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.stfalcon.frescoimageviewer.ImageViewer
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Event
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.UserLogin
import exairie.com.quranq.services.DataLoader
import kotlinx.android.synthetic.main.activity_event_detail.*
import kotlinx.android.synthetic.main.view_img_thumb.view.*
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ActivityEventDetail : ExtActivity(R.layout.activity_event_detail) {
    var event_id : Long= -1
    val eventPictures : MutableList<String> = mutableListOf()
    val imgAdapter = EventPicturesAdapter()
    var currentEvent : Event? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExtActionBar(this.toolbar as Toolbar)
        title = "Informasi Event"
        hasBackButton(true)
        val w = supportActionBar
        try {
            w!!.setDefaultDisplayHomeAsUpEnabled(true)
            w.setHomeButtonEnabled(true)
        }catch (e : Exception){}

        event_id = intent.getLongExtra("id",-1)

        recycler_pictures.visibility = View.VISIBLE
        l_nopicture.visibility = View.GONE

        if (event_id < 0){
            finish()
            return
        }

        setupView()

        recycler_pictures.layoutManager = LinearLayoutManager(thisContext(),HORIZONTAL,false)
        recycler_pictures.adapter = imgAdapter



    }

    fun setupView(){
        val db = DBHelper(thisContext())
        val c = db.writableDatabase.rawQuery("SELECT * FROM events WHERE id = ?", arrayOf(event_id.toString()))
        if (c.count  < 1) {
            finish()
            return
        }

        c.moveToFirst()

        currentEvent = Event.fromCursor(c)
        c.close()
        db.close()
        l_title.text = currentEvent!!.nama_event
        l_date.text = DateFormat.format("d-MMM-yyyy HH:mm:ss", currentEvent!!.date_start)
        l_descriptions.text = currentEvent!!.deskripsi

        btn_register.setOnClickListener({
            val i = Intent(thisContext(),ActivityRegisterEvent::class.java)
            i.putExtra("id",event_id)
            startActivity(i)
        })
        loadPictures()

        DataLoader.getUserData(currentEvent!!.id_user,object : DataLoader.LoadResult<UserLogin?>{
            override fun finish(result: UserLogin?) {
                if(result != null){
                    runOnUiThread({l_organizer.setText("${result.firstname} ${result.lastname} / ${result.username}")})
                }else{

                }
            }
        },thisContext())
    }
    fun loadPictures(){
        val body = FormBody.Builder()
                .add("event_id",currentEvent!!.id.toString())
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/event_getpictures"))
                .build()
        NetRequest(request,object  : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val array = JSONObject(string).getJSONArray("data")
                Log.d("Picture","Size ${array.length()}")
                runOnUiThread({
                    if(array.length() < 1){
                        recycler_pictures.visibility = View.GONE
                        l_nopicture.visibility = View.VISIBLE
                    }else{
                        recycler_pictures.visibility = View.VISIBLE
                        l_nopicture.visibility = View.GONE
                    }
                })
                for (i in 0 until array.length()){
                    eventPictures.add(SERVER_URL + "/assets/uploads/" + array.getJSONObject(i).getString("file"))
                    runOnUiThread({imgAdapter.notifyDataSetChanged()})
                }
            }

            override fun OnFail(msg: String?) {
            }

            override fun Finish() {
            }
        })

    }
    inner class EventPicturesAdapter : RecyclerView.Adapter<Holder>(){

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.view_img_thumb,parent,false)
            return Holder(v)
        }

        override fun onBindViewHolder(parent: Holder, p1: Int) {
            Picasso.with(thisContext())
                    .load(eventPictures[p1])
                    .resize(500,500)
                    .placeholder(R.mipmap.logoapp)
                    .into(parent.itemView.image)
            parent.itemView.image.setOnClickListener({
                ImageViewer.Builder(thisContext(),eventPictures)
                        .setStartPosition(p1)
                        .show()
            })
        }

        override fun getItemCount(): Int = eventPictures.size
    }
    inner class Holder(v : View) : RecyclerView.ViewHolder(v)
}
