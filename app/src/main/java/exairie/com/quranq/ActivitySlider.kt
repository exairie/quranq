package exairie.com.quranq

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.view.View
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_dashboard.*
import kotlinx.android.synthetic.main.activity_event.*

class ActivitySlider : ExtActivity(R.layout.activity_slider) {
    lateinit var adapter : SliderPager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        adapter = SliderPager(supportFragmentManager)
        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider1))
        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider2))
//        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider3))
        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider4))
//        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider5))
        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider6))
        adapter.addFragment(FragmentSlider.newInstance(R.drawable.slider7))
        adapter.addFragment(FragmentSliderFinish())
        adapter.notifyDataSetChanged()
        viewpager.adapter = adapter

        setupdots()
        viewpager.addOnPageChangeListener(object : ViewPager.SimpleOnPageChangeListener(){
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                changePage(position)
            }
        })
    }

    private fun setupdots() {
        this.layout_event_indicator.removeAllViews()
        val size = viewpager.adapter?.count ?: 0
        val density = this.resources.displayMetrics.density
        for (i in 0 until size){
            val v = View(this)
            val params = LinearLayout.LayoutParams((8*density).toInt(),(8*density).toInt())
            params.setMargins((4f*density).toInt(),0,0,0)
            v.setBackgroundResource(R.drawable.dot_grey)
            v.layoutParams = params
            this.layout_event_indicator.addView(v)
        }
        changePage(0)
    }
    fun changePage(pos : Int){
        if(layout_event_indicator.childCount - 1 < pos) return
        for (v : Int in 0..this.layout_event_indicator.childCount - 1){
            this.layout_event_indicator.getChildAt(v).setBackgroundResource(R.drawable.dot_grey)
        }
        this.layout_event_indicator.getChildAt(pos).setBackgroundResource(R.drawable.dot_green)
    }
    inner class SliderPager(fm : FragmentManager) : FragmentPagerAdapter(fm){
        val list = mutableListOf<Fragment>()
        fun addFragment(f : Fragment){
            list.add(f)
        }
        override fun getItem(p0: Int): Fragment {
            return list[p0]
        }

        override fun getCount(): Int = list.size
    }
}
