package exairie.com.quranq.models;

import android.database.Cursor;

import java.util.Calendar;

/**
 * Created by exain on 5/24/2017.
 */

public class QuranBookmark {
    int id;
    long timeBookmark;
    int ayat_id;
    Ayat ayatInfo;
    Surah surahInfo;

    public Ayat getAyatInfo() {
        return ayatInfo;
    }

    public void setAyatInfo(Ayat ayatInfo) {
        this.ayatInfo = ayatInfo;
    }

    public Surah getSurahInfo() {
        return surahInfo;
    }

    public void setSurahInfo(Surah surahInfo) {
        this.surahInfo = surahInfo;
    }

    public Calendar getTimeInCalendar(){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(timeBookmark);
        return c;
    }
    public static QuranBookmark createFromCursor(Cursor c){
        QuranBookmark bookmark = new QuranBookmark();
        bookmark.setId(c.getInt(0));
        bookmark.setTimeBookmark(c.getLong(1));
        bookmark.setAyat_id(c.getInt(2));

        return bookmark;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTimeBookmark() {
        return timeBookmark;
    }

    public void setTimeBookmark(long timeBookmark) {
        this.timeBookmark = timeBookmark;
    }

    public int getAyat_id() {
        return ayat_id;
    }

    public void setAyat_id(int ayat_id) {
        this.ayat_id = ayat_id;
    }
}
