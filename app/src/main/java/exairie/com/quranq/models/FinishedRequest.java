package exairie.com.quranq.models;

import okhttp3.Response;

/**
 * Created by exain on 7/17/2017.
 */

public interface FinishedRequest{
    @Deprecated
    void OnSuccess(Response response);
    void OnSuccess(String string);
    void OnFail(String msg);
    void Finish();
}
