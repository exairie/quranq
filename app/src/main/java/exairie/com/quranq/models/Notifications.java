package exairie.com.quranq.models;


import android.database.Cursor;
import android.text.format.DateFormat;

import org.codehaus.jackson.annotate.JsonProperty;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by exain on 12/23/2017.
 */

public class Notifications {
    @JsonProperty("id")
    public long id;
    @JsonProperty("notif_text")
    public String notif_text;
    @JsonProperty("notif_desc")
    public String notif_desc;
    @JsonProperty("notif_img")
    public String notif_img;
    @JsonProperty("expiration_date")
    public String expire;

    public long expiration_date;
    @JsonProperty("create_date")
    public String create;

    public long create_date;


    public void parseExpiration_date(String sexpiration_date){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = f.parse(sexpiration_date);
            expiration_date = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();

        }

    }
    public void parseCreate_date(String screate_date){
        SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            Date d = f.parse(screate_date);
            create_date = d.getTime();
        } catch (ParseException e) {
            e.printStackTrace();

        }

    }

    public static Notifications fromCursor(Cursor c){
        Notifications n = new Notifications();
        n.id = c.getLong(c.getColumnIndexOrThrow("id"));
        n.notif_text = c.getString(c.getColumnIndexOrThrow("notif_text"));
        n.notif_desc = c.getString(c.getColumnIndexOrThrow("notif_desc"));
        n.notif_img = c.getString(c.getColumnIndexOrThrow("notif_img"));
        n.expiration_date = c.getLong(c.getColumnIndexOrThrow("expiration_date"));
        n.create_date = c.getLong(c.getColumnIndexOrThrow("create_date"));

        return n;




    }
}
