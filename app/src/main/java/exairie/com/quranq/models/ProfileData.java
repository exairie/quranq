package exairie.com.quranq.models;

import android.content.ContentValues;
import android.database.Cursor;
import android.util.Log;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;

/**
 * Created by exain on 1/10/2018.
 */

public class ProfileData {
    List<QoriVid.Recent> recentQoriVid = new ArrayList<>();
    List<QoriVid.Favorite> favQoriVid = new ArrayList<>();
    List<Option> options = new ArrayList<>();
    List<Fahmil.Answer> fahmilAnswers = new ArrayList<>();

    @JsonProperty("qoriRecentData")
    public QoriVid.Recent[] qoriRecentData;
    @JsonProperty("qoriFavoriteData")
    public QoriVid.Favorite[] qoriFavoriteData;
    @JsonProperty("optionData")
    public Option[] optionData;
    @JsonProperty("fahmilData")
    public Fahmil.Answer[] fahmilData;

    public void fetch(String jsonStr, DBHelper db){
        try {

            ProfileData data = Configs.mapper().readValue(jsonStr,ProfileData.class);

            db.getWritableDatabase().delete("fahmil_result","", new String[]{});
            db.getWritableDatabase().delete("qori_recent","", new String[]{});
            db.getWritableDatabase().delete("qori_fav","", new String[]{});
            db.getWritableDatabase().delete("config","ckey != 'login_token' and ckey != 'fcm_token' and ckey != 'login_info'", new String[]{});

            for(Fahmil.Answer row : data.fahmilData){
                ContentValues c = new ContentValues();

                c.put("id",row.id);
                c.put("question",row.question);
                c.put("answer_true",row._answer_true);
                c.put("answer_false",row._answer_false);
                c.put("session_start",row.session_start);
                c.put("session_end",row.session_end);

                db.getWritableDatabase().insert("fahmil_result",null,c);
            }

            for(QoriVid.Recent row : data.qoriRecentData){
                ContentValues c = new ContentValues();
                c.put("id",row.id);
                c.put("recent",row.recent_id);
                c.put("time",row.time);

                db.getWritableDatabase().insert("qori_recent",null,c);
            }

            for(QoriVid.Favorite row : data.qoriFavoriteData){
                ContentValues c = new ContentValues();
                c.put("id",row.id);
                c.put("favorite",row.favorite);

                db.getWritableDatabase().insert("qori_fav",null,c);
            }

            for(Option row : data.optionData){
                if(row.key.equals("login_info")) continue;
                if(row.key.equals("login_token")) continue;
                if(row.key.equals("firebase_token"))continue;
                ContentValues c = new ContentValues();
                c.put("ckey",row.key);
                c.put("cvalue",row.value);

                db.getWritableDatabase().insert("config",null,c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void populate(DBHelper db){
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM qori_recent",new String[]{});
        if (c.getCount() > 0){
            c.moveToFirst();
            while (!c.isAfterLast()){
                recentQoriVid.add(QoriVid.Recent.fromCursor(c));
                c.moveToNext();
            }
        }

        c.close();
        c = db.getReadableDatabase().rawQuery("SELECT * FROM qori_fav",new String[]{});
        if (c.getCount() > 0){
            c.moveToFirst();
            while (!c.isAfterLast()){
                favQoriVid.add(QoriVid.Favorite.fromCursor(c));
                c.moveToNext();
            }
        }
        c.close();
        c = db.getReadableDatabase().rawQuery("SELECT * FROM config",new String[]{});
        if (c.getCount() > 0){
            c.moveToFirst();
            while (!c.isAfterLast()){
                options.add(Option.fromCursor(c));
                c.moveToNext();
            }
        }
        c = db.getReadableDatabase().rawQuery("SELECT * FROM fahmil_result",new String[]{});
        if (c.getCount() > 0){
            c.moveToFirst();
            while (!c.isAfterLast()){
                fahmilAnswers.add(Fahmil.Answer.fromQuizResult(c));
                c.moveToNext();
            }
        }
        c.close();

        qoriFavoriteData = new QoriVid.Favorite[favQoriVid.size()];
        favQoriVid.toArray(qoriFavoriteData);
        qoriRecentData = new QoriVid.Recent[recentQoriVid.size()];
        recentQoriVid.toArray(qoriRecentData);
        optionData = new Option[options.size()];
        options.toArray(optionData);
        fahmilData = new Fahmil.Answer[fahmilAnswers.size()];
        fahmilAnswers.toArray(fahmilData);

        favQoriVid = null;
        recentQoriVid = null;
        options = null;
        fahmilAnswers = null;
    }
}
