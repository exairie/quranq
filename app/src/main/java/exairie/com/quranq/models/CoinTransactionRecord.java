package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 12/3/2017.
 */

public class CoinTransactionRecord {
    @JsonProperty("id")
    public long id;
    @JsonProperty("user_id")
    public long user_id;
    @JsonProperty("coin_transaction")
    public double coin_transaction;
    @JsonProperty("transaction_date")
    public long transaction_date;
    @JsonProperty("remark")
    public String remark;

    public static CoinTransactionRecord fromCursor(Cursor c){
        CoinTransactionRecord m = new CoinTransactionRecord();
        m.id = c.getLong(c.getColumnIndexOrThrow("id"));
        m.user_id = c.getLong(c.getColumnIndexOrThrow("user_id"));
        m.coin_transaction = c.getDouble(c.getColumnIndexOrThrow("coin_transaction"));
        m.transaction_date = c.getLong(c.getColumnIndexOrThrow("transaction_date"));
        m.remark = c.getString(c.getColumnIndexOrThrow("remark"));
        return m;
    }
}
