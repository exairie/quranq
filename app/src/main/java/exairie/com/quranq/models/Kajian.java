package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by Exainsane on 7/27/2017.
 */

public class Kajian {
    @JsonProperty("id")
    public long id;
    @JsonProperty("judul")
    public String judul;
    @JsonProperty("gambar")
    public String gambar;
    @JsonProperty("isi")
    public String isi;
    @JsonProperty("create_date")
    public long create_date;

    public static Kajian create(Cursor c){
        Kajian k = new Kajian();
        k.id = c.getLong(c.getColumnIndexOrThrow("id"));
        k.judul = c.getString(c.getColumnIndexOrThrow("judul"));
        k.gambar = c.getString(c.getColumnIndexOrThrow("gambar"));
        k.isi = c.getString(c.getColumnIndexOrThrow("isi"));
        k.create_date = c.getLong(c.getColumnIndexOrThrow("create_date"));
        return k;
    }
}
