package exairie.com.quranq.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 7/17/2017.
 */

public class RequestResult {
    @JsonProperty("status")
    public String status;
    @JsonProperty("msg")
    public String message;
    @JsonProperty("code")
    public int code;
}
