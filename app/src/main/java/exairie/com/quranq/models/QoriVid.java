package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 8/25/2017.
 */

public class QoriVid {
    @JsonProperty("id")
    public long id;
    @JsonProperty("title")
    public String title;
    @JsonProperty("category")
    public int category;
    @JsonProperty("reciter")
    public String reciter;
    @JsonProperty("reference")
    public String reference;
    @JsonProperty("added")
    public long added;
    @JsonProperty("youtube_url")
    public String youtube_url;

    @JsonProperty("cat")
    public String cat;

    public boolean isinfav;

    public static QoriVid fromCursor(Cursor c){
        QoriVid obj = new QoriVid();
        obj.id = c.getLong(c.getColumnIndexOrThrow("id"));
        obj.title = c.getString(c.getColumnIndexOrThrow("title"));
        obj.category = c.getInt(c.getColumnIndexOrThrow("category"));
        obj.reciter = c.getString(c.getColumnIndexOrThrow("reciter"));
        obj.reference = c.getString(c.getColumnIndexOrThrow("reference"));
        obj.added = c.getLong(c.getColumnIndexOrThrow("added"));
        obj.youtube_url = c.getString(c.getColumnIndexOrThrow("youtube_url"));
        obj.cat = c.getString(c.getColumnIndexOrThrow("cat"));

        return obj;
    }
    public static class QoriSync{
        public long id;
        public long timestamp;
    }
    public static class Recent{
        @JsonProperty("id")
        public long id;
        @JsonProperty("recent_id")
        public long recent_id;
        @JsonProperty("time")
        public long time;

        public static Recent fromCursor(Cursor c){
            Recent r = new Recent();
            r.id = c.getLong(c.getColumnIndexOrThrow("id"));
            r.recent_id = c.getLong(c.getColumnIndexOrThrow("recent"));
            r.time = c.getLong(c.getColumnIndexOrThrow("time"));

            return r;
        }
    }
    public static class Favorite{
        @JsonProperty("id")
        public long id;
        @JsonProperty("favorite")
        public long favorite;

        public static Favorite fromCursor(Cursor c){
            Favorite f = new Favorite();
            f.id = c.getLong(c.getColumnIndexOrThrow("id"));
            f.favorite = c.getLong(c.getColumnIndexOrThrow("favorite"));
            return f;

        }
    }
    public static class Request{
        @JsonProperty("id")
        public long id;
        @JsonProperty("request_title")
        public String request_title;
        @JsonProperty("request_description")
        public String request_description;
        @JsonProperty("request_reference")
        public String request_reference;
        @JsonProperty("request_date")
        public long request_date;
        @JsonProperty("request_status")
        public String request_status;
        @JsonProperty("video_url")
        public String video_url;
    }
}
