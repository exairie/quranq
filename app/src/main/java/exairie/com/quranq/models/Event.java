package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 9/26/2017.
 */

public class Event {
    @JsonProperty("id")
    public long id;
    @JsonProperty("id_user")
    public long id_user;
    @JsonProperty("nama_event")
    public String nama_event;
    @JsonProperty("deskripsi")
    public String deskripsi;
    @JsonProperty("latitude")
    public double latitude;
    @JsonProperty("longitude")
    public double longitude;
    @JsonProperty("notification_radius")
    public double notification_radius;
    @JsonProperty("date_start")
    public long date_start;
    @JsonProperty("date_end")
    public long date_end;
    @JsonProperty("date_create")
    public long date_create;
    @JsonProperty("date_edit")
    public long date_edit;
    @JsonProperty("contact_person")
    public String contact_person;
    @JsonProperty("status")
    public int status;
    @JsonProperty("capacity")
    public int capacity;

    @JsonProperty("current_capacity")
    public int current_capacity;

    public long coin_cost;

    public static Event fromCursor(Cursor c){
        Event e = new Event();
        e.id = c.getLong(c.getColumnIndexOrThrow("id"));
        e.id_user = c.getLong(c.getColumnIndexOrThrow("id_user"));
        e.nama_event = c.getString(c.getColumnIndexOrThrow("nama_event"));
        e.deskripsi = c.getString(c.getColumnIndexOrThrow("deskripsi"));
        e.latitude = c.getDouble(c.getColumnIndexOrThrow("latitude"));
        e.longitude = c.getDouble(c.getColumnIndexOrThrow("longitude"));
        e.notification_radius = c.getDouble(c.getColumnIndexOrThrow("notification_radius"));
        e.date_start = c.getLong(c.getColumnIndexOrThrow("date_start"));
        e.date_end = c.getLong(c.getColumnIndexOrThrow("date_end"));
        e.date_create = c.getLong(c.getColumnIndexOrThrow("date_create"));
        e.date_edit = c.getLong(c.getColumnIndexOrThrow("date_edit"));
        e.contact_person = c.getString(c.getColumnIndexOrThrow("contact_person"));
        e.status = c.getInt(c.getColumnIndexOrThrow("status"));
        e.capacity = c.getInt(c.getColumnIndexOrThrow("capacity"));
        return e;
    }
}
