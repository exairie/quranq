package exairie.com.quranq.models;

/**
 * Created by exain on 5/17/2017.
 */

public class StringWithID {
    public StringWithID(int id, String string) {
        this.id = id;
        this.string = string;
    }

    public int getId() {
        return id;
    }

    int id;
    String string;

    @Override
    public String toString() {
        return string;
    }

}
