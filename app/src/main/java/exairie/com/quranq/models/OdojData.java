package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Calendar;

import exairie.com.quranq.libs.Configs;

/**
 * Created by exain on 5/24/2017.
 */

public class OdojData {
    Ayat _ayat_from;
    Ayat _ayat_to;
    @JsonProperty("id_sync")
    long odoj_id;
    @JsonProperty("time")
    long time;
    @JsonProperty("from")
    int ayat_id;
    @JsonProperty("to")
    int ayat_to;
    @JsonProperty("letters")
    int letter_count;

    public Ayat get_ayat_from() {
        return _ayat_from;
    }

    public void set_ayat_from(Ayat _ayat_from) {
        this._ayat_from = _ayat_from;
    }

    public Ayat get_ayat_to() {
        return _ayat_to;
    }

    public void set_ayat_to(Ayat _ayat_to) {
        this._ayat_to = _ayat_to;
    }

    public double getJuzCalculation(){
        return letter_count/ Configs.LETTER_PER_JUZ;
    }
    public int getAyat_to() {
        return ayat_to;
    }

    public void setAyat_to(int ayat_to) {
        this.ayat_to = ayat_to;
    }

    public int getLetter_count() {
        return letter_count;
    }

    public void setLetter_count(int letter_count) {
        this.letter_count = letter_count;
    }

    public long getOdojId() {
        return this.odoj_id;
    }

    public void setOdojId(long id) {
        this.odoj_id = id;
    }

    public Calendar getTimeInCalendar(){
        Calendar c = Calendar.getInstance();
        c.setTimeInMillis(time);
        return c;
    }

    public static OdojData createFromCursor(Cursor c){
        OdojData odojData = new OdojData();

        odojData.setOdojId(c.getLong(c.getColumnIndexOrThrow("id")));
        odojData.setTime(c.getLong(c.getColumnIndexOrThrow("time")));
        odojData.setAyat_id(c.getInt(c.getColumnIndexOrThrow("ayat_id")));
        odojData.setAyat_to(c.getInt(c.getColumnIndexOrThrow("ayat_to")));
        odojData.setLetter_count(c.getInt(c.getColumnIndexOrThrow("letter_no")));

        return odojData;
    }
    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getAyat_id() {
        return ayat_id;
    }

    public void setAyat_id(int ayat_id) {
        this.ayat_id = ayat_id;
    }
}
