package exairie.com.quranq.models;

import android.database.Cursor;

/**
 * Created by exain on 5/12/2017.
 */

public class Ayat {
    private int id;
    private int surah_no;
    private int ayat_id;
    private String arabic;
    private String indonesian;
    private int juz;
    public static Ayat createFromCursor(Cursor cursor){
        Ayat a = new Ayat();
        a.setId(cursor.getInt(0));
        a.setSurah_no(cursor.getInt(1));
        a.setAyat_id(cursor.getInt(2));
        a.setArabic(cursor.getString(3));
        a.setIndonesian(cursor.getString(4));
        a.setJuz(cursor.getInt(5));

        return a;
    }
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurah_no() {
        return surah_no;
    }

    public void setSurah_no(int surah_no) {
        this.surah_no = surah_no;
    }

    public int getAyat_id() {
        return ayat_id;
    }

    public void setAyat_id(int ayat_id) {
        this.ayat_id = ayat_id;
    }

    public String getArabic() {
        return arabic;
    }

    public void setArabic(String arabic) {
        this.arabic = arabic;
    }

    public String getIndonesian() {
        return indonesian;
    }

    public void setIndonesian(String indonesian) {
        this.indonesian = indonesian;
    }

    public int getJuz() {
        return juz;
    }

    public void setJuz(int juz) {
        this.juz = juz;
    }
}
