package exairie.com.quranq.models;

import android.content.ContentValues;
import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 1/18/2018.
 */

public class HifdzilResult {
    @JsonProperty("quizdate")
    public long quizdate;
    @JsonProperty("surah_name")
    public String surah_name;
    @JsonProperty("ayat_id")
    public String ayat_id;
    @JsonProperty("answer_true")
    public int answer_true;

    public static HifdzilResult fromCursor(Cursor c){
        HifdzilResult h = new HifdzilResult();
        h.quizdate = c.getLong(c.getColumnIndexOrThrow("quizdate"));
        h.surah_name = c.getString(c.getColumnIndexOrThrow("surah_name"));
        h.ayat_id = c.getString(c.getColumnIndexOrThrow("ayat_id"));
        h.answer_true = c.getInt(c.getColumnIndexOrThrow("answer_true"));

        return h;
    }

    public ContentValues getContentValues(){
        ContentValues v = new ContentValues();
        v.put("quizdate",quizdate);
        v.put("surah_name",surah_name);
        v.put("ayat_id",ayat_id);
        v.put("answer_true",answer_true);

        return v;
    }
}
