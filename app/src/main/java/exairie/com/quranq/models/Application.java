package exairie.com.quranq.models;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.support.v7.app.AlertDialog;
import android.text.format.DateFormat;
import android.util.Log;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;

import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import exairie.com.quranq.ExtActivity;
import exairie.com.quranq.R;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.services.OdojNotifier;
import exairie.com.quranq.services.ProfileDataUpdater;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

import static exairie.com.quranq.ExtActivity.server;

/**
 * Created by exain on 5/7/2017.
 */

public class Application extends MultiDexApplication{
    public static Context appContext;
    public interface CoinTransRequest{
        void transactionApplied(String token);
        void transactionRejected(String msg);
    }
    public interface CoinStatusListener{
        void CoinChanged(long coin);
    }

    public static List<CoinTransactionRecord> getCoinTransactionRecords() {
        return transactionRecords;
    }

    private static List<CoinTransactionRecord> transactionRecords = new ArrayList<>();
    private static List<CoinStatusListener> coinStatusListeners = new ArrayList<>();
    public static Application AppContext;
    public final static String REMOTE_CONFIG_UPDATED = "exairie.com.quranq.REMOTE_CONFIG_UPDATED";

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
        Configs.init(appContext);
        Fresco.initialize(this);

        FirebaseApp.initializeApp(this);
        fetchRemoteConfig();

//        Intent iSync = new Intent(this, ProfileDataUpdater.class);
//        startService(iSync);

//        Intent iOdoj = new Intent(this, OdojNotifier.class);
//        startService(iOdoj);



    }
    public static void fetchRemoteConfig(){
        FirebaseRemoteConfig rm = FirebaseRemoteConfig.getInstance();

        rm.setDefaults(R.xml.firebase_default);
        Log.d("Remote Config","Reloading");
        rm.fetch().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d("Remote Config","Reload Finish");
                if(task.isSuccessful()){
                    rm.activateFetched();
                    appContext.sendBroadcast(new Intent(REMOTE_CONFIG_UPDATED));
                }else
                {
                    Log.d("REMOTE CFG","ERROR " + task.getException().getMessage());
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d("Load Config Error",""+String.valueOf(e.getMessage()));
                e.printStackTrace();
            }
        });
    }

    public static void requestCoinPurchase(long amount, CoinTransRequest afterRequest){
        UserLogin user = Configs.getLoginInfo();
        if (user == null){
            afterRequest.transactionRejected("User not logged in!");
            return;
        }
        FormBody body = new FormBody.Builder()
                .add("amount",String.valueOf(amount))
                .add("id",user.id)
                .build();
        Request request = new Request.Builder()
                .post(body)
                .url(server("api/trans_request"))
                .header("origin", server(""))
                .build();
        ExtActivity.ApiCall(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    JSONObject json = new JSONObject(string);
                    String token = json.getJSONObject("data").getString("token");
                    afterRequest.transactionApplied(token);
                } catch (JSONException e) {
                    e.printStackTrace();
                    afterRequest.transactionRejected(e.getMessage());
                }

            }

            @Override
            public void OnFail(String msg) {
                afterRequest.transactionRejected(msg);
            }

            @Override
            public void Finish() {

            }
        },appContext);
//        val user = Configs.getLoginInfo()
//        val body = FormBody.Builder()
//                .add("amount",buildup_event.coin_cost.toString())
//                .add("id",user.id)
//                .build()
//        val request = Request.Builder()
//                .post(body)
//                .url(server("api/trans_request"))
//                .header("origin",server(""))
//                .build()
//        NetRequest(request, object : FinishedRequest{
//            override fun OnSuccess(response: Response?) {
//            }
//
//            override fun OnSuccess(string: String?) {
//                val result = Configs.mapper().readValue(string, RequestResult::class.java)
//                val json = JSONObject(string)
//                val token = json.getJSONObject("data").getString("token");
//                registerToken(token)
//            }
//
//            override fun OnFail(msg: String?) {
//            }
//
//            override fun Finish() {
//            }
//
//        })
    }
    public static void addListenerForCoinChange(CoinStatusListener listener){
        boolean immediateUpdate = false;
        if (coinStatusListeners.size() < 1) immediateUpdate = true;
        coinStatusListeners.add(listener);

        if (immediateUpdate) updateCoinStatus();
    }
    public static void removeListenerForCoinChange(CoinStatusListener listener){
        coinStatusListeners.remove(listener);
    }
    public static void updateCoinStatus(){
        UserLogin login = Configs.getLoginInfo();
        if (login == null){
            for (CoinStatusListener l : coinStatusListeners){
                l.CoinChanged(0);
            }
            return;
        }

        DBHelper db = new DBHelper(Configs.appContext);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM coin_trans",new String[]{});
        long coin = 0;
        if (c.getCount() > 0){
            transactionRecords.clear();
            c.moveToFirst();
            while (!c.isAfterLast()){
                CoinTransactionRecord rec = CoinTransactionRecord.fromCursor(c);
                transactionRecords.add(rec);
                coin += rec.coin_transaction;
                c.moveToNext();
            }
        }

        c.close();
        db.close();

        for (CoinStatusListener l : coinStatusListeners){
            l.CoinChanged(coin);
        }

        FormBody body = new FormBody.Builder()
                .add("id",login.id)
                .build();

        Request request = new Request.Builder()
                .url(server("api/coins"))
                .post(body)
                .build();

        ExtActivity.ApiCall(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    JSONObject o = new JSONObject(string);
                    double amount = o.getJSONObject("data").getJSONObject("amount").getDouble("coins");
                    CoinTransactionRecord[] records = Configs.mapper().readValue(o.getJSONObject("data").getString("trans"),CoinTransactionRecord[].class);


                    DBHelper helper = new DBHelper(Configs.appContext);
                    db.getWritableDatabase().delete("coin_trans","",new String[]{});
                    if (transactionRecords == null){
                        transactionRecords = new ArrayList<CoinTransactionRecord>();
                    }

                    transactionRecords.clear();


                    for (CoinTransactionRecord rec : records){
                        transactionRecords.add(rec);
                        ContentValues values = new ContentValues();
                        values.put("id",rec.id);
                        values.put("user_id",rec.user_id);
                        values.put("coin_transaction",rec.coin_transaction);
                        values.put("transaction_date",rec.transaction_date);
                        values.put("remark",rec.remark);

                        helper.getWritableDatabase().replace("coin_trans",null,values);

                    }

                    helper.close();

                    for (CoinStatusListener l : coinStatusListeners){
                        l.CoinChanged((long)amount);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (JsonParseException e) {
                    e.printStackTrace();
                } catch (JsonMappingException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void OnFail(String msg) {

            }

            @Override
            public void Finish() {

            }
        },appContext);
    }
}
