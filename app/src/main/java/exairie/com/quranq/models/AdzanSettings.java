package exairie.com.quranq.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 1/10/2018.
 */

public class AdzanSettings {
    @JsonProperty("subuh")
    public int subuh;
    @JsonProperty("dzuhur")
    public int dzuhur;
    @JsonProperty("ashar")
    public int ashar;
    @JsonProperty("maghrib")
    public int maghrib;
    @JsonProperty("isya")
    public int isya;
}
