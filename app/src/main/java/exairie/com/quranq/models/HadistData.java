package exairie.com.quranq.models;

public class HadistData {
        public String labelRiwayat;
        public String jumlahBab;
        public HadistData(){}

        public HadistData(String labelRiwayat, String jumlahBab) {
            this.labelRiwayat = labelRiwayat;
            this.jumlahBab = jumlahBab;
        }
    }