package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 10/15/2017.
 */

public class Fahmil {
    public static class Questions {
        public long id;
        public long bab_id;
        public String question;
        public String reference;
        public String reference_quran;
        public long added;
        public Answer[] answers;
    }
    public static class Answer {
        @JsonProperty("id")
        public long id;
        public long question_id;
        public String answer;
        public int answer_true;
        public long added;


        public static Answer fromQuizResult(Cursor c){
            Answer a = new Answer();
            a.id = c.getLong(c.getColumnIndexOrThrow("id"));
            a.question = c.getString(c.getColumnIndexOrThrow("question"));
            a._answer_true = c.getString(c.getColumnIndexOrThrow("answer_true"));
            a._answer_false = c.getString(c.getColumnIndexOrThrow("answer_false"));
            a.session_start = c.getString(c.getColumnIndexOrThrow("session_start"));
            a.session_end = c.getString(c.getColumnIndexOrThrow("session_end"));

            return a;

        }
        @JsonProperty("question")
        public String question;
        @JsonProperty("_answer_true")
        public String _answer_true;
        @JsonProperty("_answer_false")
        public String _answer_false;
        @JsonProperty("session_start")
        public String session_start;
        @JsonProperty("session_end")
        public String session_end;
    }
//    " id integer primary key," +
//            " question varchar(255)," +
//            " answer_true varchar(255)," +
//            " answer_false varchar(255)," +
//            " session_start integer," +
//            " session_end integer" +
}
