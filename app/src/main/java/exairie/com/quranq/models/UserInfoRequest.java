package exairie.com.quranq.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 9/23/2017.
 */

public class UserInfoRequest extends RequestResult {
    @JsonProperty("info")
    public UserInfoCheck info;
}
