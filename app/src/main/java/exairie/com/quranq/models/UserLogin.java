package exairie.com.quranq.models;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 9/23/2017.
 */

public class UserLogin {
    @JsonProperty("id")
    public String id;
    @JsonProperty("login_type")
    public String login_type;
    @JsonProperty("username")
    public String username;
    @JsonProperty("password")
    public String password;
    @JsonProperty("firstname")
    public String firstname;
    @JsonProperty("lastname")
    public String lastname;
    @JsonProperty("phone")
    public String phone;
    @JsonProperty("email")
    public String email;
    @JsonProperty("gender")
    public String gender;
    @JsonProperty("fcm_token")
    public String fcm_token;
    @JsonProperty("login_token")
    public String login_token;
    @JsonProperty("last_seen")
    public long last_seen;
    @JsonProperty("photo_url")
    public String photo_url;
}
