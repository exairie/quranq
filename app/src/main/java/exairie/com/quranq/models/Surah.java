package exairie.com.quranq.models;

import android.database.Cursor;

/**
 * Created by exain on 5/7/2017.
 */

public class Surah {

    private int id;
    private int surah_no;
    private String surah_name;
    private String surah_name_arabic;
    private String meaning;
    private int num_of_ayat;
    private String in_city;
    private int juzFrom;
    private int juzTo;



    public static Surah createFromCursor(Cursor cursor){
        Surah a = new Surah();
        a.setId(cursor.getInt(0));
        a.setSurah_no(cursor.getInt(1));
        a.setSurah_name(cursor.getString(2));
        a.setSurah_name_arabic(cursor.getString(3));
        a.setMeaning(cursor.getString(4));
        a.setNum_of_ayat(cursor.getInt(5));
        a.setIn_city(cursor.getString(6));
        return a;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSurah_no() {
        return surah_no;
    }

    public void setSurah_no(int surah_no) {
        this.surah_no = surah_no;
    }

    public String getSurah_name() {
        return surah_name;
    }

    public void setSurah_name(String surah_name) {
        this.surah_name = surah_name;
    }

    public String getSurah_name_arabic() {
        return surah_name_arabic;
    }

    public void setSurah_name_arabic(String surah_name_arabic) {
        this.surah_name_arabic = surah_name_arabic;
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }

    public int getNum_of_ayat() {
        return num_of_ayat;
    }

    public void setNum_of_ayat(int num_of_ayat) {
        this.num_of_ayat = num_of_ayat;
    }

    public String getIn_city() {
        return in_city;
    }

    public void setIn_city(String in_city) {
        this.in_city = in_city;
    }
    public int getJuzFrom() {
        return juzFrom;
    }

    public void setJuzFrom(int juzFrom) {
        this.juzFrom = juzFrom;
    }

    public int getJuzTo() {
        return juzTo;
    }

    public void setJuzTo(int juzTo) {
        this.juzTo = juzTo;
    }
}
