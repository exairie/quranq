package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 7/17/2017.
 */

public class Guru {
    @JsonProperty("id")
    public long id;
    @JsonProperty("nama_guru")
    public String nama_guru;
    @JsonProperty("phone")
    public String phone;
    @JsonProperty("location_lat")
    public double location_lat;
    @JsonProperty("location_long")
    public double location_long;
    @JsonProperty("address")
    public String address;

    public static Guru create(Cursor c){
        Guru g = new Guru();
        g.id = c.getLong(c.getColumnIndexOrThrow("id"));
        g.nama_guru = c.getString(c.getColumnIndexOrThrow("nama_guru"));
        g.phone = c.getString(c.getColumnIndexOrThrow("phone"));
        g.location_lat = c.getDouble(c.getColumnIndexOrThrow("location_lat"));
        g.location_long = c.getDouble(c.getColumnIndexOrThrow("location_long"));
        g.address = c.getString(c.getColumnIndexOrThrow("address"));
        return g;
    }
}
