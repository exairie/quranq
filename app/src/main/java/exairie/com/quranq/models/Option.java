package exairie.com.quranq.models;

import android.database.Cursor;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * Created by exain on 5/17/2017.
 */

public class Option {
    public static String KEY_LAST_READ = "quran_last_read";
    @JsonProperty("key")
    String key;
    @JsonProperty("value")
    String value;

    public Option() {
    }

    public Option(String key, String value) {

        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Option fromCursor(Cursor c){
        return new Option(c.getString(1),c.getString(2));
    }
}
