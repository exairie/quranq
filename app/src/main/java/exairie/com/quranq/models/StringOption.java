package exairie.com.quranq.models;

/**
 * Created by exain on 10/10/2017.
 */

public class StringOption<T> extends StringWithID {
    public T optionValue;

    public T getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(T optionValue) {
        this.optionValue = optionValue;
    }

    public StringOption(int id, String string, T optionValue) {
        super(id, string);
        this.setOptionValue(optionValue);
    }
}
