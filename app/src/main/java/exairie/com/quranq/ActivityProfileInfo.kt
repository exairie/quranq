package exairie.com.quranq

import android.app.AlertDialog
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.Toast
import com.crashlytics.android.Crashlytics
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.reward.RewardItem
import com.google.android.gms.ads.reward.RewardedVideoAd
import com.google.android.gms.ads.reward.RewardedVideoAdListener
import com.google.firebase.messaging.FirebaseMessaging
import com.squareup.picasso.Picasso
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Application
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.UserLogin
import exairie.com.quranq.models.UserLoginRequest
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_profile_info.*
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject
import java.io.IOException

class ActivityProfileInfo : ExtActivity(R.layout.activity_profile_info), RewardedVideoAdListener {
    lateinit var videoReward : RewardedVideoAd
    override fun onRewardedVideoAdClosed() = Unit

    override fun onRewardedVideoAdLeftApplication() = Unit

    override fun onRewardedVideoAdLoaded(){
        btn_ad_coin.visibility = View.VISIBLE

        l_ad_availability.text = "Click Here"
        img_add_availability.setImageResource(R.drawable.ic_add_circle_amber_500_24dp)
        btn_ad_coin.setOnClickListener(View.OnClickListener {
            videoReward.show()
        })
    }

    override fun onRewardedVideoAdOpened() {
        btn_ad_coin.setOnClickListener(null)
        l_ad_availability.text = "Coin Added"
        img_add_availability.setImageResource(R.drawable.ic_add_circle_amber_500_24dp)
    }

    override fun onRewarded(p0: RewardItem?) {
        val amount = p0!!.amount.toLong()
        Application.requestCoinPurchase(amount,object : Application.CoinTransRequest{
            override fun transactionApplied(token: String?) {
                rewardCoin(amount,token!!)
            }

            override fun transactionRejected(msg: String?) {

            }
        })
    }

    override fun onRewardedVideoStarted() = Unit

    override fun onRewardedVideoAdFailedToLoad(p0: Int) = Unit

    fun rewardCoin(amount : Long, token : String){
        val user = Configs.getLoginInfo()

        val body = FormBody.Builder()
                .add("id",user.id)
                .add("token",token)
                .add("amount",amount.toString())
                .add("remark", "Watching video Ad")
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/coin_trans"))
                .build()
        NetRequest(request,object : FinishedRequest {
            override fun OnSuccess(response: Response?) {

            }

            override fun OnSuccess(string: String?) {
                val obj = JSONObject(string)
                val trans_id = obj.getJSONObject("data").getString("trans_id")
                runOnUiThread({Application.updateCoinStatus()})
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage("+ $amount Coin added to your account!")
                            .setNegativeButton("Close",{d,_->d.dismiss()})
                            .show()

                })
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",{d,_->d.dismiss()})
                            .setPositiveButton("Retry",{d,_->
                                rewardCoin(amount,token)
                                d.dismiss()
                            })
                            .show()

                })
            }

            override fun Finish() {
            }

        })
    }
    lateinit var loginInfo : UserLogin
    val coinChangeList = Application.CoinStatusListener { coin -> l_coin.setText("$coin Koin") }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setupLoginInfo()
        Application.addListenerForCoinChange(coinChangeList)

        Application.updateCoinStatus()

        MobileAds.initialize(thisContext(),getString(R.string.ad_id))

        videoReward = MobileAds.getRewardedVideoAdInstance(thisContext())
        videoReward.rewardedVideoAdListener = this@ActivityProfileInfo
        val request = AdRequest.Builder()
                .addTestDevice("EB1DBC5F40E284EB568134257ACFC0EE")
                .addTestDevice("116C295249E121532EE8532440F7B5BD")
                .addTestDevice("116C295249E121532EE8532440F7B5BD")
                .build()
        videoReward.loadAd(getString(R.string.ad_unit_video_coin),request)
        l_ad_availability.text = "Loading ads..."
        img_add_availability.setImageResource(R.drawable.ic_add_circle_grey_600_24dp)

        l_progressbar.visibility = View.GONE

    }

    private fun setupLoginInfo() {
        loginInfo = Configs.getLoginInfo()
        if (loginInfo == null){
            Crashlytics.log("Attempt to open profile info without login credential")
            finish()
            return
        }

        if(loginInfo.photo_url != ""){
            Picasso.with(thisContext())
                    .load(loginInfo.photo_url)
                    .placeholder(R.drawable.ic_person_grey_900_24dp)
                    .into(img_profilepic)
        }
        l_fullname.setText("${loginInfo.firstname} ${loginInfo.lastname}")
        l_email.setText(loginInfo.email)
        t_firstname.setText(loginInfo.firstname)
        t_lastname.setText(loginInfo.lastname)
        l_username.setText(loginInfo.username)
        t_phone.setText(loginInfo.phone)

        btn_save.setOnClickListener({
            updateData()
        })
        if(loginInfo.login_type != "internal")
        {
            t_firstname.isEnabled = false
            t_lastname.isEnabled = false
            t_phone.isEnabled = false
            btn_save.visibility = View.GONE
            l_info_google_login.visibility = View.VISIBLE
        }
        else{
            l_info_google_login.visibility = View.GONE
        }
    }

    override fun onDestroy() {
        Application.removeListenerForCoinChange(coinChangeList)
        super.onDestroy()
    }

    fun updateData(){
        runOnUiThread{
            l_progressbar.visibility = View.VISIBLE
            btn_save.visibility = View.GONE
        }
        val firstname = t_firstname.text.toString()
        val lastname = t_lastname.text.toString()
        val phone = t_phone.text.toString()

        val body = FormBody.Builder()
                .add("firstname",firstname)
                .add("lastname",lastname)
                .add("phone",phone)
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/profile"))
                .build()
        NetRequest(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) = Unit

            override fun OnSuccess(string: String?) = requestUserData()

            override fun OnFail(msg: String?){
                runOnUiThread{
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .show()
                    l_progressbar.visibility = View.GONE
                    btn_save.visibility = View.VISIBLE
                }
            }

            override fun Finish() = Unit
        })
    }

    private fun requestUserData() {
        runOnUiThread{
            l_progressbar.visibility = View.VISIBLE
            btn_save.visibility = View.GONE
        }
        val body = FormBody.Builder()
                .build()
        val request = Request.Builder()
                .post(body)
                .url(ExtActivity.server("auth/userdata"))
                .build()

        NetRequest(request, object : FinishedRequest {
            override fun OnSuccess(response: Response) = Unit

            override fun OnSuccess(string: String) = try {
                val req = Configs.mapper().readValue(string, UserLoginRequest::class.java)
                if (req.status == "ok") {
                    Configs.setLoginInfo(req.loginInfo)

                    FirebaseMessaging.getInstance().subscribeToTopic("event_notifications")
                    FirebaseMessaging.getInstance().subscribeToTopic("notification_" + req.loginInfo.id)

                    setupLoginInfo()
                }else{
                    Toast.makeText(thisContext(),"Unable to load user profile data",Toast.LENGTH_LONG).show()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

            override fun OnFail(msg: String) {

            }

            override fun Finish() {
                runOnUiThread{
                    l_progressbar.visibility = View.GONE
                    btn_save.visibility = View.VISIBLE
                }
            }
        })
    }

}
