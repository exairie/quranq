package exairie.com.quranq;

import android.content.Intent;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.Toast;

import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.UserLogin;

public class ActivitySplash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window w = getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            w.setStatusBarColor(ContextCompat.getColor(this, R.color.Transparent));
        }
        UserLogin login = Configs.getLoginInfo();

        new Handler().postDelayed(() -> {
            if (login == null) {
                Intent i = new Intent(this, ActivityLogin.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(this, ActivityDashboard.class);
                startActivity(i);
                finish();
            }
        }, 2000);
    }
}
