package exairie.com.quranq

import android.Manifest.permission.READ_EXTERNAL_STORAGE
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.ProgressDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.location.Geocoder
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.EditText
import android.widget.Toast
import com.github.jjobes.slidedatetimepicker.SlideDateTimeListener
import com.github.jjobes.slidedatetimepicker.SlideDateTimePicker
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.sleepbot.datetimepicker.time.TimePickerDialog
import com.squareup.picasso.Picasso
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.*
import kotlinx.android.synthetic.main.activity_start_event.*
import kotlinx.android.synthetic.main.view_img_thumb.view.*
import okhttp3.*
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.URI
import java.net.URLDecoder
import java.util.*
import java.util.Calendar.*

class ActivityStartEvent : ExtActivity(R.layout.activity_start_event) {
    var intentUploadData : Intent? = null
    class ObjectMap(id : Long, value : String){

        var id : Long = 0
        var value : String? = null
        init {
            this.id = id
            this.value = value
        }
    }
    companion object {
        val PICK_IMAGE: Int = 1
    }
    var buildup_event : Event
    val start_calendar : Calendar
    val end_calendar : Calendar
    var coin_per_km : Long
    val receiver: BroadcastReceiver
    lateinit var dialog : ProgressDialog
    init {
        val cfg = FirebaseRemoteConfig.getInstance()
        receiver = object : BroadcastReceiver(){
            override fun onReceive(p0: Context?, p1: Intent?) {
                coin_per_km = cfg.getLong("cost_coin_per_km")
            }
        }
        coin_per_km = cfg.getLong("cost_coin_per_km")
        start_calendar = Calendar.getInstance()
        end_calendar = Calendar.getInstance()
        buildup_event = Event()
    }

    lateinit var picAdapter: PicAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dialog = ProgressDialog(thisContext())

        page_detail.visibility = View.VISIBLE
        page_promote.visibility = View.GONE
        page_summary.visibility = View.GONE
        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)

        picAdapter = PicAdapter()

        recycler_photo.adapter = picAdapter
        recycler_photo.layoutManager = LinearLayoutManager(thisContext())

        setupEvents()
    }

    override fun onStart() {
        super.onStart()
        try {
            registerReceiver(receiver, IntentFilter(Application.REMOTE_CONFIG_UPDATED))
        }catch (e : Exception){
            e.printStackTrace()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        try{
            unregisterReceiver(receiver)
        }catch (e : Exception){
            e.printStackTrace()
        }
    }
    fun setupEvents(){
        t_notification_radius.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
                if (p0!!.length < 1) return

                var km : Long
                try {
                    km = p0.toString().toLong()
                    Log.d("KM","KM : "+km)
                }catch (e : Exception){
                    e.printStackTrace()
                    km = 0
                }

                l_coin_cost.setText(String.format("%d Koin (%d Koin/km)",(coin_per_km * km),coin_per_km))
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

        })
        chk_allowpromote.setOnCheckedChangeListener({ compoundButton, _ ->
            run {
                layout_promote_numofcoins.visibility = if (compoundButton!!.isChecked) View.VISIBLE else View.GONE
                layout_promote_coincost.visibility = if (compoundButton.isChecked) View.VISIBLE else View.GONE
                l_currentloc.visibility = if (compoundButton.isChecked) View.VISIBLE else View.GONE

                val geocoder = Geocoder(thisContext(), Locale.getDefault())
                val last_lat = Configs.getConfiguration("last_latitude")
                val last_long = Configs.getConfiguration("last_longitude")
                try {
                    buildup_event.latitude = last_lat.value.toDouble()
                    buildup_event.longitude = last_long.value.toDouble()
                    val addresses = geocoder.getFromLocation(last_lat.value.toDouble(), last_long.value.toDouble(),1)
                    if (addresses.size > 0) {
                        val city: String
                        if (addresses[0].locality != null) {
                            city = addresses[0].locality
                        } else if (addresses[0].adminArea != null) {
                            city = addresses[0].adminArea
                        } else {
                            city = String.format("Dari Koordinat %s, %s", last_lat.value, last_long.value)
                        }
                        //runOnUiThread(() -> lCityname.setText(city));
                        l_currentloc.setText(String.format("Dari Lokasi anda (%s)",city))
                        val lastpos = Option("last_loc", city)
                        Configs.setConfiguration(lastpos)
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                    val city = String.format("Dari Koordinat %s, %s", last_lat.value, last_long.value)
                    l_currentloc.setText(city)
                } finally {
                }
            }
        })
        t_event_date_from.setOnFocusChangeListener { view, b ->
            if(b){
                val calendar = Calendar.getInstance()
                val newCalendar = Calendar.getInstance()
                newCalendar.set(HOUR_OF_DAY,12)
                newCalendar.set(MINUTE,0)
                val datePicker = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance({ datePickerDialog, y, m, d ->
                    newCalendar.set(YEAR,y)
                    newCalendar.set(MONTH,m)
                    newCalendar.set(DAY_OF_MONTH,d)

                    start_calendar.timeInMillis = newCalendar.timeInMillis

                    (view as EditText).setText(DateFormat.format("dd MMM yyyy HH:mm", newCalendar.timeInMillis))

                    TimePickerDialog.newInstance({ radialPickerLayout, h, min ->
                        newCalendar.set(HOUR_OF_DAY,h)
                        newCalendar.set(MINUTE,min)

                        start_calendar.timeInMillis = newCalendar.timeInMillis

                        view.setText(DateFormat.format("dd MMM yyyy HH:mm", newCalendar.timeInMillis))
                    },calendar.get(HOUR_OF_DAY),calendar.get(MINUTE),true).show(supportFragmentManager,"TIMEPICKER_FROM")

                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH))
                datePicker.show(supportFragmentManager,"DATEPICKER_FROM")
            }
        }
        t_event_date_to.setOnFocusChangeListener { view, b ->
            if(b){
                val calendar = Calendar.getInstance()
                val newCalendar = Calendar.getInstance()
                newCalendar.set(HOUR_OF_DAY,12)
                newCalendar.set(MINUTE,0)
                val datePicker = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance({ datePickerDialog, y, m, d ->
                    newCalendar.set(YEAR,y)
                    newCalendar.set(MONTH,m)
                    newCalendar.set(DAY_OF_MONTH,d)

                    end_calendar.timeInMillis = newCalendar.timeInMillis
                    (view as EditText).setText(DateFormat.format("dd MMM yyyy HH:mm", newCalendar.timeInMillis))

                    TimePickerDialog.newInstance({ radialPickerLayout, h, m ->
                        newCalendar.set(HOUR_OF_DAY,h)
                        newCalendar.set(MINUTE,m)

                        end_calendar.timeInMillis = newCalendar.timeInMillis

                        view.setText(DateFormat.format("dd MMM yyyy HH:mm", newCalendar.timeInMillis))
                    },calendar.get(HOUR_OF_DAY),calendar.get(MINUTE),true).show(supportFragmentManager,"TIMEPICKER_TO")

                },calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH))
                datePicker.show(supportFragmentManager,"DATEPICKER_TO")
            }
        }
        btn_detail_finish.setOnClickListener({
            if(t_event_name.text.length < 5){
                AlertDialog.Builder(thisContext())
                        .setMessage("Nama event harus diisi dan minimal terdiri dari 5 huruf!")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            t_event_name.requestFocus()
                            t_event_name.setTextColor(resources.getColor(R.color.red_darken_3))
                            Handler().postDelayed(Runnable { t_event_name.setTextColor(resources.getColor(R.color.Black)) },3000)
                        }).show()
                return@setOnClickListener
            }
            if(t_event_description.text.length < 50){
                AlertDialog.Builder(thisContext())
                        .setMessage("Deskripsi event harus diisi dan minimal terdiri dari 50 huruf!")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            t_event_description.requestFocus()
                            t_event_description.setTextColor(resources.getColor(R.color.red_darken_3))
                            Handler().postDelayed(Runnable { t_event_description.setTextColor(resources.getColor(R.color.Black)) },3000)
                        }).show()
                return@setOnClickListener
            }
            if(t_event_contactperson.text.length < 10){
                AlertDialog.Builder(thisContext())
                        .setMessage("Contact Person harus diisi dan minimal terdiri dari 10 huruf!")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            t_event_contactperson.requestFocus()
                            t_event_contactperson.setTextColor(resources.getColor(R.color.red_darken_3))
                            Handler().postDelayed(Runnable { t_event_contactperson.setTextColor(resources.getColor(R.color.Black)) },3000)
                        }).show()
                return@setOnClickListener
            }
            if(t_event_date_from.text.length < 1){
                AlertDialog.Builder(thisContext())
                        .setMessage("Tanggal mulai harus diisi!")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            t_event_date_from.requestFocus()
                            t_event_date_from.setTextColor(resources.getColor(R.color.red_darken_3))
                            Handler().postDelayed(Runnable { t_event_date_from.setTextColor(resources.getColor(R.color.Black)) },3000)
                        }).show()
                return@setOnClickListener
            }
            if(t_event_date_to.text.length < 10){
                AlertDialog.Builder(thisContext())
                        .setMessage("Tanggal akhir harus diisi!")
                        .setPositiveButton("OK", DialogInterface.OnClickListener { dialogInterface, i ->
                            dialogInterface.dismiss()
                            t_event_date_to.requestFocus()
                            t_event_date_to.setTextColor(resources.getColor(R.color.red_darken_3))
                            Handler().postDelayed(Runnable { t_event_date_to.setTextColor(resources.getColor(R.color.Black)) },3000)
                        }).show()
                return@setOnClickListener
            }

            var cap: Int
            try {
                cap = Integer.parseInt(t_event_capacity.text.toString())
            }catch (e : Exception){
                Toast.makeText(thisContext(),"Masukkan kapasitas event dengan benar!",Toast.LENGTH_LONG).show()
                return@setOnClickListener
            }

            buildup_event.nama_event = t_event_name.text.toString()
            buildup_event.capacity = cap
            buildup_event.contact_person = t_event_contactperson.text.toString()
            buildup_event.date_start = start_calendar.timeInMillis
            buildup_event.date_end = start_calendar.timeInMillis
            buildup_event.deskripsi = t_event_description.text.toString()
            buildup_event.contact_person = t_event_contactperson.text.toString()

            page_promote.visibility = View.VISIBLE
            page_detail.visibility = View.GONE

            t_notification_radius.setText("5")

            flow_detail.setImageResource(R.drawable.circle_bg_green)
            detail_line.setBackgroundColor(resources.getColor(R.color.green_darken_3))
        })
        btn_promote_finish.setOnClickListener(View.OnClickListener {
            if(chk_allowpromote.isChecked){

                var km : Long
                try {
                    km = t_notification_radius.text.toString().toLong()
                    Log.d("KM","KM : "+km)
                }catch (e : Exception){
                    e.printStackTrace()
                    km = 0
                }
                buildup_event.notification_radius = km.toDouble()
                buildup_event.coin_cost = (coin_per_km * km) * -1
            }else{
                buildup_event.notification_radius = -1.0
            }

            page_promote.visibility = View.GONE
            page_summary.visibility = View.VISIBLE

            flow_promote.setImageResource(R.drawable.circle_bg_green)
            flow_promote_advance.setBackgroundColor(resources.getColor(R.color.green_darken_3))

            moveToSummary()
        })
        btn_start_publish.setOnClickListener({publishEvent()})
        btn_addphoto.setOnClickListener({
            uploadPhoto()
        })
    }



    private fun uploadPhoto() {
        val chooser = Intent()
        chooser.setType("image/*")
        chooser.setAction(Intent.ACTION_GET_CONTENT)
        startActivityForResult(Intent.createChooser(chooser,"Pilih poster/brosur/foto event"),PICK_IMAGE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE){
            uploadStart(data)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when(requestCode)
        {
            132 -> {
                if(grantResults[0] == PERMISSION_GRANTED){
                    reinitUpload(intentUploadData)
                }else
                {
                    runOnUiThread {
                        AlertDialog.Builder(thisContext())
                                .setMessage("Please allow this app to read external storage!")
                                .show()
                    }
                }
            }

        }
    }
    fun reinitUpload(data : Intent?){
        uploadStart(data)
    }
    fun uploadStart(data : Intent?) = if(data != null)
    {
        run {
            intentUploadData = data
            if (ContextCompat.checkSelfPermission(thisContext(), READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, arrayOf(READ_EXTERNAL_STORAGE), 132)

                return
            }
            val dialog = ProgressDialog(thisContext())
            runOnUiThread({
                dialog.setCancelable(false)
                dialog.setMessage("Mengupload")
                dialog.show()
            })
            val uri = data.data
            val decodedURI = URLDecoder.decode(uri.path)
            var file : File? = File(getPath(thisContext(),uri))
//            if(decodedURI.matches(Regex(".*\\.(jpg|png|jpeg|gif)"))){
//                file = File(decodedURI)
//            }else if(decodedURI.matches(Regex(".*:[0-9]*"))){
//                file = File(getPathFromURI(uri))
//            }else{
//                Toast.makeText(thisContext(),"Tidak dapat memuat gambar!",Toast.LENGTH_LONG).show()
//                dialog.dismiss()
//                return
//            }
            Log.d("UPLOAD", "Get file")
            val bmp = MediaStore.Images.Media.getBitmap(contentResolver, uri)
            val type = contentResolver.getType(uri)
            val ext = MimeTypeMap.getSingleton().getExtensionFromMimeType(type)
            val body = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart("file", file!!.name, RequestBody.create(MediaType.parse(type), file))
                    .build()
            val request = Request.Builder()
                    .post(body)
                    .url(server("api/event_uploadpicture"))
                    .build()
            Log.d("UPLOAD", "Recycling")
            bmp.recycle()
            NetRequest(request, object : FinishedRequest {
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val json = JSONObject(string).getJSONObject("data")
                    picAdapter.addPicture(json.getString("file"), json.getLong("id"))
                }

                override fun OnFail(msg: String?) {
                    runOnUiThread({
                        AlertDialog.Builder(thisContext())
                                .setMessage("Failed : " + msg)
                                .setPositiveButton("Retry", { c, _ ->
                                    c.dismiss()
                                    val ndata = data
                                    Handler().postDelayed({
                                        reinitUpload(ndata)
                                    }, 100)
                                }).show()
                    })
                }

                override fun Finish() {
                    dialog.dismiss()
                }

            })
        }
        Unit
    }else{
        Toast.makeText(thisContext(),"Please pick a valid image!",Toast.LENGTH_LONG).show()
    }
    fun publishEvent(){
        val userinfo = Configs.getLoginInfo()
        val body = FormBody.Builder()
                .add("id_user",userinfo.id.toString())
                .add("nama_event",buildup_event.nama_event)
                .add("deskripsi",buildup_event.deskripsi)
                .add("capacity",buildup_event.capacity.toString())
                .add("latitude",buildup_event.latitude.toString())
                .add("longitude",buildup_event.longitude.toString())
                .add("notification_radius",buildup_event.notification_radius.toString())
                .add("date_start",buildup_event.date_start.toString())
                .add("date_end",buildup_event.date_end.toString())
                .add("contact_person",buildup_event.contact_person)
                .add("status","0")
                .add("pictures",Configs.mapper().writeValueAsString(picAdapter.data.toTypedArray()))
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api2/event"))
                .build()

        dialog.setMessage("Mengirim data")
        dialog.setTitle("Pendaftaran Event")
        dialog.setCancelable(false)
        dialog.show()
        NetRequest(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                val result = Configs.mapper().readValue(string, RequestResult::class.java)
                if(result.status == "ok"){
                    val id = JSONObject(string)
                            .getJSONObject("data")
                            .getString("id")
                    buildup_event.id = id.toLong()
                     if(buildup_event.notification_radius > 0){
                         placePaymentForNotifications()
                         runOnUiThread({
                             dialog.setMessage("Mengirim pembayaran koin untuk notifikasi")
                         })
                     }else{
                         runOnUiThread({dialog.dismiss()})
                         runOnUiThread({
                             AlertDialog.Builder(thisContext())
                                     .setMessage("Event telah berhasil didaftarkan!")
                                     .setPositiveButton("Tutup",{d,_->
                                         d.dismiss()
                                         this@ActivityStartEvent.finish()
                                     })
                                     .show()
                         })
                     }
                }
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({dialog.dismiss()})
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage("Error : " + msg)
                            .setNegativeButton("Close", DialogInterface.OnClickListener { dialogInterface, i ->
                                dialogInterface.dismiss()
                            })
                            .setPositiveButton("Retry",{d,_->
                                d.dismiss()
                                publishEvent()
                            })
                            .show()
                })

            }

            override fun Finish() {
            }

        })

    }

    private fun placePaymentForNotifications() {
        Application.requestCoinPurchase(buildup_event.coin_cost,object : Application.CoinTransRequest{
            override fun transactionApplied(token: String?) {
                runOnUiThread({dialog.setMessage("Transaksi valid, melakukan pembayaran koin")})
                applyPurchase(token)
            }

            override fun transactionRejected(msg: String?) {
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",{d, i -> d.dismiss()})
                            .show()
                })
                runOnUiThread({dialog.dismiss()})
            }

        })
    }

    private fun applyPurchase(token: String?) {
        val user = Configs.getLoginInfo()

        val body = FormBody.Builder()
                .add("id",user.id)
                .add("token",token)
                .add("amount",buildup_event.coin_cost.toString())
                .add("remark", String.format("Create event notification radius %.0f, %s",
                        buildup_event.notification_radius,
                        DateFormat.format("yyyy-MMM-dd HH:mm:ss",buildup_event.date_create)))
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/coin_trans"))
                .build()
        NetRequest(request,object : FinishedRequest{
            override fun OnSuccess(response: Response?) {

            }

            override fun OnSuccess(string: String?) {
                val obj = JSONObject(string)
                val trans_id = obj.getJSONObject("data").getString("trans_id")

                sendNotification(trans_id)
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",{d,_->d.dismiss()})
                            .setPositiveButton("Retry",{d,_->
                                d.dismiss()
                                applyPurchase(token)
                            })
                            .show()

                })
                runOnUiThread({dialog.dismiss()})
            }

            override fun Finish() {
            }

        })
    }

    private fun sendNotification(trans_id: String?) {
        runOnUiThread({dialog.setMessage("Sending Notifications...")})
        val body = FormBody.Builder()
                .add("trans_id",trans_id)
                .add("notification_radius",buildup_event.notification_radius.toString())
                .add("event_id",buildup_event.id.toString())
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/event_notif"))
                .build()
        NetRequest(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
            }

            override fun OnSuccess(string: String?) {
                runOnUiThread({dialog.dismiss()})
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage("Event telah berhasil didaftarkan!")
                            .setPositiveButton("Tutup",{d,_->d.dismiss()})
                            .show()
                })
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage("Gagal mengirim notifikasi!")
                            .setPositiveButton("Retry",{d,_->
                                sendNotification(trans_id)
                            })
                            .setNegativeButton("Tutup",{d,_->
                                d.dismiss()
                            })
                            .show()
                })
            }

            override fun Finish() {
            }

        })
    }

    override fun onBackPressed() {
        if(page_summary.visibility == View.VISIBLE){
            page_promote.visibility = View.VISIBLE
            page_summary.visibility = View.GONE
            flow_promote_advance.setBackgroundColor(resources.getColor(R.color.White))
            flow_promote.setImageResource(R.drawable.circle_bg)
            return
        }
        if (page_promote.visibility == View.VISIBLE){
            page_detail.visibility = View.VISIBLE
            page_promote.visibility = View.GONE
            detail_line.setBackgroundColor(resources.getColor(R.color.White))
            flow_detail.setImageResource(R.drawable.circle_bg)
            return
        }
        super.onBackPressed()

    }
    fun moveToSummary(){
        val loginInfo = Configs.getLoginInfo();
        val date_from = DateFormat.format("dd-MMM-yyyy HH:mm:ss",buildup_event.date_start)
        val date_to = DateFormat.format("dd-MMM-yyyy HH:mm:ss",buildup_event.date_end)
        l_sum_date.setText(String.format("%s s/d\n%s",date_from,date_to))
        l_sum_eventname.setText(buildup_event.nama_event)
        l_sum_user.setText(String.format("%s %s",loginInfo!!.firstname,loginInfo.lastname))
        l_sum_desc.setText(buildup_event.deskripsi)

        if(buildup_event.notification_radius >= 0){
            chk_sum_notifications.setText(String.format("Kirim notifikasi ke setiap pengguna dalam radius %.0f km",buildup_event.notification_radius))
            chk_sum_notifications.isChecked = true
        }else{
            chk_sum_notifications.setText("Tidak mengirimkan notifikasi")
            chk_sum_notifications.isChecked = false
        }
    }
    class Holder (v : View) : RecyclerView.ViewHolder(v){

    }
    inner class PicAdapter : RecyclerView.Adapter<Holder>(){
        val data : MutableList<ObjectMap>
        init {
            Picasso.with(thisContext()).isLoggingEnabled = true
            Picasso.with(thisContext()).setIndicatorsEnabled(true)
            data = mutableListOf()
        }
        fun addPicture(url : String, id : Long){
            data.add(ObjectMap(id,SERVER_URL + "/assets/uploads/" + url))
            runOnUiThread({notifyDataSetChanged()})
        }
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val view = LayoutInflater.from(p0!!.context)
                    .inflate(R.layout.view_img_thumb,p0,false)
            return Holder(view)
        }

        override fun onBindViewHolder(p0: Holder, p1: Int) {
            Picasso.with(thisContext())
                    .load(data.get(p1).value)
                    .resize(500,500)
                    .placeholder(R.mipmap.logoapp)
                    .into(p0!!.itemView.image)

        }

        override fun getItemCount(): Int  = data.size

    }
}
