package exairie.com.quranq


import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Intent
import android.location.Geocoder
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Option
import exairie.com.quranq.services.DataLoader
import kotlinx.android.synthetic.main.fragment_fragment_registered_events.view.*
import kotlinx.android.synthetic.main.view_event_organizing.view.*
import kotlinx.android.synthetic.main.view_event_registered.view.*
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.launch
import org.json.JSONObject
import java.util.*


/**
 * A simple [Fragment] subclass.
 */
class FragmentRegisteredEvents() : Fragment() {
    lateinit var mView : View

    var fragmentType = "MYEVENT"

    lateinit var upcomingAdapter : EventAdapter
    lateinit var finishedAdapter: EventAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val v = inflater!!.inflate(R.layout.fragment_fragment_registered_events, container, false)
        mView = v
        initComponents()
        mView.recycler_finished_events.isNestedScrollingEnabled = false
        mView.recycler_upcoming_events.isNestedScrollingEnabled = false
        return v
    }
    @SuppressLint("ValidFragment")
    constructor(type : String) : this(){
        fragmentType = type
    }

    private fun initComponents() {
        mView.content_main.visibility = View.GONE
        mView.content_preloader.visibility = View.VISIBLE

        if(fragmentType == "MYEVENT"){
            DataLoader.getCurrentRegisteredEvent(object : DataLoader.LoadResult<JSONObject?>{
                override fun finish(result: JSONObject?): Unit = if(result != null){
                    val upcomingData = mutableListOf<JSONObject>()
                    val finishedData = mutableListOf<JSONObject>()
                    val currenttime = System.currentTimeMillis()

                    val array = result.getJSONArray("data")
                    for(i in 0 until array.length()){
                        var eventObj = array.getJSONObject(i)
                        if(eventObj.optLong("date_start") < currenttime){
                            finishedData.add(eventObj)
                        }else{
                            upcomingData.add(eventObj)
                        }
                    }

                    upcomingAdapter = EventAdapter(upcomingData)
                    finishedAdapter = EventAdapter(finishedData)



                    activity?.runOnUiThread({
                        mView.content_main.visibility = View.VISIBLE
                        mView.content_preloader.visibility = View.GONE

                        mView.recycler_upcoming_events.adapter = upcomingAdapter
                        mView.recycler_finished_events.adapter = finishedAdapter

                        mView.recycler_upcoming_events.layoutManager = LinearLayoutManager(activity)
                        mView.recycler_finished_events.layoutManager = LinearLayoutManager(activity)

                        mView.l_no_upcoming_event.visibility = if(upcomingData.size < 1) View.VISIBLE else View.GONE
                        mView.l_no_finished_event.visibility = if(finishedData.size < 1) View.VISIBLE else View.GONE

                        mView.recycler_upcoming_events.visibility = if(upcomingData.size < 1) View.GONE else View.VISIBLE
                        mView.recycler_finished_events.visibility = if(finishedData.size < 1) View.GONE else View.VISIBLE
                    })
                    Unit
                }else{
                    activity?.runOnUiThread({
                        mView.content_main.visibility = View.GONE
                        mView.content_preloader.visibility = View.VISIBLE
                    })
                    Unit
                }
            },context!!)
        }else if(fragmentType == "ORGANIZING"){
            DataLoader.getCurrentOrganizingEvent(object : DataLoader.LoadResult<JSONObject?>{
                override fun finish(result: JSONObject?): Unit = if(result != null){
                    val upcomingData = mutableListOf<JSONObject>()
                    val finishedData = mutableListOf<JSONObject>()
                    val currenttime = System.currentTimeMillis()

                    val array = result.getJSONArray("data")
                    for(i in 0 until array.length()){
                        var eventObj = array.getJSONObject(i)
                        if(eventObj.optLong("date_start") < currenttime){
                            finishedData.add(eventObj)
                        }else{
                            upcomingData.add(eventObj)
                        }
                    }

                    upcomingAdapter = EventAdapter(upcomingData)
                    finishedAdapter = EventAdapter(finishedData)



                    activity?.runOnUiThread({
                        mView.content_main.visibility = View.VISIBLE
                        mView.content_preloader.visibility = View.GONE

                        mView.recycler_upcoming_events.adapter = upcomingAdapter
                        mView.recycler_finished_events.adapter = finishedAdapter

                        mView.recycler_upcoming_events.layoutManager = LinearLayoutManager(activity)
                        mView.recycler_finished_events.layoutManager = LinearLayoutManager(activity)

                        mView.l_no_upcoming_event.visibility = if(upcomingData.size < 1) View.VISIBLE else View.GONE
                        mView.l_no_finished_event.visibility = if(finishedData.size < 1) View.VISIBLE else View.GONE

                        mView.recycler_upcoming_events.visibility = if(upcomingData.size < 1) View.GONE else View.VISIBLE
                        mView.recycler_finished_events.visibility = if(finishedData.size < 1) View.GONE else View.VISIBLE
                    })
                    Unit
                }else{
                    activity?.runOnUiThread({
                        mView.content_main.visibility = View.GONE
                        mView.content_preloader.visibility = View.VISIBLE
                    })
                    Unit
                }
            },context!!)
        }
    }
    suspend fun loadLocation(lat : Double, long : Double) : String{
        val geocoder = Geocoder(this@FragmentRegisteredEvents.context, Locale.getDefault())
        var cityname = "Unknown location"
        try {
            val addresses = geocoder.getFromLocation(lat, long, 1)
            if (addresses.size > 0) {
                val city: String
                if (addresses[0].locality != null) {
                    city = addresses[0].locality
                } else if (addresses[0].adminArea != null) {
                    city = addresses[0].adminArea
                } else {
                    city = String.format("%.4f, %.4f", lat, long)
                }
                cityname = city
            }
        } catch (e: Exception) {
            val city = String.format("%.4f, %.4f", lat, long)

            val lastpos = Option("last_loc", city)
            Configs.setConfiguration(lastpos)
            cityname = city
            //runOnUiThread(() -> lCityname.setText(city));
            e.printStackTrace()
        } finally {
            Log.d("CITYNAME",""+cityname)
            return if( cityname == "") "Unknown Location" else cityname
        }
    }
    inner class EventAdapter(data : MutableList<JSONObject>) : RecyclerView.Adapter<Holder>(){
        val data : MutableList<JSONObject>
        init{
            this.data = data
        }
        override fun getItemCount(): Int = data.size

        override fun onBindViewHolder(p0: Holder, p1: Int) {
            val obj = data[p1]
            if(fragmentType == "MYEVENT"){
                p0!!.itemView.l_event_name.setText(obj.optString("nama_event"))
                p0.itemView.l_event_date.setText(DateFormat.format("dd MMM yyyy",obj.optLong("date_start")))
                p0.itemView.l_contactperson.setText(obj.optString("contact_person"))
                if (obj.optLong("date_start") < System.currentTimeMillis()){
                    p0.itemView.btn_cancel.visibility = View.GONE
                    p0.itemView.btn_cancel.setOnClickListener(null)
                }else {
                    p0.itemView.btn_cancel.visibility = View.VISIBLE
                    p0.itemView.btn_cancel.setOnClickListener({
                        cancelEvent(obj, this)
                    })
                }
            }else if(fragmentType == "ORGANIZING"){
                p0!!.itemView.l_organizing_event_name.setText(obj.optString("nama_event"))
                p0.itemView.l_organizing_event_date.setText(DateFormat.format("dd MMM yyyy",obj.optLong("date_start")))
                p0.itemView.l_organizing_contactperson.setText(obj.optString("contact_person"))
                launch(UI) {
                    p0.itemView.l_organizing_location.setText(loadLocation(obj.optDouble("latitude"),obj.optDouble("longitude")))
                }
                p0.itemView.btn_organizing_cancel.setTextColor(resources.getColor(R.color.blue_darken_3))
                p0.itemView.btn_organizing_cancel.setText("See participants")
                p0.itemView.btn_organizing_cancel.setOnClickListener({
                    val intent = Intent(this@FragmentRegisteredEvents.context,ActivityEventParticipants::class.java)
                    intent.putExtra("id",obj.getLong("id"))
                    startActivity(intent)
                })
            }
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val v = if(fragmentType == "MYEVENT"){
                LayoutInflater.from(p0!!.context)
                        .inflate(R.layout.view_event_registered,p0,false)
            }else if(fragmentType == "ORGANIZING"){
                LayoutInflater.from(p0!!.context)
                        .inflate(R.layout.view_event_organizing,p0,false)
            }else{
                LayoutInflater.from(p0!!.context)
                        .inflate(R.layout.view_event_registered,p0,false)
            }
            return Holder(v)
        }

    }

    private fun cancelEvent(obj: JSONObject, data: EventAdapter) {
        val eventName = obj.getString("nama_event")
        val eventId = obj.getLong("id")
        AlertDialog.Builder(context)
                .setTitle("Batalkan pendaftaran")
                .setMessage("Anda akan membatalkan pendaftaran anda di event $eventName")
                .setNegativeButton("Tetap bergabung", {d,_ -> d.dismiss()})
                .setPositiveButton("Batalkan",{d,_ ->
                    d.dismiss()
                    cancelEventExecute(eventId, data)
                }).show()
    }

    private fun cancelEventExecute(eventId: Long, adapter: EventAdapter) {
        val progress = ProgressDialog(context)
        progress.setMessage("Membatalkan pendaftaran")
        progress.setCancelable(false)

        progress.show()

        DataLoader.cancelEventRegistration(eventId,object : DataLoader.LoadResult<JSONObject?>{
            override fun finish(result: JSONObject?) = if (result == null){
                Toast.makeText(context,"Pembatalan event gagal!",Toast.LENGTH_LONG).show()
                progress.dismiss()
            }else{
                for (i in 0 until adapter.data.size){
                    if(adapter.data[i].getLong("id") == eventId){
                        adapter.data.removeAt(i)
                        adapter.notifyDataSetChanged()
                        break
                    }
                }
                progress.dismiss()
            }
        },context!!)
    }

    class Holder(v : View) : RecyclerView.ViewHolder(v)

    interface Finish{
        fun result(address : String)
    }

}// Required empty public constructor
