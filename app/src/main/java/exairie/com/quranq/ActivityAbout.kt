package exairie.com.quranq

import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import exairie.com.quranq.libs.Configs
import kotlinx.android.synthetic.main.activity_about.*

class ActivityAbout : ExtActivity(R.layout.activity_about) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        setTitle("About")

        val remoteConfig = FirebaseRemoteConfig.getInstance()
        val versionCode = BuildConfig.VERSION_CODE

        link_email.setOnClickListener {
            val uri = Uri.parse("mailto:admin@baitulquran.id")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
        }
        link_instagram.setOnClickListener {
            val uri = Uri.parse("https://www.instagram.com/baitulquran.id")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
        }
        link_web.setOnClickListener {
            val uri = Uri.parse("https://www.baitulquran.id")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)
        }
        link_youtube.setOnClickListener {
            val uri = Uri.parse("https://www.youtube.com/c/BaitulQuranOfficial")
            val intent = Intent(Intent.ACTION_VIEW,uri)
            startActivity(intent)

        }
    }
}
