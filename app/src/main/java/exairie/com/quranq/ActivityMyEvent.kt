package exairie.com.quranq

import android.os.Build
import android.os.Bundle
import android.support.v7.widget.Toolbar
import exairie.com.quranq.libs.ViewPagerAdapter
import kotlinx.android.synthetic.main.activity_my_event.*

class ActivityMyEvent : ExtActivity(R.layout.activity_my_event) {
    lateinit var pagerAdapter : ViewPagerAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setExtActionBar(toolbar as Toolbar)

        hasBackButton(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            (toolbar as Toolbar).elevation = 0f
        }

        setTitle("My Events")

        pagerAdapter = ViewPagerAdapter(supportFragmentManager)

        pagerAdapter.addFragment(FragmentRegisteredEvents(),"Registered")
        pagerAdapter.addFragment(FragmentRegisteredEvents("ORGANIZING"),"Organizing")

        pager_event.adapter = pagerAdapter
        tab.setupWithViewPager(pager_event)
    }
}
