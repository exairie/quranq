package exairie.com.quranq;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.msarhan.ummalqura.calendar.UmmalquraCalendar;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;
import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.libs.CustomGridView;
import exairie.com.quranq.libs.DBHelper;
import exairie.com.quranq.libs.IconContent;
import exairie.com.quranq.libs.OdojHistoryAdapter;
import exairie.com.quranq.libs.PrayTime;
import exairie.com.quranq.libs.ViewPagerAdapter;
import exairie.com.quranq.models.AdzanSettings;
import exairie.com.quranq.models.Application;
import exairie.com.quranq.models.Notifications;
import exairie.com.quranq.models.OdojData;
import exairie.com.quranq.models.Option;
import exairie.com.quranq.models.UserLogin;
import exairie.com.quranq.services.PrayTimeService;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this playerFragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link FragmentDashboardContent#newInstance} factory method to
 * create an instance of this playerFragment.
 */
public class FragmentDashboardContent extends Fragment implements Application.CoinStatusListener {
    public final static int TYPE_ODOJ = 1;
    public final static int TYPE_TILAWAH = 2;
    public final static int TYPE_QORI = 3;
    public final static int TYPE_NOTIF = 4;
    public final static int TYPE_FAHMIL = 5;
    public final static double JAKARTA_LATITUDE = -6.1745;
    public final static double JAKARTA_LONGITUDE = 106.8227;
    double JAKARTA_TIMEZONE = 7;
    String clock_currenttime = "";
    String city = "";
    GridAdapter adapter = new GridAdapter();
    ActivityAdapter activityAdapter = new ActivityAdapter();

    Timer timer = new Timer();
    PrayTime.PrayTimeInformation prayTimeInformation;

    // TODO: Rename parameter arguments, choose names that match
    // the playerFragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    @InjectView(R.id.l_username)
    TextView lUsername;
    @InjectView(R.id.l_clock_cityname)
    TextView lClockCityname;
    @InjectView(R.id.linearLayout4)
    LinearLayout linearLayout4;
    @InjectView(R.id.pagerSlider)
    ViewPager sliderPager;
    //    @InjectView(R.id.l_hijriyah_date)
//    TextView lHijriyahDate;
//    @InjectView(R.id.l_pray_subuh)
//    TextView lPraySubuh;
//    @InjectView(R.id.l_pray_dzuhur)
//    TextView lPrayDzuhur;
//    @InjectView(R.id.l_pray_ashar)
//    TextView lPrayAshar;
//    @InjectView(R.id.l_pray_maghrib)
//    TextView lPrayMaghrib;
//    @InjectView(R.id.l_pray_isya)
//    TextView lPrayIsya;
//    @InjectView(R.id.l_progress_praytime)
//    ProgressBar lProgressPraytime;
//    @InjectView(R.id.l_shalat_time_remaining)
//    TextView lShalatTimeRemaining;
//    @InjectView(R.id.cardView2)
//    CardView cardView2;
    @InjectView(R.id.recycler_activities)
    RecyclerView recyclerActivities;
    @InjectView(R.id.l_coin_number)
    TextView lCoinNumber;
    @InjectView(R.id.layout_coins)
    LinearLayout layoutCoins;
    @InjectView(R.id.grid_menu)
    CustomGridView gridMenu;
    @InjectView(R.id.img_profile)
    ImageView imgProfile;
    private ActivityDashboard activityDashboard;
    private UserLogin userInfo;

    BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            setupEvents();
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(locationChangeListener, new IntentFilter(ActivityDashboard.LOCATION_CHANGED));
        setupTimer();
    }

    @Override
    public void onPause() {
        timer.cancel();

        getActivity().unregisterReceiver(locationChangeListener);
        super.onPause();

    }

    OdojHistoryAdapter historyAdapter;
    BroadcastReceiver locationChangeListener;

    @Override
    public void onStart() {
        super.onStart();
        setupPrayTimeInfo();
        activityAdapter.loadLog();

        Application.updateCoinStatus();
        activityDashboard = (ActivityDashboard) getActivity();
        locationChangeListener = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                setupPrayTimeInfo();
            }
        };
        if (userInfo != null) {
            lUsername.setText(String.format("%s %s", userInfo.firstname, userInfo.lastname));

            if (userInfo.photo_url != null && userInfo.photo_url.length() > 0) {
                Picasso.with(getContext())
                        .load(userInfo.photo_url)
                        .placeholder(R.drawable.ic_person_grey_900_24dp)
                        .into(imgProfile);
            }

        }


    }

    public void setupUserInfo(UserLogin login) {
        userInfo = login;
    }

    private void setupEvents() {
        gridMenu.setAdapter(adapter);

        adapter.views.clear();

        FirebaseRemoteConfig cfg = FirebaseRemoteConfig.getInstance();
        boolean fm = cfg.getBoolean("fahmil_menu");
        if (cfg.getBoolean("fahmil_menu")) {
            IconContent fahmil = new IconContent(this.getContext());
            fahmil.getImg().setImageResource(R.drawable.fahmil);
            fahmil.getLabelText().setText("Fahmil Quran");


            adapter.views.add(fahmil);

            fahmil.setOnClickListener(view -> {
                new AlertDialog.Builder(this.getContext())
                        .setMessage("Apakah anda ingin memulai quiz Fahmil Quran?")
                        .setTitle("Fahmil Quran")
                        .setPositiveButton("Mulai!", (dialogInterface, i) -> {
                            Intent in = new Intent(getActivity(), ActivityFahmilStart.class);
                            startActivity(in);
                            dialogInterface.dismiss();
                        })
                        .setNeutralButton("Lihat laporan Quiz", (dialogInterface, i) -> {
                            dialogInterface.dismiss();
                            Intent in = new Intent(getActivity(), ActivityFahmilReport.class);
                            startActivity(in);
                        })
                        .show();
            });
        }

        if (cfg.getBoolean("hifdzil_menu")) {
            IconContent hifdzil = new IconContent(this.getContext());
            hifdzil.getImg().setImageResource(R.drawable.hifzil);
            hifdzil.getLabelText().setText("Hifdzil");

            adapter.views.add(hifdzil);

            hifdzil.setOnClickListener(view -> {
                Intent i = new Intent(getActivity(), ActivityHifdzilStart.class);
                startActivity(i);
            });
        }

        if (cfg.getBoolean("tilawah_menu")) {
            IconContent tilawatil = new IconContent(this.getContext());
            tilawatil.getImg().setImageResource(R.drawable.tilawah);
            tilawatil.getLabelText().setText("Tilawatil Quran");

            adapter.views.add(tilawatil);

            tilawatil.setOnClickListener(view -> {
                Intent i = new Intent(getActivity(), ActivityQoriDashboard.class);
                startActivity(i);
            });
        }

        if (cfg.getBoolean("event_menu")) {
            IconContent event = new IconContent(this.getContext());
            event.getImg().setImageResource(R.drawable.event);
            event.getLabelText().setText("Event");

            adapter.views.add(event);

            event.setOnClickListener(view -> {
                Intent i = new Intent(getActivity(), ActivityEvents.class);
                startActivity(i);
            });
        }

        IconContent odoj = new IconContent(this.getContext());
        odoj.getImg().setImageResource(R.drawable.odoj);
        odoj.getLabelText().setText("ODOJ");

        adapter.views.add(odoj);
        adapter.notifyDataSetChanged();


        odoj.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ActivityODOJ.class);
            startActivity(i);
        });

//        IconContent slider = new IconContent(this.getContext());
//        slider.getImg().setImageResource(R.drawable.quran_qu);
//        slider.getLabelText().setText("SLIDER");
//
//        adapter.views.add(slider);
//        adapter.notifyDataSetChanged();
//
//        slider.setOnClickListener(view -> {
//            Intent i = new Intent(getActivity(), ActivitySlider.class);
//            startActivity(i);
//        });

        lUsername.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ActivityProfileInfo.class);
            startActivity(i);
        });
        imgProfile.setOnClickListener(view -> {
            Intent i = new Intent(getActivity(), ActivityProfileInfo.class);
            startActivity(i);
        });

    }

    private void setupActivityLogs() {
        recyclerActivities.setNestedScrollingEnabled(false);
        recyclerActivities.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerActivities.setAdapter(activityAdapter);
        activityAdapter.loadLog();
    }


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public FragmentDashboardContent() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this playerFragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of playerFragment FragmentDashboardContent.
     */
    // TODO: Rename and change types and number of parameters
    public static FragmentDashboardContent newInstance(String param1, String param2) {
        FragmentDashboardContent fragment = new FragmentDashboardContent();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private void setupTimer() {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                calculateTiming();
            }
        }, 0, 1000);
    }

    private void calculateTiming() {
//        final ActivityDashboard parent = (ActivityDashboard) getActivity();
//        Calendar calendar = Calendar.getInstance();
//        lClockCityname.post(() -> {
//            try {
//                city = parent.cityname;
//                clock_currenttime = DateFormat.format("HH:mm", calendar).toString();
//                lClockCityname.setText(String.format("%s\n%s", clock_currenttime, city));
//                PrayTime.CurrentPrayTime time = new PrayTime.CurrentPrayTime(prayTimeInformation);
//
//                Option o = Configs.getConfiguration("adzan_settings");
//                if (o != null) {
//                    try {
//                        AdzanSettings as = Configs.mapper().readValue(o.getValue(), AdzanSettings.class);
//                        time.useSettings(as);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//                Calendar c_subuh_next = (Calendar) prayTimeInformation.c_subuh.clone();
//                c_subuh_next.add(Calendar.DAY_OF_MONTH, 1);
//
//
//                double progress = ((double) time.getDeltaTime() / (double) time.getTimeProgress()) * 1000;
//                //Log.d("PROGRESS", String.format("%d / %d * 1000 = %.3f",time.getDeltaTime(), time.getTimeProgress(), progress));
//                long remaining = time.getRemaining();
//
//                long timecurrent = Calendar.getInstance().getTimeInMillis() - prayTimeInformation.c_subuh.getTimeInMillis();
//
//                lProgressPraytime.setMax((int) (c_subuh_next.getTimeInMillis() - prayTimeInformation.c_subuh.getTimeInMillis()));
//                lProgressPraytime.setProgress((int) timecurrent);
////            lProgressPraytime.setText(time.getCurrentShalatName());
//                lShalatTimeRemaining.setText(String.format("%s - %s", time.getCurrentShalatName(), PrayTime.CurrentPrayTime.getHour(remaining)));
//                time = null;
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        });
    }

    private void setupPrayTimeInfo() {
//        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
//            @Override
//            protected Void doInBackground(Void... voids) {
//                try {
//                    ActivityDashboard activityDashboard = (ActivityDashboard) getActivity();
//                    Option last_lat = Configs.getConfiguration("last_latitude");
//                    Option last_long = Configs.getConfiguration("last_longitude");
//                    Option last_loc = Configs.getConfiguration("last_loc");
//                    if (activityDashboard != null) {
//                        activityDashboard.latitude = last_lat == null ? JAKARTA_LATITUDE : Double.parseDouble(last_lat.getValue());
//                        activityDashboard.longitude = last_long == null ? JAKARTA_LONGITUDE : Double.parseDouble(last_long.getValue());
//                    }
//                    String loc = last_loc == null ? "" : last_loc.getValue();
//
//                    UmmalquraCalendar ummalquraCalendar = new UmmalquraCalendar();
//                    String hijridate = String.format("%s %s %s",
//                            String.valueOf(ummalquraCalendar.get(Calendar.DAY_OF_MONTH)),
//                            ummalquraCalendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH),
//                            String.valueOf(ummalquraCalendar.get(Calendar.YEAR)));
//
//                    Option adzanSettingsOpt = Configs.getConfiguration("adzan_settings");
//
//                    AdzanSettings _adzanSettings = null;
//                    if (adzanSettingsOpt != null) {
//                        _adzanSettings = Configs.mapper().readValue(adzanSettingsOpt.getValue(), AdzanSettings.class);
//                    }
//
//                    final AdzanSettings adzanSettings = _adzanSettings;
//                    getActivity().runOnUiThread(() -> {
//                        try {
//                            lClockCityname.setText(loc);
//                            prayTimeInformation = PrayTime.getInformation(activityDashboard.latitude, activityDashboard.longitude, activityDashboard.timezone, adzanSettings);
//
//                            lPraySubuh.setText(prayTimeInformation.getSubuh());
//                            lPrayDzuhur.setText(prayTimeInformation.getDzuhur());
//                            lPrayAshar.setText(prayTimeInformation.getAshar());
//                            lPrayMaghrib.setText(prayTimeInformation.getMaghrib());
//                            lPrayIsya.setText(prayTimeInformation.getIsya());
//
//                            lHijriyahDate.setText(hijridate);
//                        } catch (Exception exc) {
//                            exc.printStackTrace();
//                        }
//                    });
//                } catch (Exception exc) {
//                    exc.printStackTrace();
//                }
//
//
//                return null;
//            }
//        };
//        task.execute();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
        if (getArguments().getBoolean("loginstatus", false)) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this playerFragment
        View view = inflater.inflate(R.layout.fragment_dashboard_content, container, false);
        ButterKnife.inject(this, view);

        Application.addListenerForCoinChange(this);

        setupEvents();
        setupActivityLogs();

        try {
            getActivity().registerReceiver(receiver, new IntentFilter(Application.REMOTE_CONFIG_UPDATED));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            Application.fetchRemoteConfig();
        }

        setupSlider();

        return view;
    }

    void setupSlider() {
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(SliderFragment.newInstance(R.drawable.slider01));
        fragments.add(SliderFragment.newInstance(R.drawable.slider02));
        fragments.add(SliderFragment.newInstance(R.drawable.slider03));
        fragments.add(SliderFragment.newInstance(R.drawable.slider04));

        if (getFragmentManager() != null) {
            SliderPageViewAdapter adapter = new SliderPageViewAdapter(getFragmentManager(), fragments);
            sliderPager.setAdapter(adapter);
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.reset(this);
        Application.removeListenerForCoinChange(this);
        try {
            getActivity().unregisterReceiver(receiver);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void CoinChanged(long coin) {
        getActivity().runOnUiThread(() -> {
            try {
                if (Configs.getLoginInfo() != null) {
                    lCoinNumber.setText(String.format("%d Coins", coin));
                    layoutCoins.setVisibility(View.VISIBLE);
                } else {
                    layoutCoins.setVisibility(View.GONE);
                }
            } catch (Exception exc) {

            } finally {

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * playerFragment to allow an interaction in this playerFragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    class GridAdapter extends BaseAdapter {
        List<View> views = new ArrayList<>();

        @Override
        public int getCount() {
            return views.size();
        }

        @Override
        public Object getItem(int i) {
            return views.get(i);
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            return views.get(i);
        }
    }

    class LogData {
        public String title;
        public String desc;
        public Intent intent;
        public long time;
        public int type;
    }

    class ActivityAdapter extends RecyclerView.Adapter<ActivityAdapter.Holder> {
        boolean isInLoadingState = false;
        boolean isIterating = false;

        List<LogData> data = new ArrayList<>();
        List<LogData> _load = new ArrayList<>();
        Runnable loadCallback = null;

        public void loadLog() {
            if (isInLoadingState) return;
            AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
                @Override
                protected Void doInBackground(Void... voids) {
                    try {
                        isInLoadingState = true;
                        _load.clear();
                        //Load ODOJ
                        DBHelper db = new DBHelper(getContext());
                        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM odoj order by time desc limit 0,50", new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                                OdojData odoj = OdojData.createFromCursor(c);
                                LogData log = new LogData();
                                log.time = odoj.getTime();
                                log.title = "Entri ODOJ";
                                log.desc = String.format("%.2f Juz Dibaca", (Double.valueOf(odoj.getLetter_count()) / Configs.LETTER_PER_JUZ));
                                log.intent = new Intent(getContext(), ActivityODOJ.class);
                                log.type = TYPE_ODOJ;
                                _load.add(log);
                                odoj = null;
                                c.moveToNext();
                            }
                        }
                        c.close();
                        //Notifications
                        c = db.getReadableDatabase().rawQuery("SELECT * FROM notifications_pending order by create_date desc limit 0,50", new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                                Notifications notif = Notifications.fromCursor(c);
                                LogData log = new LogData();
                                log.title = "Notifikasi";
                                log.desc = notif.notif_text;
                                log.time = notif.create_date;
                                Intent i = new Intent(getContext(), ActivityNotification.class);
                                i.putExtra("forceopen", true);
                                i.putExtra("id", notif.id);
                                log.intent = i;
                                log.type = TYPE_NOTIF;
                                _load.add(log);

                                notif = null;
                                c.moveToNext();
                            }
                        }
                        c.close();

                        //Video Tilawah
                        c = db.getReadableDatabase().rawQuery("SELECT a.*,b.title,b.reciter FROM qori_recent a " +
                                "left join qori b on a.recent = b.id " +
                                "order by a.time desc limit 0,50", new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                            /*
                            " id integer primary key autoincrement," +
                            " recent integer," +
                            " time integer" +
                             */
                                LogData log = new LogData();
                                log.title = "Video Tilawah";
                                log.desc = String.format("%s - %s", c.getString(c.getColumnIndexOrThrow("title")), c.getString(c.getColumnIndexOrThrow("reciter")));
                                log.time = c.getLong(c.getColumnIndexOrThrow("time"));
                                Intent i = new Intent(getContext(), ActivityQoriList.class);
                                i.putExtra("jump", c.getLong(c.getColumnIndexOrThrow("id")));
                                log.intent = i;
                                log.type = TYPE_TILAWAH;
                                _load.add(log);

                                c.moveToNext();
                            }
                        }
                        c.close();

                        //Fahmil Quran
                        c = db.getReadableDatabase().rawQuery("SELECT session_start, count(id) as questions,count(answer_false) as answer_false" +
                                " FROM fahmil_result group by session_start order by session_start desc limit 0,50", new String[]{});
                        if (c.getCount() > 0) {
                            c.moveToFirst();
                            while (!c.isAfterLast()) {
                            /*
                            " id integer primary key," +
                            " question varchar(255)," +
                            " answer_true varchar(255)," +
                            " answer_false varchar(255)," +
                            " session_start integer," +
                            " session_end integer" +
                             */
                                LogData log = new LogData();
                                log.title = "Quiz Fahmil";
                                log.desc = String.format("Anda menjawab %d benar dari %d soal",
                                        c.getInt(c.getColumnIndexOrThrow("questions")) - c.getInt(c.getColumnIndexOrThrow("answer_false")),
                                        c.getInt(c.getColumnIndexOrThrow("questions")));
                                log.time = c.getLong(c.getColumnIndexOrThrow("session_start"));
                                Intent i = new Intent(getContext(), ActivityFahmilResult.class);
                                i.putExtra("session", log.time);
                                log.intent = i;
                                log.type = TYPE_FAHMIL;
                                _load.add(log);

//                            notif = null;
                                c.moveToNext();
                            }
                        }
                        c.close();
                        db.close();
                        Collections.sort(_load, (logData, t1) -> logData.time > t1.time ? -1 : logData.time < t1.time ? 1 : 0);
                        loadCallback = new Runnable() {
                            @Override
                            public void run() {
                                data = _load;
                                getActivity().runOnUiThread(() -> {
                                    try {
                                        recyclerActivities.getRecycledViewPool().clear();
                                        recyclerActivities.post(() -> {
                                            recyclerActivities.getAdapter().notifyDataSetChanged();
                                        });
                                        isIterating = true;
                                    } catch (Exception exc) {
                                        exc.printStackTrace();
                                    } finally {
                                        isInLoadingState = false;
                                    }
                                });
                            }
                        };

                        if (!isIterating) {
                            loadCallback.run();
                            loadCallback = null;
                        }
                    } catch (Exception exc) {
                        exc.printStackTrace();
                    } finally {

                    }

                    return null;
                }
            };
            task.execute();
        }

        @Override
        public Holder onCreateViewHolder(ViewGroup viewGroup, int i) {
            View v = LayoutInflater.from(viewGroup.getContext())
                    .inflate(R.layout.view_activity_log, viewGroup, false);
            return new Holder(v);
        }

        @Override
        public void onBindViewHolder(Holder holder, int i) {
            if (data.size() < 1) return;
            LogData rdata = data.get(i);
            switch (rdata.type) {
                case TYPE_FAHMIL:
                    holder.imageIcon.setImageResource(R.drawable.fahmil);
                    break;
                case TYPE_NOTIF:
                    holder.imageIcon.setImageResource(R.drawable.ic_notifications_amber_500_24dp);
                    break;
                case TYPE_ODOJ:
                    holder.imageIcon.setImageResource(R.drawable.quran_qu);
                    break;
                case TYPE_TILAWAH:
                    holder.imageIcon.setImageResource(R.drawable.tilawah);
                    break;
            }

            holder.lDescNotif.setText(rdata.desc);
            holder.lLabelNotif.setText(rdata.title);

            //Smart Timing
            Calendar now = Calendar.getInstance();
            //Under 1 minute
            long delta = (now.getTimeInMillis() - rdata.time);
            if (delta < (60 * 1000)) {
                long timing = delta / 1000;
                holder.lTime.setText(String.format("%d detik", timing));
            }
            //under 1 hour
            else if (delta < (60 * 60 * 1000)) {
                long timing = delta / (60 * 1000);
                holder.lTime.setText(String.format("%d menit", timing));
            }
            //under 24 hour
            else if (delta < (24 * 60 * 60 * 1000)) {
                long timing = delta / (60 * 60 * 1000);
                holder.lTime.setText(String.format("%d jam", timing));
            }
            //under a week
            else if (delta < (7 * 24 * 60 * 60 * 1000)) {
                long timing = delta / (24 * 60 * 60 * 1000);
                holder.lTime.setText(String.format("%d hari", timing));
            }
            //more than that
            else {
                holder.lTime.setText(DateFormat.format("dd MMM", rdata.time));
            }

            holder.itemView.setOnClickListener(view -> {
                startActivity(rdata.intent);
            });
            if (i <= 0) {
                isIterating = true;
            }
            if (i >= data.size() - 1) {
                Log.d("THREAD", "Callback");
                isIterating = false;
                if (loadCallback != null) {
                    loadCallback.run();
                    loadCallback = null;
                }
            }
        }

        @Override
        public int getItemCount() {
            return data.size() > 50 ? 50 : data.size();
        }

        class Holder extends RecyclerView.ViewHolder {
            @InjectView(R.id.image_icon)
            ImageView imageIcon;
            @InjectView(R.id.l_label_notif)
            TextView lLabelNotif;
            @InjectView(R.id.l_time)
            TextView lTime;
            @InjectView(R.id.l_desc_notif)
            TextView lDescNotif;

            public Holder(View itemView) {
                super(itemView);
                ButterKnife.inject(this, itemView);
            }
        }
    }

}

