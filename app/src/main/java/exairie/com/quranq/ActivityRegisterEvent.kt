package exairie.com.quranq

import android.content.DialogInterface
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.text.format.DateFormat
import android.view.View
import android.widget.Toast
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Event
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.UserLogin
import exairie.com.quranq.services.DataLoader
import kotlinx.android.synthetic.main.activity_register_event.*
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ActivityRegisterEvent : ExtActivity(R.layout.activity_register_event) {
    var event_id: Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initializeContent()
    }
    fun initializeContent(){
        val login = Configs.getLoginInfo()
        if(login == null){
            AlertDialog.Builder(thisContext())
                    .setMessage("Anda harus login terlebih dahulu!")
                    .setTitle("Log in required")
                    .setNegativeButton("Close", { d,_ -> d.dismiss()})
                    .setPositiveButton("Login", { dialogInterface, _ ->
                        val i = Intent(thisContext(),ActivityLogin::class.java)
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TOP)
                        startActivity(i)
                        dialogInterface.dismiss()
                        finish()
                    })
        }
        btn_register.text = "Checking status..."
        btn_register.isEnabled = false
        event_id = intent.getLongExtra("id",-1)

        if(event_id == -1L){
            Toast.makeText(applicationContext,"Event invalid",Toast.LENGTH_LONG).show()
            finish()
            return
        }

        val db = DBHelper(thisContext())
        val c = db.readableDatabase.rawQuery("SELECT * FROM events where id = ?", arrayOf<String>(event_id.toString()))

        if (c.count < 1){
            Toast.makeText(applicationContext,"Event invalid",Toast.LENGTH_LONG).show()
            finish()
            return
        }

        c.moveToFirst()

        val event = Event.fromCursor(c);

        l_event_name.setText(event.nama_event)
        l_event_date.setText(DateFormat.format("dd MMM yyyy",event.date_start))
        l_event_inititator.setText("...")
        DataLoader.getUserData(event.id_user,object : DataLoader.LoadResult<UserLogin?>{
            override fun finish(result: UserLogin?) {
                if(result != null){
                    runOnUiThread({l_event_inititator.setText("Diselenggarakan oleh ${result!!.firstname} ${result.lastname}")})
                }else{
                    runOnUiThread({l_event_inititator.setText("Data penyelenggara tidak ditemukan")})
                }
            }
        },thisContext())

        DataLoader.getEventRegisterStatus(event_id,object : DataLoader.LoadResult<JSONObject?>{
            override fun finish(result: JSONObject?) = if (result == null){
                runOnUiThread({
                    btn_register.alpha = 1f
                    btn_register.isEnabled = true
                    btn_register.text = "Daftar"
                })
            }else{
                runOnUiThread({
                    btn_register.alpha = 0.5f
                    btn_register.isEnabled = false
                    btn_register.text = "Anda sudah terdaftar"
                })
            }
        },thisContext())

        btn_register.setOnClickListener({
            btn_register.setText("Mendaftarkan...")
            btn_register.isEnabled = false
            val body = FormBody.Builder()
                    .add("id",event_id.toString())
                    .add("id_user",login.id.toString())
                    .add("note",t_event_note.text.toString())
            val request = Request.Builder()
                    .post(body.build())
                    .url(server("api2/registerevent"))
                    .build()
            NetRequest(request,object : FinishedRequest{
                override fun OnSuccess(response: Response?) {
                }

                override fun OnSuccess(string: String?) {
                    val obj = JSONObject(string)
                    val book_id = obj.getString("data")
                    runOnUiThread({
                        AlertDialog.Builder(thisContext())
                                .setTitle("Pendaftaran berhasil")
                                .setMessage("Anda telah terdaftar ke event ${event.nama_event}!\nNomor Booking anda adalah #$book_id")
                                .setPositiveButton("Tutup",{d, _->
                                    d.dismiss()
                                    finish()
                                }).show()
                    })
                }

                override fun OnFail(msg: String?) {
                    runOnUiThread({
                        AlertDialog.Builder(thisContext())
                                .setTitle("Pendaftaran gagal")
                                .setMessage(msg)
                                .setPositiveButton("Tutup",{d, _->
                                    d.dismiss()
                                    finish()
                                }).show()
                    })
                }

                override fun Finish() {
                    runOnUiThread({
                        btn_register.alpha = 1f
                        btn_register.isEnabled = true
                        btn_register.text = "Daftar"
                    })
                }
            })
        })

        c.close()
        db.close()
    }
}
