package exairie.com.quranq

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.widget.Toolbar
import android.view.View
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Application
import exairie.com.quranq.models.FinishedRequest
import kotlinx.android.synthetic.main.activity_video_request.*
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ActivityVideoRequest : ExtActivity(R.layout.activity_video_request) {
    private lateinit var receiver: BroadcastReceiver

    private var request_coin_cost: Long = 0

    private lateinit var dialog : ProgressDialog
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        dialog = ProgressDialog(thisContext())
        dialog.setTitle("Please wait...")
        dialog.setCancelable(false)

        initializeCost()

        l_coin_cost.setText(request_coin_cost.toString())

        setExtActionBar(toolbar as Toolbar)

        hasBackButton(true)

        setTitle("Request")

        btn_create_request.setOnClickListener({
            if(t_video_detail.text.length < 5){
                validateWindow(t_video_detail,"Detil video harus berisikan 5 huruf atau lebih!")
                return@setOnClickListener
            }
            if(t_video_title.text.length < 5){
                validateWindow(t_video_title,"Judul video harus berisikan 5 huruf atau lebih!")
                return@setOnClickListener
            }
            if(t_video_reference.text.length < 1){
                validateWindow(t_video_reference,"Referensi video harus dituliskan, atau tuliskan strip (-) apabila tidak ada referensi!")
                return@setOnClickListener
            }

            initializeCoinPayment()
        })
    }
    fun validateWindow(view : View,msg : String){
        runOnUiThread {
            AlertDialog.Builder(this)
                    .setTitle("Data kurang lengkap")
                    .setMessage(msg)
                    .setPositiveButton("Tutup",{d,_->
                        d.dismiss()
                        view.requestFocus()
                    })
                    .show()
        }
    }

    fun initializeCost() {
        val cfg = FirebaseRemoteConfig.getInstance()
        receiver = object : BroadcastReceiver(){
            override fun onReceive(p0: Context?, p1: Intent?) {
                request_coin_cost = cfg.getLong("cost_coin_request")
                runOnUiThread({
                    l_coin_cost.setText(request_coin_cost.toString())
                })

            }
        }
        request_coin_cost = cfg.getLong("cost_coin_request")
    }

    fun initializeCoinPayment(){
        dialog.setMessage("Requesting purchase")
        dialog.show()
        Application.requestCoinPurchase(-1 * request_coin_cost, object : Application.CoinTransRequest{
            override fun transactionApplied(token: String?) {
                applyTransaction(token)
            }

            override fun transactionRejected(msg: String?) {
                runOnUiThread({
                    dialog.dismiss()
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setTitle("Permintaan ditolak")
                            .setPositiveButton("Close", { dialogInterface, _ ->
                                dialogInterface.dismiss()
                            }).show()
                })
            }

        })
    }

    private fun applyTransaction(token: String?) {
        runOnUiThread({dialog.setMessage("Applying purchase")})
        val user = Configs.getLoginInfo()

        val body = FormBody.Builder()
                .add("id",user.id)
                .add("token",token)
                .add("amount",(-1 * request_coin_cost).toString())
                .add("remark", "Requesting Video")
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/coin_trans"))
                .build()
        NetRequest(request,object : FinishedRequest {
            override fun OnSuccess(response: Response?) {

            }

            override fun OnSuccess(string: String?) {
                val obj = JSONObject(string)
                val trans_id = obj.getJSONObject("data").getString("trans_id")
                sendRequest(trans_id)
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    dialog.dismiss()
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",{d,_->d.dismiss()})
                            .setPositiveButton("Retry",{d,_->
                                d.dismiss()
                            })
                            .show()

                })
            }

            override fun Finish() {
            }

        })
    }

    private fun sendRequest(trans_id: String?) {
        runOnUiThread({dialog.setMessage("Sending request")})
        val user = Configs.getLoginInfo()

        val body = FormBody.Builder()
                .add("request_title",t_video_title.text.toString())
                .add("request_description",t_video_detail.text.toString())
                .add("request_reference", t_video_reference.text.toString())
                .add("request_date", System.currentTimeMillis().toString())
                .add("trans_id",trans_id)
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/videorequest"))
                .build()
        NetRequest(request,object : FinishedRequest {
            override fun OnSuccess(response: Response?) {

            }

            override fun OnSuccess(string: String?) {
                runOnUiThread({
                    dialog.dismiss()
                    AlertDialog.Builder(thisContext())
                            .setTitle("Berhasil")
                            .setMessage("Request anda sudah terkirim!")
                            .setNegativeButton("Close",{d,_->
                                d.dismiss()
                                finish()
                            })
                            .show()

                })
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    dialog.dismiss()
                    AlertDialog.Builder(thisContext())
                            .setMessage(msg)
                            .setNegativeButton("Close",{d,_->d.dismiss()})
                            .setPositiveButton("Retry",{d,_->
                                d.dismiss()
                            })
                            .show()

                })
            }

            override fun Finish() {
            }

        })
    }
}
