package exairie.com.quranq

import android.app.ProgressDialog
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.squareup.picasso.Picasso
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Event
import exairie.com.quranq.services.DataLoader
import kotlinx.android.synthetic.main.activity_event_participants.*
import kotlinx.android.synthetic.main.view_event_participant.view.*
import kotlinx.coroutines.experimental.async
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.android.UI
import org.json.JSONArray
import org.json.JSONObject

class ActivityEventParticipants : ExtActivity(R.layout.activity_event_participants) {
    var event_id : Long = -1
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExtActionBar(toolbar as Toolbar)
        hasBackButton(true)
        event_id = intent.getLongExtra("id",-1)
        if (event_id == -1L){
            Toast.makeText(thisContext(),"Invalid Event",Toast.LENGTH_LONG).show()
            finish()
            return
        }

        initComponents()
    }

    private fun initComponents() {
        val dialog = ProgressDialog(thisContext())
        dialog.setMessage("Fetching data...")
        dialog.setCancelable(false)
        dialog.show()


        DataLoader.getEventParticipationData(event_id, object : DataLoader.LoadResult<JSONObject?>{
            override fun finish(result: JSONObject?) = if (result == null){
                runOnUiThread {
                    Toast.makeText(thisContext(),"Invalid Event",Toast.LENGTH_LONG).show()
                }
                finish()
                dialog.dismiss()
            }else{
                val event = Configs.mapper().readValue(result.getString("detail"),Event::class.java)
                val participants = result.getJSONArray("data")
                runOnUiThread {
                    showParticipants(participants,event)
                    calculateMeta(participants,event)
                }
                dialog.dismiss()
            }
        },thisContext())
    }

    fun showParticipants(participants: JSONArray, event: Event) {
        val adapter = ParticipantsAdapter(participants, event)

        runOnUiThread {
            setTitle(event.nama_event)
            supportActionBar!!.setSubtitle(DateFormat.format("dd MMM yyyy",event.date_start))
            recycler_event_participants.adapter = adapter
            recycler_event_participants.layoutManager = LinearLayoutManager(thisContext())

            if(participants.length() > 0){
                layout_no_participants.visibility = View.GONE
                recycler_event_participants.visibility = View.VISIBLE
            }else{
                layout_no_participants.visibility = View.VISIBLE
                recycler_event_participants.visibility = View.GONE
            }
        }
    }
    fun calculateMeta(participants: JSONArray, event: Event) = if(event.capacity != 0){
        val registered = participants.length()
        l_event_max_capacity.text = event.capacity.toString()
        l_event_current_registered.text = registered.toString()

        val progress = registered.toDouble() / event.capacity.toDouble() * 100

        progress_participation.progress = progress.toInt()
        l_reg_percentage.text = "${progress.toInt()}%"
    }else{
        val registered = participants.length()
        l_event_max_capacity.text = "Unlimited"
        l_event_current_registered.text = registered.toString()

        progress_participation.progress = 100
        l_reg_percentage.text = "unlimited"
    }
    suspend fun calculateLocation(loc1 : Location, loc2 : Location) : String{
        val dist = loc1.distanceTo(loc2) / 1000
        Log.d("Distance","dist ")
        return if(dist > 100) "> 100 km" else (dist.toInt()).toString() + " km"
    }
    inner class ParticipantsAdapter(data : JSONArray, eventData : Event) : RecyclerView.Adapter<Holder>(){
        val data : JSONArray
        val eventData : Event
        val eventLocation : Location
        init {
            this.data = data
            this.eventData = eventData

            eventLocation = Location("")
            eventLocation.latitude = eventData.latitude
            eventLocation.longitude = eventData.longitude

        }
        override fun onBindViewHolder(holder: Holder, p1: Int) {
            val p = data.getJSONObject(p1)

            holder.itemView.l_name.text = String.format("%s %s",p.getString("firstname"),p.getString("lastname"))
            holder.itemView.l_email.text = p.getString("email")
            holder.itemView.l_register_date.text = String.format("Mendaftar pada %s",DateFormat.format("dd MMM yyyy",p.getLong("book_date")))
            holder.itemView.l_note.text = String.format("Note : %s",if(p.getString("note") == "") "-" else p.getString("note"))
            if (p.getString("profile_pic") != ""){
                Picasso.with(thisContext())
                        .load(p.getString("profile_pic"))
                        .placeholder(R.drawable.ic_person_grey_900_24dp)
                        .into(holder.itemView.img_profilepic)
            }

            launch(UI) {
                val location = Location("")
                location.latitude = p.getDouble("last_latitude")
                location.longitude = p.getDouble("last_longitude")
                holder.itemView.l_distance.setText(calculateLocation(eventLocation,location))
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
            val v = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_event_participant,parent,false)
            return Holder(v)
        }

        override fun getItemCount(): Int = data.length()
    }
    class Holder(v : View) : RecyclerView.ViewHolder(v)
}
