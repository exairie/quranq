package exairie.com.quranq

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout.HORIZONTAL
import com.squareup.picasso.Picasso
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.QoriVid
import exairie.com.quranq.services.QoriVidLoader
import kotlinx.android.synthetic.main.activity_qori_dashboard.*
import kotlinx.android.synthetic.main.view_img_thumb.view.*
import kotlinx.android.synthetic.main.view_qori_dashboard_content.view.*

class ActivityQoriDashboard : ExtActivity(R.layout.activity_qori_dashboard) {
    val receiver = object : BroadcastReceiver(){
        override fun onReceive(p0: Context?, p1: Intent?) {
            if(recycler_dashboard.adapter != null)
                (recycler_dashboard.adapter as QoriDashboardAdapter).load()
        }

    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setExtActionBar(toolbar as Toolbar)
        setTitle("Tilawatil Quran")
        hasBackButton(true)
        recycler_dashboard.adapter = QoriDashboardAdapter()
        recycler_dashboard.layoutManager = LinearLayoutManager(thisContext())
        (recycler_dashboard.adapter as QoriDashboardAdapter).load()

        registerReceiver(receiver, IntentFilter(QoriVidLoader.BROADCAST_QORIVID_UPDATED))

        startService(Intent(thisContext(),QoriVidLoader::class.java))
        layout_no_video.visibility = View.VISIBLE
    }

    override fun onDestroy() {
        unregisterReceiver(receiver)
        super.onDestroy()
    }
    inner class QoriDashboardAdapter : RecyclerView.Adapter<DashboardHolder>(){
        val videoAdapters = mutableListOf<VideoAdapter>()

        fun load(){
            videoAdapters.clear()
            val db = DBHelper(thisContext())
            val c = db.readableDatabase.rawQuery("SELECT cat,count(cat) FROM qori group by cat", arrayOf())
            if(c.count > 0){
                layout_no_video.visibility = View.GONE
            }
            c.moveToFirst()
            while (!c.isAfterLast){
                videoAdapters.add(VideoAdapter(c.getString(0),c.getInt(1)))
                c.moveToNext()
            }
            c.close()
            db.close()
            notifyDataSetChanged()
            layout_no_video.visibility = View.GONE
        }
        override fun getItemCount(): Int = videoAdapters.size

        override fun onBindViewHolder(p0: DashboardHolder, p1: Int) {
            val v = p0!!.itemView
            val adapter = videoAdapters[p1]
            adapter.load()
            v.recycler.adapter = adapter
            v.recycler.layoutManager = LinearLayoutManager(thisContext(),HORIZONTAL,false)
            v.l_vid_count.setText("${adapter.vCount} Video")
            v.l_category_name.setText(adapter.adapterCategory)
            v.btn_seeall.setOnClickListener({
                val i = Intent(thisContext(),ActivityQoriList::class.java)
                i.putExtra("category",adapter.adapterCategory)
                startActivity(i)
            })
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): DashboardHolder {
            val v = LayoutInflater.from(p0!!.context).inflate(R.layout.view_qori_dashboard_content,p0,false)
            return DashboardHolder(v)
        }

    }
    inner class VideoAdapter(cat : String, vCount : Int) : RecyclerView.Adapter<VidHolder>(){
        val adapterCategory : String
        val vCount : Int
        val videos = mutableListOf<QoriVid>()
        init {
            adapterCategory = cat
            this.vCount = vCount
        }
        fun load(){
            val db = DBHelper(thisContext())
            val c = db.readableDatabase.rawQuery("SELECT * FROM qori WHERE cat = ? limit 0,5", arrayOf(adapterCategory))
            c.moveToFirst()
            while (!c.isAfterLast){
                val vid = QoriVid.fromCursor(c)
                videos.add(vid)
                c.moveToNext()
            }
            c.close()
            db.close()
            notifyDataSetChanged()
        }
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): VidHolder {
            val v = LayoutInflater.from(p0!!.context).inflate(R.layout.view_img_qorithumb,p0,false)
            return VidHolder(v)
        }

        override fun getItemCount(): Int = videos.size

        override fun onBindViewHolder(p0: VidHolder, p1: Int) {
            val video = videos[p1]
            Picasso.with(thisContext())
                    .load("https://i.ytimg.com/vi/${video.youtube_url}/maxresdefault.jpg")
                    .error(R.mipmap.app_icon)
                    .placeholder(R.drawable.tilawah)
                    .into(p0!!.itemView.image)
            p0.itemView.image.setOnClickListener(View.OnClickListener {
                val i = Intent(thisContext(),ActivityQoriList::class.java)
                i.putExtra("category",video.cat)
                i.putExtra("jump",video.id)
                startActivity(i)
            })
            p0!!.itemView.post(Runnable {
                val i = p0!!.itemView.layoutParams
                i.height = i.width
                p0!!.itemView.layoutParams = i
                val i2 = p0!!.itemView.image.layoutParams
                i2.height = i2.width
                p0!!.itemView.image.layoutParams = i2
            })
        }

    }
    inner class DashboardHolder(v : View) : RecyclerView.ViewHolder(v){

    }
    inner class VidHolder(v : View) : RecyclerView.ViewHolder(v){

    }
}
