package exairie.com.quranq;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import butterknife.InjectView;
import exairie.com.quranq.libs.Configs;
import exairie.com.quranq.models.FinishedRequest;
import exairie.com.quranq.models.RequestResult;
import exairie.com.quranq.models.UserInfoRequest;
import exairie.com.quranq.models.UserLoginRequest;
import exairie.com.quranq.services.ProfileDataUpdater;
import okhttp3.FormBody;
import okhttp3.Request;
import okhttp3.Response;

public class ActivityLogin extends ExtActivity {
    public static short login_stat = -1;
    @InjectView(R.id.t_username)
    EditText tUsername;
    @InjectView(R.id.btn_checkusername)
    Button btnCheckusername;
    @InjectView(R.id.layout_input_username)
    LinearLayout layoutInputUsername;
    @InjectView(R.id.frame_userlogin)
    CardView frameUserlogin;
    @InjectView(R.id.btn_googlelogin)
    LinearLayout btnGooglelogin;


    Animation showAnim, hideAnim;
    @InjectView(R.id.imageView6)
    ImageView imageView6;
    @InjectView(R.id.layout_signup)
    RelativeLayout layoutSignup;
    @InjectView(R.id.l_errormsg)
    TextView lErrormsg;

    private Animation hideAnimRev;
    private Animation showAnimRev;

    @InjectView(R.id.frame_username)
    LinearLayout frameUsername;
    @InjectView(R.id.progress_login)
    ProgressBar progressLogin;
    @InjectView(R.id.l_accountname)
    TextView lAccountname;
    @InjectView(R.id.l_accountemail)
    TextView lAccountemail;
    @InjectView(R.id.t_password)
    EditText tPassword;
    @InjectView(R.id.btn_login)
    Button btnLogin;
    @InjectView(R.id.layout_user)
    LinearLayout layoutUser;
    private int ACT_SIGNUP = 1;
    private GoogleApiClient googleApiClient;
    private int ACT_SIGN_GOOGLE = 2;

    ProgressDialog dialog;

    public ActivityLogin() {
        super(R.layout.activity_login);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("LOGOUT", String.valueOf(getIntent().getStringExtra("logout")));
        if (getIntent().getStringExtra("logout") != null) {
            if (dialog == null) dialog = new ProgressDialog(thisContext());
            dialog.setMessage("Signing Out...");
            dialog.setCancelable(false);
            dialog.show();
            googleLogin();
            return;
        }

        String message = getIntent().getStringExtra("message");
        if (message != null) {
            new AlertDialog.Builder(thisContext())
                    .setMessage(message)
                    .setNegativeButton("tutup", (dialogInterface, i) -> dialogInterface.dismiss())
                    .show();
        }

        dialog = new ProgressDialog(thisContext());
        dialog.setCancelable(false);

        changeStatusBarColor(R.color.transparent);
        Window w = getWindow();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            w.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }

        showAnim = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade);
        hideAnim = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade_close);

        showAnimRev = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade);
        hideAnimRev = AnimationUtils.loadAnimation(thisContext(), R.anim.slidup_fade_close);

        showAnimRev.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                frameUsername.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        hideAnimRev.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                frameUserlogin.setVisibility(View.GONE);
                frameUsername.startAnimation(showAnimRev);
                btnCheckusername.setVisibility(View.VISIBLE);
                progressLogin.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        hideAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                frameUsername.setVisibility(View.GONE);
                frameUserlogin.startAnimation(showAnim);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        showAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                frameUserlogin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                tPassword.requestFocus();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        btnCheckusername.setOnClickListener(view -> {
            progressLogin.setVisibility(View.VISIBLE);
            view.setVisibility(View.GONE);
            checkUsername();
        });
        layoutUser.setOnClickListener(view -> frameUserlogin.startAnimation(hideAnimRev));
        btnLogin.setOnClickListener(view -> login());

        layoutSignup.setOnClickListener(view -> {
            Intent i = new Intent(thisContext(), ActivitySignUp.class);
            i.putExtra("origin", "login");
            startActivityForResult(i, ACT_SIGNUP);
        });

        btnGooglelogin.setOnClickListener(view -> {
            googleLogin();
        });

    }

    private void googleLogin() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.oauth_id))
                .requestEmail()
                .requestProfile()
                .build();
        if (googleApiClient == null) {
            googleApiClient = new GoogleApiClient.Builder(thisContext())
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }


        googleApiClient.registerConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
            @Override
            public void onConnected(@Nullable Bundle bundle) {
                if (getIntent().getStringExtra("logout") != null) {

                    Auth.GoogleSignInApi.revokeAccess(googleApiClient).setResultCallback(status -> {
                        dialog.dismiss();
                        if (status.isSuccess()) {
//                            Log.d("LOGIN",status.getStatusMessage());
                        } else {
                            Toast.makeText(thisContext(), "Unable to log out from google account!", Toast.LENGTH_LONG).show();
                        }
                        finish();
                        Intent i2 = new Intent(thisContext(), ActivityLogin.class);
                        i2.putExtra("loginstatus", false);
                        startActivity(i2);

                    });
                    return;
                }
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(signInIntent, ACT_SIGN_GOOGLE);
            }

            @Override
            public void onConnectionSuspended(int i) {

            }
        });
        googleApiClient.connect();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACT_SIGN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result == null) {
                Toast.makeText(this, "Log in dibatalkan", Toast.LENGTH_LONG).show();
            }
            if (result.isSuccess()) {
                GoogleSignInAccount account = result.getSignInAccount();
                uploadCredential(account);
            } else {
                Log.d("GSIGNINERR", "Errorcode " + result.getStatus().getStatusCode());
                Toast.makeText(this, "Some error happened :( " + result.getStatus().getStatusCode(), Toast.LENGTH_LONG).show();
            }
        }
    }

    private void uploadCredential(GoogleSignInAccount account) {
        dialog.setMessage("Signing Up...");
        dialog.show();
        FormBody body = new FormBody.Builder()
                .add("form-login_type", "google")
                .add("form-username", String.valueOf(account.getEmail()))
                .add("form-email", String.valueOf(account.getEmail()))
                .add("form-password", String.valueOf(account.getIdToken()))
                .add("form-firstname", String.valueOf(account.getGivenName()))
                .add("form-lastname", String.valueOf(account.getFamilyName()))
                .add("form-fcm_token", String.valueOf(FirebaseInstanceId.getInstance().getToken()))
                .add("form-login_token", String.valueOf(account.getIdToken()))
                .add("form-profile_pic", String.valueOf(account.getPhotoUrl()))
                .build();
        Request request = new Request.Builder()
                .url(server("auth/signup"))
                .post(body)
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    RequestResult requestResult = Configs.mapper().readValue(string, RequestResult.class);
                    autoLogin(account);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFail(String msg) {
                dialog.dismiss();
                runOnUiThread(() -> {
                    new AlertDialog.Builder(thisContext())
                            .setMessage("Error occured")
                            .setNegativeButton("Close", (dialogInterface, i) -> dialogInterface.dismiss())
                            .show();
                });
            }

            @Override
            public void Finish() {

            }
        });
    }

    private void autoLogin(GoogleSignInAccount account) {
        runOnUiThread(() -> {
            tUsername.setText(String.valueOf(account.getEmail()));
            tPassword.setText(String.valueOf(account.getIdToken()));
            login();
        });
    }

    private void login() {
        dialog.setMessage("Logging you in...");
        dialog.show();
        String username = tUsername.getText().toString();
        String password = tPassword.getText().toString();

        FormBody body = new FormBody.Builder()
                .add("username", username)
                .add("password", password)
                .build();

        Request request = new Request.Builder()
                .post(body)
                .url(server("auth/login"))
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    RequestResult requestResult = Configs.mapper().readValue(string, RequestResult.class);
                    if (requestResult.status.equals("ok")) {
                        JSONObject object = new JSONObject(string);
                        String token = object.getString("token");
                        Configs.setLoginToken(token);

                        requestUserData();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(()->{
                    lErrormsg.setText(msg);
                    lErrormsg.setVisibility(View.VISIBLE);
                });
            }

            @Override
            public void Finish() {
                runOnUiThread(dialog::dismiss);
            }
        });
    }

    private void requestUserData() {
        FormBody body = new FormBody.Builder()
                .build();
        Request request = new Request.Builder()
                .post(body)
                .url(server("auth/userdata"))
                .build();

        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    UserLoginRequest req = Configs.mapper().readValue(string, UserLoginRequest.class);
                    if (req.status.equals("ok")) {
                        Configs.setLoginInfo(req.loginInfo);

                        FirebaseMessaging.getInstance().subscribeToTopic("event_notifications");
                        FirebaseMessaging.getInstance().subscribeToTopic("notification_" + req.loginInfo.id);

                        redirectToHome();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void OnFail(String msg) {

            }

            @Override
            public void Finish() {

            }
        });
    }

    private void redirectToHome() {
        login_stat = -1;
        Intent i = new Intent(thisContext(), ActivityDashboard.class);
        i.putExtra("loginstatus", true);
        startActivity(i);

        Intent i2 = new Intent(thisContext(), ProfileDataUpdater.class);
        i2.putExtra("download", "init");
        startService(i2);

        finish();
    }

    private void checkUsername() {
        FormBody body = new FormBody.Builder()
                .add("username", tUsername.getText().toString())
                .build();
        Request request = new Request.Builder()
                .url(server("auth/hasusername"))
                .post(body)
                .build();
        NetRequest(request, new FinishedRequest() {
            @Override
            public void OnSuccess(Response response) {

            }

            @Override
            public void OnSuccess(String string) {
                try {
                    UserInfoRequest info = Configs.mapper().readValue(string, UserInfoRequest.class);
                    if (info.info != null) {
                        runOnUiThread(() -> {
                            lAccountname.setText(info.info.username);
                            lAccountemail.setText(info.info.email);
                            frameUsername.startAnimation(hideAnim);
                        });
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                    runOnUiThread(() -> {
                        progressLogin.setVisibility(View.VISIBLE);
                        btnCheckusername.setVisibility(View.GONE);
                        Toast.makeText(thisContext(), "Technical Failed!", Toast.LENGTH_LONG).show();
                    });
                }
            }

            @Override
            public void OnFail(String msg) {
                runOnUiThread(() -> {
                    progressLogin.setVisibility(View.VISIBLE);
                    btnCheckusername.setVisibility(View.GONE);
                });
                runOnUiThread(() -> new AlertDialog.Builder(thisContext())
                        .setTitle("Error")
                        .setMessage(msg)
                        .setPositiveButton("Close", (dialogInterface, i) -> dialogInterface.dismiss())
                        .show());
            }

            @Override
            public void Finish() {
                runOnUiThread(() -> {
                    progressLogin.setVisibility(View.VISIBLE);
                    btnCheckusername.setVisibility(View.GONE);
                });
            }
        });
    }

}
