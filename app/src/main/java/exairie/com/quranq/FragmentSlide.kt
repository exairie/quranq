package exairie.com.quranq

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso
import exairie.com.quranq.models.Event
import kotlinx.android.synthetic.main.*
import kotlinx.android.synthetic.main.custom_icontext.*
import kotlinx.android.synthetic.main.fragment_slide_show.*
import kotlinx.android.synthetic.main.fragment_slide_show.view.*
import java.net.URI

/**
 * A simple [Fragment] subclass.
 * Activities that contain this playerFragment must implement the
 * [FragmentSlide.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [FragmentSlide.newInstance] factory method to
 * create an instance of this playerFragment.
 */
class FragmentSlide : Fragment() {

    // TODO: Rename and change types of parameters
    private var mParam1: String? = null
    private var mParam2: String? = null
    lateinit var d : Bitmap
    init {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (arguments != null) {
            if (arguments!!.getString("img") != null){

//                Picasso.with(this.context)
//                        .load(arguments.getString("img"))
//                        .into(this.img_slide)
//                this.img_slide.setOnClickListener(View.OnClickListener {
//                    //Navigate to event detail
//                })
            }
        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this playerFragment
        val v = inflater!!.inflate(R.layout.fragment_slide_show, container, false)
        return v

    }

    override fun onStart() {
        super.onStart()
        d = BitmapFactory.decodeResource(context?.resources, R.drawable.splash)
//        val img = view!!.findViewById(R.id.img_slide)
//        val w = view!!.width
//        val h = view!!.height
//        d = Bitmap.createScaledBitmap(d,w,h,false)
//        view!!.img_slide.setImageBitmap(d)
        view!!.img_slide.setImageBitmap(d)
    }



    override fun onAttach(context: Context?) {
        super.onAttach(context)

    }

    override fun onDetach() {
        super.onDetach()
    }



    companion object {
        fun newInstance(evt : Event): FragmentSlide {
            val fragment = FragmentSlide()
            val args = Bundle()
            args.putString("img",evt.nama_event)
            args.putString("title",evt.nama_event)
            args.putLong("id",evt.id)
            fragment.arguments = args
            return fragment
        }
    }
}// Required empty public constructor
