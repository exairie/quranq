package exairie.com.quranq

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.LinearLayout
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.models.Fahmil
import exairie.com.quranq.models.FinishedRequest
import exairie.com.quranq.models.RequestResult
import exairie.com.quranq.models.StringOption
import kotlinx.android.synthetic.main.activity_fahmil_start.*
import okhttp3.FormBody
import okhttp3.Request
import okhttp3.Response
import org.json.JSONObject

class ActivityFahmilStart : ExtActivity(R.layout.activity_fahmil_start) {
    lateinit var adapter : BabAdapter
    var density : Float = 0f
    init {
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val w = window
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            w.statusBarColor = ContextCompat.getColor(thisContext(), R.color.Transparent)
        }

        btn_start.setOnClickListener({
            getQuestions()
        })

        density = this.resources.displayMetrics.density
        adapter = BabAdapter()

        recycler_bab.adapter = adapter
        recycler_bab.layoutManager = LinearLayoutManager(thisContext())
        recycler_bab.isNestedScrollingEnabled = false

        val prog = ProgressDialog(thisContext())
        prog.setMessage("Loading...")
        prog.setCancelable(false)
        prog.show()
        adapter.loadBab({prog.dismiss()})

    }

    private fun getQuestions() {

        val ids = mutableListOf<String>()
        for (data in adapter.data){
            if (data.optionValue){
                ids.add(data.id.toString())
            }
        }
        if (ids.size < 1){
            AlertDialog.Builder(thisContext())
                    .setMessage("Pilih minimal satu bab!")
                    .setNegativeButton("Close", { dialogInterface, i ->
                        dialogInterface.dismiss()
                    })
                    .show()
            return
        }
        val dlg = ProgressDialog(thisContext())
        dlg.setMessage("Loading questions...")
        dlg.setCancelable(false)
        dlg.show()
        val body = FormBody.Builder()
                .add("ids",Configs.mapper().writeValueAsString(ids))
                .build()
        val request = Request.Builder()
                .post(body)
                .url(server("api/question_fahmil"))
                .build()

        NetRequest(request, object : FinishedRequest{
            override fun OnSuccess(response: Response?) {
//                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun OnSuccess(string: String?) {
                val result = Configs.mapper().readValue(string, RequestResult::class.java)
                if (result.status.equals("ok")){
                    val json = JSONObject(string)
                    val questions = json.optString("data")
                    val i = Intent(thisContext(),ActivityFahmilGame::class.java)
                    i.putExtra("questions",questions)
                    i.putExtra("mode","offline")
                    startActivity(i)
//                    if (play_online.isChecked){
//                        val questions = json.optString("data")
//                        setupOnlinePlay(questions)
//                    }else{
//
//                    }

                    //var data = Configs.mapper().readValue(json.optString("data"),Fahmil.Questions::class.java)
                }
            }

            override fun OnFail(msg: String?) {
                runOnUiThread({
                    AlertDialog.Builder(thisContext())
                            .setMessage("Unable to load questions!\nMake sure you have a working internet connections")
                            .setNegativeButton("Close", { dialogInterface, i ->
                                dialogInterface.dismiss()
                            })
                            .show()
                })
            }

            override fun Finish() {
                dlg.dismiss()
            }

        })

    }

    private fun setupOnlinePlay(questions: String?) {

    }

    inner class BabAdapter : RecyclerView.Adapter<BabAdapter.Holder>(){
        val data : MutableList<StringOption<Boolean>> = mutableListOf()
        fun loadBab(finish: () -> Unit){
            val task = object : AsyncTask<Void,Void,Void>()
            {
                override fun doInBackground(vararg p0: Void?): Void? {
                    val request = Request.Builder()
                            .get()
                            .url(server("api/bab_fahmil"))
                            .build()
                    NetRequest(request, object : FinishedRequest{
                        override fun OnFail(msg: String?) {

                        }

                        override fun OnSuccess(response: Response?) {

                        }

                        override fun OnSuccess(string: String?) {
                            val result = Configs.mapper().readValue(string,RequestResult::class.java)
                            if (result.status.equals("ok")){
                                val json = JSONObject(string);
                                val str = json.getJSONArray("data")
                                for (i in 0 until str.length()){
                                    val bab = str.getJSONObject(i)
                                    data.add(StringOption<Boolean>(bab.optInt("id"), bab.optString("bab"),false))
                                }
                            }
                            runOnUiThread({
                                try {
                                    notifyDataSetChanged()
                                }catch (exc:Exception){
                                    exc.printStackTrace()
                                }
                            })


                        }

                        override fun Finish() {
                            finish()
                        }

                    })
                    return null
                }

            }
            task.execute()
        }
        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val chk = CheckBox(p0!!.context)
            val param = LinearLayout.LayoutParams(p0.layoutParams)
            param.setMargins((8 * density).toInt(),(8 * density).toInt(),(8 * density).toInt(),(8 * density).toInt())
            chk.layoutParams = param
            return Holder(chk)

        }

        override fun onBindViewHolder(p0: Holder, p1: Int) {
            val chk = p0!!.itemView as CheckBox
            val d = data[p1]
            chk.text = d.toString()

            chk.setOnCheckedChangeListener({ _, b ->
                data[p1].setOptionValue(b)
                Log.d("OptionChange",String.format("Data %s : value : %s",data[p1].toString(),data[p1].optionValue.toString()))
            })
        }

        override fun getItemCount(): Int = data.size

        inner class Holder(view : View) : RecyclerView.ViewHolder(view)
    }
}
