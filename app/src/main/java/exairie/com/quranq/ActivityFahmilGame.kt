package exairie.com.quranq

import android.app.AlertDialog
import android.content.ContentValues
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.*
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import exairie.com.quranq.libs.Configs
import exairie.com.quranq.libs.DBHelper
import exairie.com.quranq.models.Fahmil
import kotlinx.android.synthetic.main.activity_fahmil_game.*
import kotlinx.android.synthetic.main.view_fahmil_answer.view.*
import java.util.*

class ActivityFahmilGame : ExtActivity(R.layout.activity_fahmil_game) {
    var session_start : Long = 0
    var session_finish : Long = 0
    public data class QuizResult(val ansTrue : Fahmil.Answer?, val ansFalse : Fahmil.Answer?, val quest : Fahmil.Questions)

    lateinit var question_current: Fahmil.Questions
    lateinit var questions : Array<Fahmil.Questions>
    lateinit var adapter : AnswerAdapter
    var quiz_record = mutableListOf<QuizResult>()

    lateinit var anim_alpha_enter : Animation
    lateinit var anim_alpha_exit : Animation

    var index : Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setExtActionBar(toolbar as Toolbar)

        setTitle("Game")


        session_start = System.currentTimeMillis()

        if(intent.getStringExtra("mode") == null){
            Toast.makeText(thisContext(),"Error : No mode specified",LENGTH_LONG).show()
            finish()
        }

        adapter = AnswerAdapter()
        recycler_answers.adapter = adapter
        recycler_answers.layoutManager = LinearLayoutManager(thisContext())
        recycler_answers.visibility = View.GONE

        anim_alpha_enter = AnimationUtils.loadAnimation(thisContext(),R.anim.alpha)
        anim_alpha_exit = AnimationUtils.loadAnimation(thisContext(),R.anim.alpha_exit)

        anim_alpha_enter.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
            }

            override fun onAnimationStart(p0: Animation?) {
                view_overlay_answer.visibility = View.VISIBLE
                recycler_answers.visibility = View.VISIBLE
            }

        })
        anim_alpha_exit.setAnimationListener(object : Animation.AnimationListener{
            override fun onAnimationRepeat(p0: Animation?) {

            }

            override fun onAnimationEnd(p0: Animation?) {
                view_overlay_answer.visibility = View.GONE
                recycler_answers.visibility = View.GONE
            }

            override fun onAnimationStart(p0: Animation?) {

            }

        })

        layout_answer_btn.setOnClickListener({
            view_overlay_answer.startAnimation(anim_alpha_enter)
            view_overlay_answer.visibility = View.VISIBLE

        })

        if (intent.getStringExtra("mode") == "offline"){
            questions = Configs.mapper().readValue(intent.getStringExtra("questions"),Array<Fahmil.Questions>::class.java)
            if(questions.size < 1){
                finish()
            }
            playOffline()
        }
    }

    private fun playOffline() {
        layout_online_player.visibility = View.GONE
        next()
    }

    override fun onBackPressed() {
        AlertDialog.Builder(thisContext())
                .setMessage("Keluar dari quiz?\nProgress anda tidak akan tersimpan")
                .setNegativeButton("Tidak", DialogInterface.OnClickListener { dialogInterface, _ -> dialogInterface.dismiss() })
                .setPositiveButton("Ya", DialogInterface.OnClickListener { dialogInterface, _ ->
                    dialogInterface.dismiss()
                    super.onBackPressed()
                }).show()
    }
    private fun next() {
        l_numofquestion.text = String.format("Question %d of %d",index+1,(if(questions.size > 15) 15 else questions.size));
        if (index > (questions.size -1) || index >= 15){
            finishGame()
        }else {
            index++
            question_current = questions[if(questions.size > 1) Random().nextInt(questions.size - 1) else 0]
            l_quizayat.text = question_current.question
            adapter.loadArray(question_current.answers)

        }
        if (view_overlay_answer.visibility == View.VISIBLE) {
            view_overlay_answer.startAnimation(anim_alpha_exit)
        }

    }

    private fun finishGame() {
        session_finish = System.currentTimeMillis()
        val db = DBHelper(thisContext())
        for (d in quiz_record){
            val c = ContentValues()
            c.put("question",d.quest.question)
            if (d.ansTrue != null)
                c.put("answer_true", d.ansTrue.answer)
            if (d.ansFalse != null)
                c.put("answer_false", d.ansFalse.answer)

            c.put("session_start",session_start)
            c.put("session_end",session_finish)

            db.writableDatabase.insert("fahmil_result","null",c)

        }

        db.close()

        val i = Intent(thisContext(), ActivityFahmilResult::class.java)
        i.putExtra("session",session_start)
        startActivity(i)
        finish()
    }

    fun answer(ans : Fahmil.Answer){
        var ans_true : Fahmil.Answer? = null
        var ans_false : Fahmil.Answer? = null


        if (ans.answer_true == 1)
            ans_true = ans
        else{
            ans_false = ans
            for (answer in question_current.answers){
                if(answer.answer_true == 1){
                    ans_true = answer
                }
            }
        }


        quiz_record.add(QuizResult(ans_true,ans_false,question_current))

        next()
    }
    inner class AnswerAdapter : RecyclerView.Adapter<Holder>(){
        lateinit var _data : Array<Fahmil.Answer>
        fun loadArray(arr : Array<Fahmil.Answer>){
            this._data = arr
            notifyDataSetChanged()
        }
        override fun onBindViewHolder(p0: Holder, p1: Int) {
            val ans = _data[p1]

            p0.itemView.l_answer.text = ans.answer
            p0.itemView.setOnClickListener({
                answer(ans)
            })
        }

        override fun onCreateViewHolder(p0: ViewGroup, p1: Int): Holder {
            val v = LayoutInflater.from(p0.context)
                    .inflate(R.layout.view_fahmil_answer,p0,false)
            return Holder(v)
        }

        override fun getItemCount(): Int = if (_data == null) 0 else _data.size

    }
    class Holder(v : View) : RecyclerView.ViewHolder(v)
}
