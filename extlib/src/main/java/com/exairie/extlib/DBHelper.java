package com.exairie.extlib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
/**
 * Created by exain on 5/17/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static String name = "data.sqlite";
    public DBHelper(Context context) {
        super(context, name, null, 6);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE config (" +
                "id integer primary key autoincrement," +
                "ckey text," +
                "cvalue text)");
        db.execSQL("CREATE TABLE IF NOT EXISTS odoj ( " +
                "    id      INTEGER PRIMARY KEY AUTOINCREMENT," +
                "    time    NUMERIC," +
                "    ayat_id INTEGER, " +
                "    ayat_to INTEGER," +
                "    letter_no INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS quran_bookmark (" +
                "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "   bookmark_time NUMERIC," +
                "   ayat_id INTEGER)");
        db.execSQL("CREATE TABLE IF NOT EXISTS guru(" +
                "   id integer primary key autoincrement," +
                "   nama_guru varchar(255)," +
                "   phone varchar(255)," +
                "   location_lat real(18,4)," +
                "   location_long real(18,4)," +
                "   address varchar(255))");
        db.execSQL("CREATE TABLE kajian (" +
                "  id integer primary key autoincrement," +
                "  judul varchar(200)," +
                "  gambar varchar(255)," +
                "  isi text," +
                "   create_date integer" +
                ") ");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        if(i1 >= 2){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS odoj ( \n" +
                        "    id      INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "    time    NUMERIC," +
                        "    ayat_id INTEGER " +
                        ")");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 3){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS quran_bookmark (" +
                        "   id INTEGER PRIMARY KEY AUTOINCREMENT," +
                        "   bookmark_time NUMERIC," +
                        "   ayat_id INTEGER)");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE odoj ADD ayat_to INTEGER");
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                db.execSQL("ALTER TABLE odoj ADD letter_no INTEGER");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 4){
            try {
                db.execSQL("CREATE TABLE IF NOT EXISTS guru(" +
                        "   id integer primary key autoincrement," +
                        "   nama_guru varchar(255)," +
                        "   phone varchar(255)," +
                        "   location_lat real(18,4)," +
                        "   location_long real(18,4)," +
                        "   address varchar(255))");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (i1 >= 5){
            db.execSQL("CREATE TABLE IF NOT EXISTS kajian (" +
                    "  id integer primary key autoincrement," +
                    "  judul varchar(200)," +
                    "  gambar varchar(255)," +
                    "  isi text," +
                    "   create_date integer" +
                    ") ");
        }
    }

}
