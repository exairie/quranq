package com.exairie.extlib;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

/**
 * Created by exain on 8/25/2017.
 */

public class Configs {
    public static final String SERVER_URL = "http://quranqu.fsialbiruni.org/admin";
    public static String server(String dest){
        return String.format("%s/index.php/%s",SERVER_URL,dest);
    }
    public static ObjectMapper mapper(){
        ObjectMapper o = new ObjectMapper();
        o.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return o;
    }
    private String wQuery = "select sum(words)/count(words) from (" +
            "   SELECT (sum(length(arabic))) as words, juz from `quran` group by juz\n" +
            ") as T";
    public final static double LETTER_PER_JUZ = 23175  ;
    private static Context appContext;
    public static void init(Context context){
        appContext = context;
    }
    private Configs instance = null;
    public Configs getInstance(){
        if(instance == null)
            instance = new Configs();

        return instance;
    }
    public static void setConfiguration(Option option){
        DBHelper db = new DBHelper(appContext);
        db.getWritableDatabase().delete("config","ckey = ?",new String[]{option.getKey()});

        ContentValues values = new ContentValues();
        values.put("ckey",option.getKey());
        values.put("cvalue",option.getValue());
        db.getWritableDatabase().insert("config",null,values);
        //db.getWritableDatabase().execSQL("INSERT INTO config (ckey,cvalue) VALUES ('?','?')", new String[]{option.getKey(),option.getValue()});
        db.close();
    }
    public static Option getConfiguration(String key){
        DBHelper db = new DBHelper(appContext);
        Cursor c = db.getReadableDatabase().rawQuery("SELECT * FROM config WHERE ckey = ?",new String[] {key});
        if(c.getCount() < 1){
            return null;
        }
        c.moveToFirst();
        Option o = new Option();
        o.setKey(c.getString(1));
        o.setValue(c.getString(2));
        c.close();
        db.close();
        return o;
    }

}